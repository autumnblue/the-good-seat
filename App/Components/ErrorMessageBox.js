import { Alert } from 'react-native';

const titleMapping = {
  en: {
    title: 'Error',
  },
  fr: {
    title: 'Erreur',
  },
};

const ErrorMessageBox = (language = 'fr', errorCode, errorsDictionary, onOk) => {
  console.log('code', language, errorCode);
  const translate = text => (errorsDictionary
        && errorsDictionary[text]
        && errorsDictionary[text][language] ? errorsDictionary[text][language] : text);

  const defaultTitle = titleMapping[language].title;
  console.log('code', language, errorCode, defaultTitle);
  let title;
  title = errorCode && errorCode.message ? errorCode.message : errorCode;
  title = translate(title);
  console.log('code', title);

  let message;
  if (errorCode && errorCode.errors && Array.isArray(errorCode.errors)) {
    console.log('ready to parse the rest');
    const errors = errorCode.errors.map(text => translate(text.message || text.toString()));
    console.log('ready to parse the rest', errors);
    message = errors.join('\n');
  } else {
    message = title;
    title = defaultTitle;
  }
  console.log(title, message);
  Alert.alert(title, message, [{
    text: 'OK',
    onPress: () => {
      if (onOk) {
        onOk();
      }
    },
  }]);
};

export default ErrorMessageBox;
