import React, { Component } from 'react';
import { Item, Input } from 'native-base';
import { Image, View, TouchableWithoutFeedback } from 'react-native';
import { Images } from '../Theme';

import Styles from './Styles/TextInputMyProfileStyles';

/*
=============================================================
 специальный класс для input
=============================================================
*/
class TextInputMyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enabled: false,
    };
  }

  //----------------------------------------------------
  // enable edit mode
  //-----------------------------------------------------
  handleEnable = () => {
    this.setState({ enabled: true }, () => {
      if (this.InputRef) {
        this.InputRef._root.focus();
      }
    });
  };

  //------------------------------------------------------
  // on blur input field
  //------------------------------------------------------
  onBlur = () => {
    const { onSubmit } = this.props;
    this.setState({ enabled: false }, () => {
      if (onSubmit) onSubmit();
    });
  };

  //------------------------------------------------------
  // handle submit
  //------------------------------------------------------
  handleSubmit = () => {
    const { onSubmit } = this.props;

    this.setState({ enabled: false });

    if (onSubmit) onSubmit();
  };

  render() {
    const {
      disabledText, secureText, placeholder, ...attributes
    } = this.props;

    const { enabled } = this.state;

    if (enabled) {
      return (
        <View style={Styles.textInputContainer}>
          <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
            <Input
              ref={(ref) => {
                this.InputRef = ref;
              }}
              {...attributes}
              disabled={!enabled}
              defaultValue={
                placeholder === 'Entrer votre numéro de téléphone +33X XXXX XXXX' ? '+33' : ''
              }
              placeholder={placeholder}
              style={Styles.inputText}
              returnKeyType="done"
              onSubmitEditing={this.handleSubmit}
              onBlur={this.onBlur}
              secureTextEntry={secureText}
            />
          </Item>
        </View>
      );
    }
    if (placeholder !== 'Entrer votre numéro de téléphone +33X XXXX XXXX') { // Condition To Remove when SMS validation is activated
      return ( // To keep
        <Item style={Styles.inputContainer}>
          <Input
            disabled
            style={Styles.inputText}
            defaultValue={disabledText}
            secureTextEntry={secureText}
          />
          <TouchableWithoutFeedback onPress={this.handleEnable}>
            <Image style={Styles.inputEditContainer} source={Images.inputEdit} />
          </TouchableWithoutFeedback>
        </Item>
      );
    } // To Remove when SMS validation is activated
    return (
      <Item style={Styles.inputContainer}>
        <Input
          disabled
          style={Styles.inputText}
          defaultValue={disabledText}
          secureTextEntry={secureText}
        />
      </Item>
    );
  }
}

export default TextInputMyProfile;
