import React, { Component } from 'react';
import { View, DatePickerIOS } from 'react-native';
import { Button, Text } from 'native-base';

import Modal from './Modal';

import Styles from './Styles/DateTimePickerStyles';

class DateTimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultDate: props.defaultDate ? props.defaultDate : new Date(),
      chosenDate: undefined,
    };
  }

  //---------------------------------------------
  // on date change
  //---------------------------------------------
  handleOnDateChange = (date) => {
    this.setState({ chosenDate: new Date(date) });
  };

  //----------------------------------------------
  // handle select
  //----------------------------------------------
  handleSelect = () => {
    const { chosenDate } = this.state;
    const { onSelect } = this.props;

    if (onSelect) onSelect(chosenDate);
  };

  //----------------------------------------------
  // handle cancel
  //----------------------------------------------
  handleCancel = () => {
    const { onCancel } = this.props;

    if (onCancel) onCancel();
  };

  render() {
    const { mode, visible } = this.props;

    const { chosenDate, defaultDate } = this.state;

    return (
      <Modal visible={visible}>
        <View style={Styles.container}>
          <DatePickerIOS
            date={chosenDate || defaultDate}
            onDateChange={this.handleOnDateChange}
            mode={mode}
          />
          <View style={Styles.footer}>
            <Button transparent info onPress={this.handleCancel}>
              <Text>Cancel</Text>
            </Button>
            <Button transparent info onPress={this.handleSelect}>
              <Text>Select</Text>
            </Button>
          </View>
        </View>
      </Modal>
    );
  }
}

DateTimePicker.defaultProps = {
  mode: 'date',
  visible: false,
};

export default DateTimePicker;
