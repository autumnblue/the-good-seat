import { StyleSheet, Platform } from 'react-native';
import { addFontFamily } from '../../Theme';

export default StyleSheet.create({
  iconFont: {
    ...addFontFamily('Roboto'),
  },
  button: {
    margin: 7,
  },
  raised: {
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(69, 91, 99, 0.1)',
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowRadius: 16,
        shadowOpacity: 1,
      },
      android: {
        elevation: 3,
      },
    }),
  },
});
