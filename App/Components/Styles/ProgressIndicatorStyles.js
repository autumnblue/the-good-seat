import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    borderRadius: 10,
    borderWidth: 0,
    width: '100%',
  },
  progress: {
    borderRadius: 10,
    borderWidth: 0,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: '#fff',
    opacity: 0.3,
    width: '40%',
  },
});
