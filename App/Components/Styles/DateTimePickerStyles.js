import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  container: {
    width: 328,
    borderRadius: 12,
    paddingTop: 44,
    paddingBottom: 7,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  footer: {
    marginVertical: 10,
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    flexDirection: 'row',
  },
});
