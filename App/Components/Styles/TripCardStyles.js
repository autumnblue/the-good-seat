import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 20,
    height: 122,
    width: 329,
    marginBottom: 10,
    paddingLeft: 22,
    paddingRight: 7,
    paddingTop: 9,
    paddingBottom: 8,
  },
  mainText: {
    ...addFontFamily('Gibson'),
    fontSize: 16,
    color: Colors.onPrimaryText,
  },
  secondaryText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 10,
    color: Colors.onPrimaryText,
    flexWrap: 'wrap',
  },
  gridRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingButton: {
    borderRadius: 9,
    borderWidth: 1,
    borderColor: '#78849e',
    minWidth: 82,
    maxWidth: 120,
    paddingHorizontal: 3,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageLabel: {
    width: 20,
    height: 20,
    marginRight: 14,
  },
  imageDriver: {
    width: 24,
    height: 24,
    marginHorizontal: 7,
    borderRadius: 4,
  },
});
