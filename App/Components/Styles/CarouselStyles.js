import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  indicatorContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 10,
  //  alignItems: 'center',
  //  alignSelf: 'center',
  //  backgroundColor: '#ff4345',
  },
  indicator: {
    width: 44,
    height: 4,
    marginLeft: 8,
  },
  indicatorActive: {
    width: 44,
    height: 3,
    marginLeft: 8,
  },
});
