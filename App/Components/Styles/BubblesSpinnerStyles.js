import { StyleSheet } from 'react-native';
import { Colors } from '../../Theme';

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  bubble: {
    width: 20,
    height: 20,
    borderRadius: 50,
    marginLeft: 10,
    backgroundColor: Colors.primary,
  },
});
