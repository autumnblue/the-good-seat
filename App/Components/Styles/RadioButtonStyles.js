import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../Theme';

export default StyleSheet.create({
  container: {
    //  margin: 5,
    //  marginLeft: 10,
    // marginRight: 10,
    //  padding: 10,
    // width: '100%',
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
  },
});
