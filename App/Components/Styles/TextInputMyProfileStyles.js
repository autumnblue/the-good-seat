import { StyleSheet } from 'react-native';
import { addFontFamily, Metrics, Colors } from '../../Theme';

export default StyleSheet.create({
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  itemMenuContainer: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  disabledTextContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  actionButtonContainer: {
    justifyContent: 'center',
    flex: 0.1,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  formContainer: {
    marginTop: 85.5,
  },
  inputText: {
    ...addFontFamily('SegoeUI', '400'),
    fontSize: 14,
    color: '#454f63',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
  placeholderStyle: {
    ...addFontFamily('SegoeUI', '300'),
    fontSize: 14,
    color: '#454f63',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
  inputContainer: {
    height: 60,
    borderBottomColor: 'transparent',
  },
  inputEditContainer: {
    marginRight: 19,
  },
  inputContainerDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.2)',
  },
  textInputContainer: {
    flex: 1,
    marginHorizontal: 10,
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    marginVertical: 15,
  },
  menuFooter: {
    justifyContent: 'center',
    marginTop: 152 * Metrics.scaleFactor,
    marginHorizontal: 20,
  },
  footerButton: {
    borderColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 12,
  },
  footerButtonText: {
    ...addFontFamily('SegoeUI', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
  },
});
