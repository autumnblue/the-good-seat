import { StyleSheet } from 'react-native';
import { addFontFamily } from '../../Theme';

export default StyleSheet.create({
  container: {
    width: 101,
    height: 76,
    borderWidth: 1,
    borderRadius: 8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    textAlign: 'center',
    color: '#78849e',
  },
  subText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    textAlign: 'center',
    color: '#78849e',
  },
  secondText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    opacity: 0.56,
    textAlign: 'center',
    color: '#78849e',
  },
  descriptionContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
