import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  button: {
    margin: 7,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(69, 91, 99, 0.1)',
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowRadius: 16,
        shadowOpacity: 1,
      },
      android: {
        elevation: 3,
      },
    }),
  },
});
