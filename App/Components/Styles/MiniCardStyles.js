import { StyleSheet } from 'react-native';
import { addFontFamily } from '../../Theme';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  avatarContainer: {
    width: 48,
    height: 48,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },

  bodyContainer: {
    paddingLeft: 15,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  mainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    textAlign: 'center',
    color: '#78849e',
  },
  secondText: {
    ...addFontFamily('Gibson'),
    fontSize: 14,
    opacity: 0.56,
    textAlign: 'center',
    color: '#78849e',
  },
});
