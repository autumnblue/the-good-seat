import React, { Component } from 'react';
import { View, Modal as RNModal, TouchableWithoutFeedback } from 'react-native';

import Styles from './Styles/ModalStyles';

class Modal extends Component {
  //----------------------------------------------
  // while clicking outside the modal
  //----------------------------------------------
  onBackdropPress = () => {};

  render() {
    const {
      children, visible, backdropColor, ...otherProps
    } = this.props;

    return (
      <RNModal
        animationType="none"
        transparent
        visible={visible}
        {...otherProps}
        onRequestClose={() => {}}
      >
        <TouchableWithoutFeedback onPress={this.onBackdropPress}>
          <View style={[{ backgroundColor: backdropColor }, Styles.backdropContainer]} />
        </TouchableWithoutFeedback>
        <View pointerEvents="box-none" style={Styles.container}>
          {children}
        </View>
      </RNModal>
    );
  }
}

Modal.defaultProps = {
  visible: false,
  backdropColor: 'rgba(20, 20, 20, 0.4)',
};

export default Modal;
