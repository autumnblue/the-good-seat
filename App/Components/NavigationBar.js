import React, { Component } from 'react';
import {
  Left, Body, Right, Button, Icon, Title, View,
} from 'native-base';

class NavigationBar extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  //--------------------------------------------------------
  //
  //--------------------------------------------------------
  componentDidMount() {}

  //--------------------------------------------------------
  //
  //--------------------------------------------------------
  componentWillUnmount() {}

  //--------------------------------------------------------
  // go back
  //--------------------------------------------------------
  onBackPress = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  render() {
    const { title, onBackPress } = this.props;

    /* return (
      <Header
        transparent
        style={{ backgroundColor: Colors.primary }}
        androidStatusBarColor={Colors.primary}
      >
        <Left>
          <Button
            transparent
            onPress={() => {
              onBackPress();
            }}
          >
            <Icon name="arrow-back" style={{ color: '#fff' }} />
          </Button>
        </Left>
        <Body>
          <Title>{title}</Title>
        </Body>
        <Right />
      </Header>
    ); */

    return (
      <View style={{
        height: 90, flexDirection: 'row', paddingTop: 18, backgroundColor: '#fff',
      }}
      >
        <Left>
          <Button
            transparent
            onPress={() => {
              onBackPress();
            }}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>{title}</Title>
        </Body>
        <Right />
      </View>
    );
  }
}

export default NavigationBar;
