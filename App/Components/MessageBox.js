import { Alert } from 'react-native';

const MessageBox = (title, message, onOk) => {
  Alert.alert(title, message, [
    {
      text: 'OK',
      onPress: () => {
        if (onOk) {
          onOk();
        }
      },
    },
  ]);
};

export default MessageBox;
