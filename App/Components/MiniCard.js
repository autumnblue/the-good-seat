import React, { Component } from 'react';
import { View } from 'react-native';
import { Text } from 'native-base';

import Styles from './Styles/MiniCardStyles';

class MiniCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { avatar, header, subheader } = this.props;

    return (
      <View style={Styles.container}>
        <View style={Styles.avatarContainer}>{avatar}</View>
        <View style={Styles.bodyContainer}>
          <Text numberOfLines={2} style={Styles.mainText}>{header}</Text>
          <Text numberOfLines={2} style={Styles.secondText}>{subheader}</Text>
        </View>
      </View>
    );
  }
}

export default MiniCard;
