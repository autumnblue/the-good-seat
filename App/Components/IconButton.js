import React from 'react';
import { Icon } from 'native-base';
import { View, TouchableOpacity } from 'react-native';

import Styles from './Styles/IconButtonStyles';

const IconButton = (props) => {
  const {
    name, type, color, size, buttonStyle, onPress,
  } = props;

  const Component = onPress ? TouchableOpacity : View;

  return (
    <Component onPress={onPress}>
      <View style={[Styles.button, buttonStyle, { height: size * 2, width: size * 2 }]}>
        <Icon name={name} type={type} style={{ color, fontSize: size }} />
      </View>
    </Component>
  );
};

//--------------------------------------------------------
// default props
//--------------------------------------------------------
IconButton.defaultProps = {
  // color: 'black',
  //  size: 24,
  onPress: null,
  onLongPress: null,
};

export default IconButton;
