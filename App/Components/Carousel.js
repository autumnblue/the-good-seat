import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';

import Styles from './Styles/CarouselStyles';
import { Metrics } from '../Theme';

class Carousel extends Component {
  //-----------------------------------------
  // constructor
  //------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      activePage: props.initialPage > 0 ? props.initialPage : 0,
    };
  }

  //-------------------------------------------
  // while updating the component
  //-------------------------------------------
  componentDidUpdate() {
    const { initialPage } = this.props;

    if (initialPage >= 0) {
      this.refPager.scrollTo({ x: initialPage * this.getWidth(), y: 0, animated: true });
    }
  }

  //-------------------------------------------
  // after the page scrolling
  //-------------------------------------------
  handleOnMomentumScrollEnd = (e) => {
    const activePage = e.nativeEvent.contentOffset.x / this.getWidth();
    this.setState({ activePage });
  };

  //--------------------------------------------
  // scroll the page
  //--------------------------------------------
  indicatorPressed = (activePage) => {
    this.setState({ activePage });
    this.refPager.scrollTo({ x: activePage * this.getWidth(), y: 0, animated: true });
  };

  //-----------------------------------------
  // get element width
  //-----------------------------------------
  getWidth = () => Metrics.screenWidth;

  //-------------------------------------------
  // render page indicator
  //-------------------------------------------
  renderPageIndicators = () => {
    const { children, indicatorColor, indicatorColorActive } = this.props;
    const { activePage } = this.state;

    const indicators = [];

    for (let i = 0, l = children.length; i < l; i += 1) {
      if (typeof children[i] !== 'undefined') {
        const indicatorStyle = i === activePage
          ? [Styles.indicatorActive, { backgroundColor: indicatorColorActive }]
          : [Styles.indicator, { backgroundColor: indicatorColor }];

        indicators.push(<View key={i} style={indicatorStyle} />);
      }
    }

    return <View style={Styles.indicatorContainer}>{indicators}</View>;
  };

  render() {
    const { containerStyle, enabled, children } = this.props;

    return (
      <React.Fragment>
        <ScrollView
          ref={(c) => {
            this.refPager = c;
          }}
          automaticallyAdjustContentInsets={false}
          contentContainerStyle={containerStyle}
          horizontal
          pagingEnabled
          scrollEnabled={enabled}
          showsHorizontalScrollIndicator={false}
          bounces={false}
          scrollsToTop={false}
          width={this.getWidth()}
          onMomentumScrollEnd={this.handleOnMomentumScrollEnd}
        >
          {children}
        </ScrollView>
        {this.renderPageIndicators()}
      </React.Fragment>
    );
  }
}

Carousel.defaultProps = {
  indicatorColor: 'rgba(255, 255, 255, 0.2)',
  indicatorColorActive: 'rgba(255, 255, 255, 0.8)',
  enabled: true,
};

export default Carousel;
