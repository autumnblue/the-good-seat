import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';

import Styles from './Styles/IconStyles';

const Icon = (props) => {
  const {
    name,
    onPress,
    color,
    size,
    raised,
    disabled,
    reverse,
    reverseColor,
    containerStyle,
    buttonRadius,
    children,
    ...attributes
  } = props;

  const Component = onPress ? TouchableHighlight : View;

  const childrenComponents = name.lenght ? (
    <Text
      style={[
        Styles.iconFont,
        { color: reverse ? reverseColor : color, fontSize: size },
      ]}
    >
      {name}
    </Text>
  ) : (
    children
  );

  return (
    <View style={containerStyle && containerStyle}>
      <Component
        {...attributes}
        activeOpacity={0.6}
        underlayColor="transparent"
        style={[
          raised && Styles.button,
          raised && {
            borderRadius: buttonRadius || size + 4,
            height: size * 2 + 4,
            width: size * 2 + 4,
          },
          raised && Styles.raised,
          {
            backgroundColor: reverse ? color : raised ? 'white' : 'transparent',
            alignItems: 'center',
            justifyContent: 'center',
          },
        ]}
        {...onPress && { disabled }}
        onPress={onPress}
      >
        {childrenComponents}
      </Component>
    </View>
  );
};

//--------------------------------------------------------
// default values
//--------------------------------------------------------
Icon.defaultProps = {
  color: 'black',
  reverseColor: 'white',
  size: 24,
  onPress: null,
  onLongPress: null,
  raised: false,
  reverse: false,
  text: '',
};

export default Icon;
