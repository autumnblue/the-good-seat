import React, { Component } from 'react';
import { Text, Icon } from 'native-base';
import { View, TouchableOpacity } from 'react-native';

import Styles from './Styles/VTCButtonStyles';

class VTCButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { borderColor, onPress, offer } = this.props;

    let separator = '';

    if (offer.arrivalTime && offer.arrivalTime > 0 && offer.minPrice && offer.minPrice > 0) {
      separator = ' - ';
    }
    if (offer.providerName === 'G7' || offer.providerName === 'Uber') {
      separator = ' ≈ ';
    }

    return (
      <TouchableOpacity onPress={onPress}>
        <View style={[{ borderColor: borderColor && borderColor }, Styles.container]}>
          <Text style={Styles.mainText}>{offer.providerName}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <Text style={Styles.subText}>{offer.displayName}</Text>
            <Text style={Styles.mainText}>{offer.providerRating && `${' - '}${offer.providerRating}`}</Text>
            {offer.providerRating && <Icon name="star" style={{ marginLeft: 5, color: '#000', fontSize: 16 }} />}
          </View>
          <View style={Styles.descriptionContainer}>
            <Text style={Styles.secondText}>
              {offer.arrivalTime
                ? `${Number(parseInt(offer.arrivalTime, 10) / 60).toFixed(0)} min`
                : ''}
            </Text>
            <Text style={Styles.secondText}>
              {separator}
            </Text>
            <Text style={Styles.secondText}>
              {offer.minPrice && offer.minPrice > 0 ? offer.minPrice + offer.currency : ''}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default VTCButton;
