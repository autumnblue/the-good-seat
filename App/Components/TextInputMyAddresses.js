import React, { Component } from 'react';
import {
  Button, Item, Input, Form,
} from 'native-base';
import {
  Image, View, TouchableOpacity, Text,
} from 'react-native';

import Modal from './Modal';

import { Images } from '../Theme';

import Styles from './Styles/TextInputMyAddressesStyles';

/*
-------------------------------------------------
 modal view state
-------------------------------------------------
*/
const MODAL_STATE = {
  NONE: 0,
  QUESTION: 1,
  CONFIRMATION: 2,
};

/*
-------------------------------------------------------------
 component render states
-------------------------------------------------------------
*/
const COMPONENT_RENDER_STATES = {
  INITIAL: 0,
  INSERT_ADDRESS: 1,
  LOCATION_LOCK: 2,
};
/*
=============================================================
 class for text input in drawer my addresses
=============================================================
*/
class TextInputMyAddresses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderState: COMPONENT_RENDER_STATES.INITIAL,
      modalState: MODAL_STATE.NONE,
      locationAddress: '',
    };
  }

  //----------------------------------------------
  // change modal state
  //----------------------------------------------
  changeModalState = (newState) => {
    this.setState({ modalState: MODAL_STATE[newState] });
  };

  //----------------------------------------------------
  // handle change state
  //-----------------------------------------------------
  handleChangeRenderState = (renderState) => {
    const { locationAddress } = this.state;

    if (
      COMPONENT_RENDER_STATES[renderState] === COMPONENT_RENDER_STATES.LOCATION_LOCK
      && locationAddress.length
    ) {
      this.setState({ renderState: COMPONENT_RENDER_STATES[renderState] });
    } else if (COMPONENT_RENDER_STATES[renderState] !== COMPONENT_RENDER_STATES.LOCATION_LOCK) {
      this.setState({
        renderState: COMPONENT_RENDER_STATES[renderState],
        modalState: MODAL_STATE.INITIAL,
      });
    }
  };

  //-----------------------------------------------------
  // get user location
  //-----------------------------------------------------
  handleLocation = () => {};

  //-----------------------------------------------------
  // handle user text input
  //-----------------------------------------------------
  handleOnTextChange = (text) => {
    const { onTextChange } = this.props;
    if (onTextChange) {
      onTextChange(text);
    }
  };

  //-----------------------------------------------
  // render modal (dialog) windows
  //-----------------------------------------------
  renderModalWindows = () => {
    const { modalState } = this.state;
    const { value } = this.props;

    if (modalState === MODAL_STATE.QUESTION) {
      return (
        <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
          <View style={Styles.modalContainer}>
            <View style={Styles.modalTextContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                Etes vous sûr(e) de vouloir supprimer votre adresse
                {'"'}
                {value}
                {'"'}
?
              </Text>
            </View>
            <View style={[{ justifyContent: 'space-between' }, Styles.modalFooter]}>
              <Button
                style={Styles.modalButton}
                bordered
                onPress={this.changeModalState.bind(this, 'CONFIRMATION')}
              >
                <Text style={Styles.modalButtonText}>CONFIRMER</Text>
              </Button>
              <Button
                style={Styles.modalButton}
                bordered
                onPress={this.changeModalState.bind(this, 'NONE')}
              >
                <Text style={Styles.modalButtonText}>ANNULER</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }
    if (modalState === MODAL_STATE.CONFIRMATION) {
      return (
        <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
          <View style={Styles.modalContainer}>
            <View style={Styles.modalTextContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                {'"'}
                {value}
                {'"'}
                {' '}
? a bien été supprimée
              </Text>
            </View>
            <View style={[{ justifyContent: 'center' }, Styles.modalFooter]}>
              <Button
                style={Styles.modalButton}
                bordered
                onPress={this.handleChangeRenderState.bind(this, 'INITIAL')}
              >
                <Text style={Styles.modalButtonText}>OK</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }
    return null;
  };

  //------------------------------------------------------
  // render initial state
  //------------------------------------------------------
  renderInitialState = () => {
    const { imageLabel, containerStyle, labelText } = this.props;

    return (
      <TouchableOpacity onPress={this.handleChangeRenderState.bind(this, 'INSERT_ADDRESS')}>
        <View style={[Styles.container, containerStyle]}>
          <View style={Styles.imageContainer}>{imageLabel}</View>
          <View style={Styles.divider} />
          <Text style={Styles.mainText}>{labelText}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  //------------------------------------------------------
  // render inser address state
  //------------------------------------------------------
  renderInsertAddressState = () => {
    const { imageLabel, containerStyle } = this.props;

    return (
      <View style={[Styles.container, containerStyle]}>
        <View style={Styles.imageContainer}>{imageLabel}</View>
        <View style={Styles.divider} />
        <Form style={{ flex: 1 }}>
          <View style={Styles.textInputContainer}>
            <Item style={{ height: 20, width: 210, borderBottomColor: 'transparent' }}>
              <Input
                placeholderTextColor="#78849E"
                style={Styles.textInputField}
                placeholder="Entrez votre adresse de domicile"
                onSubmitEditing={this.handleChangeRenderState.bind(this, 'LOCATION_LOCK')}
                onChangeText={this.onTextChange}
              />
            </Item>
            <TouchableOpacity onPress={this.handleLocation}>
              <Image style={Styles.pickGeoPosition} source={Images.pickGeoPosition} />
            </TouchableOpacity>
          </View>
        </Form>
      </View>
    );
  };

  //------------------------------------------------------
  // render location lock state
  //------------------------------------------------------
  renderLocationLockState = () => {
    const { imageLabel, containerStyle, value } = this.props;

    return (
      <View style={[Styles.container, containerStyle]}>
        <View style={Styles.imageContainer}>{imageLabel}</View>
        <View style={Styles.divider} />
        <Text style={[Styles.mainText, Styles.locationContainer]}>{value}</Text>
        <TouchableOpacity onPress={this.handleChangeRenderState.bind(this, 'INSERT_ADDRESS')}>
          <Image style={Styles.actionImage} source={Images.inputEdit} />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.changeModalState.bind(this, 'QUESTION')}>
          <Image style={Styles.actionImage} source={Images.modalClose} />
        </TouchableOpacity>
      </View>
    );
  };

  //------------------------------------------------------
  // render current component state
  //------------------------------------------------------
  renderCurrentComponentState = () => {
    const { renderState } = this.state;

    const { value } = this.props;

    if (renderState === COMPONENT_RENDER_STATES.INSERT_ADDRESS) {
      return this.renderInsertAddressState();
    }

    if (value.length) {
      return this.renderLocationLockState();
    }
    return this.renderInitialState();
  };

  render() {
    return (
      <React.Fragment>
        {this.renderCurrentComponentState()}
        {this.renderModalWindows()}
      </React.Fragment>
    );
  }
}

TextInputMyAddresses.defaultProps = {
  value: '',
  editable: false,
};

export default TextInputMyAddresses;
