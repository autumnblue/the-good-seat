import React from 'react';
import {
  TouchableOpacity, View, Platform, Text, StyleSheet,
} from 'react-native';

import { Icon } from 'native-base';
import { Colors } from '../Theme';

import Styles from './Styles/RadioButtonStyles';

/*
-------------------------------------------------------------
 various icon types
-------------------------------------------------------------
*/
const CheckedIcon = (props) => {
  const { color } = props;

  const iconName = Platform.OS === 'ios' ? 'radio-button-on' : 'radio-button-on';

  return <Icon style={{ color }} name={iconName} size={22} />;
};

const UnchekedIcon = (props) => {
  const { color } = props;

  const iconName = Platform.OS === 'ios' ? 'radio-button-off' : 'radio-button-off';

  return <Icon style={{ color }} name={iconName} size={22} />;
};

const RadioButton = (props) => {
  const { ...rest } = props;

  const {
    checked, color, label, containerStyle, textStyle, onPress, ...attributes
  } = rest;

  const RadioButtonIcon = checked ? <CheckedIcon color={color} /> : <UnchekedIcon color={color} />;

  return (
    <TouchableOpacity
      {...attributes}
      onPress={onPress}
      style={StyleSheet.flatten([Styles.container, containerStyle && containerStyle])}
    >
      <View style={Styles.wrapper}>
        {React.isValidElement(label)
          ? label
          : label && (
          <Text testID="radioButtonTitle" style={Styles.mainText}>
            {label}
          </Text>
          )}
        {RadioButtonIcon}
      </View>
    </TouchableOpacity>
  );
};

RadioButton.defaultProps = {
  color: Colors.primary,
};

export default RadioButton;
