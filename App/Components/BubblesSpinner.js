import React, { Component } from 'react';
import { Animated, View } from 'react-native';

import Styles from './Styles/BubblesSpinnerStyles';

class BubblesSpinner extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);

    this.state = {
      bubble01: new Animated.Value(0),
      bubble02: new Animated.Value(0),
      bubble03: new Animated.Value(0),
    };
  }

  //--------------------------------------------------------
  //
  //--------------------------------------------------------
  componentDidMount() {
    this.animate();
  }

  //--------------------------------------------------------
  //
  //--------------------------------------------------------
  componentWillUnmount() {
    this.unmounted = true;
  }

  //---------------------------------------------------------
  // animation
  //---------------------------------------------------------
  animate() {
    const { bubble01, bubble02, bubble03 } = this.state;

    const animation = Animated.parallel([
      Animated.sequence([
        Animated.timing(bubble01, {
          toValue: 1.0,
          duration: 600,
          //   useNativeDriver: true
        }),
        //  Animated.timing(this.state.bubble01, {
        //    toValue: 0.0,
        //   duration: 600,
        //   useNativeDriver: true
        //  })
      ]),
      Animated.sequence([
        Animated.timing(bubble02, {
          toValue: 1.0,
          duration: 600,
          delay: 100,
          //     useNativeDriver: true
        }),
        //  Animated.timing(this.state.bubble02, {
        //    toValue: 1,
        //    duration: 600,
        //    useNativeDriver: true
        //  })
      ]),
      Animated.sequence([
        Animated.timing(bubble03, {
          toValue: 1.0,
          duration: 600,
          delay: 220,
          //      useNativeDriver: true
        }),
        //    Animated.timing(this.state.bubble03, {
        //     toValue: 1,
        //     duration: 400,
        //     useNativeDriver: true
        //   })
      ]),
    ]);

    Animated.loop(animation).start();
  }

  render() {
    const { bubble01, bubble02, bubble03 } = this.state;

    return (
      <View style={Styles.container}>
        <Animated.View
          style={[
            Styles.bubble,
            {
              transform: [{ scale: bubble01 }],
            },
          ]}
        />
        <Animated.View
          style={[
            Styles.bubble,
            {
              transform: [{ scale: bubble02 }],
            },
          ]}
        />
        <Animated.View
          style={[
            Styles.bubble,
            {
              transform: [{ scale: bubble03 }],
            },
          ]}
        />
      </View>
    );
  }
}

BubblesSpinner.defaultProps = {
  size: 90,
};

export default BubblesSpinner;
