import React, { Component } from 'react';
import { Text } from 'native-base';
import { Image, View, TouchableOpacity } from 'react-native';
import moment from 'moment';
import Rating from './Rating';
import Styles from './Styles/TripCardStyles';
import { Colors, Images } from '../Theme';
import { TRIP_CARD_TYPES, TYPE_CAR_COLOR_MAPPING } from '../MobX/Constants';
import { dateToDMY } from '../Services/Utils';

/*
================================================
 trip card component
================================================
*/
class TripCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  //----------------------------------------------
  // repeat trip action
  //----------------------------------------------
  handleRepeatTrip = () => {
    const { onRepeatTrip } = this.props;

    if (onRepeatTrip) onRepeatTrip();
  };

  //----------------------------------------------
  // handle send review trip
  //----------------------------------------------
  handleSentReviewTrip = () => {
    const { onSentReviewTrip } = this.props;
    if (onSentReviewTrip) onSentReviewTrip();
  };

  //----------------------------------------------
  // cancel trip action
  //----------------------------------------------
  onCancelTrip = () => {
    const { onCancelTrip } = this.props;

    if (onCancelTrip) onCancelTrip();
  };

  //----------------------------------------------
  // render raing label
  //----------------------------------------------
  renderRatingLabel = () => {
    const { rating } = this.props;

    if (rating >= 0) {
      return <Image source={Images.dumpRating} />;
    }
    return <Text style={Styles.secondaryText}>Noter la course</Text>;
  };

  //-----------------------------------------------------
  // render canceled trip card
  //-----------------------------------------------------
  renderCanceledTripCard = () => {
    const { rideData } = this.props;

    const borderColor = rideData.providerCode.length
      ? Colors[TYPE_CAR_COLOR_MAPPING[rideData.providerCode]]
      : Colors.unknownCar;

    return (
      <View style={[{ borderColor }, Styles.container]}>
        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Course&nbsp;
            {rideData.providerName}
          </Text>
          <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
            <View style={Styles.ratingButton}>
              <Text style={Styles.secondaryText}>Annulee</Text>
            </View>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Type de course :&nbsp;
            {rideData.providerOfferName}
          </Text>
          <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.startDate
                ? dateToDMY(new Date(rideData.startDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.home} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={2} style={Styles.secondaryText}>
              {rideData.startFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.endDate
                ? dateToDMY(new Date(rideData.endDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
              -
              {' '}
              {moment()
                .startOf('day')
                .seconds(rideData.duration)
                .format('H:mm:ss')}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.flag} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={1} style={Styles.secondaryText}>
              {rideData.endFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.price}
              &nbsp;
              {rideData.currency}
            </Text>
          </View>
        </View>
        <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
          <Text style={Styles.secondaryText}>
            Chauffeur : &nbsp;
            {rideData.driverName ? rideData.driverName : 'no data'}
          </Text>
          {rideData.driverPhoto ? (
            <Image style={Styles.imageDriver} source={{ uri: rideData.driverPhoto }} />
          ) : (
            <Image style={Styles.imageDriver} source={Images.dumpDriverPhoto} />
          )}
        </View>
      </View>
    );
  };

  //----------------------------------------------------
  // render ended cut trip card
  //----------------------------------------------------
  renderEndedCutTripCard = () => {
    const { rideData } = this.props;

    const borderColor = rideData.providerCode.length
      ? Colors[TYPE_CAR_COLOR_MAPPING[rideData.providerCode]]
      : Colors.unknownCar;

    let ratingBlockRenderData = (
      <TouchableOpacity onPress={this.handleSentReviewTrip}>
        <View style={Styles.ratingButton}>
          <Text style={Styles.secondaryText}>Noter la course</Text>
        </View>
      </TouchableOpacity>
    );

    if (rideData.userRatingAveraged && rideData.userRatingAveraged > 0) {
      ratingBlockRenderData = (
        <View style={Styles.ratingButton}>
          <Rating startingRating={parseInt(rideData.userRatingAveraged, 10)} size={12} readonly />
        </View>
      );
    }

    return (
      <View style={[{ borderColor }, Styles.container]}>
        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Course&nbsp;
            {rideData.providerName}
          </Text>
          <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
            {ratingBlockRenderData}
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Type de course :&nbsp;
            {rideData.providerOfferName}
          </Text>
          <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.startDate
                ? dateToDMY(new Date(rideData.startDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.home} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={2} style={Styles.secondaryText}>
              {rideData.startFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.endDate
                ? dateToDMY(new Date(rideData.endDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
              -
              {' '}
              {moment()
                .startOf('day')
                .seconds(rideData.duration)
                .format('H:mm:ss')}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.flag} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={1} style={Styles.secondaryText}>
              {rideData.endFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.price}
              &nbsp;
              {rideData.currency}
            </Text>
          </View>
        </View>
        <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
          <Text style={Styles.secondaryText}>
            Chauffeur : &nbsp;
            {rideData.driverName ? rideData.driverName : 'no data'}
          </Text>
          {rideData.driverPhoto ? (
            <Image style={Styles.imageDriver} source={{ uri: rideData.driverPhoto }} />
          ) : (
            <Image style={Styles.imageDriver} source={Images.dumpDriverPhoto} />
          )}
        </View>
      </View>
    );
  };

  //----------------------------------------------------
  // render ended trip card
  //----------------------------------------------------
  renderEndedTripCard = () => {
    const { rideData } = this.props;

    const borderColor = rideData.providerCode.length
      ? Colors[TYPE_CAR_COLOR_MAPPING[rideData.providerCode]]
      : Colors.unknownCar;

    let ratingBlockRenderData = (
      <TouchableOpacity onPress={this.handleSentReviewTrip}>
        <View style={Styles.ratingButton}>
          <Text style={Styles.secondaryText}>Noter la course</Text>
        </View>
      </TouchableOpacity>
    );

    if (rideData.userRatingAveraged && rideData.userRatingAveraged > 0) {
      ratingBlockRenderData = (
        <View style={Styles.ratingButton}>
          <Rating startingRating={parseInt(rideData.userRatingAveraged, 10)} size={12} readonly />
        </View>
      );
    }

    return (
      <View style={[{ borderColor }, Styles.container]}>
        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Course&nbsp;
            {rideData.providerName}
          </Text>
          <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
            <TouchableOpacity onPress={this.handleRepeatTrip}>
              <View style={[{ marginRight: 5 }, Styles.ratingButton]}>
                <Text style={Styles.secondaryText}>Commander à nouveau</Text>
              </View>
            </TouchableOpacity>
            {ratingBlockRenderData}
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Type de course :&nbsp;
            {rideData.providerOfferName}
          </Text>
          <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.startDate
                ? dateToDMY(new Date(rideData.startDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.home} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={2} style={Styles.secondaryText}>
              {rideData.startFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.endDate
                ? dateToDMY(new Date(rideData.endDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
              -
              {' '}
              {moment()
                .startOf('day')
                .seconds(rideData.duration)
                .format('H:mm:ss')}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.flag} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={1} style={Styles.secondaryText}>
              {rideData.endFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.price}
              &nbsp;
              {rideData.currency}
            </Text>
          </View>
        </View>
        <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
          <Text style={Styles.secondaryText}>
            Chauffeur : &nbsp;
            {rideData.driverName ? rideData.driverName : 'no data'}
          </Text>
          {rideData.driverPhoto ? (
            <Image style={Styles.imageDriver} source={{ uri: rideData.driverPhoto }} />
          ) : (
            <Image style={Styles.imageDriver} source={Images.dumpDriverPhoto} />
          )}
        </View>
      </View>
    );
  };

  //----------------------------------------------------
  // render futured trip card
  //----------------------------------------------------
  renderFuturedTripCard = () => {
    const { rideData } = this.props;

    const borderColor = rideData.providerCode.length
      ? Colors[TYPE_CAR_COLOR_MAPPING[rideData.providerCode]]
      : Colors.unknownCar;

    return (
      <View style={[{ borderColor }, Styles.container]}>
        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Course&nbsp;
            {rideData.providerName}
          </Text>
          <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
            <TouchableOpacity>
              <View style={[{ marginRight: 5 }, Styles.ratingButton]}>
                <Text style={Styles.secondaryText}>Modifier la course</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={Styles.ratingButton}>
                <Text style={Styles.secondaryText}>Annuler la course</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Text style={Styles.mainText}>
            Type de course :&nbsp;
            {rideData.providerOfferName}
          </Text>
          <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.startDate
                ? dateToDMY(new Date(rideData.startDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.home} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={2} style={Styles.secondaryText}>
              {rideData.startFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.endDate
                ? dateToDMY(new Date(rideData.endDate), '/')
                : dateToDMY(new Date(), '/')}
              {' '}
              -
              {' '}
              {moment()
                .startOf('day')
                .seconds(rideData.duration)
                .format('H:mm:ss')}
            </Text>
          </View>
        </View>

        <View style={Styles.gridRow}>
          <Image style={Styles.imageLabel} source={Images.flag} />
          <View style={{ width: 0, flexGrow: 1 }}>
            <Text numberOfLines={1} style={Styles.secondaryText}>
              {rideData.endFullAddress}
            </Text>
          </View>
          <View style={{ flex: 0.6, alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={Styles.secondaryText}>
              {rideData.price}
              &nbsp;
              {rideData.currency}
            </Text>
          </View>
        </View>
        <View style={[{ justifyContent: 'flex-end' }, Styles.gridRow]}>
          <Text style={Styles.secondaryText}>
            Chauffeur : &nbsp;
            {rideData.driverName ? rideData.driverName : 'no data'}
          </Text>
          {rideData.driverPhoto ? (
            <Image style={Styles.imageDriver} source={{ uri: rideData.driverPhoto }} />
          ) : (
            <Image style={Styles.imageDriver} source={Images.dumpDriverPhoto} />
          )}
        </View>
      </View>
    );
  };

  render() {
    const { type } = this.props;

    switch (type) {
      case TRIP_CARD_TYPES.ENDED:
        return this.renderEndedTripCard();
      case TRIP_CARD_TYPES.ENDED_CUT:
        return this.renderEndedCutTripCard();
      case TRIP_CARD_TYPES.CANCELED:
        return this.renderCanceledTripCard();
      case TRIP_CARD_TYPES.FUTURED:
        return this.renderFuturedTripCard();
      default:
        return this.renderEndedTripCard();
    }
  }
}

TripCard.defaultProps = {
  type: TRIP_CARD_TYPES.ENDED,
  typeCarColor: Colors.taxiBrand,
  rating: -1,
};

export default TripCard;
