import React, { Component } from 'react';
import { View, Animated } from 'react-native';

import Styles from './Styles/ProgressIndicatorStyles';
import { Colors } from '../Theme';

class ProgressIndicator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      widthValue: new Animated.Value(0),
    };
  }

  //--------------------------------------------------------
  //
  //--------------------------------------------------------
  componentDidMount() {
    this.animate();
  }

  //--------------------------------------------------------
  //
  //--------------------------------------------------------
  componentWillUnmount() {
    this.unmounted = true;
  }

  //---------------------------------------------------------
  // animation
  //---------------------------------------------------------
  animate() {
    const { widthValue } = this.state;
    const animation = Animated.timing(widthValue, {
      toValue: 1.0,
      duration: 900,
      useNativeDriver: false,
    });

    Animated.loop(animation).start();
  }

  render() {
    const { widthValue } = this.state;
    const { height, color } = this.props;

    return (
      <View style={[Styles.container, { height, backgroundColor: color }]}>
        <Animated.View
          style={[
            Styles.progress,
            {
              width: widthValue.interpolate({
                inputRange: [0, 1],
                outputRange: ['0%', '100%'],
              }),
            },
          ]}
        />
      </View>
    );
  }
}

ProgressIndicator.defaultProps = {
  height: 15,
  color: Colors.primary,
};

export default ProgressIndicator;
