import React, { Component } from 'react';
import { View, Image, TouchableHighlight } from 'react-native';

import Styles from './Styles/RatingStyles';
import { Images } from '../Theme';

class Rating extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratingPosition: 0,
    };
  }

  componentDidMount() {
    const { startingRating } = this.props;

    this.setCurrentRating(startingRating);
  }

  //--------------------------------------------------------
  // set current rating
  //--------------------------------------------------------
  setCurrentRating = (rating) => {
    const { ratingCount, onRatingChange, size } = this.props;
    const imageSize = size;

    let value = 0;

    if (rating > ratingCount) {
      value = ratingCount * imageSize;
    } else if (rating < 0) {
      value = 0;
    } else {
      value = rating * imageSize;
    }

    if (onRatingChange) onRatingChange(rating);

    this.setState({ ratingPosition: value });
  };

  //------------------------------------------------------------
  // render rating
  //------------------------------------------------------------
  renderRatings = () => {
    const { ratingImageSource, ratingCount, size } = this.props;

    const ratingRenderData = [];

    const ratingImage = <Image source={ratingImageSource} style={{ width: size, height: size }} />;

    for (let index = 0; index < ratingCount; index += 1) {
      ratingRenderData.push(
        <TouchableHighlight underlayColor="#ffffff" key={index} activeOpacity={0.5} onPress={this.setCurrentRating.bind(this, index + 1)}>
          <View style={Styles.starContainer}>{ratingImage}</View>
        </TouchableHighlight>,
      );
    }
    return ratingRenderData;
  };

  render() {
    const {
      style, readonly, ratingColor, size,
    } = this.props;
    const { ratingPosition } = this.state;

    return (
      <View pointerEvents={readonly ? 'none' : 'auto'} style={style}>
        <View style={Styles.starsWrapper}>
          <View style={Styles.starsInsideWrapper}>
            <View style={{ width: ratingPosition, height: size, backgroundColor: ratingColor }} />
          </View>
          {this.renderRatings()}
        </View>
      </View>
    );
  }
}

Rating.defaultProps = {
  readonly: false,
  ratingImageSource: Images.ratingStar,
  ratingColor: '#f1c40f',
  ratingBackgroundColor: 'white',
  ratingCount: 5,
  size: 24,
};

export default Rating;
