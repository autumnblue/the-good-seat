import React, { Component } from 'react';
import { DatePickerAndroid, TimePickerAndroid, View } from 'react-native';

class DateTimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultDate: props.defaultDate ? props.defaultDate : new Date(),
      chosenDate: undefined,
    };
  }

  //----------------------------------------------
  // handle select
  //----------------------------------------------
  handleSelect = (chosenDate) => {
    // const { chosenDate } = this.state;
    const { onSelect } = this.props;

    if (onSelect) onSelect(chosenDate);
  };

  //----------------------------------------------
  // handle cancel
  //----------------------------------------------
  handleCancel = () => {
    const { onCancel } = this.props;

    if (onCancel) onCancel();
  };

  //-----------------------------------------------------
  // open time picker
  //-----------------------------------------------------
  async openTimePicker() {
    const { defaultDate } = this.state;

    console.log('default android - time', defaultDate);

    const timeOptions = {
      is24Hour: false,
      hour: defaultDate.getHours(),
      minute: defaultDate.getMinutes(),
    };
    try {
      const newDate = await TimePickerAndroid.open(timeOptions);
      const { action, hour, minute } = newDate;
      if (action === 'timeSetAction') {
        const selectedDate = new Date(defaultDate);
        selectedDate.setHours(hour);
        selectedDate.setMinutes(minute);
        this.handleSelect(selectedDate);
        // this.setState({ chosenDate: selectedDate }, () => this.handleSelect());
      } else {
        this.handleCancel();
        //  this.setState({ chosenDate: defaultDate }, () => this.handleSelect());
      }
    } catch ({ code, message }) {
      console.warn('Cannot open time picker', message);
    }
  }

  //-----------------------------------------------------
  // open date picker
  //-----------------------------------------------------
  async openDatePicker() {
    const { chosenDate, defaultDate } = this.state;

    console.log('default android - date', defaultDate);

    try {
      const newDate = await DatePickerAndroid.open({
        date: chosenDate || defaultDate,
      });
      const {
        action, year, month, day,
      } = newDate;
      if (action === 'dateSetAction') {
        const selectedDate = new Date(
          year,
          month,
          day,
          defaultDate.getHours(),
          defaultDate.getMinutes(),
        );
        this.handleSelect(selectedDate);
        // this.setState({ chosenDate: selectedDate }, () => this.handleSelect());
      } else {
        this.handleCancel();
        // this.setState({ chosenDate: defaultDate }, () => this.handleSelect());
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  render() {
    const { mode } = this.props;

    if (mode === 'date') {
      this.openDatePicker();
    } else if (mode === 'time') {
      this.openTimePicker();
    }
    return <View />;
  }
}

DateTimePicker.defaultProps = {
  mode: 'date',
  visible: false,
};

export default DateTimePicker;
