import { StyleSheet } from 'react-native';
/*
=========================================================================
 colors palette
=========================================================================
*/
export default StyleSheet.create({
  actionContainer: {
    marginTop: 19.5,
    justifyContent: 'flex-start',
    paddingLeft: 25.5,
  },
  actionButton: {
    height: 30,
    backgroundColor: '#009FE3',
    borderRadius: 4,
  },
  actionButtonDisabled: {
    height: 30,
    backgroundColor: '#d1d1d1',
    borderRadius: 4,
  },
  actionButtonText: {
    fontSize: 15,
    color: '#fff',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },

  actionButtonTextDisabled: {
    fontSize: 15,
    color: '#555',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
});
