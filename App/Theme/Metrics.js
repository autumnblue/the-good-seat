import { Dimensions, PixelRatio } from 'react-native';

const { width, height } = Dimensions.get('window');
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen');

const screenWidthHeightSumm = screenHeight + screenWidth;
/*
screen vertical size (iPhone X)
*/
const refScreenHeight = 812;

/*
----------------------------------------------------------------
 conver % to DP
----------------------------------------------------------------
*/
const widthPercentToDP = (widthPercent) => {
  // parse float
  const elemWidth = parseFloat(widthPercent);

  // round pixels
  return PixelRatio.roundToNearestPixel((width * elemWidth) / 100);
};

/*
----------------------------------------------------------------
 conver % to DP
----------------------------------------------------------------
*/
const heightPercentToDP = (heightPercent) => {
  // parse float
  const elemHeight = parseFloat(heightPercent);

  // round pixels
  return PixelRatio.roundToNearestPixel((height * elemHeight) / 100);
};

// can be used as Metrics.buttonRadius
const metrics = {
  buttonRadius: 12,
  scaleFactor: height / refScreenHeight,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  widthPercentToDP,
  heightPercentToDP,
  screenUnit: screenWidthHeightSumm * 0.004,
};

export default metrics;
