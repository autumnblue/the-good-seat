/*
=========================================================================
 main colors palette
=========================================================================
*/
const Colors = {
  white: '#fff',
  black: '#000',
  primary: '#009FE3',
  secondary: '#881877',
  facebook: '#3B5994',
  onPrimaryText: '#78849E',
  placeholderText: 'rgba(255, 255, 255, 0.5)',
  taxiCar: '#FFB900',
  uberCar: '#C840E9',
  lecabCar: '#009DDF',
  unknownCar: '#3B5994',
  disable: '#d1d1d1',
  error: '#B00020',
};

export default Colors;
