import { Platform } from 'react-native';

const ANDROID_FONT_MAPPINGS = {
  Gibson: {
    300: 'Gibson-Light',
    400: 'Gibson-Regular',
    500: 'Gibson-bold',
    600: 'Gibson-bold',
  },
  SegoeUI: {
    300: 'SegoeUI-Light',
    400: 'SegoeUI',
    500: 'SegoeUI',
  },
  Roboto: {
    400: 'Roboto-Regular',
    500: 'Roboto-Medium',
  },
  'Open Sans': {
    400: 'OpenSans-Regular',
    700: 'OpenSans-Bold',
  },
};

export default function addFontFamily(family, weight = '400') {
  if (Platform.OS === 'ios') {
    return { fontFamily: family, fontWeight: weight };
  }

  return { fontFamily: ANDROID_FONT_MAPPINGS[family === undefined ? 'Roboto' : family][weight] };
}
