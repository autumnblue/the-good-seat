import React, { PureComponent } from 'react';
// import codePush from "react-native-code-push";
import { Provider } from 'mobx-react/native';
import { Sentry, SentryLog } from 'react-native-sentry';
import appState from '../MobX';
import App from './App';

/* eslint max-len: [2, 100,  { "ignoreUrls": true }] */
/* eslint max-len: [2, 100, { "ignoreTrailingComments": true }] */
try {
  Sentry.config(
    'https://18ab223023084b6bb180f495413a5e0b:37974de9adc94b79b1120008c1b3a63a@sentry.io/1371474', {
      deactivateStacktraceMerging: false, // default: true | Deactivates the stacktrace merging feature
      logLevel: SentryLog.Debug, // default SentryLog.None | Possible values:  .None, .Error, .Debug, .Verbose
      disableNativeIntegration: false, // default: false | Deactivates the native integration and only uses raven-js
      handlePromiseRejection: true, // default: true | Handle unhandled promise rejections
      // ---------------------------------
    },
  ).install();
  global.globalStore = appState;
  global.Sentry = Sentry;
} catch (e) {
  console.log(e);
}

/*
===================================================================
  main entry point
==================================================================
*/
class RootContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Provider appState={appState} text="chula">
        <App />
      </Provider>
    );
  }
}

// RootContainer = codePush(RootContainer);

export default RootContainer;
