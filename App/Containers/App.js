import React, { Component } from 'react';
import {
  AsyncStorage, View, NativeModules, Platform,
} from 'react-native';
import 'moment/locale/ru';
import 'moment/locale/fr';
import 'moment/locale/en-gb';
import moment from 'moment';
import { observer, inject } from 'mobx-react';
import OneSignal from 'react-native-onesignal';
import { Sentry } from 'react-native-sentry';
import Navigation from '../Navigation/Navigation';
import { getErrorCodes } from '../Services/BackEnd/TheGoodSeat';
// import EventTracker from '../Services/BackEnd/thegoodseat/EventTracker';

/*
===================================================================
 main (entry point)
 ==================================================================
 */
@inject('appState')
@observer
class App extends Component {
  constructor() {
    super();
    OneSignal.init('12c0de39-486c-49d9-bd31-5e114060e789', {
      kOSSettingsKeyAutoPrompt: true,
    });
  }

  //--------------------------------------------------
  // before render
  //--------------------------------------------------
  async componentWillMount() {
    const { appState } = this.props;

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    let language;

    if (Platform.OS === 'android') {
      language = NativeModules.I18nManager.localeIdentifier;
    } else {
      language = NativeModules.SettingsManager.settings.AppleLocale;
    }

    // get code and set language
    if (language) {
      language = language.substr(0, 2);

      // only two language support
      if (language !== 'en' && language !== 'fr') {
        language = 'en';
      }

      appState.sessionParameters.language = language;

      // set locale for date and time
      moment.locale(language);
    }
    // load auth token
    const result = await AsyncStorage.getItem('AUTH_TOKEN');
    if (result && appState) {
      appState.sessionParameters.setAppToken(result);
    } else {
      // NOTE - deleted (not applicable)
      // this.getAuthToken();
    }

    // load USER
    AsyncStorage.getItem('CURRENT_USER', (err, user) => {
      if (user === null) {
        // NOTE - deleted (not applicable)
        // this.getAuthToken();
      } else {
        try {
          appState.sessionParameters.setCurrentUser(JSON.parse(user));
          const {
            email,
            _id,
            username,
            roles,
            mangoPayId,
          } = appState.sessionParameters.currentUser;
          Sentry.setUserContext({
            email,
            userId: _id && `${_id}`,
            username,
            roles,
            mangoPayId,
          });
        } catch (e) {
          console.warn('Error in app user init', e);
        }
      }
    });

    AsyncStorage.getItem('ERROR_CODES_CACHE', (err, codes) => {
      try {
        if (
          !appState.sessionParameters.errorsDictionary
          || Object.keys(appState.sessionParameters.errorsDictionary).length < 1
        ) {
          appState.sessionParameters.errorsDictionary = codes;
        }
      } catch (e) {
        console.warn('Error while getting codes from cache', e);
      }
    });

    // fetch errors async because this should stop the app from launching
    try {
      getErrorCodes(appState.constants.THEGOODSEAT_API).then((data) => {
        if (data && data.body) {
          appState.sessionParameters.errorsDictionary = data.body;
        }
      });
    } catch (e) {
      console.warn('Error while getting codes from api', e);
    }
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived = (notification) => {
    console.log('Notification received: ', notification);
  };

  onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  onIds(device) {
    console.log('Device info: ', device);
    if (this.props) {
      const { appState } = this.props;
      appState.sessionParameters.setCurrentUser({
        ...appState.sessionParameters.currentUser,
        oneSignalPlayerId: device,
      });
    }
  }

  // gets the current screen from navigation state
  getActiveRouteName(navigationState) {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return this.getActiveRouteName(route);
    }
    return route.routeName;
  }

  //----------------------------------------------------
  // get (fetch) auth token
  //----------------------------------------------------
  getAuthToken = () => {
    const { appState } = this.props;

    const url = `${appState.constants.THEGOODSEAT_API}/auth/user`;

    fetch(url)
      .then((response) => {
        if (response.status === 401) {
          return {
            error: 'wrong auth',
          };
        }
        if (response.ok) {
          return response.json();
        }
        return 'error';
      })
      .then(() => {})
      .catch(() => {});
  };

  render() {
    // const { appState } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Navigation
          onNavigationStateChange={() => {
            // onNavigationStateChange={(prevState, currentState) => {
            //  const currentScreen = this.getActiveRouteName(currentState);
            //  const prevScreen = this.getActiveRouteName(prevState);
            /*   if (prevScreen !== currentScreen) {
              // the line below uses the Google Analytics tracker
              // change the tracker here to use other Mobile analytics SDK.
              // tracker.trackScreenView(currentScreen);
              try {
                EventTracker.track(`pageview:${currentScreen}`);
                Sentry.captureBreadcrumb({
                  message: `Page: ${currentScreen}`,
                  category: 'pageview',
                  data: {
                    userId: appState.sessionParameters.currentUser
                      ? appState.sessionParameters.currentUser._id
                      : '',
                  },
                });
              } catch (e) {
                console.log('nav error', e);
              }
            } */
          }}
        />
      </View>
    );
  }
}

export default App;
