import { observable, action } from 'mobx';
import Config from 'react-native-config';
import axios from 'axios';
import MXUiState from './UiState';
import MXSessionParameters from './SessionParameters';
import MXDriveStageState from './DriveStage';
import MXFavoriteAddresses from './FavoriteAddresses';
import TextData from '../Services/TextData';
import appTextValues from '../Services/appTextValues';


/*
--------------------------------------------------
constants store
-------------------------------------------------
*/
class MXConstants {
  @observable MAPBOX_TOKEN = Config && Config.MAPBOX_TOKEN ? Config.MAPBOX_TOKEN : 'pk.eyJ1IjoiZGV2dGhlZ29vZHNlYXQiLCJhIjoiY2pzaDgzaDJxMXUzazQ1bWwyNjRpMjZ3eSJ9.gr5iyfaden7yQQJqaKwo5g';

  @observable MAPBOX_GEOCODING_API = Config && Config.MAPBOX_GEOCODING_API ? Config.MAPBOX_GEOCODING_API : 'https://api.tiles.mapbox.com/geocoding/v5/';

  @observable MAPBOX_DIRECTION_API = Config && Config.MAPBOX_DIRECTION_API ? Config.MAPBOX_DIRECTION_API : 'https://api.mapbox.com/directions/v5/mapbox/';

  @observable THEGOODSEAT_API = Config && Config.API_URL ? Config.API_URL : 'https://api.thegoodseat.fr:20443/api'; // 'https://thegoodseat-api.dev.enyosolutions.com/api';

  @observable MANGOPAY_API = Config && Config.MANGOPAY_API ? Config.MANGOPAY_API : 'https://api.mangopay.com'; // 'https://api.sandbox.mangopay.com';

  @observable MANGOPAY_TOKEN = Config && Config.MANGOPAY_TOKEN ? Config.MANGOPAY_TOKEN : '9g6HbbtcbTBJWmVraUt674TQykc4UbqCaKLMaHBddMoKCDQi8N';

  @observable MANGOPAY_CLIENTID = Config && Config.MANGOPAY_CLIENTID ? Config.MANGOPAY_CLIENTID : 'thegoodseatprod';

  @observable GOOGLE_CLIENT_ID = Config && Config.GOOGLE_CLIENT_ID ? Config.GOOGLE_CLIENT_ID : '960143972329-1vnb7sb1ied2i2gk8bc5p0kih0hb1b2o.apps.googleusercontent.com'; // wip 228806129489-3cgtge46qdc2ut4kbdfjpvfonq5nldb1.apps.googleusercontent.com

  @observable FACEBOOK_CLIENT_ID = Config && Config.FACEBOOK_CLIENT_ID ? Config.FACEBOOK_CLIENT_ID : '340434749921012'; // wip 1572953176184887

  @observable UBER_CLIENT_ID = Config && Config.UBER_CLIENT_ID ? Config.UBER_CLIENT_ID : 'MvhHkYO2s2Mo82w_lctD3oR4rV7_BiRQ'; // wip MvhHkYO2s2Mo82w_lctD3oR4rV7_BiRQ

  @observable firstRun = true;

  @observable USER_AGE_LIMIT = 18;
}
/*
-------------------------------------------------
main store
-------------------------------------------------
*/
class MXAppState {
  @observable appIsReady = false;

  constructor() {
    this.constants = new MXConstants();
    this.sessionParameters = new MXSessionParameters();
    this.uiState = new MXUiState();
    this.sessionParameters.uiState = this.uiState;

    this.driveStageState = new MXDriveStageState();
    this.favoriteAddresses = new MXFavoriteAddresses();

    axios.defaults.withCredentials = true;
    this.$http = axios.create({
      baseURL: this.constants.THEGOODSEAT_API,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        test: new Date(),
      },
    });

    this.sessionParameters.$http = this.$http;

    this.appTextData = new TextData(appTextValues);
  }

  @action.bound
  appStart() {
    this.appIsReady = true;
  }

  @action.bound
  appShutdown() {
    this.appIsReady = false;
  }
}

export default new MXAppState();
