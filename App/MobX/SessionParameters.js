/* eslint-disable no-underscore-dangle */
import { observable, action, computed } from 'mobx';
import { AsyncStorage } from 'react-native';
import OneSignal from 'react-native-onesignal';

/*
--------------------------------------------------
session parameters store
-------------------------------------------------
*/
class MXSessionParameters {
  @observable appToken = false;

  @observable
  newUserRegistrationState = {
    isSocialLogon: false,
    isPhonenumberVerified: false,
    newUserID: -1,
    firstName: '',
    lastName: '',
    dateOfBirth: '',
    country: '',
    email: '',
    phoneNumber: '',
    password: '',
    currentStage: 1,
    prevStage: 1,
  };

  @observable gpsLocationData = {
    latitude: 48.293024,
    longitude: 4.079306,
    timestamp: 0,
  };

  // user location by default !!!
  // don't edit, otherwise you see blue screen on Android
  // this is coordinates is ( 6bis Rue des Bas Trévois, 10000 Troyes, France )
  @observable userLocation = {
    latitude: 48.293024,
    longitude: 4.079306,
  };

  @observable currentUser = {};

  @observable _mapCenter = { latitude: 48.293024, longitude: 4.079306, timestamp: 0 };

  @observable mapCenteringRequested = false;

  @observable _currentPaymentCardId = 0;

  @observable _errorsDictionary = {};

  @observable _language = 'fr';

  @action.bound
  setAppToken(token) {
    this.appToken = token;
    // AsyncStorage.setItem('AUTH_TOKEN', token);
    console.log('APP TOKEN IS SET');
    try {
      this.$http.defaults.headers.authorization = `Bearer ${token}`;
    } catch (e) {
      console.warn(e);
    }
  }

  @action.bound
  setCurrentUser(user) {
    this.currentUser = user;
    this.setUserNotificationTags();
  }

  @action.bound
  setUserLocation(location) {
    this.userLocation.latitude = location.latitude;
    this.userLocation.longitude = location.longitude;
    AsyncStorage.setItem('LAST_USER_POSITION', JSON.stringify(this.userLocation));
  }

  @action.bound
  setUserNotificationTags() {
    const {
      email, firstname, lastname, postalCode,
    } = this.currentUser;
    OneSignal.sendTags({
      email,
      firstname,
      lastname,
      postalCode,
    });
    OneSignal.setEmail(email);
  }

  @computed
  get mapCenter() {
    return this._mapCenter;
  }

  set mapCenter(data) {
    this._mapCenter.latitude = data.latitude;
    this._mapCenter.longitude = data.longitude;
    this._mapCenter.timestamp = data.timestamp;
  }

  @action.bound
  logout() {
    this.currentUser = {};
    this.appToken = null;

    AsyncStorage.removeItem('AUTH_TOKEN');
    AsyncStorage.removeItem('CURRENT_USER');
  }

  @computed
  get currentPaymentCardId() {
    return this._currentPaymentCardId;
  }

  set currentPaymentCardId(id) {
    this._currentPaymentCardId = id;
  }

  @computed
  get errorsDictionary() {
    return this._errorsDictionary;
  }

  set errorsDictionary(data) {
    this._errorsDictionary = data;
  }

  @computed
  get language() {
    return this._language;
  }

  set language(data) {
    this._language = data;
  }

  @computed
  get userRegistrationState() {
    return this.newUserRegistrationState;
  }

  set setUserRegistrationState(state) {
    this.newUserRegistrationState = state;
  }

  @action
  resetRegistrationState() {
    this.newUserRegistrationState = {
      isSocialLogon: false,
      isPhonenumberVerified: false,
      newUserID: -1,
      firstName: '',
      lastName: '',
      dateOfBirth: '',
      country: '',
      email: '',
      phoneNumber: '',
      password: '',
      currentStage: 1,
      prevStage: 1,
    };
  }
}

export default MXSessionParameters;
