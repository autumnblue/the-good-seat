/*
-------------------------------------------------------
 drawer menu section
-------------------------------------------------------
*/
export const DRAWER_MENU_SECTIONS = {
  MAIN: 0,
  MY_PROFILE: 1,
  MY_PAYMENT_METODS: 2,
  MY_ADRESSES: 3,
  MY_TRIPS: 4,
  MY_VOUCHERS: 5,
};
/*
-------------------------------------------------------
 address bar (menu) section
-------------------------------------------------------
*/
export const ADDRESS_BAR_VIEW_STATE = {
  CLOSED: 0,
  OPENED: 1,
  ENTER_STARTING_ADDRESS: 2,
  ENTER_DESTINATION_ADDRESS: 3,
  SHOW_DESTINATION: 4,
  SHOW_LOCATION: 5,
};

/*
-------------------------------------------------------
 brand color taxi mapping
-------------------------------------------------------
*/
export const TYPE_CAR_COLOR_MAPPING = {
  uber: 'uberCar',
  alloCab: 'allocabCar',
  lecab: 'lecabCar',
  letaxi: 'taxiCar',
  unknown: 'unknownCar',
};

/*
--------------------------------------------------------
 trip card types
--------------------------------------------------------
*/
export const TRIP_CARD_TYPES = {
  ENDED: 0,
  ENDED_CUT: 1,
  CANCELED: 2,
  FUTURED: 3,
};

/*
--------------------------------------------------------
 map view (render) states
--------------------------------------------------------
*/
export const MAP_VIEW_STATE = {
  LOADING: 0,
  NAVIGATION: 1,
  SHOW_STARTING_POINT: 2,
  TAXI_OFFERS: 3,
  SHOW_DESTINATION_ROUTE: 4,
  SHOW_ROUTE_FROM_OFFER: 5,
  ROUTE_TAXI: 6,
  WAITING_TAXI: 7,
  RIDING: 8,
};
/*
--------------------------------------------------------
offers sort keys
--------------------------------------------------------
*/
export const OFFER_SORT = {
  PRICE_LOW_FIRST: 0,
  WAIT_TIMES_LOW_FIRST: 1,
  RATING_GREAT_FIRST: 2,
};
/*
--------------------------------------------------------
offers filter keys
--------------------------------------------------------
*/
export const OFFER_FILTER = {
  TOUS: 0,
  VAN: 1,
  GREEN: 2,
  ECONOMIQUE: 3,
  TOP: 4,
  POOL: 5,
};
