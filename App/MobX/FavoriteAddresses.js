/* eslint-disable no-underscore-dangle */

import { observable, computed } from 'mobx';

/*
=====================================================================
 favorite addresses model
=====================================================================
*/
class MXFavoriteAddresses {
  @observable
  _favoriteHome = {
    longitude: 0.0,
    latitude: 0.0,
    placeName: '',
  };

  @observable
  _favoriteWork = {
    longitude: 0.0,
    latitude: 0.0,
    placeName: '',
  };

  @observable
  _favoritePlace = {
    longitude: 0.0,
    latitude: 0.0,
    placeName: '',
  };

  @computed
  get favoriteHome() {
    return this._favoriteHome;
  }

  set favoriteHome(favoriteHome) {
    this._favoriteHome = favoriteHome;
  }

  @computed
  get favoriteWork() {
    return this._favoriteWork;
  }

  set favoriteWork(favoriteWork) {
    this._favoriteWork = favoriteWork;
  }

  @computed
  get favoritePlace() {
    return this._favoritePlace;
  }

  set favoritePlace(favoritePlace) {
    this._favoritePlace = favoritePlace;
  }
}

export default MXFavoriteAddresses;
