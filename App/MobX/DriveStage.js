/* eslint-disable no-underscore-dangle */
import { observable, action, computed } from 'mobx';

/*
=====================================================================
 drive stage model state (trip)
=====================================================================
*/
class MXDriveStageState {
  @observable
  currentStage = 1;

  @observable
  prevStageState = 0;

  @observable
  _selectedOffer = false;

  @observable
  _lastestRides = [];

  @observable
  _bookingDataTime = {
    changed: false,
    dateTime: '',
  };

  @observable
  startingAddressData = {
    longitude: 0.0,
    latitude: 0.0,
    placeName: '',
    region: '',
    street: '',
    city: '',
    country: '',
    zipCode: '',
    internalId: -1,
  };

  @observable
  destinationAddressData = {
    longitude: 0.0,
    latitude: 0.0,
    placeName: '',
    region: '',
    street: '',
    city: '',
    country: '',
    zipCode: '',
    internalId: -1,
  };

  @observable
  destinationRouteData = null;

  @observable
  waitingRouteData = null;

  @observable
  _providersOffers = [];

  @observable
  _currentRide = {};

  @observable
  _paidOffer = false;

  @observable
  _currentRideServerData = {
    driverRate: 3,
    carLicense: '12323',
    carModel: 'Audi 7',
    driverName: 'Jhon Smith',
  };

  @action
  resetRideEngine() {
    this._selectedProvider = false;
    this.startingAddressData = {
      longitude: 0.0,
      latitude: 0.0,
      placeName: '',
      region: '',
      street: '',
      city: '',
      country: '',
      zipCode: '',
      internalId: -1,
    };
    this.destinationAddressData = {
      longitude: 0.0,
      latitude: 0.0,
      placeName: '',
      region: '',
      street: '',
      city: '',
      country: '',
      zipCode: '',
      internalId: -1,
    };
    this.destinationRouteData = null;
    this.waitingRouteData = null;
    this._providersOffers = [];
    this._selectedOffer = false;
    this._currentRide = {};
    this._paidOffer = false;
    this._bookingDataTime = {
      dateTime: '',
      changed: false,
    };
  }

  @action
  setStage(stage) {
    this.currentStage = stage <= 0 ? 1 : stage;
  }

  @action
  prevStage() {
    const cachedCurrentStage = this.currentStage;
    this.prevStageState = this.currentStage;
    this.currentStage = cachedCurrentStage - 1;
  }

  @action
  setStartingAddress(addressData) {
    this.startingAddressData = addressData;
  }

  @action
  setDestinationAddress(addressData) {
    this.destinationAddressData = addressData;
  }

  @action
  setDestinationRouteData(data) {
    this.destinationRouteData = data;
  }

  @action
  setWaitingRouteData(data) {
    this.waitingRouteData = data;
  }

  @computed
  get providersOffers() {
    return this._providersOffers;
  }

  set providersOffers(providersOffers) {
    this._providersOffers = providersOffers;
  }

  @computed
  get currentRide() {
    return this._currentRide;
  }

  set currentRide(currentRide) {
    this._currentRide = currentRide;
  }

  @computed
  get bookingDateTime() {
    return this._bookingDataTime;
  }

  set bookingDateTime(data) {
    this._bookingDataTime = data;
  }

  @computed
  get selectedOffer() {
    return this._selectedOffer;
  }

  set selectedOffer(offer) {
    this._selectedOffer = offer;
  }

  @computed
  get paidOffer() {
    return this._paidOffer;
  }

  set paidOffer(paidOffer) {
    this._paidOffer = paidOffer;
  }

  @computed
  get currentRideData() {
    return this._currentRideServerData;
  }

  set currentRideData(rideServerData) {
    this._currentRideServerData = rideServerData;
  }

  @computed
  get selectedProvider() {
    return this._selectedProvider;
  }

  set selectedProvider(selectedProvider) {
    this._selectedProvider = selectedProvider;
  }

  @computed
  get lastestRides() {
    return this._lastestRides;
  }

  set lastestRides(lastestRides) {
    this._lastestRides = lastestRides;
  }
}

export default MXDriveStageState;
