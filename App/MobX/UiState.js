import { observable, action, computed } from 'mobx';
import { Animated, Platform } from 'react-native';
import { DRAWER_MENU_SECTIONS, MAP_VIEW_STATE, ADDRESS_BAR_VIEW_STATE } from './Constants';

/*
-------------------------------------------------
 ui state store
-------------------------------------------------
*/
class MXUiState {
  @observable drawerMenuVisible = false;

  @observable drawerMenuSection = DRAWER_MENU_SECTIONS.MAIN;

  @observable addressBarViewState = ADDRESS_BAR_VIEW_STATE.CLOSED;

  // on iOS default value (map preloader)
  @observable mapViewState =
    Platform.OS === 'ios' ? MAP_VIEW_STATE.LOADING : MAP_VIEW_STATE.NAVIGATION;

  @observable mapCenteringRequested = false;

  @observable dateAndTimeDialogVisible = false;

  @observable stagesMenuOpen = false;

  @observable addressSelectedMenuOpen = false;

  @observable showPaymentBonusDialog = false;

  @observable showCancelTripDialog = false;

  @observable showModifyDestinationDialog = false;

  @observable showTripFiltersDialog = false;

  @observable showDeletePaymentMethodDialog = false;

  @observable stagesMenuTopBorder = new Animated.Value(86);

  @observable showTripButtonFlag = false;

  @observable maxRegistrationStages = 3;

  //--------------------------------------
  // reset
  //--------------------------------------
  constructor() {
    this.reset();
  }

  //--------------------------------------
  // reset
  //--------------------------------------
  @action reset() {
    this.drawerMenuVisible = false;
    this.dateAndTimeDialogVisible = false;
    this.drawerMenuSection = DRAWER_MENU_SECTIONS.MAIN;
    this.addressBarViewState = ADDRESS_BAR_VIEW_STATE.CLOSED;
    this.stagesMenuOpen = false;
    this.showPaymentBonusDialog = false;
    this.showCancelTripDialog = false;
    this.showModifyDestinationDialog = false;
    this.showTripFiltersDialog = false;
    this.addressSelectedMenuOpen = false;
    this.showDeletePaymentMethodDialog = false;
  }

  @action.bound
  showDrawerMenu() {
    this.drawerMenuVisible = true;
  }

  @action.bound
  hideDrawerMenu() {
    this.drawerMenuVisible = false;
  }

  @action
  navigateDrawerSection(menuSection) {
    this.drawerMenuSection = DRAWER_MENU_SECTIONS[menuSection];
  }

  @action
  setMapViewState(viewState) {
    if (this.mapViewState !== MAP_VIEW_STATE[viewState]) {
      this.mapViewState = MAP_VIEW_STATE[viewState];
    }
  }

  @action
  setAddressBarViewState(viewState) {
    this.addressBarViewState = ADDRESS_BAR_VIEW_STATE[viewState];
  }

  @action.bound
  showDateAndTimeSelect() {
    this.dateAndTimeDialogVisible = true;
  }

  @action.bound
  requestMapCentering() {
    this.mapCenteringRequested = true;
    setTimeout(() => {
      this.mapCenteringRequested = false;
    }, 200);
  }

  @action.bound
  hideDateAndTimeSelect() {
    this.dateAndTimeDialogVisible = false;
  }

  @action.bound
  openStagesMenu() {
    Animated.timing(this.stagesMenuTopBorder, {
      toValue: 256.0,
      duration: 400,
      useNativeDriver: false,
    }).start();
    this.stagesMenuOpen = true;
  }

  @action.bound
  closeStagesMenu() {
    // hide first
    this.stagesMenuOpen = false;
    Animated.timing(this.stagesMenuTopBorder, {
      toValue: 86.0,
      duration: 300,
      useNativeDriver: false,
    }).start();
  }

  @action.bound
  openPaymentBonusDialog() {
    this.showPaymentBonusDialog = true;
  }

  @action.bound
  hidePaymentBonusDialog() {
    this.showPaymentBonusDialog = false;
  }

  @action.bound
  openCancelTripDialog() {
    this.showCancelTripDialog = true;
  }

  @action.bound
  hideCancelTripDialog() {
    this.showCancelTripDialog = false;
  }

  @action.bound
  openModifyDestinationDialog() {
    this.showModifyDestinationDialog = true;
  }

  @action.bound
  hideModifyDestinationDialog() {
    this.showModifyDestinationDialog = false;
  }

  @action.bound
  openTripFiltersDialog() {
    this.showTripFiltersDialog = true;
  }

  @action.bound
  hideTripFiltersDialog() {
    this.showTripFiltersDialog = false;
  }


  @action.bound
  openAddressSelectedMenu() {
    this.addressSelectedMenuOpen = true;
  }

  @action.bound
  closeAddressSelectedMenu() {
    this.addressSelectedMenuOpen = false;
  }

  @action.bound
  openDeletePaymentMethodDialog() {
    this.showDeletePaymentMethodDialog = true;
  }

  @action.bound
  closeDeletePaymentMethodDialog() {
    this.showDeletePaymentMethodDialog = false;
  }

  @computed
  get showTripButton() {
    return this.showTripButtonFlag;
  }

  set showTripButton(show) {
    this.showTripButtonFlag = show;
  }
}

export default MXUiState;
