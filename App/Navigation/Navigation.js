import { createStackNavigator } from 'react-navigation';
import React from 'react';
import LogonScreen from '../Views/Screens/LogonScreen';
import RegistrationScreen from '../Views/Screens/RegistrationScreen/RegistrationScreen';
import RegistrationSocialScreen from '../Views/Screens/RegistrationScreen/RegistrationSocialScreen';
import SMSValidationScreen from '../Views/Screens/RegistrationScreen/SMSValidation';
import NewPayCardScreen from '../Views/Screens/RegistrationScreen/NewPayCard';
import TOCScreen from '../Views/Screens/TOCScreen';
import AppNavigation from './AppNavigation';
import { NavigationBar } from '../Components';

import FindAddressScreen from '../Views/Shared/FindAddress';
import PaymentsCardsScreen from '../Views/Shared/PaymentsCardsScreen';

const transitionConfig = () => ({
  transitionSpec: {
    duration: 250,
    useNativeDriver: true,
  },

  screenInterpolator: (sceneProps) => {
    const { layout, position, scene } = sceneProps;

    const thisSceneIndex = scene.index;
    const height = layout.initHeight;
    const width = layout.initWidth;

    const translateX = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [width, 0, 0],
    });

    const translateY = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [height, 0],
    });

    const slideFromRight = { transform: [{ translateX }] };
    const slideInFromBottom = { transform: [{ translateY }] };

    if (thisSceneIndex === 1) {
      return slideInFromBottom;
    }

    return slideFromRight;
  },
});

const RootNavigation = createStackNavigator(
  {
    logon: { screen: LogonScreen, navigationOptions: { gesturesEnabled: false, header: null } },
    registration: {
      screen: RegistrationScreen,
      navigationOptions: { gesturesEnabled: false, header: null },
    },
    registrationSocial: {
      screen: RegistrationSocialScreen,
      navigationOptions: { gesturesEnabled: false, header: null },
    },
    smsValidation: {
      screen: SMSValidationScreen,
      navigationOptions: { gesturesEnabled: false, header: null },
    },
    newPayCard: {
      screen: NewPayCardScreen,
      navigationOptions: { gesturesEnabled: false, header: null },
    },
    tocScreen: { screen: TOCScreen, navigationOptions: { gesturesEnabled: false, header: null } },
    appNavigation: {
      screen: AppNavigation,
      navigationOptions: { gesturesEnabled: false, header: null },
    },
    findAddressScreen: {
      screen: FindAddressScreen,
      navigationOptions: ({ navigation }) => ({
        header: (
          <NavigationBar
            navigation={navigation}
            title={navigation.state.params.title}
            onBackPress={navigation.goBack}
          />
        ),
      }),
    },
    paymentsCardsScreen: {
      screen: PaymentsCardsScreen,
      navigationOptions: ({ navigation }) => ({
        header: (
          <NavigationBar
            navigation={navigation}
            title="Payments methods"
            onBackPress={navigation.goBack}
          />
        ),
      }),
    },
  },
  {
    initialRouteName: 'logon',
    transitionConfig,
  },
);

export default RootNavigation;
