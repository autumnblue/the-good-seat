import { createStackNavigator } from 'react-navigation';
import MainScreen from '../Views/Screens/MainScreen';

const AppNavigation = createStackNavigator(
  {
    mainScreen: {
      screen: MainScreen,
      navigationOptions: { gesturesEnabled: false },
      headerLeft: null,
    },
  },
  {
    initialRouteName: 'mainScreen',
    headerMode: 'none',
    navigationOptions: { gesturesEnabled: false },
  },
);

export default AppNavigation;
