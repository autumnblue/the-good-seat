import { createStackNavigator } from 'react-navigation';
import React from 'react';
import { NavigationBar } from '../Components';
import FindAddressScreen from '../Views/Shared/FindAddress';

const SharedScreenNavigation = createStackNavigator(
  {
    findAddressScreen: {
      screen: FindAddressScreen,
      navigationOptions: ({ navigation }) => ({
        header: <NavigationBar navigation={navigation} title="Select address" />,
      }),
    },
  },
  {
    initialRouteName: 'findAddressScreen',
  },
);

export default SharedScreenNavigation;
