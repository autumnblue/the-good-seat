import { StyleSheet } from 'react-native';
import { addFontFamily, Metrics, Colors } from '../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    // paddingBottom: 40,
  },
  mainText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 16,
    color: '#78849E',
  },
  itemMenuContainerUnderline: {
    borderBottomWidth: 1,
    borderBottomColor: '#78849E',
  },
  secondaryText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: '#78849E',
  },
  confirmText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 10,
    color: '#78849E',
  },
  mainContainer: {
    flexDirection: 'column',
    paddingBottom: 40,
  },
  helperHeader: {
    alignItems: 'center',
    // paddingHorizontal: 10,
  },
  headerTermsText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 8,
    color: '#78849E',
    textDecorationLine: 'underline',
    lineHeight: 10,
  },
  blockTermsText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 8,
    color: '#78849E',
    lineHeight: 9,
    textAlign: 'left',
  },
  termsScrollContainer: {
    paddingHorizontal: 8,
  },
  termsContainer: {
    marginHorizontal: 16,
    padding: 16,
    backgroundColor: '#F0F2F4',
    borderRadius: 20,
  },
  helperFooter: {
    marginTop: 16,
    paddingBottom: 16,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingRight: 15,
  },
  buttonText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: '#009FE3',
  },
  buttonTextDisabled: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: Colors.disable,
  },
  confirmContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 19.3,
  },
  checkBoxTextContainer: {
    marginLeft: Metrics.screenUnit * 5,
    flex: 0.95,
  },
  linkContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 16,
  },
  linkText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: '#3B5994',
    textDecorationLine: 'underline',
  },
});
