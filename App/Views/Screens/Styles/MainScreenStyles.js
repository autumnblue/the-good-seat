import { StyleSheet } from 'react-native';
import { Metrics } from '../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  mapboxContainer: {
    flex: 1,
  },
  mapElementsContainer: {
    position: 'absolute',
    flex: 1,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    paddingLeft: 3,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  gradientImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    resizeMode: 'stretch',
    width: Metrics.screenWidth + 3,
  },
  footerContainer: {
    // position: 'absolute',
    paddingHorizontal: 9,
    // bottom: 105,
    //  width: Metrics.screenWidth,// + 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  helpButton: {
    borderRadius: 25,
  },
  currentUserPositionButton: {
    borderRadius: 12,
  },
  drawerMenuButton: {
    marginLeft: 9,
  },
  iconButton: {
    borderRadius: 30,
  },
});
