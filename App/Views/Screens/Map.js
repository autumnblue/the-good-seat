import React, { Component } from 'react';
import { Image, View, Text } from 'react-native';

import Mapbox from '@mapbox/react-native-mapbox-gl';
import { observer, inject } from 'mobx-react';
import { BubblesSpinner } from '../../Components';
import VTCCar from '../Shared/MapBoxEntities/VTCCar';
import { MAP_VIEW_STATE } from '../../MobX/Constants';
import { Images, Colors } from '../../Theme';

import Styles from './Styles/MapStyles';

let appState;

@inject('appState')
@observer
class Map extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    console.info('[MAP] Constructor');
    this.state = {};
  }

  componentDidMount() {}

  componentDidUpdate() {}

  componentWillUnmount() {}

  //----------------------------------------------------
  // handle map loading complete
  //----------------------------------------------------
  onMapLoadingComplete = () => {
    appState.uiState.setMapViewState('NAVIGATION');
  };

  //----------------------------------------------------
  // handle map press
  //----------------------------------------------------
  handleMapPress = () => {
    appState.uiState.setAddressBarViewState('CLOSED');
    appState.uiState.hideDrawerMenu();
  };

  handleUpdateGeolocation = (e) => {
    console.log('geolocation update', e);
  };

  //-----------------------------------------------------
  // render route trip
  //-----------------------------------------------------
  renderRouteDestinationTrip = () => {
    const { selectedOffer, destinationRouteData } = appState.driveStageState;

    if (!destinationRouteData) {
      return null;
    }

    const layerStyles = Mapbox.StyleSheet.create({
      route: {
        lineColor: selectedOffer.colorCode ? selectedOffer.colorCode : Colors.primary,
        lineWidth: 10,
        lineCap: 'round',
        lineJoin: 'round',
      },
      routeShadow: {
        lineColor: '#26596E',
        lineWidth: 10,
        lineCap: 'round',
        lineJoin: 'round',
        lineTranslate: [2, 5],
      },
    });

    return (
      <Mapbox.ShapeSource id="routeSource" shape={destinationRouteData}>
        <Mapbox.LineLayer id="routeFill" style={layerStyles.route} />
        <Mapbox.LineLayer
          id="routeShadow"
          style={layerStyles.routeShadow}
          belowLayerID="routeFill"
        />
      </Mapbox.ShapeSource>
    );
  };

  //-----------------------------------------------------
  // render offer route
  //-----------------------------------------------------
  renderOfferedRoute = () => {
    const { waitingRouteData } = appState.driveStageState;

    if (!waitingRouteData) {
      return null;
    }

    const layerStyles = Mapbox.StyleSheet.create({
      route: {
        lineColor: '#78849E',
        lineWidth: 6,
        lineCap: 'round',
        lineJoin: 'round',
        lineDasharray: [1, 2],
      },
    });

    return (
      <Mapbox.ShapeSource id="offeredRouteSource" shape={waitingRouteData}>
        <Mapbox.LineLayer id="offeredRouteFill" style={layerStyles.route} />
      </Mapbox.ShapeSource>
    );
  };

  //----------------------------------------------
  // render destination route point
  //----------------------------------------------
  renderDestinationRoutePoint = () => {
    const { latitude, longitude } = appState.driveStageState.destinationAddressData;
    const { selectedOffer } = appState.driveStageState;

    const layerStyles = Mapbox.StyleSheet.create({
      destination: {
        circleRadius: 21,
        circleColor: selectedOffer.colorCode ? selectedOffer.colorCode : Colors.primary,
      },
      flag: {
        iconImage: Images.flagWhite,
        iconAllowOverlap: true,
        iconColor: '#fff',
      },
    });

    return (
      <Mapbox.ShapeSource
        id="origin"
        shape={Mapbox.geoUtils.makePoint([parseFloat(longitude), parseFloat(latitude)])}
      >
        <Mapbox.CircleLayer id="destinationCircle" style={layerStyles.destination} />
        <Mapbox.SymbolLayer
          id="symbolLocationSymbols"
          minZoomLevel={1}
          aboveLayerID="destinationCircle"
          style={layerStyles.flag}
        />
      </Mapbox.ShapeSource>
    );
  };

  //---------------------------------------------------
  // render user location marker
  //---------------------------------------------------
  renderUserLocation = ({ latitude, longitude }) => (
    <Mapbox.PointAnnotation
      key="userLocation"
      id="userLocation"
      coordinate={[parseFloat(longitude), parseFloat(latitude)]}
    >
      <View style={Styles.userLocationContainer}>
        <Image source={Images.userPin} />
      </View>
    </Mapbox.PointAnnotation>
  );

  //-----------------------------------------------------
  // render car
  //-----------------------------------------------------
  renderCar = () => {
    const { latitude, longitude } = appState.sessionParameters.userLocation;
    const { providerCode } = appState.driveStageState.paidOffer;

    return <VTCCar VTC={providerCode} latitude={latitude} longitude={longitude} />;
  };

  //-----------------------------------------------------
  // render navigation state
  //-----------------------------------------------------
  renderNavigationState = () => {
    const { latitude, longitude } = appState.sessionParameters.userLocation;
    return this.renderUserLocation({ latitude, longitude });
  };

  //-----------------------------------------------------
  // render show starting point state
  //-----------------------------------------------------
  renderShowStartingPointState = () => {
    const { latitude, longitude } = appState.driveStageState.startingAddressData;
    return this.renderUserLocation({ latitude, longitude });
  };

  //----------------------------------------------
  // render route destination state
  //----------------------------------------------
  renderDestinationRouteState = () => {
    const { latitude, longitude } = appState.driveStageState.startingAddressData;

    return (
      <React.Fragment>
        {this.renderRouteDestinationTrip()}
        {this.renderDestinationRoutePoint()}
        {this.renderUserLocation({ latitude, longitude })}
      </React.Fragment>
    );
  };

  //-----------------------------------------------
  // render route from offer state
  //-----------------------------------------------
  renderRouteFromOfferState = () => {
    const { latitude, longitude } = appState.driveStageState.startingAddressData;
    const { selectedOffer } = appState.driveStageState;
    const { availableCars, providerCode } = selectedOffer;

    let carRenderData = null;
    if (selectedOffer) {
      if (availableCars && availableCars.length >= 1) {
        carRenderData = (
          <VTCCar
            VTC={providerCode}
            glRenderIndex={`${providerCode}offeredTaxi`}
            key={availableCars[0].lat}
            latitude={availableCars[0].lat}
            longitude={availableCars[0].lon}
            angle={availableCars[0].angle}
          />
        );
      }
    }

    return (
      <React.Fragment>
        {this.renderUserLocation({ latitude, longitude })}
        {this.renderOfferedRoute()}
        {carRenderData}
      </React.Fragment>
    );
  };

  //-----------------------------------------------
  // render cars offers
  //-----------------------------------------------
  renderOfferedCars = () => {
    const { selectedOffer, providersOffers } = appState.driveStageState;
    const { availableCars, providerCode } = selectedOffer;

    if (providersOffers.length && selectedOffer) {
      if (!availableCars || availableCars.length < 1) {
        return null;
      }
      const carsRenderData = availableCars.map((carData, index) => (
        <VTCCar
          VTC={providerCode}
          glRenderIndex={index}
          key={carData.lat}
          latitude={carData.lat}
          longitude={carData.lon}
          angle={carData.angle}
        />
      ));

      return carsRenderData;
    }

    return null;
  };

  //-----------------------------------------------
  // render offers state
  //-----------------------------------------------
  renderTaxiOffersState = () => {
    const { latitude, longitude } = appState.driveStageState.startingAddressData;

    // this.setState({ mapZoomLevel: 15 });

    return (
      <React.Fragment>
        {this.renderOfferedCars()}
        {this.renderUserLocation({ latitude, longitude })}
      </React.Fragment>
    );
  };

  //-----------------------------------------------
  // waiting taxi state
  //-----------------------------------------------
  renderWaitingTaxi = () => {
    const { latitude, longitude } = appState.driveStageState.startingAddressData;
    const { currentLat, currentLon } = appState.driveStageState.currentRideData;
    const { selectedOffer } = appState.driveStageState;
    const { providerCode } = selectedOffer;

    // build waiting taxi route

    return (
      <React.Fragment>
        <VTCCar VTC={providerCode} latitude={currentLat} longitude={currentLon} />
        {this.renderUserLocation({ latitude, longitude })}
      </React.Fragment>
    );
  };

  //-----------------------------------------------
  // driving (ride) state
  //-----------------------------------------------
  renderRidingState = () => {
    const { currentLat, currentLon } = appState.driveStageState.currentRideData;
    const { providerCode } = appState.driveStageState.paidOffer;

    // build ride route
    // this.handleBuildRideRoute();

    return (
      <React.Fragment>
        {/* this.renderRouteDestinationTrip() */}
        {this.renderDestinationRoutePoint()}
        <VTCCar VTC={providerCode} latitude={currentLat} longitude={currentLon} />
      </React.Fragment>
    );
  };

  //-----------------------------------------------
  // loading map state (preloader)
  //-----------------------------------------------
  renderLoadingState = () => {
    const { mapViewState } = appState.uiState;

    if (mapViewState === MAP_VIEW_STATE.LOADING) {
      return (
        <View style={Styles.processingContainer}>
          <Text style={Styles.processingText}>Chargement de la carte</Text>
          <View style={{ height: 20 }} />
          <BubblesSpinner size={40} />
        </View>
      );
    }

    return null;
  };

  /*
  ------------------------------------------------
   render current view state
  ------------------------------------------------
  */
  renderCurrentViewState = () => {
    const { mapViewState } = appState.uiState;

    // return this.renderNavigationState();

    console.log('view', mapViewState);

    switch (mapViewState) {
      case MAP_VIEW_STATE.NAVIGATION:
        return this.renderNavigationState();
      case MAP_VIEW_STATE.SHOW_STARTING_POINT:
        return this.renderShowStartingPointState();
      case MAP_VIEW_STATE.SHOW_DESTINATION_ROUTE:
        return this.renderDestinationRouteState();
      case MAP_VIEW_STATE.SHOW_ROUTE_FROM_OFFER:
        return this.renderRouteFromOfferState();
      case MAP_VIEW_STATE.TAXI_OFFERS:
        return this.renderTaxiOffersState();
      case MAP_VIEW_STATE.WAITING_TAXI:
        return this.renderWaitingTaxi();
      case MAP_VIEW_STATE.RIDING:
        return this.renderRidingState();
      default:
        return this.renderNavigationState();
    }
  };

  render() {
    const { latitude, longitude } = appState.sessionParameters.mapCenter;
    const { mapRef } = this.props;

    return (
      <React.Fragment>
        {this.renderLoadingState()}
        <Mapbox.MapView
          ref={mapRef}
          styleURL={Mapbox.StyleURL.Street}
          centerCoordinate={[parseFloat(longitude), parseFloat(latitude)]}
          style={Styles.container}
          zoomLevel={15}
          localizeLabels
          logoEnabled={false}
          showUserLocation={false}
          onUserLocationUpdate={this.handleUpdateGeolocation}
          onPress={this.handleMapPress}
          onDidFinishLoadingMap={this.onMapLoadingComplete}
        >
          {this.renderCurrentViewState()}
        </Mapbox.MapView>
      </React.Fragment>
    );
  }
}

export default Map;
