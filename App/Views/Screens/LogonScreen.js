import React, { Component } from 'react';
import {
  Input, Text, Button, Item,
} from 'native-base';

import {
  BackHandler,
  AsyncStorage,
  View,
  Image,
  StatusBar,
  Linking,
  Platform,
  Animated,
  Keyboard,
  ScrollView,
  Alert,
} from 'react-native';

import { NavigationActions } from 'react-navigation';

import SafariView from 'react-native-safari-view';

import { observer, inject } from 'mobx-react';
import { Modal, ErrorMessageBox } from '../../Components';
import { Images, Colors } from '../../Theme';

import LoadingScreen from './LoadingScreen';

import Styles from './Styles/LogonScreenStyles';
import { loginUser, forgotPassword, sendEventToServer } from '../../Services/BackEnd/TheGoodSeat';
import { validateEmail, validateFormValue, getUrlParams } from '../../Services/Utils';

let appState = {};

const IMAGE_LOGO_HEIGHT = 203;
const IMAGE_LOGO_WIDTH = 230;

/*
===================================================================
 logon screen (entry point)
==================================================================
*/
@inject('appState')
@observer
class LogonScreen extends Component {
  //-----------------------------------------
  //
  //------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */

    this.state = {
      showLostPasswordModal: false,
      showPasswordReset: false,
      emailForResetPassword: '',
      email: '',
      password: '',
      wrongEmail: false,
      wrongPassword: false,
      isLoading: false,
      resetLostPasswordButtonActive: false,
    };

    // flag
    this.logonIsSocial = false;

    // keyboard handle
    this.keyboardHeight = new Animated.Value(0);
    this.imageLogoHeight = new Animated.Value(IMAGE_LOGO_HEIGHT);
    this.imageLogoWidth = new Animated.Value(IMAGE_LOGO_WIDTH);

    // validation config
    this.validationConfig = {
      firstname: {
        required: true,
      },
      lastname: {
        required: true,
      },
      birthdate: {
        required: true,
      },
      country: {
        required: true,
      },
      email: {
        required: true,
        validFunc: validateEmail,
      },
      phonenumber: {
        required: true,
      },
    };
  }

  async componentDidMount() {
    const { navigation } = this.props;

    const token = await AsyncStorage.getItem('AUTH_TOKEN');

    // AsyncStorage.clear();

    let currentUser;

    try {
      currentUser = await AsyncStorage.getItem('CURRENT_USER');
      currentUser = JSON.parse(currentUser);
    } catch (e) {
      currentUser = null;
      console.warn('Error while parsing existing user', currentUser);
    }

    console.log('USER', currentUser, token);

    if (token && currentUser && currentUser._id) {
      const { termsAccepted } = currentUser;

      // console.log('USER', currentUser, token);

      // set current token and user
      appState.sessionParameters.setAppToken(token);
      appState.sessionParameters.setCurrentUser(currentUser);

      // intermedate log on
      if (termsAccepted) {
        navigation.reset([NavigationActions.navigate({ routeName: 'appNavigation' })], 0);
      } else {
        navigation.navigate('tocScreen');
      }

      return;
    }

    // appState.sessionParameters.logout();

    // Add event listener to handle thegoodseat:// URLs
    Linking.addEventListener('url', this.handleOpenURL);
    // Launched from an external URL
    Linking.getInitialURL().then((url) => {
      console.log('GETINITIAL URL ', url);
      if (url) {
        this.handleOpenURL({ url });
      }
    });

    // add keyboard listener
    if (Platform.OS === 'ios') {
      this.keyboardWillShowEvent = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
      this.keyboardWillHideEvent = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    } else if (Platform.OS === 'android') {
      this.keyboardWillShowEvent = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
      this.keyboardWillHideEvent = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
      BackHandler.addEventListener('hardwareBackPress', this.backButtonPress);
    }

    // this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    // this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    // URL
    Linking.removeEventListener('url', this.handleOpenURL);

    // keyboard
    if (this.keyboardWillShowSub) {
      this.keyboardWillShowSub.remove();
    }

    if (this.keyboardWillHideSub) {
      this.keyboardWillHideSub.remove();
    }

    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.backButtonPress);
    }
  }

  //------------------------------------------------
  // only for Android
  //------------------------------------------------
  backButtonPress = () => {
    BackHandler.exitApp();
  };

  /*
   ANIMATION BLOCK
  */
  //------------------------------------------------------------
  // on keyboard show
  //------------------------------------------------------------
  keyboardWillShow = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: 250,
        toValue: event.endCoordinates.height + 140,
      }),
      Animated.timing(this.imageLogoHeight, {
        duration: 250,
        toValue: 80,
      }),
      Animated.timing(this.imageLogoWidth, {
        duration: 250,
        toValue: 90,
      }),
    ]).start();
  };

  //------------------------------------------------------------
  // on keyboard hide
  //------------------------------------------------------------
  keyboardWillHide = () => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: 250,
        toValue: 0,
      }),
      Animated.timing(this.imageLogoHeight, {
        duration: 250,
        toValue: IMAGE_LOGO_HEIGHT,
      }),
      Animated.timing(this.imageLogoWidth, {
        duration: 250,
        toValue: IMAGE_LOGO_WIDTH,
      }),
    ]).start();
  };

  //------------------------------------------------------------
  // navigate to application
  //------------------------------------------------------------
  navigateToApplication = () => {
    const { navigation } = this.props;
    const { termsAccepted } = appState.sessionParameters.currentUser;

    // intermedate log on
    if (termsAccepted && termsAccepted === 1) {
      navigation.navigate('appNavigation');
    } else {
      navigation.navigate('tocScreen');
    }
  };

  //------------------------------------------------------------
  // validate user data
  //------------------------------------------------------------
  validateUserData = (userData) => {
    let valid = true;

    const keys = Object.keys(this.validationConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      valid = validateFormValue(this.validationConfig, key, userData[key]);
      if (!valid) break;
    }

    return valid;
  };

  //------------------------------------------------------------
  // handle open URL linking
  //------------------------------------------------------------
  handleOpenURL = (urlString) => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { navigation } = this.props;

    console.log('USER', urlString);

    if (!urlString) {
      console.warn('[LOGON] MISSING URL STRING');
    }
    // cache it
    let url = typeof urlString === 'string' ? urlString : urlString.url;

    console.log('[LOGON] URL', url);

    if (!url) {
      Alert.alert('Erreur', "Pas d'infos reçues lors de la redirection.\n (missing url)");
      return;
    }
    // find # and delete it
    if (url.endsWith('#')) {
      url = url.substring(0, url.length - 1);
    }

    console.log('NEW URL', url);

    let data = {};

    if (getUrlParams(url).error) {
      ErrorMessageBox(language, getUrlParams(url).error, errorsDictionary);
      return;
    }

    if (Platform.OS === 'ios') {
      SafariView.dismiss();
    }

    try {
      // get user data (only FB and Google)
      if (getUrlParams(url).user) {
        data = getUrlParams(url);
        data.user = JSON.parse(getUrlParams(url).user);

        console.log('data', data.token);

        if (data.user && data.user._id) {
          // is social logon ?
          if (this.logonIsSocial) {
            // save token
            appState.sessionParameters.setAppToken(data.token);
            // save current user (dont save!!! user not entered in app)
            appState.sessionParameters.setCurrentUser(data.user);

            // fix @facebook.com email addresses
            if (data.user.email) {
              if (data.user.email.indexOf('@facebook.com') !== -1) {
                data.user.email = '';
              }
            }

            // validate user data fields
            if (!this.validateUserData(data.user) || !data.user.isPhonenumberVerified) {
              // gather input data and set it
              appState.sessionParameters.setUserRegistrationState = {
                isSocialLogon: true,
                isPhonenumberVerified: false,
                firstName: data.user.firstname,
                lastName: data.user.lastname,
                dateOfBirth: data.user.birthdate,
                country: data.user.country,
                email: data.user.email,
                phoneNumber: data.user.phonenumber,
                newUserID: data.user._id,
                currentStage: 1,
              };
              // navigate to reg social form
              navigation.navigate('registrationSocial');
            } else {
              // post login
              this.postLogin(data);
            }
          } else {
            // post login
            this.postLogin(data);
          }
        } else {
          ErrorMessageBox(language, data, errorsDictionary);
        }
      } else {
        this.navigateToApplication();
      }
    } catch (error) {
      console.error(error);
    }
  };

  //------------------------------------------------------
  // Open URL in a browser
  //------------------------------------------------------
  openURL = (url) => {
    // Use SafariView on iOS
    if (Platform.OS === 'ios') {
      SafariView.show({
        url,
        fromBottom: true,
      });
    } else {
      Linking.openURL(url);
    }
  };

  //-----------------------------------------------
  // uber auth
  //-----------------------------------------------
  uberAuth = async () => {
    const { THEGOODSEAT_API } = appState.constants;
    const { _id } = appState.sessionParameters.currentUser;

    this.logonIsSocial = false;

    this.openURL(
      [
        'https://login.uber.com/oauth/v2/authorize?response_type=code',
        '&client_id=MvhHkYO2s2Mo82w_lctD3oR4rV7_BiRQ&scope=all_trips+profile+request+ride_widgets',
        `&redirect_uri=${THEGOODSEAT_API}/auth/uber&state=${_id}`,
      ].join(''),
    );
  };

  //-----------------------------------------------
  // facebook logon
  //-----------------------------------------------
  facebookAuth = async () => {
    const { THEGOODSEAT_API, FACEBOOK_CLIENT_ID } = appState.constants;
    appState.sessionParameters.logout();
    // flag
    this.logonIsSocial = true;

    this.openURL(
      [
        'https://www.facebook.com/v3.2/dialog/oauth',
        `?client_id=${FACEBOOK_CLIENT_ID}`,
        `&redirect_uri=${THEGOODSEAT_API}/auth/facebook/callback`,
      ].join(''),
    );
  };

  //-----------------------------------------------
  // google logon
  //-----------------------------------------------
  googleAuth = async () => {
    const { THEGOODSEAT_API, GOOGLE_CLIENT_ID } = appState.constants;
    appState.sessionParameters.logout();
    // flag
    this.logonIsSocial = true;

    console.log('ID', GOOGLE_CLIENT_ID);

    this.openURL(
      [
        'https://accounts.google.com/o/oauth2/v2/auth?',
        'access_type=offline&prompt=consent',
        '&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20email',
        '&response_type=code',
        `&client_id=${GOOGLE_CLIENT_ID}`,
        `&redirect_uri=${THEGOODSEAT_API}/auth%2Fgoogle%2Fcallback`,
      ].join(''),
    );
  };

  //-----------------------------------------------
  // registration
  //-----------------------------------------------
  handleRegistrationButton = () => {
    const { navigation } = this.props;

    navigation.navigate('registration');
  };

  //-----------------------------------------------
  // while pressing on forgot password
  //-----------------------------------------------
  handleShowLostPasswordModal = () => {
    this.setState({ showLostPasswordModal: true });
  };

  //-----------------------------------------------
  // close modal
  //-----------------------------------------------
  handleCloseLostPasswordModal = () => {
    this.setState({ showLostPasswordModal: false, showPasswordReset: false });
  };

  //-----------------------------------------------
  // reset password
  //-----------------------------------------------
  handleResetPassword = async () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { emailForResetPassword } = this.state;

    const data = await forgotPassword(appState.constants.THEGOODSEAT_API, {
      email: emailForResetPassword,
    });

    if (data.errors || data.message) {
      ErrorMessageBox(language, data, errorsDictionary);
    } else {
      this.setState({ showPasswordReset: true });
    }
  };

  //-----------------------------------------------
  // log on application
  //-----------------------------------------------
  handleLogOn = async () => {
    const { navigation } = this.props;
    const { language, errorsDictionary } = appState.sessionParameters;
    const { password, email } = this.state;
    const emailFormated = email && email.trim();

    if (!password || !emailFormated) {
      return;
    }

    // sendEventToServer('userLogon', emailFormated);
    // sendEventToServer('API', appState.constants.THEGOODSEAT_API);

    this.setState({ isLoading: true });
    const data = await loginUser(appState.constants.THEGOODSEAT_API, {
      password,
      email: emailFormated,
    });

    // sendEventToServer('userLogon-serverResponse', emailFormated);
    // sendEventToServer('userModel', data);

    // only post off loading screen
    this.setState({ isLoading: false }, () => {
      console.log('ERRR', data);
      if (data.errors || data.message) {
        if (data.errors[0] === 'error_invalid_credentials') {
          this.setState({ wrongPassword: true, wrongEmail: false });
        } else {
          this.setState({ wrongPassword: false, wrongEmail: true });
        }
        ErrorMessageBox(language, data, errorsDictionary);
      } else {
        // validate TOC
        const { termsAccepted } = data.user;

        // intermedate log on
        if (termsAccepted) {
          // sendEventToServer('userLogon-postLogin', emailFormated);
          this.postLogin(data);
        } else {
          // sendEventToServer('userLogon-tocScreen', emailFormated);
          // change token
          appState.sessionParameters.setAppToken(data.token);
          appState.sessionParameters.setCurrentUser(data.user);
          navigation.navigate('tocScreen');
        }
      }
    });
  };

  //-------------------------------------------------------
  // post login process data
  //-------------------------------------------------------
  postLogin = (data) => {
    console.log('USER MODEL:', data);
    appState.sessionParameters.setAppToken(data.token);
    appState.sessionParameters.setCurrentUser(data.user);
    // save to local
    AsyncStorage.setItem('AUTH_TOKEN', appState.sessionParameters.appToken);
    AsyncStorage.setItem('CURRENT_USER', JSON.stringify(appState.sessionParameters.currentUser));
    sendEventToServer('postLogin-navigateToHome');
    // navigate
    this.goToHomeScreen();
  };

  //-------------------------------------------------
  // hanlde user input
  //-------------------------------------------------
  handleChangeTextInput = (type, text) => {
    // reset state
    this.setState({ wrongPassword: false, wrongEmail: false });

    // change
    this.setState({ [type]: text }, () => {
      if (type === 'emailForResetPassword') {
        if (validateEmail(text)) {
          this.setState({ resetLostPasswordButtonActive: true });
        } else {
          this.setState({ resetLostPasswordButtonActive: false });
        }
      }
    });
  };

  //-------------------------------------------------
  // go to home screen
  //-------------------------------------------------
  goToHomeScreen = () => {
    const { navigation } = this.props;
    navigation.reset([NavigationActions.navigate({ routeName: 'appNavigation' })], 0);
  };

  //-------------------------------------------------
  // handle on focus email input
  //-------------------------------------------------
  onFocusEmailInput = () => {
    const { errorAuthentification } = this.state;

    // reset error state
    if (errorAuthentification) {
      this.setState({ errorAuthentification: false });
    }
  };

  //-----------------------------------------------
  // draw modal
  //-----------------------------------------------
  renderLostPasswordModal = () => {
    const { showLostPasswordModal, showPasswordReset, resetLostPasswordButtonActive } = this.state;

    if (!showPasswordReset) {
      return (
        <Modal visible={showLostPasswordModal}>
          <View style={Styles.lostpasswordModalContainer}>
            <View style={Styles.recoveryEmailInputContainer}>
              <Item
                style={{
                  height: 30,
                  width: 210,
                  borderBottomColor: 'transparent',
                }}
              >
                <Input
                  style={Styles.recoveryEmailInputText}
                  placeholder="entrez votre email"
                  placeholderTextColor={Colors.placeholderText}
                  autoCapitalize="none"
                  onChangeText={this.handleChangeTextInput.bind(this, 'emailForResetPassword')}
                />
              </Item>
            </View>
            <Text style={[Styles.lostPasswordText, { paddingVertical: 10 }]}>
              Souhaitez-vous réinitialiser votre mot de passe ?
            </Text>
            <View style={Styles.lostpasswordModalFooter}>
              <Button
                bordered
                light
                style={[Styles.lostpasswordModalFooterButton, { marginRight: 43 }]}
                disabled={!resetLostPasswordButtonActive}
                onPress={this.handleResetPassword}
              >
                <Text
                  style={resetLostPasswordButtonActive ? { color: '#fff' } : { color: '#A0A1A4' }}
                >
                  Oui
                </Text>
              </Button>
              <Button
                bordered
                light
                style={Styles.lostpasswordModalFooterButton}
                onPress={this.handleCloseLostPasswordModal}
              >
                <Text>Non</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }
    return (
      <Modal visible={showLostPasswordModal}>
        <View style={Styles.lostpasswordModalContainer}>
          <Text style={[Styles.lostPasswordText, { marginTop: 50 }]}>
            Un email vous a été envoyé afin de réinitialiser votre mot de passe
          </Text>
          <View style={Styles.lostpasswordModalFooter}>
            <Button
              bordered
              light
              style={Styles.lostpasswordModalFooterButton}
              onPress={this.handleCloseLostPasswordModal}
            >
              <Text>Ok</Text>
            </Button>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const {
      email, wrongPassword, wrongEmail, isLoading,
    } = this.state;

    return (
      <View style={Styles.container}>
        <Image source={Images.backgroundImage} style={Styles.backgroundImage} />
        <LoadingScreen visible={isLoading} processingText="Autorisation en cours" opacity={0.7} />
        <StatusBar barStyle="light-content" hidden />

        <ScrollView contentContainerStyle={{ paddingBottom: 20 }}>
          <Animated.Image
            source={Images.logoSmall}
            style={[
              Styles.logoContainer,
              { height: this.imageLogoHeight, width: this.imageLogoWidth },
            ]}
          />

          <View
            style={[
              Styles.textInputContainer,
              wrongEmail ? Styles.textInputContainerError : Styles.textInputContainerNormal,
            ]}
          >
            <Input
              id="email_input"
              value={email}
              placeholderTextColor={wrongEmail ? '#FF0000' : Colors.placeholderText}
              style={Styles.textInputField}
              keyboardType="email-address"
              placeholder={wrongEmail ? 'Email incorrect, veuillez réessayer' : 'Email'}
              autoCapitalize="none"
              onChangeText={this.handleChangeTextInput.bind(this, 'email')}
              onFocus={this.onFocusEmailInput}
            />
          </View>
          <View
            style={[
              Styles.textInputContainer,
              wrongPassword ? Styles.textInputContainerError : Styles.textInputContainerNormal,
            ]}
          >
            <Input
              id="password_input"
              secureTextEntry
              placeholderTextColor={Colors.placeholderText}
              style={Styles.textInputField}
              placeholder="Mot de passe"
              autoCapitalize="none"
              onChangeText={this.handleChangeTextInput.bind(this, 'password')}
            />
          </View>
          <Button block style={Styles.connectButtonStyle} onPress={this.handleLogOn}>
            <Text style={Styles.textButtonStyle}>
              Connexion
              {isLoading && '...'}
            </Text>
          </Button>
          <View style={Styles.passHelpContainer}>
            <Text style={Styles.infoText}>Pas encore membre ?&nbsp;</Text>
            <Text style={Styles.infoTextLink} onPress={this.handleRegistrationButton}>
              Créez un compte gratuitement
            </Text>
          </View>
          <Text
            style={[Styles.infoText, { marginTop: 10 }]}
            onPress={this.handleShowLostPasswordModal}
          >
            Mot de passe oublié ?
          </Text>
          <Text style={[Styles.infoText, { marginTop: 19 }]}>Ou connectez-vous :</Text>
          <Button block style={Styles.emailButtonStyle} onPress={this.googleAuth}>
            <Text style={Styles.textButtonStyle}>Google</Text>
          </Button>

          <Button block style={Styles.facebokButtonStyle} onPress={this.facebookAuth}>
            <Text style={Styles.textButtonStyle}>Facebook</Text>
          </Button>
          {this.renderLostPasswordModal()}
        </ScrollView>
      </View>
    );
  }
}

/* block #1
<Text style={[Styles.infoText, { marginTop: 19 }]}>Ou connectez-vous :</Text>
          <Button block style={Styles.emailButtonStyle} onPress={this.googleAuth}>
            <Text style={Styles.textButtonStyle}>Google</Text>
          </Button>

          <Button block style={Styles.facebokButtonStyle} onPress={this.facebookAuth}>
            <Text style={Styles.textButtonStyle}>Facebook</Text>
          </Button> */

export default LogonScreen;
