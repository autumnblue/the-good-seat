import React from 'react';
import { View, Text } from 'react-native';
import { BubblesSpinner } from '../../Components';

import Styles from './Styles/LoadingScreenStyles';

function LoadingScreen(props) {
  const { processingText, opacity, visible } = props;

  if (visible) {
    return (
      <View style={[Styles.container, { opacity }]}>
        <Text style={Styles.processingText}>{processingText}</Text>
        <View style={{ height: 20 }} />
        <BubblesSpinner size={40} />
      </View>
    );
  }

  return null;
}

LoadingScreen.defaultProps = {
  opacity: 1.0,
};

export default LoadingScreen;
