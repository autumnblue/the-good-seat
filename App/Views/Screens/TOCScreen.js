import React, { Component } from 'react';
import {
  View, Text, ScrollView, Linking, TouchableOpacity, SafeAreaView,
} from 'react-native';
import { Button, CheckBox } from 'native-base';
import { withNavigation } from 'react-navigation';
import { observer, inject } from 'mobx-react';
import LoadingScreen from './LoadingScreen';

import Styles from './Styles/TOCScreenStyles';
import { updateUserInfo, getCurrentTerm } from '../../Services/BackEnd/TheGoodSeat';
import { ErrorMessageBox, MessageBox } from '../../Components';

let appState = {};

@inject('appState')
@observer
class TOCScreen extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */

    this.state = {
      agreements: {},
      termText: '',
      showLoadingScreen: false,
      loadingScreenText: '',
      disableNextButton: true,
    };

    // validation config
    this.validationConfig = {
      agreement01: {
        required: true,
      },
      // agreement02: {
      //   required: true,
      // },
    };
  }

  componentDidMount() {
    const { language, errorsDictionary } = appState.sessionParameters;

    console.log('TOC', appState.sessionParameters.appToken);

    this.setState({
      showLoadingScreen: true,
      loadingScreenText: 'Loading...',
    });

    getCurrentTerm(appState.constants.THEGOODSEAT_API).then((data) => {
      console.log('TERMS', data);
      if (data.body && data.body.content) {
        this.setState({
          termText: data.body.content,
          showLoadingScreen: false,
          loadingScreenText: '',
        });
      } else if (data.errors) {
        this.setState(
          {
            showLoadingScreen: false,
            loadingScreenText: '',
          },
          () => {
            ErrorMessageBox(language, data.message, errorsDictionary);
          },
        );
      }
    });
  }

  //------------------------------------------
  // open webview
  //-----------------------------------------
  openWebView = (url) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        MessageBox(`Don't know how to open URI: ${url}`);
      }
    });
  };

  //--------------------------------------------
  // validate agreements
  //--------------------------------------------
  validateAgreements = () => {
    const { agreements } = this.state;

    let error = false;

    const keys = Object.keys(this.validationConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];

      error = !agreements[key];
      if (error) break;
    }

    // show button
    this.setState({ disableNextButton: error });
  };

  //--------------------------------------------
  // handle check
  //--------------------------------------------
  onPressConfirm = (type) => {
    this.setState(
      ({ agreements }) => ({
        agreements: {
          ...agreements,
          [type]: true,
        },
      }),
      () => {
        this.validateAgreements();
      },
    );
  };

  onPressRefused = () => {
    const { navigation } = this.props;
    console.log('onPressRefused');
    appState.sessionParameters.logout();
    navigation.navigate('logon');
  };

  //--------------------------------------------
  // accept
  //--------------------------------------------
  pressApproved = () => {
    const { userRegistrationState } = appState.sessionParameters;
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    // JSON.parse(JSON.stringify(appState.sessionParameters.currentUser)));

    const { disableNextButton } = this.state;
    const { navigation } = this.props;

    if (!disableNextButton) {
      this.setState({
        showLoadingScreen: true,
        loadingScreenText: 'Update data...',
      });

      // update user info
      updateUserInfo(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken, {
        userId: _id,
        termsAccepted: true,
        password: userRegistrationState.password,
      })
        .then((data) => {
          console.log('UPDATE', data);
          //
          this.setState({ showLoadingScreen: false });
          // change data
          appState.sessionParameters.setCurrentUser(data.user);
          appState.uiState.setMapViewState('NAVIGATION');
          appState.appStart();
          // main screen
          navigation.navigate('appNavigation');
        })
        .catch((err) => {
          console.warn(err);
        });
    }
  };

  render() {
    const {
      agreements,
      termText,
      showLoadingScreen,
      loadingScreenText,
      disableNextButton,
    } = this.state;

    console.log('SATTE', this.state);

    return (
      <SafeAreaView style={Styles.container}>
        <View style={Styles.helperHeader}>
          <View style={{ marginTop: 14, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={Styles.secondaryText}>Conditions Générales d’Utilisation</Text>
            <View style={[{ width: 183, marginBottom: 15 }, Styles.itemMenuContainerUnderline]} />
          </View>
        </View>
        <ScrollView
          style={Styles.termsContainer}
          contentContainerStyle={Styles.termsScrollContainer}
          showsVerticalScrollIndicator
        >
          <View>
            <Text style={Styles.blockTermsText}>{termText}</Text>
          </View>
        </ScrollView>
        <TouchableOpacity
          style={Styles.confirmContainer}
          onPress={() => this.onPressConfirm('agreement01')}
        >
          <CheckBox color="#009FE3" checked={agreements.agreement01} />
          <View style={Styles.checkBoxTextContainer}>
            <Text style={Styles.confirmText}>
              {appState.appTextData.get('tocscreen.checkbox.allowText01').value}
            </Text>
          </View>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={Styles.confirmContainer}
          onPress={() => this.onPressConfirm('agreement02')}
        >
         <CheckBox color="#009FE3" checked={agreements.agreement02} />
          <View style={Styles.checkBoxTextContainer}>
            <Text style={Styles.confirmText}>
              {appState.appTextData.get('tocscreen.checkbox.allowText02').value}
            </Text>
          </View>
       </TouchableOpacity> */}

        <View style={Styles.linkContainer}>
          {/* <TouchableOpacity
            onPress={() => this.openWebView(
              'https://static1.squarespace.com/static/59e21d01ccc5c51659dd007b/t/59fb086e692670568433f292/1509623920338/Mentions+l%C3%A9gales+-+Conditions+G%C3%A9n%C3%A9rales+d%27Utilisation+2.pdf',
            )
            }
          >
            <Text style={Styles.linkText}>CGU Youngo</Text>
          </TouchableOpacity> */}
          {/* <TouchableOpacity
            onPress={() => this.openWebView('http://allovtclyon.com/mentions-legales-2/')}
          >
            <Text style={Styles.linkText}>CGU AlloVTC Lyon</Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => this.openWebView('https://mysam.fr/condition-dutilisations/')}
          >
            <Text style={Styles.linkText}>CGU MySAM</Text>
          </TouchableOpacity>
        </View>

        <View style={Styles.helperFooter}>
          <Button transparent onPress={this.onPressRefused}>
            <Text style={[Styles.buttonText, { marginRight: 50 }]}>Refuser</Text>
          </Button>
          <Button disabled={disableNextButton} transparent onPress={this.pressApproved}>
            <Text style={disableNextButton ? Styles.buttonTextDisabled : Styles.buttonText}>
              ACCEPTER
            </Text>
          </Button>
        </View>

        <LoadingScreen
          visible={showLoadingScreen}
          processingText={loadingScreenText}
          opacity={0.7}
        />
      </SafeAreaView>
    );
  }
}

export default withNavigation(TOCScreen);

/*
<View style={Styles.termsTextContainer}>
            <Text style={Styles.blockTermsText}>{termText}</Text>
          </View>
          <View style={Styles.confirmContainer}>
            <CheckBox
              color="#009FE3"
              checked={agreements.agreement01}
              onPress={() => this.onPressConfirm('agreement01')}
            />
            <View style={Styles.checkBoxTextContainer}>
              <Text style={Styles.confirmText}>
                {appState.appTextData.get('tocscreen.checkbox.allowText01').value}
              </Text>
            </View>
          </View>
          <View style={Styles.confirmContainer}>
            <CheckBox
              color="#009FE3"
              checked={agreements.agreement02}
              onPress={() => this.onPressConfirm('agreement02')}
            />
            <View style={Styles.checkBoxTextContainer}>
              <Text style={Styles.confirmText}>
                {appState.appTextData.get('tocscreen.checkbox.allowText02').value}
              </Text>
            </View>
          </View>
          <View style={Styles.linkContainer}>
            <TouchableOpacity
              onPress={() => this.openWebView(
                'https://static1.squarespace.com/static/59e21d01ccc5c51659dd007b/t/59fb086e692670568433f292/1509623920338/Mentions+l%C3%A9gales+-+Conditions+G%C3%A9n%C3%A9rales+d%27Utilisation+2.pdf',
              )
              }
            >
              <Text style={Styles.linkText}>CGU Youngo</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.openWebView('http://allovtclyon.com/mentions-legales-2/')}
            >
              <Text style={Styles.linkText}>CGU AlloVTC Lyon</Text>
            </TouchableOpacity>
            <Text style={Styles.linkText}>CGU MySAM</Text>
          </View>
          <View style={Styles.helperFooter}>
            <Button transparent onPress={this.onPressRefused}>
              <Text style={[Styles.buttonText, { marginRight: 50 }]}>Refuser</Text>
            </Button>
            <Button disabled={disableNextButton} transparent onPress={this.pressApproved}>
              <Text style={disableNextButton ? Styles.buttonTextDisabled : Styles.buttonText}>
                ACCEPTER
              </Text>
            </Button>
          </View> */
