import React, { Component } from 'react';
import {
  Button, Item, Input, Text, Icon,
} from 'native-base';

import firebase from 'react-native-firebase';

import moment from 'moment';

import { TextInputMask } from 'react-native-masked-text';

import {
  ImageBackground,
  StatusBar,
  View,
  Animated,
  TouchableOpacity,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomPicker } from 'react-native-custom-picker';

import { observer, inject } from 'mobx-react';
import LoadingScreen from '../LoadingScreen';

import countriesData from './Countries';

import { Images, Colors } from '../../../Theme';

import Styles from '../Styles/RegistrationScreenStyles';

import {
  limitLength,
  removeNonNumber,
  dateToDMY,
  validateFormValue,
  validateEmail,
  strengthPassword,
} from '../../../Services/Utils';
import { MessageBox } from '../../../Components';

const IMAGE_LOGO_HEIGHT = 32;
const IMAGE_LOGO_WIDTH = 230;

let appState = {};

// default phone country
const defaultFlag = countriesData.filter(obj => obj.name === 'France')[0].flag;

/*
===================================================================
registration new user screen
==================================================================
*/
@inject('appState')
@observer
class RegistrationSocialScreen extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.unsubscribe = null;
    // password strength
    this.passwordStrength = false;
    this.state = {
      showLoadingScreen: false,
      loadingScreenText: '',

      firstNameDisabled: false,
      lastNameDisabled: false,
      countryDisabled: false,
      emailDisabled: false,
      dateOfBirthDisabled: false,
      phoneNumberDisabled: false,

      disableContinueSocialButton: true,
      errorInputs: {},
      formInputs: {},

      flag: defaultFlag,
      countryCode: '+33',
      modalVisible: false,
    };

    // keyboard handle
    this.keyboardHeight = new Animated.Value(0);
    this.imageLogoHeight = new Animated.Value(IMAGE_LOGO_HEIGHT);
    this.imageLogoWidth = new Animated.Value(IMAGE_LOGO_WIDTH);

    // validation social
    this.validationConfig = {
      firstName: {
        required: true,
      },
      lastName: {
        required: true,
      },
      dateOfBirth: {
        required: true,
        validFunc: this.validateDate,
      },
      country: {
        required: true,
      },
      email: {
        required: true,
        validFunc: validateEmail,
      },
      phoneNumber: {
        required: true,
      },
      password: {
        required: true,
      },
      passwordConfirm: {
        required: true,
      },
    };
  }

  componentDidMount() {
    const { userRegistrationState } = appState.sessionParameters;

    // set state
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      currentStage: 1,
      prevStage: 1,
    };

    this.setState(() => ({
      formInputs: {
        firstName: userRegistrationState.firstName,
        lastName: userRegistrationState.lastName,
        phoneNumber: userRegistrationState.phoneNumber,
        email: userRegistrationState.email,
        dateOfBirth: userRegistrationState.dateOfBirth
          ? dateToDMY(new Date(userRegistrationState.dateOfBirth), '/')
          : '',
        country: userRegistrationState.country ? userRegistrationState.country : 'France',
      },
    }));

    // disabled fields
    this.setState({
      firstNameDisabled: userRegistrationState.firstName,
      lastNameDisabled: userRegistrationState.lastName,
      emailDisabled: false, // userRegistrationState.email,
      phoneNumberDisabled: userRegistrationState.phoneNumber,
      dateOfBirthDisabled: userRegistrationState.dateOfBirth,
      countryDisabled: userRegistrationState.country,
    });

    // always logout from firebase
    // required by client
    firebase.auth().signOut();

    // add keyboard listener
    // this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    // this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    // firebase
    if (this.unsubscribe) this.unsubscribe();

    // keyboard
    if (this.keyboardWillShowSub) {
      this.keyboardWillShowSub.remove();
    }

    if (this.keyboardWillHideSub) {
      this.keyboardWillHideSub.remove();
    }
  }

  //-------------------------------------------------------
  // get country list
  //-------------------------------------------------------
  async getCountry(country) {
    const countryData = await countriesData;
    try {
      const countryCode = await countryData.filter(obj => obj.name === country)[0].dial_code;
      const countryFlag = await countryData.filter(obj => obj.name === country)[0].flag;
      // Set data from user choice of country
      // this.setState({ phoneNumber: countryCode, flag: countryFlag });
      this.setState({
        countryCode,
        flag: countryFlag,
      });
      await this.hideModal();
    } catch (err) {
      console.log(err);
    }
  }

  /*
  ANIMATION BLOCK
  */
  //------------------------------------------------------------
  // on keyboard show
  //------------------------------------------------------------
  keyboardWillShow = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height,
      }),
      Animated.timing(this.imageLogoHeight, {
        duration: event.duration,
        toValue: 16,
      }),
      Animated.timing(this.imageLogoWidth, {
        duration: event.duration,
        toValue: 115,
      }),
    ]).start();
  };

  //------------------------------------------------------------
  // on keyboard hide
  //------------------------------------------------------------
  keyboardWillHide = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: 0,
      }),
      Animated.timing(this.imageLogoHeight, {
        duration: event.duration,
        toValue: IMAGE_LOGO_HEIGHT,
      }),
      Animated.timing(this.imageLogoWidth, {
        duration: event.duration,
        toValue: IMAGE_LOGO_WIDTH,
      }),
    ]).start();
  };

  //------------------------------------------------
  // navigate back
  //------------------------------------------------
  handleNavigateBack = () => {
    const { navigation } = this.props;
    appState.sessionParameters.resetRegistrationState();

    navigation.goBack();
  };

  //------------------------------------------------
  // next step
  //------------------------------------------------
  nextStep = () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { formInputs, countryCode, phoneNumberDisabled } = this.state;
    const { navigation } = this.props;

    // if phone number field disabled, cut region, just phone number
    const fullPhoneNumber = phoneNumberDisabled
      ? formInputs.phoneNumber
      : String('').concat(countryCode, formInputs.phoneNumber || '');

    // console.log('SMS', JSON.parse(JSON.stringify(userRegistrationState)));

    // chek strength password
    if (!this.passwordStrength && this.validateSameInputData()) {
      MessageBox(
        'Info',
        'Le mot de passe doit contenir au minimum 8 caractères, 1 majuscule et 1 chiffre',
      );
    } else {
      // check firebase auth
      this.setState({
        showLoadingScreen: true,
        loadingScreenText: 'Processing...',
      });

      appState.sessionParameters.setUserRegistrationState = {
        ...userRegistrationState,
        firstName: formInputs.firstName,
        lastName: formInputs.lastName,
        dateOfBirth: formInputs.dateOfBirth,
        country: formInputs.country,
        email: formInputs.email,
        phoneNumber: fullPhoneNumber,
        password: formInputs.password,
        currentStage: 2,
        isPhonenumberVerified: false,
      };
      // change state
      this.setState(
        {
          showLoadingScreen: false,
        },
        () => navigation.navigate('smsValidation'),
      );
    }
  };

  //-------------------------------------------------
  // handle text input (social)
  //-------------------------------------------------
  handleChangeTextInput = (type, text) => {
    const { countryCode } = this.state;
    let formatedText = text;
    if (type === 'dateOfBirth') {
      const parts = text.split('/');
      if (parts.length === 3) {
        parts[1] -= 1;
        const birthDate = new Date(parts[2], parts[1], parts[0]).getTime();
        const today = new Date().getTime();
        if (birthDate > today) {
          formatedText = '';
        }
      }
    }

    if (type === 'phoneNumber') {
      const maxLength = 17;
      formatedText = formatedText.substr(
        countryCode.length,
        formatedText.length - countryCode.length,
      );
      formatedText = limitLength(removeNonNumber(formatedText), maxLength);
      formatedText = formatedText.replace(/^0+/, '');
    }

    if (type === 'password') {
      this.passwordStrength = strengthPassword(text);
    }

    this.setState(
      ({ formInputs }) => ({
        formInputs: {
          ...formInputs,
          [type]: formatedText,
        },
      }),
      () => {
        this.validateForm();
      },
    );
  };

  //-------------------------------------------------
  // validate same input data (email, password)
  //-------------------------------------------------
  validateSameInputData = () => {
    const { formInputs } = this.state;

    console.log('validate', formInputs.password, formInputs.passwordConfirm);

    // validate same password
    if (formInputs.password !== formInputs.passwordConfirm) {
      this.setState(({ errorInputs }) => ({
        errorInputs: {
          ...errorInputs,
          password: true,
        },
      }));

      this.setState(({ errorInputs }) => ({
        errorInputs: {
          ...errorInputs,
          passwordConfirm: true,
        },
      }));

      return false;
    }
    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        password: false,
      },
    }));

    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        passwordConfirm: false,
      },
    }));

    return true;
  };

  //-------------------------------------------------
  // validate date
  //-------------------------------------------------
  validateDate = () => {
    const { USER_AGE_LIMIT } = appState.constants;

    let validate = false;

    validate = this.dateOfBirthText.isValid();

    if (!validate) return validate;

    const currentYear = moment();
    const userYear = this.dateOfBirthText.getRawValue();

    validate = currentYear.diff(userYear, 'years') >= USER_AGE_LIMIT;

    return validate;
  };

  //------------------------------------------------
  // validate social input fields
  //------------------------------------------------
  validateForm = () => {
    const { formInputs } = this.state;

    let error = false;

    const keys = Object.keys(this.validationConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      error = !validateFormValue(this.validationConfig, key, formInputs[key]);
      // if error == true

      if (error) break;
    }

    if (!error) {
      error = !this.validateSameInputData();
    }

    // show button
    this.setState({ disableContinueSocialButton: error });
  };

  //-------------------------------------------------
  // handle on blur input field
  //-------------------------------------------------
  onBlurInputField = (type) => {
    const { formInputs } = this.state;
    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        [type]: !validateFormValue(this.validationConfig, type, formInputs[type]),
      },
    }));
  };

  //-------------------------------------------------
  // handle on focus input field
  //-------------------------------------------------
  onFocusInputField = () => {
    /*   const { formInputs } = this.state;
    if (type === 'phoneNumber') {
      if (
        formInputs.phoneNumber === null
        || formInputs.phoneNumber === undefined
        || formInputs.phoneNumber === ''
      ) {
        this.setState(() => ({
          formInputs: {
            ...formInputs,
            phoneNumber: '+33',
          },
        }));
      }
    } */
  };

  //------------------------------------------------
  // Country Picker Template
  //------------------------------------------------
  renderPickerField = (settings) => {
    const { selectedItem, defaultText, getLabel } = settings;
    return (
      <View style={Styles.textInputField}>
        {!selectedItem && <Text style={Styles.pickerTextField}>{defaultText}</Text>}
        {selectedItem && <Text style={Styles.pickerTextField}>{getLabel(selectedItem)}</Text>}
      </View>
    );
  };

  //-----------------------------------------------------------
  // show country modal
  //-----------------------------------------------------------
  showModal = () => {
    this.setState({ modalVisible: true });
  };

  //-----------------------------------------------------------
  // hide country modal
  //-----------------------------------------------------------
  hideModal = () => {
    this.setState({ modalVisible: false });
    // Refocus on the Input field after selecting the country code
    this.phoneNumberInputRef._root.focus();
  };

  //-----------------------------------------------------------
  // render modal screen
  //----------------------------------------------------------
  renderModalScreen = () => {
    const { modalVisible } = this.state;
    return (
      <Modal animationType="slide" transparent={false} visible={modalVisible}>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 10, paddingTop: 80, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
            <FlatList
              contentContainerStyle={Styles.coountryList}
              data={countriesData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <TouchableWithoutFeedback onPress={() => this.getCountry(item.name)}>
                  <View
                    style={[
                      Styles.countryStyle,
                      {
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      },
                    ]}
                  >
                    <Text style={{ fontSize: 32 }}>{item.flag}</Text>
                    <Text style={Styles.countryText}>
                      {item.name}
                      {' '}
(
                      {item.dial_code}
)
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
            />
          </View>
          <TouchableOpacity onPress={() => this.hideModal()} style={Styles.closeButtonStyle}>
            <Text style={Styles.closeButtonText}>Close</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  //-----------------------------------------------------------
  // render registration social form
  //-----------------------------------------------------------
  renderRegistrationSocialForm = () => {
    const {
      formInputs,
      firstNameDisabled,
      lastNameDisabled,
      countryDisabled,
      emailDisabled,
      phoneNumberDisabled,
      dateOfBirthDisabled,
      disableContinueSocialButton,
      errorInputs,
      countryCode,
      flag,
    } = this.state;

    const fullPhoneNumber = phoneNumberDisabled
      ? formInputs.phoneNumber
      : String('').concat(countryCode, formInputs.phoneNumber || '');

    return (
      <React.Fragment>
        <View style={Styles.formHeaderContainer}>
          <Text style={Styles.mainText}>
            {appState.appTextData.get('registration.social.form.header').value}
          </Text>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.firstName}>
            <Input
              placeholderTextColor={Colors.placeholderText}
              style={firstNameDisabled ? Styles.textInputDisabledField : Styles.textInputField}
              placeholder={appState.appTextData.get('registration.form.firstName').value}
              value={formInputs.firstName}
              disabled={firstNameDisabled}
              onChangeText={this.handleChangeTextInput.bind(this, 'firstName')}
              onBlur={this.onBlurInputField.bind(this, 'firstName')}
            />
          </Item>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.lastName}>
            <Input
              placeholderTextColor={Colors.placeholderText}
              style={lastNameDisabled ? Styles.textInputDisabledField : Styles.textInputField}
              placeholder={appState.appTextData.get('registration.form.lastName').value}
              value={formInputs.lastName}
              disabled={lastNameDisabled}
              onChangeText={this.handleChangeTextInput.bind(this, 'lastName')}
              onBlur={this.onBlurInputField.bind(this, 'lastName')}
            />
          </Item>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.country}>
            {countryDisabled ? (
              <Input
                value={formInputs.country}
                placeholderTextColor={Colors.placeholderText}
                style={countryDisabled ? Styles.textInputDisabledField : Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.country').value}
                disabled={countryDisabled}
                onChangeText={this.handleChangeTextInput.bind(this, 'country')}
                onBlur={this.onBlurInputField.bind(this, 'country')}
              />
            ) : (
              <View style={Styles.pickerContainer}>
                <CustomPicker
                  value={formInputs.country}
                  placeholder="Please select your country..."
                  fieldTemplate={this.renderPickerField.bind(this)}
                  options={appState.appTextData.get('country.picker.name').value}
                  onValueChange={(itemValue) => {
                    this.setState(() => ({
                      formInputs: {
                        ...formInputs,
                        country: itemValue,
                      },
                    }));
                  }}
                />
              </View>
            )}
          </Item>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.dateOfBirth}>
            <TextInputMask
              ref={(ref) => {
                this.dateOfBirthText = ref;
              }}
              placeholderTextColor={Colors.placeholderText}
              style={dateOfBirthDisabled ? Styles.textInputDisabledField : Styles.textInputField}
              placeholder={appState.appTextData.get('registration.form.dateOfBirth').value}
              value={formInputs.dateOfBirth}
              keyboardType="decimal-pad"
              returnKeyType="done"
              type="datetime"
              options={{
                format: 'DD/MM/YYYY',
              }}
              customTextInput={Input}
              disabled={dateOfBirthDisabled}
              onChangeText={this.handleChangeTextInput.bind(this, 'dateOfBirth')}
              onBlur={this.onBlurInputField.bind(this, 'dateOfBirth')}
            />
          </Item>
        </View>
        <View style={Styles.textInputContainer}>
          <Item
            error={errorInputs.phoneNumber}
            style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}
          >
            {phoneNumberDisabled ? (
              <Input
                value={fullPhoneNumber}
                placeholderTextColor={Colors.placeholderText}
                style={phoneNumberDisabled ? Styles.textInputDisabledField : Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.phoneNumber').value}
                disabled={phoneNumberDisabled}
                onChangeText={this.handleChangeTextInput.bind(this, 'country')}
                onBlur={this.onBlurInputField.bind(this, 'country')}
              />
            ) : (
              <React.Fragment>
                <View style={{ marginLeft: 50 }}>
                  <Text style={{ fontSize: 32 }}>{flag}</Text>
                </View>
                <Icon
                  active
                  name="md-arrow-dropdown"
                  style={[Styles.iconStyle, { marginLeft: 5 }]}
                  onPress={this.showModal}
                />

                <Input
                  ref={(ref) => {
                    this.phoneNumberInputRef = ref;
                  }}
                  placeholderTextColor={Colors.placeholderText}
                  style={
                    phoneNumberDisabled
                      ? Styles.textPhoneInputDisabledField
                      : Styles.textPhoneInputField
                  }
                  placeholder={appState.appTextData.get('registration.form.phoneNumber').value}
                  keyboardType="phone-pad"
                  returnKeyType="done"
                  value={fullPhoneNumber}
                  disabled={phoneNumberDisabled}
                  onChangeText={this.handleChangeTextInput.bind(this, 'phoneNumber')}
                  onFocus={this.onFocusInputField.bind(this, 'phoneNumber')}
                  onBlur={this.onBlurInputField.bind(this, 'phoneNumber')}
                />
              </React.Fragment>
            )}
          </Item>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.email}>
            <Input
              placeholderTextColor={Colors.placeholderText}
              style={emailDisabled ? Styles.textInputDisabledField : Styles.textInputField}
              placeholder={appState.appTextData.get('registration.form.email').value}
              autoCapitalize="none"
              value={formInputs.email}
              disabled={emailDisabled}
              keyboardType="email-address"
              onChangeText={this.handleChangeTextInput.bind(this, 'email')}
              onBlur={this.onBlurInputField.bind(this, 'email')}
            />
          </Item>
        </View>
        <View style={Styles.infoContainer}>
          <Text style={Styles.infoText}>
            Entrez votre mot de passe pour l&apos;inscription chez nos partenaires
          </Text>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.password}>
            <Input
              ref={(ref) => {
                this.passwordInputRef = ref;
              }}
              secureTextEntry
              placeholderTextColor={Colors.placeholderText}
              style={Styles.textInputField}
              placeholder={appState.appTextData.get('registration.form.password').value}
              autoCapitalize="none"
              returnKeyType="next"
              onChangeText={this.handleChangeTextInput.bind(this, 'password')}
              onBlur={this.onBlurInputField.bind(this, 'password')}
            />
          </Item>
        </View>
        <View style={Styles.textInputContainer}>
          <Item error={errorInputs.passwordConfirm}>
            <Input
              ref={(ref) => {
                this.passwordConfirmInputRef = ref;
              }}
              secureTextEntry
              placeholderTextColor={Colors.placeholderText}
              style={Styles.textInputField}
              placeholder={appState.appTextData.get('registration.form.passwordConfirm').value}
              autoCapitalize="none"
              returnKeyType="next"
              onChangeText={this.handleChangeTextInput.bind(this, 'passwordConfirm')}
              onBlur={this.onBlurInputField.bind(this, 'passwordConfirm')}
            />
          </Item>
        </View>
        <Text onPress={this.handleNavigateBack} style={Styles.infoTextLink}>
          Retour
        </Text>
        <Button
          block
          disabled={disableContinueSocialButton}
          style={
            disableContinueSocialButton ? Styles.disabledButtonStyle : Styles.continueButtonStyle
          }
          onPress={this.nextStep}
        >
          <Text style={Styles.textButtonStyle}>Continuer</Text>
        </Button>
      </React.Fragment>
    );
  };

  //-------------------------------------------
  // render indicators
  //-------------------------------------------
  renderPageIndicators = (number, activePage) => {
    const indicators = [];

    for (let i = 0, l = number; i < l; i += 1) {
      const indicatorStyle = i === activePage - 1
        ? [Styles.indicatorActive, { backgroundColor: 'rgba(255, 255, 255, 0.8)' }]
        : [Styles.indicator, { backgroundColor: 'rgba(255, 255, 255, 0.2)' }];

      indicators.push(<View key={i} style={indicatorStyle} />);
    }

    return <View style={Styles.indicatorContainer}>{indicators}</View>;
  };

  render() {
    const { maxRegistrationStages } = appState.uiState;

    const { userRegistrationState } = appState.sessionParameters;

    const { showLoadingScreen, loadingScreenText } = this.state;

    return (
      <ImageBackground style={Styles.backgroundImage} source={Images.backgroundImage}>
        <KeyboardAwareScrollView
          scrollEnabled
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          extraScrollHeight={55}
          enableResetScrollToCoords
          innerRef={(ref) => {
            this.scrollRef = ref;
          }}
        >
          <LoadingScreen
            visible={showLoadingScreen}
            processingText={loadingScreenText}
            opacity={0.7}
          />

          <StatusBar barStyle="light-content" />
          <Animated.Image
            source={Images.logoTitle}
            style={[
              Styles.logoContainer,
              { height: this.imageLogoHeight, width: this.imageLogoWidth },
            ]}
          />
          {this.renderRegistrationSocialForm()}
        </KeyboardAwareScrollView>
        {this.renderPageIndicators(maxRegistrationStages, userRegistrationState.currentStage)}
        {this.renderModalScreen()}
      </ImageBackground>
    );
  }
}

export default RegistrationSocialScreen;
