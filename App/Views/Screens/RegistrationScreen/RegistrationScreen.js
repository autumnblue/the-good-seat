import React, { Component } from 'react';
import {
  Button, Item, Input, Text, Icon,
} from 'native-base';

import moment from 'moment';

import firebase from 'react-native-firebase';

import { TextInputMask } from 'react-native-masked-text';

import {
  ImageBackground,
  StatusBar,
  View,
  Animated,
  Keyboard,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomPicker } from 'react-native-custom-picker';

import { observer, inject } from 'mobx-react';
import countriesData from './Countries';
import LoadingScreen from '../LoadingScreen';

import { Images, Colors } from '../../../Theme';

import Styles from '../Styles/RegistrationScreenStyles';

import {
  limitLength,
  removeNonNumber,
  validateFormValue,
  validateEmail,
  strengthPassword,
} from '../../../Services/Utils';
import { MessageBox } from '../../../Components';

const IMAGE_LOGO_HEIGHT = 32;
const IMAGE_LOGO_WIDTH = 230;

let appState = {};

// default phone country
const defaultFlag = countriesData.filter(obj => obj.name === 'France')[0].flag;

/*
===================================================================
registration new user screen
==================================================================
*/
@inject('appState')
@observer
class RegistrationScreen extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.unsubscribe = null;
    this.state = {
      showLoadingScreen: false,
      loadingScreenText: '',

      disableContinueButton: true,
      errorInputs: {},
      formInputs: {},
      firstNameFocus: false,
      phoneFocus: false,

      flag: defaultFlag,
      countryCode: '+33',
      modalVisible: false,
    };

    // keyboard handle
    this.keyboardHeight = new Animated.Value(0);
    this.imageLogoHeight = new Animated.Value(IMAGE_LOGO_HEIGHT);
    this.imageLogoWidth = new Animated.Value(IMAGE_LOGO_WIDTH);

    // password strength
    this.passwordStrength = false;

    // validation config
    this.validationConfig = {
      firstName: {
        required: true,
      },
      lastName: {
        required: true,
      },
      dateOfBirth: {
        required: true,
        validFunc: this.validateDate,
      },
      country: {
        required: true,
      },
      email: {
        required: true,
        validFunc: validateEmail,
      },
      emailConfirm: {
        required: true,
        validFunc: validateEmail,
      },
      phoneNumber: {
        required: true,
      },
      password: {
        required: true,
      },
      passwordConfirm: {
        required: true,
      },
    };
  }

  componentDidMount() {
    const { userRegistrationState } = appState.sessionParameters;

    // set state
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      currentStage: 1,
      prevStage: 1,
    };

    this.setState(() => ({
      formInputs: {
        country: 'France',
      },
    }));

    // add keyboard listener
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);

    // always logout from firebase
    // required by client
    firebase.auth().signOut();
  }

  componentWillUnmount() {
    // firebase
    if (this.unsubscribe) this.unsubscribe();

    // keyboard
    if (this.keyboardWillShowSub) {
      this.keyboardWillShowSub.remove();
    }

    if (this.keyboardWillHideSub) {
      this.keyboardWillHideSub.remove();
    }
  }

  //-------------------------------------------------------
  // get country list
  //-------------------------------------------------------
  async getCountry(country) {
    const countryData = await countriesData;
    try {
      const countryCode = await countryData.filter(obj => obj.name === country)[0].dial_code;
      const countryFlag = await countryData.filter(obj => obj.name === country)[0].flag;
      // Set data from user choice of country
      // this.setState({ phoneNumber: countryCode, flag: countryFlag });
      this.setState({
        countryCode,
        flag: countryFlag,
      });
      await this.hideModal();
    } catch (err) {
      console.log(err);
    }
  }

  /*
  ANIMATION BLOCK
  */
  //------------------------------------------------------------
  // on keyboard show
  //------------------------------------------------------------
  keyboardWillShow = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height,
      }),
      Animated.timing(this.imageLogoHeight, {
        duration: event.duration,
        toValue: 16,
      }),
      Animated.timing(this.imageLogoWidth, {
        duration: event.duration,
        toValue: 115,
      }),
    ]).start();
  };

  //------------------------------------------------------------
  // on keyboard hide
  //------------------------------------------------------------
  keyboardWillHide = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: 0,
      }),
      Animated.timing(this.imageLogoHeight, {
        duration: event.duration,
        toValue: IMAGE_LOGO_HEIGHT,
      }),
      Animated.timing(this.imageLogoWidth, {
        duration: event.duration,
        toValue: IMAGE_LOGO_WIDTH,
      }),
    ]).start();
  };

  //------------------------------------------------
  // navigate back
  //------------------------------------------------
  handleNavigateBack = () => {
    const { navigation } = this.props;
    appState.sessionParameters.resetRegistrationState();

    navigation.goBack();
  };

  //------------------------------------------------
  // next step
  //------------------------------------------------
  nextStep = () => {
    const { formInputs, countryCode } = this.state;
    const { navigation } = this.props;

    const fullPhoneNumber = String('').concat(countryCode, formInputs.phoneNumber || '');

    // chek strength password
    if (!this.passwordStrength && this.validateSameInputData()) {
      MessageBox(
        'Info',
        'Le mot de passe doit contenir au minimum 8 caractères, 1 majuscule et 1 chiffre',
      );
    } else {
      // check firebase auth
      this.setState({
        showLoadingScreen: true,
        loadingScreenText: 'Processing...',
      });

      // gather input data and set it
      appState.sessionParameters.setUserRegistrationState = {
        isSocialLogon: false,
        firstName: formInputs.firstName,
        lastName: formInputs.lastName,
        dateOfBirth: formInputs.dateOfBirth,
        country: formInputs.country,
        email: formInputs.email,
        phoneNumber: fullPhoneNumber,
        password: formInputs.password,
        currentStage: 2,
        isPhonenumberVerified: false,
      };
      // navigate to next screen
      this.setState(
        {
          showLoadingScreen: false,
        },
        () => navigation.navigate('smsValidation'),
      );
    }
  };

  //-------------------------------------------------
  // handle text input
  //-------------------------------------------------
  handleChangeTextInput = (type, text) => {
    const { countryCode } = this.state;

    let formatedText = text;
    if (type === 'dateOfBirth') {
      const parts = text.split('/');
      if (parts.length === 3) {
        parts[1] -= 1;
        const birthDate = new Date(parts[2], parts[1], parts[0]).getTime();
        const today = new Date().getTime();
        if (birthDate > today) {
          formatedText = '';
        }
      }
    }

    if (type === 'phoneNumber') {
      const maxLength = 17;
      formatedText = formatedText.substr(
        countryCode.length,
        formatedText.length - countryCode.length,
      );
      formatedText = limitLength(removeNonNumber(formatedText), maxLength);
      formatedText = formatedText.replace(/^0+/, '');
    }

    if (type === 'password') {
      this.passwordStrength = strengthPassword(text);
    }

    this.setState(
      ({ formInputs }) => ({
        formInputs: {
          ...formInputs,
          [type]: formatedText,
        },
      }),
      () => {
        this.validateRegistrationForm();
      },
    );
  };

  //-------------------------------------------------
  // validate same input data (email, password)
  //-------------------------------------------------
  validateSameInputData = () => {
    const { formInputs } = this.state;

    if (
      !formInputs.email
      || !formInputs.password
      || !formInputs.emailConfirm
      || !formInputs.password
    ) {
      return false;
    }

    // validate same email
    if (formInputs.email && formInputs.email !== formInputs.emailConfirm) {
      this.setState(({ errorInputs }) => ({
        errorInputs: {
          ...errorInputs,
          email: true,
        },
      }));

      this.setState(({ errorInputs }) => ({
        errorInputs: {
          ...errorInputs,
          emailConfirm: true,
        },
      }));

      return false;
    }

    // validate same password
    if (formInputs.password && formInputs.password !== formInputs.passwordConfirm) {
      this.setState(({ errorInputs }) => ({
        errorInputs: {
          ...errorInputs,
          password: true,
        },
      }));

      this.setState(({ errorInputs }) => ({
        errorInputs: {
          ...errorInputs,
          passwordConfirm: true,
        },
      }));

      return false;
    }

    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        password: false,
      },
    }));

    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        passwordConfirm: false,
      },
    }));

    return true;
  };

  //-------------------------------------------------
  // validate date
  //-------------------------------------------------
  validateDate = () => {
    const { USER_AGE_LIMIT } = appState.constants;

    let validate = false;

    validate = this.dateOfBirthText.isValid();

    if (!validate) return validate;

    const currentYear = moment();
    const userYear = this.dateOfBirthText.getRawValue();

    validate = currentYear.diff(userYear, 'years') >= USER_AGE_LIMIT;

    return validate;
  };

  //------------------------------------------------
  // validate registration input fields
  //------------------------------------------------
  validateRegistrationForm = () => {
    const { formInputs } = this.state;

    let error = false;

    const keys = Object.keys(this.validationConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];

      error = !validateFormValue(this.validationConfig, key, formInputs[key]);
      // console.log('validate ', this.validationConfig, key, formInputs[key], 'error', error);
      // if error == true
      if (error) break;
    }

    if (!error) {
      error = !this.validateSameInputData();
    }

    // show button
    this.setState({ disableContinueButton: error });
  };

  //-------------------------------------------------
  // handle on blur input field
  //-------------------------------------------------
  onBlurInputField = (type) => {
    const { formInputs } = this.state;
    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        [type]: !validateFormValue(this.validationConfig, type, formInputs[type]),
      },
    }));
  };

  //-------------------------------------------------
  // handle on focus input field
  //-------------------------------------------------
  onFocusInputField = () => {
    /*   const { formInputs } = this.state;
    if (type === 'phoneNumber') {
      if (
        formInputs.phoneNumber === null
        || formInputs.phoneNumber === undefined
        || formInputs.phoneNumber === ''
      ) {
        this.setState(() => ({
          formInputs: {
            ...formInputs,
            phoneNumber: '+33',
          },
        }));
      }
    } */
  };

  //------------------------------------------------
  // Country Picker Template
  //------------------------------------------------
  renderPickerField = (settings) => {
    const { selectedItem, defaultText, getLabel } = settings;
    return (
      <View style={Styles.textInputField}>
        {!selectedItem && <Text style={Styles.pickerTextField}>{defaultText}</Text>}
        {selectedItem && <Text style={Styles.pickerTextField}>{getLabel(selectedItem)}</Text>}
      </View>
    );
  };

  //-----------------------------------------------------------
  // show country modal
  //-----------------------------------------------------------
  showModal = () => {
    this.setState({ modalVisible: true });
  };

  //-----------------------------------------------------------
  // hide country modal
  //-----------------------------------------------------------
  hideModal = () => {
    this.setState({ modalVisible: false });
    // Refocus on the Input field after selecting the country code
    this.phoneNumberInputRef._root.focus();
  };

  //-----------------------------------------------------------
  // render modal screen
  //----------------------------------------------------------
  renderModalScreen = () => {
    const { modalVisible } = this.state;
    return (
      <Modal animationType="slide" transparent={false} visible={modalVisible}>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 10, paddingTop: 80, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
            <FlatList
              contentContainerStyle={Styles.coountryList}
              data={countriesData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <TouchableWithoutFeedback onPress={() => this.getCountry(item.name)}>
                  <View
                    style={[
                      Styles.countryStyle,
                      {
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      },
                    ]}
                  >
                    <Text style={{ fontSize: 32 }}>{item.flag}</Text>
                    <Text style={Styles.countryText}>
                      {item.name}
                      {' '}
(
                      {item.dial_code}
)
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
            />
          </View>
          <TouchableOpacity onPress={() => this.hideModal()} style={Styles.closeButtonStyle}>
            <Text style={Styles.closeButtonText}>Close</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  //-----------------------------------------------------------
  // render registration standart form
  //-----------------------------------------------------------
  renderRegistrationStandartForm = () => {
    const {
      disableContinueButton,
      errorInputs,
      formInputs,
      firstNameFocus,
      phoneFocus,
      flag,
      countryCode,
    } = this.state;

    const fullPhoneNumber = String('').concat(countryCode, formInputs.phoneNumber || '');

    return (
      <View style={{ flex: 1 }}>
        <View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.firstName}>
              <Input
                ref={(ref) => {
                  this.firstNameInputRef = ref;
                }}
                returnKeyType="next"
                value={formInputs.firstName}
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.firstName').value}
                autoFocus={firstNameFocus}
                onChangeText={this.handleChangeTextInput.bind(this, 'firstName')}
                onBlur={this.onBlurInputField.bind(this, 'firstName')}
                onSubmitEditing={() => {
                  this.lastNameInputRef._root.focus();
                }}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.lastName}>
              <Input
                ref={(ref) => {
                  this.lastNameInputRef = ref;
                }}
                returnKeyType="next"
                value={formInputs.lastName}
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.lastName').value}
                onChangeText={this.handleChangeTextInput.bind(this, 'lastName')}
                onBlur={this.onBlurInputField.bind(this, 'lastName')}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.country}>
              <View style={Styles.pickerContainer}>
                <CustomPicker
                  value={formInputs.country}
                  placeholder="Please select your country..."
                  fieldTemplate={this.renderPickerField.bind(this)}
                  options={appState.appTextData.get('country.picker.name').value}
                  onValueChange={(itemValue) => {
                    this.setState(() => ({
                      formInputs: {
                        ...formInputs,
                        country: itemValue,
                      },
                    }));
                  }}
                />
              </View>
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item last error={errorInputs.dateOfBirth}>
              <TextInputMask
                ref={(ref) => {
                  this.dateOfBirthText = ref;
                }}
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.dateOfBirth').value}
                value={formInputs.dateOfBirth}
                keyboardType="decimal-pad"
                returnKeyType="done"
                type="datetime"
                options={{
                  format: 'DD/MM/YYYY',
                }}
                customTextInput={Input}
                onChangeText={this.handleChangeTextInput.bind(this, 'dateOfBirth')}
                onBlur={this.onBlurInputField.bind(this, 'dateOfBirth')}
                onSubmitEditing={() => {
                  this.phoneNumberInputRef._root.focus();
                }}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item
              error={errorInputs.phoneNumber}
              style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}
            >
              <View style={{ marginLeft: 50 }}>
                <Text style={{ fontSize: 32 }}>{flag}</Text>
              </View>
              <Icon
                active
                name="md-arrow-dropdown"
                style={[Styles.iconStyle, { marginLeft: 5 }]}
                onPress={this.showModal}
              />
              <Input
                ref={(ref) => {
                  this.phoneNumberInputRef = ref;
                }}
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textPhoneInputField}
                placeholder={appState.appTextData.get('registration.form.phoneNumber').value}
                keyboardType="phone-pad"
                returnKeyType="done"
                autoFocus={phoneFocus}
                value={fullPhoneNumber}
                onChangeText={this.handleChangeTextInput.bind(this, 'phoneNumber')}
                onFocus={this.onFocusInputField.bind(this, 'phoneNumber')}
                onBlur={this.onBlurInputField.bind(this, 'phoneNumber')}
                onSubmitEditing={() => {
                  this.emailInputRef._root.focus();
                }}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.email}>
              <Input
                ref={(ref) => {
                  this.emailInputRef = ref;
                }}
                value={formInputs.email}
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.email').value}
                autoCapitalize="none"
                returnKeyType="next"
                keyboardType="email-address"
                onChangeText={this.handleChangeTextInput.bind(this, 'email')}
                onBlur={this.onBlurInputField.bind(this, 'email')}
                onSubmitEditing={() => {
                  this.emailConfirmInputRef._root.focus();
                }}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.emailConfirm}>
              <Input
                ref={(ref) => {
                  this.emailConfirmInputRef = ref;
                }}
                value={formInputs.emailConfirm}
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.emailConfirm').value}
                autoCapitalize="none"
                returnKeyType="next"
                keyboardType="email-address"
                onChangeText={this.handleChangeTextInput.bind(this, 'emailConfirm')}
                onBlur={this.onBlurInputField.bind(this, 'emailConfirm')}
                onSubmitEditing={() => {
                  this.passwordInputRef._root.focus();
                }}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.password}>
              <Input
                ref={(ref) => {
                  this.passwordInputRef = ref;
                }}
                secureTextEntry
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.password').value}
                autoCapitalize="none"
                returnKeyType="next"
                onChangeText={this.handleChangeTextInput.bind(this, 'password')}
                onBlur={this.onBlurInputField.bind(this, 'password')}
                onSubmitEditing={() => {
                  this.passwordConfirmInputRef._root.focus();
                }}
              />
            </Item>
          </View>
          <View style={Styles.textInputContainer}>
            <Item error={errorInputs.passwordConfirm}>
              <Input
                ref={(ref) => {
                  this.passwordConfirmInputRef = ref;
                }}
                secureTextEntry
                placeholderTextColor={Colors.placeholderText}
                style={Styles.textInputField}
                placeholder={appState.appTextData.get('registration.form.passwordConfirm').value}
                autoCapitalize="none"
                returnKeyType="next"
                onChangeText={this.handleChangeTextInput.bind(this, 'passwordConfirm')}
                onBlur={this.onBlurInputField.bind(this, 'passwordConfirm')}
                onSubmitEditing={disableContinueButton ? () => {} : this.nextStep}
              />
            </Item>
          </View>
        </View>
        <Text onPress={this.handleNavigateBack} style={Styles.infoTextLink}>
          Retour
        </Text>
        <Button
          block
          style={disableContinueButton ? Styles.disabledButtonStyle : Styles.continueButtonStyle}
          onPress={this.nextStep}
          disabled={disableContinueButton}
        >
          <Text style={Styles.textButtonStyle}>Continuer</Text>
        </Button>
      </View>
    );
  };

  //-------------------------------------------
  // render indicators
  //-------------------------------------------
  renderPageIndicators = (number, activePage) => {
    const indicators = [];

    for (let i = 0, l = number; i < l; i += 1) {
      const indicatorStyle = i === activePage - 1
        ? [Styles.indicatorActive, { backgroundColor: 'rgba(255, 255, 255, 0.8)' }]
        : [Styles.indicator, { backgroundColor: 'rgba(255, 255, 255, 0.2)' }];

      indicators.push(<View key={i} style={indicatorStyle} />);
    }

    return <View style={Styles.indicatorContainer}>{indicators}</View>;
  };

  render() {
    const { maxRegistrationStages } = appState.uiState;

    const { userRegistrationState } = appState.sessionParameters;

    const { showLoadingScreen, loadingScreenText } = this.state;

    return (
      <ImageBackground style={Styles.backgroundImage} source={Images.backgroundImage}>
        <KeyboardAwareScrollView
          contentContainerStyle={{ paddingBottom: 28 }}
          scrollEnabled
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          extraScrollHeight={55}
          enableResetScrollToCoords
          innerRef={(ref) => {
            this.scrollRef = ref;
          }}
        >
          <LoadingScreen
            visible={showLoadingScreen}
            processingText={loadingScreenText}
            opacity={0.7}
          />

          <StatusBar barStyle="light-content" />
          <Animated.Image
            source={Images.logoTitle}
            style={[
              Styles.logoContainer,
              { height: this.imageLogoHeight, width: this.imageLogoWidth },
            ]}
          />
          {this.renderRegistrationStandartForm()}
        </KeyboardAwareScrollView>
        {this.renderPageIndicators(maxRegistrationStages, userRegistrationState.currentStage)}
        {this.renderModalScreen()}
      </ImageBackground>
    );
  }
}

export default RegistrationScreen;
