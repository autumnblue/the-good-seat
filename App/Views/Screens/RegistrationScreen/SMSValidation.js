import React, { Component } from 'react';
import {
  Button, Content, Input, Text,
} from 'native-base';

import { Sentry } from 'react-native-sentry';

import {
  Alert, ImageBackground, Image, StatusBar, View, Animated,
} from 'react-native';

import VirtualKeyboard from 'react-native-virtual-keyboard';
import firebase from 'react-native-firebase';

import { observer, inject } from 'mobx-react';

import LoadingScreen from '../LoadingScreen';

import { Images } from '../../../Theme';

import Styles from '../Styles/RegistrationScreenStyles';

import { validateFormValue } from '../../../Services/Utils';
import { MessageBox } from '../../../Components';

const IMAGE_LOGO_HEIGHT = 32;
const IMAGE_LOGO_WIDTH = 230;

let appState = {};

/*
===================================================================
registration new user screen
==================================================================
*/
@inject('appState')
@observer
class SMSValidationScreen extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.unsubscribe = null;

    this.state = {
      showLoadingScreen: false,
      loadingScreenText: '',

      disableVerifyContinueButton: true,
      formInputs: {},
      alertMessage: 'Merci de saisir le code envoyé au',
    };

    // keyboard handle
    this.keyboardHeight = new Animated.Value(0);
    this.imageLogoHeight = new Animated.Value(IMAGE_LOGO_HEIGHT);
    this.imageLogoWidth = new Animated.Value(IMAGE_LOGO_WIDTH);

    // validation code verification
    this.validationConfig = {
      firstCode: {
        required: true,
      },
      secondCode: {
        required: true,
      },
      thirdCode: {
        required: true,
      },
      fourthCode: {
        required: true,
      },
      fifthCode: {
        required: true,
      },
      sixthCode: {
        required: true,
      },
    };
  }

  componentDidMount() {
    const { userRegistrationState } = appState.sessionParameters;

    // set state
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      currentStage: 2,
    };

    // show loading screen
    this.setState({
      showLoadingScreen: true,
      loadingScreenText: 'Processing...',
    });

    // always logout from firebase
    // required by client
    firebase.auth().signOut();

    /* this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      // exsist user need next
      if (user) {
        this.setState(
          {
            showLoadingScreen: false,
          },
          () => this.nextStep(),
        );
      } else {
        // send sms code
        this.sendCodeVerification();
      }

      // sending SMS code


    }); */

    // sending SMS code
    this.sendCodeVerification();

    // console.log('SMS2', JSON.parse(JSON.stringify(userRegistrationState)));
  }

  componentWillUnmount() {
    // firebase
    // if (this.unsubscribe) this.unsubscribe();
  }

  //------------------------------------------------
  // navigate back
  //------------------------------------------------
  navigateBack = () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { navigation } = this.props;
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      currentStage: 1,
    };

    navigation.goBack();
  };

  //------------------------------------------------
  // next step
  //------------------------------------------------
  nextStep = () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { navigation } = this.props;
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      isPhonenumberVerified: true,
      currentStage: 3,
      prevStage: 2,
    };
    navigation.navigate('newPayCard');
  };

  //-------------------------------------------------
  // Phone verify code send to API
  //-------------------------------------------------
  sendCodeVerification = () => {
    const { userRegistrationState } = appState.sessionParameters;

    console.log('SMS2', JSON.parse(JSON.stringify(userRegistrationState)));
    this.setState({
      showLoadingScreen: true,
      loadingScreenText: 'Sending sms code...',
    });
    try {
      firebase
        .auth()
        .signInWithPhoneNumber(userRegistrationState.phoneNumber)
        .then((confirmResult) => {
          this.setState({
            showLoadingScreen: false,
            loadingScreenText: '',
            alertMessage: 'Un nouveau code a été envoyé au',
            confirmResult,
          });
        })
        .catch((error) => {
          Sentry.captureMessage(`SMS firebase - ${error.message}`);
          MessageBox('SMS send error', error.message);
          console.warn(error);
          this.setState({
            showLoadingScreen: false,
            alertMessage: `Erreur lors de l'envoi du numéro vers le ${
              userRegistrationState.phoneNumber
            }: ${error.message}`,
          });
        });
    } catch (err) {
      MessageBox('SMS send error', err.message);
      Sentry.captureMessage(`SMS firebase - ${err.message}`);
      console.warn(err);
      this.setState({
        showLoadingScreen: false,
        alertMessage: `Erreur lors de l'envoi du numéro vers le ${
          userRegistrationState.phoneNumber
        }: ${err.message}`,
      });
    }
  };

  //-------------------------------------------------
  // Code confirm
  //-------------------------------------------------
  confirmResult = async () => {
    const { formInputs, confirmResult } = this.state;
    this.setState({
      alertMessage: '',
      showLoadingScreen: true,
      loadingScreenText: 'Vérification du code en cours...',
    });

    let codeInput = '';
    const keys = Object.keys(this.validationConfig);
    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      codeInput += formInputs[key];
    }
    console.log('confirmREsult status', codeInput);

    // verify number
    try {
      const verif = await confirmResult
        .confirm(codeInput)
        .catch(err => Alert.alert('Error', err.message));
      console.log('Is verif successful ?', verif);
      if (verif) {
        // next step
        this.setState(
          {
            alertMessage: '',
            showLoadingScreen: false,
            loadingScreenText: '',
          },
          () => this.nextStep(),
        );
      } else {
        setTimeout(() => {
          this.setState({
            alertMessage: 'Le code entré est érroné. Merci de ressayer.',
            showLoadingScreen: false,
            loadingScreenText: '',
          });
        }, 300);
      }
    } catch (err) {
      console.log('global error');
      console.warn(err);
      this.setState({
        alertMessage: 'Unknown error',
        showLoadingScreen: false,
        loadingScreenText: '',
      });
    }
  };

  //----------------------------------------------
  // validate registration form
  //----------------------------------------------
  validateRegistrationVerifyForm = () => {
    const { formInputs } = this.state;

    let error = false;

    const keys = Object.keys(this.validationConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      error = !validateFormValue(this.validationConfig, key, formInputs[key]);
      // if error == true
      if (error) break;
    }

    // show button
    this.setState({ disableVerifyContinueButton: error });
  };

  //-----------------------------------------------
  // change code number field
  //-----------------------------------------------
  onChangeCodeNumberField = (text) => {
    if (text.length > 6) {
      return;
    }
    const { formInputs } = this.state;
    const keys = Object.keys(this.validationConfig);
    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      formInputs[key] = text[i] || '';
    }

    this.setState({ formInputs }, () => {
      this.validateRegistrationVerifyForm();
    });
  };

  //-------------------------------------------
  // render indicators
  //-------------------------------------------
  renderPageIndicators = (number, activePage) => {
    const indicators = [];

    for (let i = 0, l = number; i < l; i += 1) {
      const indicatorStyle = i === activePage - 1
        ? [Styles.indicatorActive, { backgroundColor: 'rgba(255, 255, 255, 0.8)' }]
        : [Styles.indicator, { backgroundColor: 'rgba(255, 255, 255, 0.2)' }];

      indicators.push(<View key={i} style={indicatorStyle} />);
    }

    return <View style={Styles.indicatorContainer}>{indicators}</View>;
  };

  //-----------------------------------------------------
  // render phone verify screen
  //-----------------------------------------------------
  renderPhoneVerifyScreen = () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { disableVerifyContinueButton, formInputs, alertMessage } = this.state;

    return (
      <React.Fragment>
        <Text style={Styles.registerVerifyTitleText}>
          {alertMessage}
          {' '}
          {userRegistrationState.phoneNumber}
        </Text>
        <View style={Styles.registerVerifyCodeInputContainer}>
          <View style={Styles.verifyTextInput}>
            <Input
              editable={false}
              returnKeyType="next"
              value={formInputs.firstCode}
              style={Styles.textInputField}
            />
          </View>
          <View style={Styles.verifyTextInput}>
            <Input
              editable={false}
              returnKeyType="next"
              value={formInputs.secondCode}
              style={Styles.textInputField}
            />
          </View>
          <View style={Styles.verifyTextInput}>
            <Input
              editable={false}
              returnKeyType="next"
              value={formInputs.thirdCode}
              style={Styles.textInputField}
            />
          </View>
          <View style={Styles.verifyTextInput}>
            <Input
              editable={false}
              returnKeyType="next"
              value={formInputs.fourthCode}
              style={Styles.textInputField}
            />
          </View>
          <View style={Styles.verifyTextInput}>
            <Input
              editable={false}
              returnKeyType="next"
              value={formInputs.fifthCode}
              style={Styles.textInputField}
            />
          </View>
          <View style={Styles.verifyTextInput}>
            <Input
              editable={false}
              returnKeyType="next"
              value={formInputs.sixthCode}
              style={Styles.textInputField}
            />
          </View>
        </View>
        <Button block style={Styles.verifyButtonStyle} onPress={this.sendCodeVerification}>
          <Text style={Styles.codeSendTextButtonStyle}>Me renvoyer le code</Text>
        </Button>
        <Button block style={Styles.verifyButtonStyle} onPress={this.navigateBack}>
          <Text style={Styles.codeSendTextButtonStyle}>Modifier le numéro</Text>
        </Button>
        <View>
          <VirtualKeyboard
            color="white"
            pressMode="string"
            backspaceImg={Images.keyboardBackButton}
            onPress={val => this.onChangeCodeNumberField(val)}
          />
        </View>
        <Text onPress={this.navigateBack} style={Styles.infoTextLink}>
          Retour
        </Text>
        <Button
          block
          style={
            disableVerifyContinueButton ? Styles.disabledButtonStyle : Styles.continueButtonStyle
          }
          onPress={this.confirmResult}
          disabled={disableVerifyContinueButton}
        >
          <Text style={Styles.textButtonStyle}>Confirmer</Text>
        </Button>
      </React.Fragment>
    );
  };

  render() {
    const { maxRegistrationStages } = appState.uiState;

    const { userRegistrationState } = appState.sessionParameters;

    const { showLoadingScreen, loadingScreenText } = this.state;

    return (
      <View style={Styles.container}>
        <StatusBar barStyle="light-content" />
        <ImageBackground style={Styles.backgroundImage} source={Images.backgroundImage}>
          <Image style={Styles.logoContainer} source={Images.logoTitle} />
          <Content>{this.renderPhoneVerifyScreen()}</Content>
          {this.renderPageIndicators(maxRegistrationStages, userRegistrationState.currentStage)}
        </ImageBackground>
        <LoadingScreen
          visible={showLoadingScreen}
          processingText={loadingScreenText}
          opacity={0.7}
        />
      </View>
    );
  }
}

export default SMSValidationScreen;
