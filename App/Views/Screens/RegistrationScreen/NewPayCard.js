import React, { Component } from 'react';
import {
  Button, Content, Item, Input, Text,
} from 'native-base';

import { Sentry, SentrySeverity } from 'react-native-sentry';

import { TextInputMask } from 'react-native-masked-text';

import {
  Alert,
  ImageBackground,
  Image,
  StatusBar,
  View,
  Animated,
} from 'react-native';

import moment from 'moment';

import { observer, inject } from 'mobx-react';
import { ErrorMessageBox } from '../../../Components';
import LoadingScreen from '../LoadingScreen';

import { Images } from '../../../Theme';

import Styles from '../Styles/RegistrationScreenStyles';
import {
  registerUser,
  updateUserInfo,
  addPaymentCard,
} from '../../../Services/BackEnd/TheGoodSeat';
import {
  validateFormValue,
  validateCheckDigit,
  formatCardDateExp,
} from '../../../Services/Utils';
import { registerCard, getCard } from '../../../Services/BackEnd/MangoPay';

const IMAGE_LOGO_HEIGHT = 32;
const IMAGE_LOGO_WIDTH = 230;

let appState = {};

/*
===================================================================
registration new user screen
==================================================================
*/
@inject('appState')
@observer
class NewPayCardScreen extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      showLoadingScreen: false,
      disableValidateButton: true,
      loadingScreenText: '',
      errorInputs: {},
      formInputs: {},
    };

    // keyboard handle
    this.keyboardHeight = new Animated.Value(0);
    this.imageLogoHeight = new Animated.Value(IMAGE_LOGO_HEIGHT);
    this.imageLogoWidth = new Animated.Value(IMAGE_LOGO_WIDTH);

    // validation config
    this.validateConfig = {
      cardNumber: {
        required: true,
        validFunc: this.validateCardNumber,
      },
      cardDateExp: {
        required: true,
        validFunc: this.validateDateCardExp,
      },
      cardCVC: {
        required: true,
      },
    };

    // track previous date input to apply custom mask logic
    this.prevCardDateExp = '';
  }

  componentDidMount() {
    const { userRegistrationState } = appState.sessionParameters;

    // set state
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      currentStage: 3,
    };

    // add keyboard listener
    // this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    // this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    // keyboard
    if (this.keyboardWillShowSub) {
      this.keyboardWillShowSub.remove();
    }

    if (this.keyboardWillHideSub) {
      this.keyboardWillHideSub.remove();
    }
  }

  //------------------------------------------------
  // navigate back
  //------------------------------------------------
  handleNavigateBack = () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { navigation } = this.props;
    appState.sessionParameters.setUserRegistrationState = {
      ...userRegistrationState,
      currentStage: 2,
    };

    Sentry.captureMessage('BACK - from register card', SentrySeverity.Info);

    navigation.goBack();
  };

  //---------------------------------------------------
  // post card to MangoPay service
  //---------------------------------------------------
  postCardToMangoPay = async (mangoPayUserId) => {
    const { formInputs } = this.state;

    const cardData = await registerCard(
      appState.constants.MANGOPAY_API,
      appState.constants.MANGOPAY_CLIENTID,
      appState.constants.MANGOPAY_TOKEN,

      {
        tag: 'Firstcard',
        userId: mangoPayUserId,
        currencyCode: 'EUR',
        cardNumber: formInputs.cardNumber.replace(/\s+/g, ''),
        cardExpirationDate: formInputs.cardDateExp.replace('/', ''),
        cardCvx: formInputs.cardCVC,
      },
    );

    const result = await getCard(
      appState.constants.MANGOPAY_API,
      appState.constants.MANGOPAY_CLIENTID,
      appState.constants.MANGOPAY_TOKEN,
      cardData.CardId,
    );

    if (result.errors) {
      Alert.alert('Get MangoPay card', result.Message);
    }

    return result;
  };

  //-----------------------------------------------
  // create new user
  //-----------------------------------------------
  createNewUser = async () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { language, errorsDictionary } = appState.sessionParameters;

    // construct user profile
    const userProfile = {
      firstname: userRegistrationState.firstName,
      lastname: userRegistrationState.lastName,
      email: userRegistrationState.email,
      password: userRegistrationState.password,
      username: userRegistrationState.email,
      birthdate: moment(userRegistrationState.dateOfBirth, ['DD/MM/YYYY', 'YYYY-MM-DD']).format(),
      phonenumber: userRegistrationState.phoneNumber,
      isPhonenumberVerified: userRegistrationState.isPhonenumberVerified,
    };

    this.setState({ showLoadingScreen: true, loadingScreenText: 'Creating new user' });

    // post to server
    const data = await registerUser(appState.constants.THEGOODSEAT_API, userProfile);

    if (data) {
      if (data.errors) {
        if (data.message === 'error_conflict_email') {
          this.setState({ showLoadingScreen: false }, () => {
            Alert.alert('Erreur', "L'Email est déjà utilisée", { cancelable: true });
          });
        } else {
          this.setState({ showLoadingScreen: false }, () => {
            ErrorMessageBox(language, data, errorsDictionary);
          });
        }
      } else {
        // set new app token !!!
        appState.sessionParameters.setAppToken(data.token);
        // return result
        return data;
      }
    }

    return null;
  };

  //-----------------------------------------------
  // update user info
  //-----------------------------------------------
  updateUserInfo = async () => {
    const { userRegistrationState } = appState.sessionParameters;
    const { language, errorsDictionary } = appState.sessionParameters;

    this.setState({ showLoadingScreen: true, loadingScreenText: 'Update user data' });

    console.log('reg card', JSON.parse(JSON.stringify(userRegistrationState)));

    // update user info
    const data = await updateUserInfo(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      {
        userId: userRegistrationState.newUserID,
        firstname: userRegistrationState.firstName,
        lastname: userRegistrationState.lastName,
        phonenumber: userRegistrationState.phoneNumber,
        birthdate: moment(userRegistrationState.dateOfBirth, ['DD/MM/YYYY', 'YYYY-MM-DD']).format(),
        country: userRegistrationState.country,
        email: userRegistrationState.email,
        isPhonenumberVerified: userRegistrationState.isPhonenumberVerified,
      },
    );

    this.setState({ showLoadingScreen: false, loadingScreenText: '' });

    if (data.message || data.errors) {
      ErrorMessageBox(language, data, errorsDictionary);
    } else {
      return data;
    }

    return null;
  };

  //-----------------------------------------------
  // create new user and enter in app
  //-----------------------------------------------
  initApp = async (RegisterCard) => {
    const { userRegistrationState } = appState.sessionParameters;
    const { navigation } = this.props;

    let userData = null;

    // variants
    if (!userRegistrationState.isSocialLogon) {
      userData = await this.createNewUser();
    } else {
      userData = await this.updateUserInfo();
    }

    console.log('USER DATA', userData);

    // register new card
    if (RegisterCard) {
      // state
      this.setState({ showLoadingScreen: true, loadingScreenText: 'Creating new pay card' });

      // post new card
      this.postCardToMangoPay(userData.user.mangoPayUserId).then((cardData) => {
        if (cardData) {
          addPaymentCard(
            appState.constants.THEGOODSEAT_API,
            appState.sessionParameters.appToken,
            userData.user._id,
            {
              name: 'First card',
              mongoPayCardId: cardData.Id,
              isFavorite: true,
            },
          ).then(() => {});
        }
      });

      // state
      this.setState({ showLoadingScreen: false, loadingScreenText: '' });
    }

    // navigate
    // appState.sessionParameters.appToken = data.token;
    // appState.sessionParameters.currentUser = data.user;
    // AsyncStorage.setItem('AUTH_TOKEN', appState.sessionParameters.appToken);
    // AsyncStorage.setItem('CURRENT_USER', JSON.stringify(appState.sessionParameters.currentUser));

    if (userData) {
      appState.sessionParameters.setCurrentUser(userData.user);
      navigation.navigate('tocScreen');
    }
  };

  //-------------------------------------------------
  // handle text input
  //-------------------------------------------------
  handleChangeTextInput = (type, text) => {
    this.setState(
      ({ formInputs }) => ({
        formInputs: {
          ...formInputs,
          [type]: this.formatCreditCardTextInput(type, text),
        },
      }),
      () => {
        this.validateForm();
      },
    );
  };

  //------------------------------------------------
  // validate date card exp
  //------------------------------------------------
  validateDateCardExp = () => {
    const dateExp = this.cardDateExp.getRawValue();

    const year = dateExp.year();
    const month = dateExp.month() + 1;

    if (month > 0 && month <= 12) {
      const currentYear = moment().year();
      if (currentYear < year) return true;

      if (currentYear === year) {
        const currentMonth = moment().month() + 1;
        if (currentMonth <= month) return true;
      }
    }

    return false;
  };

  //------------------------------------------------
  // credit card is valid
  //------------------------------------------------
  validateCardNumber = text => validateCheckDigit(text);

  //------------------------------------------------
  // validate new credit card input fields
  //------------------------------------------------
  validateForm = () => {
    const { formInputs } = this.state;

    let error = false;

    const keys = Object.keys(this.validateConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      error = !validateFormValue(this.validateConfig, key, formInputs[key]);
      // if error == true
      if (error) break;
    }

    // show button
    this.setState({ disableValidateButton: error });
  };

  //------------------------------------------------
  // format credit card text before applying to form
  //------------------------------------------------
  formatCreditCardTextInput = (type, textIn) => {
    if (type === 'cardDateExp') {
      this.prevCardDateExp = formatCardDateExp(textIn, this.prevCardDateExp);
    }
    return textIn;
  };

  //-------------------------------------------------
  // handle on blur input field
  //-------------------------------------------------
  onBlurInput = (type) => {
    const { formInputs } = this.state;

    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        [type]: !validateFormValue(this.validateConfig, type, formInputs[type]),
      },
    }));
  };

  //------------------------------------------------
  // new bank card registration
  //------------------------------------------------
  renderNewCardScreen = () => {
    const { disableValidateButton, formInputs, errorInputs } = this.state;

    return (
      <React.Fragment>
        <Image style={Styles.paymentCardsContainer} source={Images.paymentCards} />
        <View>
          <View style={Styles.textInputContainer}>
            <View style={Styles.inputContainer}>
              <Item error={errorInputs.cardNumber}>
                <TextInputMask
                  ref={(ref) => {
                    this.cardNumber = ref;
                  }}
                  value={formInputs.cardNumber}
                  placeholderTextColor="#fff"
                  style={Styles.textInputField}
                  placeholder={appState.appTextData.get('registration.form.cardNumber').value}
                  keyboardType="decimal-pad"
                  returnKeyType="done"
                  type="credit-card"
                  options={{
                    format: '9999 9999 9999 9999',
                  }}
                  customTextInput={Input}
                  onChangeText={this.handleChangeTextInput.bind(this, 'cardNumber')}
                  onBlur={this.onBlurInput.bind(this, 'cardNumber')}
                />
              </Item>
            </View>
          </View>
          <View style={Styles.textInputContainer}>
            <View style={Styles.inputContainer}>
              <Item error={errorInputs.cardDateExp}>
                <TextInputMask
                  ref={(ref) => {
                    this.cardDateExp = ref;
                  }}
                  value={formInputs.cardDateExp}
                  placeholderTextColor="#fff"
                  style={Styles.textInputField}
                  placeholder={
                    appState.appTextData.get('registration.form.cardDateExpiration').value
                  }
                  keyboardType="decimal-pad"
                  returnKeyType="done"
                  type="datetime"
                  options={{
                    format: 'MM/YY',
                  }}
                  customTextInput={Input}
                  onChangeText={this.handleChangeTextInput.bind(this, 'cardDateExp')}
                  onBlur={this.onBlurInput.bind(this, 'cardDateExp')}
                />
              </Item>
            </View>
          </View>
          <View style={Styles.textInputContainer}>
            <View style={Styles.inputContainer}>
              <Item error={errorInputs.cardCVC}>
                <TextInputMask
                  value={formInputs.cardCVC}
                  placeholderTextColor="#fff"
                  style={Styles.textInputField}
                  placeholder={appState.appTextData.get('registration.form.cardCVC').value}
                  keyboardType="decimal-pad"
                  returnKeyType="done"
                  type="custom"
                  options={{
                    mask: '999',
                  }}
                  customTextInput={Input}
                  onChangeText={this.handleChangeTextInput.bind(this, 'cardCVC')}
                  onBlur={this.onBlurInput.bind(this, 'cardCVC')}
                />
              </Item>
            </View>
          </View>
        </View>
        <View style={Styles.linkTextContainer}>
          <Text style={Styles.infoTextLink} onPress={this.handleNavigateBack}>
            Retour
          </Text>
          <Text style={Styles.enterTextLink} onPress={() => this.initApp(false)}>
            Passez cette étape pour le moment
          </Text>
        </View>
        <View style={{ marginTop: 30 }}>
          <Button
            block
            style={disableValidateButton ? Styles.disabledButtonStyle : Styles.continueButtonStyle}
            disabled={disableValidateButton}
            onPress={() => this.initApp(false)}
          >
            <Text style={Styles.textButtonStyle}>Valider</Text>
          </Button>
        </View>
        <View style={Styles.mangoLogoContainer}>
          <Image source={Images.mangoPayLogo} />
        </View>
      </React.Fragment>
    );
  };

  //-------------------------------------------
  // render indicators
  //-------------------------------------------
  renderPageIndicators = (number, activePage) => {
    const indicators = [];

    for (let i = 0, l = number; i < l; i += 1) {
      const indicatorStyle = i === activePage - 1
        ? [Styles.indicatorActive, { backgroundColor: 'rgba(255, 255, 255, 0.8)' }]
        : [Styles.indicator, { backgroundColor: 'rgba(255, 255, 255, 0.2)' }];

      indicators.push(<View key={i} style={indicatorStyle} />);
    }

    return <View style={Styles.indicatorContainer}>{indicators}</View>;
  };

  render() {
    const { maxRegistrationStages } = appState.uiState;

    const { userRegistrationState } = appState.sessionParameters;

    const { showLoadingScreen, loadingScreenText } = this.state;

    return (
      <View style={Styles.container}>
        <StatusBar barStyle="light-content" />
        <ImageBackground style={Styles.backgroundImage} source={Images.backgroundImage}>
          <Image style={Styles.logoContainer} source={Images.logoTitle} />
          <Content>{this.renderNewCardScreen()}</Content>
          {this.renderPageIndicators(maxRegistrationStages, userRegistrationState.currentStage)}
          <LoadingScreen
            visible={showLoadingScreen}
            processingText={loadingScreenText}
            opacity={0.7}
          />
        </ImageBackground>
      </View>
    );
  }
}

export default NewPayCardScreen;
