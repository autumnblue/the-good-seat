import React, { Component } from 'react';
import { Container } from 'native-base';
import {
  AsyncStorage,
  Alert,
  Image,
  ImageBackground,
  StatusBar,
  PermissionsAndroid,
  Platform,
  View,
  TouchableOpacity,
} from 'react-native';
import Mapbox from '@mapbox/react-native-mapbox-gl';
import { observer, inject } from 'mobx-react';
/* eslint import/no-extraneous-dependencies: ["off", {"devDependencies": false}] */
import { lineString as makeLineString } from '@turf/helpers';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import { MAPBOXgetDirection } from '../../Services/BackEnd/MapBox';
import Map from './Map';
import { Icon as InternalIcon, IconButton } from '../../Components';
import Baner from '../Shared/Baner';
import StagesMenu from '../Shared/StagesMenu';
import AddressSelectedMenu from '../Shared/AddressSelectedMenu';
import HelperScreen from '../Shared/HelperScreen';
import DrawerMenu from '../Shared/DrawerMenu/DrawerMenu';

import { Images, Colors } from '../../Theme';

import Styles from './Styles/MainScreenStyles';
import { MAP_VIEW_STATE, ADDRESS_BAR_VIEW_STATE } from '../../MobX/Constants';

let appState = {};

/*
===================================================================
 main screen aplication
==================================================================
*/
@inject('appState')
@observer
class MainScreen extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      banerVisible: true,
      showHelper: false,
    };

    this.myRef = React.createRef();
    this.locationFetching = false;
  }

  //-------------------------------------------------
  // before render
  //-------------------------------------------------
  componentWillMount() {
    // if the user found himself on this screen then he successfully logged
    // in and got all the permissions
    // now we can add everything to the storage
    const { appToken } = appState.sessionParameters;
    const { currentUser } = appState.sessionParameters;
    AsyncStorage.setItem('CURRENT_USER', JSON.stringify(currentUser));
    AsyncStorage.setItem('AUTH_TOKEN', appToken);
  }

  //-------------------------------------------------
  //
  //-------------------------------------------------
  componentDidMount() {
    console.info('[MAINSCREEN] => component to componentDIDMount');
    if (appState.constants.MAPBOX_TOKEN) {
      Mapbox.setAccessToken(appState.constants.MAPBOX_TOKEN);
    } else {
      console.error('missing mapbox key');
      throw new Error('Missing mapbox key');
    }
  }

  //-------------------------------------------------
  //
  //-------------------------------------------------
  componentWillUnmount() {
    if (this.watchId) {
      navigator.geolocation.clearWatch(this.watchId);
    }
  }

  //-------------------------------------------------
  // init MapBox
  //-------------------------------------------------
  mapboxInit = () => {
    console.info('mapboxInit');
    Mapbox.setAccessToken(appState.constants.MAPBOX_TOKEN);
    Mapbox.setTelemetryEnabled(false);
  };

  //-------------------------------------------------
  // request to use geolocation
  //-------------------------------------------------
  requestLocationPermission = () => {
    console.info('[MAINSCREEN] requestLocationPermission');
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      .then((granted) => {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(
            (granted01) => {
              if (granted01 === PermissionsAndroid.RESULTS.GRANTED) {
                this.getCenterOfMap();
                this.startGeolocationWatching();
                this.mapboxInit();
                this.watchGPSState();
                appState.appStart();
              }
            },
          );
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  //---------------------------------------------------
  // get center of map
  //---------------------------------------------------
  getCenterOfMap = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        appState.sessionParameters.mapCenter = position.coords;
        appState.sessionParameters.setUserLocation(position.coords);
      },
      error => console.warn('gps API', error),
      Platform.OS === 'android'
        ? { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 }
        : { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  };

  //--------------------------------------------------
  // start geolocation watching service
  //--------------------------------------------------
  startGeolocationWatching = () => {
    const { placeName } = appState.driveStageState.startingAddressData;
    console.log('[MAINSCREEN]', 'startGeolocationWatching');
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        console.log('[MAINSCREEN] location update', position);
        try {
          if (placeName.length === 0) {
            appState.sessionParameters.setUserLocation(position.coords);
          }
        } catch (e) {
          console.warn('Object freeze ?', e);
        }
      },
      (error) => {
        console.log('GPS', error);
        // POSITION_UNAVAILABLE: 2,
        // PERMISSION_DENIED: 1
      },
      Platform.OS === 'android'
        ? { enableHighAccuracy: true, timeout: 5000, maximumAge: 10000 }
        : {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 1000,
          distanceFilter: 10,
        },
    );
  };

  //--------------------------------------------------
  // watch GPS state
  //--------------------------------------------------
  watchGPSState = () => {
    if (Platform.OS === 'android') {
      console.log('WATCH GPS');
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then((data) => {
          console.log('watch gps state', data);
          // The user has accepted to enable the location services
          // data can be :
          //  - "already-enabled" if the location services has been already enabled
          //  - "enabled" if user has clicked on OK button in the popup
        })
        .catch(() => {
          // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
          // codes :
          //  - ERR00 : The user has clicked on Cancel button in the popup
          //  - ERR01 : If the Settings change are unavailable
          //  - ERR02 : If the popup has failed to open
        });
    }
  };

  //--------------------------------------------------
  // map centred
  //--------------------------------------------------
  mapCentred = () => {
    const { latitude, longitude } = appState.sessionParameters.userLocation;

    /* const min = 0.00000001;

    const max = 0.00000005;

    const highlightedNumber = Math.random() * (max - min) + min;

    const latitudeFormated = parseFloat(latitude) + highlightedNumber;

    appState.sessionParameters.mapCenter = {
      longitude,
      latitude: latitudeFormated,
      timestamp: new Date().getTime(),
    };

    // switch map state
    appState.uiState.setMapViewState('NAVIGATION'); */
    this.watchGPSState();

    // center on map
    this.mapElement.flyTo([parseFloat(longitude), parseFloat(latitude)], 200).then(() => {
      // this.mapElement.zoomTo(4, 10).then(() => {});
    });
  };

  //--------------------------------------------------
  // close baner
  //--------------------------------------------------
  handleCloseBaner = () => {
    this.setState({ banerVisible: false });
    if (Platform.OS === 'android') {
      this.requestLocationPermission();
    } else {
      this.mapboxInit();
      appState.appStart();
      this.getCenterOfMap();
      this.startGeolocationWatching();
    }
  };

  //--------------------------------------------------
  // show main menu
  //--------------------------------------------------
  handleShowDrawer = () => {
    appState.uiState.showDrawerMenu();
  };

  //---------------------------------------------------
  // handle build ride route
  //---------------------------------------------------
  handleBuildRideRoute = async () => {
    try {
      const directionData = await MAPBOXgetDirection(
        appState.constants.MAPBOX_DIRECTION_API,
        [
          {
            latitude: appState.driveStageState.startingAddressData.latitude,
            longitude: appState.driveStageState.startingAddressData.longitude,
          },
          {
            latitude: appState.driveStageState.destinationAddressData.latitude,
            longitude: appState.driveStageState.destinationAddressData.longitude,
          },
        ],
        { profile: 'driving', geometry: 'geojson', accessToken: appState.constants.MAPBOX_TOKEN },
      );

      if (directionData) {
        appState.driveStageState.setDestinationRouteData(
          makeLineString(directionData.routes[0].geometry.coordinates),
        );
        return true;
      }
    } catch (error) {
      Alert.alert('getDirection', `MainScreen.js-${error.message}`);
      return false;
    }

    return false;
  };

  //---------------------------------------------------
  // handle build route
  //---------------------------------------------------
  handleBuildRoute = () => {
    console.log('[MAINSCREEN]', 'build Route started', appState.driveStageState);
    MAPBOXgetDirection(
      appState.constants.MAPBOX_DIRECTION_API,
      [
        {
          latitude: appState.driveStageState.startingAddressData.latitude,
          longitude: appState.driveStageState.startingAddressData.longitude,
        },
        {
          latitude: appState.driveStageState.destinationAddressData.latitude,
          longitude: appState.driveStageState.destinationAddressData.longitude,
        },
      ],
      { profile: 'driving', geometry: 'geojson', accessToken: appState.constants.MAPBOX_TOKEN },
    )
      .then((data) => {
        if (data) {
          appState.driveStageState.setDestinationRouteData(
            makeLineString(data.routes[0].geometry.coordinates),
          );
        }
      })
      .catch((error) => {
        Alert.alert('geocoding API', error.message);
      });
  };

  //---------------------------------------------------
  // change map view
  //---------------------------------------------------
  handleChangeMapView = () => {
    const { mapViewState, stagesMenuOpen } = appState.uiState;
    const { latitude, longitude } = appState.driveStageState.startingAddressData;
    const {
      latitude: destLatitude,
      longitude: destLongitude,
    } = appState.driveStageState.destinationAddressData;

    // close stages menu (need big screen)
    if (stagesMenuOpen) {
      appState.uiState.closeStagesMenu();
    }

    if (mapViewState === MAP_VIEW_STATE.TAXI_OFFERS) {
      console.log('state');
      // fit to screen route
      this.mapElement.fitBounds(
        [parseFloat(longitude), parseFloat(latitude)],
        [parseFloat(destLongitude), parseFloat(destLatitude)],
        [120, 100, 100, 20],
        200,
      );
      // .then(() => {
      this.handleBuildRideRoute().then((result) => {
        if (result) appState.uiState.setMapViewState('SHOW_DESTINATION_ROUTE');
      });

      //  });
    } else if (mapViewState === MAP_VIEW_STATE.SHOW_DESTINATION_ROUTE) {
      // fit to screen route
      /* this.mapElement.flyTo([parseFloat(longitude), parseFloat(latitude)], 200).then(() => {
        this.mapElement.zoomTo(zoomLevel, 10).then(() => {
          appState.uiState.setMapViewState('TAXI_OFFERS');
        });
      }); */
      this.mapElement.setCamera({
        centerCoordinate: [parseFloat(longitude), parseFloat(latitude)],
        zoom: 15,
        duration: 200,
      });
      // .then(() => {
      appState.uiState.setMapViewState('TAXI_OFFERS');
      //  });
    }
  };

  //--------------------------------------------------
  // show helper
  //--------------------------------------------------
  handleShowHelper = () => {
    this.setState({ showHelper: true });
  };

  //--------------------------------------------------
  // close helper
  //--------------------------------------------------
  handleCloseHelper = () => {
    this.setState({ showHelper: false });
  };

  //--------------------------------------------------
  // while update geolocation
  //--------------------------------------------------
  handleUpdateGeolocation = (e) => {
    if (!e || !e.coords) {
      return;
    }
    appState.sessionParameters.setUserLocation({
      longitude: e.coords.longitude,
      latitude: e.coords.latitude,
    });
  };

  //-----------------------------------------------------
  // render travel button (paper plane)
  //-----------------------------------------------------
  renderTravelButton = () => {
    const { mapViewState } = appState.uiState;

    if (
      (mapViewState === MAP_VIEW_STATE.TAXI_OFFERS
        || mapViewState === MAP_VIEW_STATE.SHOW_DESTINATION_ROUTE)
      && appState.uiState.showTripButton
    ) {
      return (
        <IconButton
          buttonStyle={[
            Styles.iconButton,
            {
              backgroundColor:
                mapViewState === MAP_VIEW_STATE.SHOW_DESTINATION_ROUTE
                  ? Colors.white
                  : Colors.primary,
            },
          ]}
          color={
            mapViewState === MAP_VIEW_STATE.SHOW_DESTINATION_ROUTE ? Colors.primary : Colors.white
          }
          size={29}
          name="paper-plane"
          onPress={this.handleChangeMapView}
        />
      );
    }
    return null;
  };

  //-----------------------------------------------------
  // draw map
  //-----------------------------------------------------
  renderMap = () => {
    const { addressBarViewState } = appState.uiState;

    if (appState.appIsReady) {
      let drawerButtonRenderData = <View />;

      // button visible only drawer menu hide
      if (
        !appState.uiState.drawerMenuVisible
        && addressBarViewState !== ADDRESS_BAR_VIEW_STATE.OPENED
      ) {
        drawerButtonRenderData = (
          <InternalIcon
            containerStyle={Styles.drawerMenuButton}
            color={Colors.white}
            size={25}
            name="N"
            raised
            reverse
            onPress={this.handleShowDrawer}
          >
            <Image source={Images.drawerMenu} />
          </InternalIcon>
        );
      }

      return (
        <React.Fragment>
          <Map
            mapRef={(el) => {
              this.mapElement = el;
            }}
          />
          <View style={Styles.mapElementsContainer} pointerEvents="box-none">
            <Image style={Styles.gradientImage} source={Images.backGradient} />
            <AddressSelectedMenu />
            {drawerButtonRenderData}
            <View style={{ flex: 1, justifyContent: 'flex-end' }} pointerEvents="box-none">
              <View style={Styles.footerContainer}>
                <InternalIcon
                  containerStyle={Styles.helpButton}
                  color={Colors.white}
                  size={25}
                  name="N"
                  raised
                  reverse
                  onPress={this.handleShowHelper}
                >
                  <Image source={Images.question} />
                </InternalIcon>

                {this.renderTravelButton()}

                <TouchableOpacity onPress={this.mapCentred}>
                  <InternalIcon
                    containerStyle={Styles.currentUserPositionButton}
                    buttonRadius={12}
                    color={Colors.white}
                    size={25}
                    name="N"
                    raised
                    reverse
                  >
                    <Image source={Images.gpsPosition} />
                  </InternalIcon>
                </TouchableOpacity>
              </View>
              <StagesMenu />
            </View>
          </View>
        </React.Fragment>
      );
    }
    return <ImageBackground style={Styles.backgroundImage} source={Images.backgroundMap} />;
  };

  render() {
    const { banerVisible, showHelper } = this.state;
    return (
      <React.Fragment>
        <DrawerMenu>
          <Container style={Styles.container}>
            <StatusBar barStyle="dark-content" translucent backgroundColor="#fff" />
            {this.renderMap()}
            <Baner visible={banerVisible} onClose={this.handleCloseBaner} />
          </Container>
        </DrawerMenu>
        <HelperScreen visible={showHelper} onClose={this.handleCloseHelper} />
      </React.Fragment>
    );
  }
}

export default MainScreen;
