import React, { Component } from 'react';
import {
  Button, Item, Input, Form,
} from 'native-base';
import {
  Image, View, TouchableOpacity, Text,
} from 'react-native';

import { Modal } from '../../Components';

import { Images } from '../../Theme';

import Styles from './Styles/MyAddressInputStyles';

/*
-------------------------------------------------
 modal view state
-------------------------------------------------
*/
const MODAL_STATE = {
  NONE: 0,
  QUESTION: 1,
  CONFIRMATION: 2,
};

/*
=============================================================
 class for text input in drawer my addresses
=============================================================
*/
class MyAddressesInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalState: MODAL_STATE.NONE,
    };
  }

  //----------------------------------------------
  // change modal state
  //----------------------------------------------
  changeModalState = (newState) => {
    this.setState({ modalState: MODAL_STATE[newState] });
  };

  //-----------------------------------------------------
  // on editable button press
  //-----------------------------------------------------
  handleOnPressEdit = () => {
    const { onPressEdit } = this.props;
    if (onPressEdit) {
      onPressEdit();
    }
  };

  //-----------------------------------------------------
  // get user location
  //-----------------------------------------------------
  handleLocation = () => {};

  //-----------------------------------------------------
  // handle user text input
  //-----------------------------------------------------
  handleOnTextChange = (text) => {
    const { onSearchChange } = this.props;
    if (onSearchChange) {
      onSearchChange(text);
    }
  };

  //------------------------------------------------
  // modal dialog press OK (post delete)
  //------------------------------------------------
  handleOnPostClearValue = () => {
    const { onPostClear } = this.props;

    if (onPostClear) {
      onPostClear();
    }
  };

  //-----------------------------------------------
  // render modal (dialog) windows
  //-----------------------------------------------
  renderModalWindows = () => {
    const { modalState } = this.state;
    const { value } = this.props;

    if (modalState === MODAL_STATE.QUESTION) {
      return (
        <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
          <View style={Styles.modalContainer}>
            <View style={Styles.modalTextContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                Etes vous sûr(e) de vouloir supprimer votre adresse
                {'"'}
                {value}
                {'"'}
?
              </Text>
            </View>
            <View style={[{ justifyContent: 'space-between' }, Styles.modalFooter]}>
              <Button style={Styles.modalButton} bordered onPress={this.changeModalState.bind(this, 'CONFIRMATION')}>
                <Text style={Styles.modalButtonText}>CONFIRMER</Text>
              </Button>
              <Button style={Styles.modalButton} bordered onPress={this.changeModalState.bind(this, 'NONE')}>
                <Text style={Styles.modalButtonText}>ANNULER</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }
    if (modalState === MODAL_STATE.CONFIRMATION) {
      return (
        <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
          <View style={Styles.modalContainer}>
            <View style={Styles.modalTextContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                {'"'}
                {value}
                {'"'}
                {' '}
? a bien été supprimée
              </Text>
            </View>
            <View style={[{ justifyContent: 'center' }, Styles.modalFooter]}>
              <Button style={Styles.modalButton} bordered onPress={this.handleOnPostClearValue}>
                <Text style={Styles.modalButtonText}>OK</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }
    return null;
  };

  //------------------------------------------------------
  // callback render results items
  //------------------------------------------------------
  renderResultsItems = () => {
    const { resultsSearch, renderResultItem } = this.props;
    resultsSearch.map(item => renderResultItem(item));
  };

  //------------------------------------------------------
  // render initial state
  //------------------------------------------------------
  renderInitialState = () => {
    const { imageLabel, containerStyle, labelText } = this.props;

    return (
      <TouchableOpacity onPress={this.handleOnPressEdit}>
        <View style={[Styles.container, containerStyle]}>
          <View style={Styles.imageContainer}>{imageLabel}</View>
          <View style={Styles.divider} />
          <Text style={Styles.mainText}>{labelText}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  //------------------------------------------------------
  // render inser address state
  //------------------------------------------------------
  renderInsertAddressState = () => {
    const { imageLabel, containerStyle } = this.props;

    return (
      <View style={[Styles.container, containerStyle]}>
        <View style={Styles.imageContainer}>{imageLabel}</View>
        <View style={Styles.divider} />
        <Form style={{ flex: 1 }}>
          <View style={Styles.textInputContainer}>
            <Item style={{ height: 20, width: 210, borderBottomColor: 'transparent' }}>
              <Input
                placeholderTextColor="#78849E"
                style={Styles.textInputField}
                placeholder="Entrez votre adresse de domicile"
                onChangeText={this.handleOnTextChange}
              />
            </Item>
            <TouchableOpacity onPress={this.handleLocation}>
              <Image style={Styles.pickGeoPosition} source={Images.pickGeoPosition} />
            </TouchableOpacity>
          </View>
          <View style={Styles.searchResultContainer}>{this.renderResultsItems()}</View>
        </Form>
      </View>
    );
  };

  //------------------------------------------------------
  // render location lock state
  //------------------------------------------------------
  renderLocationLockState = () => {
    const { imageLabel, containerStyle, value } = this.props;

    return (
      <View style={[Styles.container, containerStyle]}>
        <View style={Styles.imageContainer}>{imageLabel}</View>
        <View style={Styles.divider} />
        <Text numberOfLines={2} style={[Styles.mainText, Styles.locationContainer]}>
          {value}
        </Text>
        <TouchableOpacity onPress={this.handleOnPressEdit}>
          <Image style={Styles.actionImage} source={Images.inputEdit} />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.changeModalState.bind(this, 'QUESTION')}>
          <Image style={Styles.actionImage} source={Images.modalClose} />
        </TouchableOpacity>
      </View>
    );
  };

  //------------------------------------------------------
  // render current component state
  //------------------------------------------------------
  renderCurrentComponentState = () => {
    const { editable, value } = this.props;

    if (editable) {
      return this.renderInsertAddressState();
    }

    if (value.length) {
      return this.renderLocationLockState();
    }
    return this.renderInitialState();
  };

  render() {
    return (
      <React.Fragment>
        {this.renderCurrentComponentState()}
        {this.renderModalWindows()}
      </React.Fragment>
    );
  }
}

MyAddressesInput.defaultProps = {
  value: '',
  editable: false,
};

export default MyAddressesInput;
