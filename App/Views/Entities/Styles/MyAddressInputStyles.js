import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Colors } from '../../../Theme';

export default StyleSheet.create({
  container: {
    minHeight: 24,
    flexDirection: 'row',
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    flexWrap: 'wrap',
  },
  itemMenuContainer: {
    height: 32,
    paddingLeft: 24,
  },
  divider: {
    backgroundColor: '#F4F4F6',
    width: 1,
    height: 24,
    marginHorizontal: 16,
  },
  imageContainer: {
    width: 22,
    height: 22,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInputContainer: {
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 39,
  },
  textInputField: {
    color: '#78849E',
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
  },
  locationContainer: {
    flex: 1,
    flexWrap: 'wrap',
  },
  actionImage: {
    width: 15,
    height: 15,
    marginHorizontal: 7,
  },
  modalContainer: {
    width: 320,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  modalTextContainer: {
    marginHorizontal: 27,
    marginTop: 29,
    alignItems: 'center',
  },
  modalFooter: {
    marginBottom: 37,
    marginTop: 54,
    flexDirection: 'row',
    marginHorizontal: 18.5,
  },
  modalButton: {
    minWidth: 125,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 10,
    color: Colors.onPrimaryText,
  },
  searchResultContainer: {
    paddingHorizontal: 8,
    flexDirection: 'column',
    position: 'absolute',
    backgroundColor: '#fff',
    marginTop: 25,
    marginRight: 12,
    borderRadius: 12,
    zIndex: 999,
    left: 0,
    top: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
});
