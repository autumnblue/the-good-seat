import { StyleSheet } from 'react-native';
import { Metrics } from '../../../Theme';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    width: Metrics.widthPercentToDP('100%'), // Metrics.screenWidth,
    height: Metrics.screenHeight,
    paddingVertical: Metrics.heightPercentToDP('5.7%'),
    paddingHorizontal: Metrics.widthPercentToDP('9.9%'),
  },
  banerCloseContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  banerContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: Metrics.heightPercentToDP('4.1%'),
    flex: 1,
  },
  backgroundImage: {
    resizeMode: 'contain',
    width: '100%',
    height: '100%',
  },
});
