import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Colors } from '../../../Theme';

export default StyleSheet.create({
  container: {
    borderRadius: 10,
    width: '85%', // 320,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  mainContainer: {
    flexDirection: 'column',
  },
  imageCloseButton: {
    width: 22,
    height: 22,
    marginRight: 2,
  },
  closeButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 14,
  },
  helperMainContainer: {
    height: 203,
    flexDirection: 'column',
    alignItems: 'center',
  },
  helperMainHeader: {
    flexDirection: 'row',
  },
  mainText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 16,
    color: '#78849E',
  },
  secondaryText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: '#78849E',
  },
  headerFAQText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 13,
    color: '#78849E',
    textDecorationLine: 'underline',
    lineHeight: 17.5,
  },
  blockFAQText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 13,
    color: '#78849E',
    lineHeight: 17,
    textAlign: 'left',
  },
  headerTermsText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 8,
    color: '#78849E',
    textDecorationLine: 'underline',
    lineHeight: 10,
  },
  blockTermsText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 8,
    color: '#78849E',
    lineHeight: 9,
    textAlign: 'left',
  },
  itemMenuContainer: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  headerLeftBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  helperContactUsContainer: {
    height: 480,
    flexDirection: 'column',
  },
  helperHeader: {
    alignItems: 'center',
  },
  formContainer: {
    flex: 1,
    marginTop: 41,
    marginHorizontal: 15,
    height: 315,
  },
  formProblemTypeContainer: {
    flex: 1,
    paddingHorizontal: 15,
  },
  textInputContainer: {
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.1)',
  },
  textInputContainerError: {
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: Colors.error,
  },
  textInputContainerDisabled: {
    backgroundColor: Colors.disable,
    borderRadius: 20,
  },
  textInputField: {
    color: '#78849E',
    textAlign: 'center',
    ...addFontFamily('SegoeUI'),
    fontSize: 14,
  },
  helperFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: '#009FE3',
  },
  buttonTextDisable: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: Colors.disable,
  },
  textareaEditInput: {
    color: '#78849E',
    textAlign: 'center',
    ...addFontFamily('SegoeUI'),
    fontSize: 14,
    height: 135,
  },
  textareaProblemEditInput: {
    color: '#78849E',
    ...addFontFamily('SegoeUI'),
    fontSize: 14,
    height: 100,
    paddingLeft: 20,
  },
  faqScrollContainer: {
    justifyContent: 'flex-start',
  },
  faqTextContainer: {
    paddingLeft: 11,
    paddingRight: 30,
  },
  termsScrollContainer: {
    justifyContent: 'flex-start',
    padding: 15,
  },
  termsTextContainer: {
    paddingLeft: 11,
    paddingRight: 21,
    paddingTop: 12,
    paddingBottom: 20,
    backgroundColor: '#F0F2F4',
    borderRadius: 33,
    marginHorizontal: 23,
  },
  pickerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
  },
  problemTypeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 27,
  },
  textProblemType: {
    paddingHorizontal: 25,
    flex: 1,
    flexWrap: 'wrap',
  },
});
