import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  textInputContainer: {
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 10,
    height: 34,
  },
  textInputField: {
    color: '#78849E',
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
  },
  searchResultItem: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderBottomColor: 'rgba(120,132,158, 0.2)',
    borderBottomWidth: 1,
  },
  searchResultText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    alignItems: 'center',
  },
  searchResultContainer: {
    marginTop: 24,
    paddingHorizontal: 20,
  },
  resultInfoText: {
    color: '#78849E',
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
  },
});
