import React, { Component } from 'react';
import { Image, View } from 'react-native';
import { Text } from 'native-base';
import { Icon } from '../../../Components';

import Styles from './Styles/DrawerMenuHeaderStyles';
import { Colors, Images } from '../../../Theme';

class DrawerMenuHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  //-----------------------------------------------------
  // hanlde menu button press
  //-----------------------------------------------------
  handleMenuButtonPress = () => {
    const { onPressMenuButton } = this.props;
    if (onPressMenuButton) onPressMenuButton();
  };

  render() {
    const { accountName, opened } = this.props;

    return (
      <View style={Styles.container}>
        <View style={[{ width: 183 }, Styles.itemMenuContainer, Styles.itemMenuContainerUnderline]}>
          <Text style={Styles.mainText}>{accountName}</Text>
        </View>

        <View style={Styles.drawerMenuButton}>
          <Icon
            containerStyle={Styles.drawerMenuButton}
            color={Colors.white}
            size={25}
            name="N"
            raised
            reverse
            onPress={this.handleMenuButtonPress}
          >
            <Image source={opened ? Images.drawerMenuBack : Images.drawerMenu} />
          </Icon>
        </View>
      </View>
    );
  }
}

DrawerMenuHeader.defaultProps = {
  opened: false,
};

export default DrawerMenuHeader;
