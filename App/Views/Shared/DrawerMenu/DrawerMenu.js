import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { Button, Drawer, Text } from 'native-base';
import { AsyncStorage, View, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import { observer, inject } from 'mobx-react';
import { Modal } from '../../../Components';
import DrawerMenuHeader from './DrawerMenuHeader';
import DrawerMenuMyProfile from './DrawerMenuMyProfile';
import DrawerMenuMyPaymentsMetods from './DrawerMenuMyPaymentsMetods';
import DrawerMenuMyAddresses from './DrawerMenuMyAddresses';
import DrawerMenuMyTrips from './DrawerMenuMyTrips';
import DrawerMenuMyVouchers from './DrawerMenuMyVouchers';

import Styles from './Styles/DrawerMenuStyles';
import { DRAWER_MENU_SECTIONS } from '../../../MobX/Constants';

let appState = {};
/*
===================================================================
   Меню
==================================================================
*/
@inject('appState')
@observer
class DrawerMenu extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      modalWindowsVisible: false,
    };
  }

  //----------------------------------------------
  // hide menu
  //----------------------------------------------
  handleHideMenu = () => {
    appState.uiState.hideDrawerMenu();
  };

  //----------------------------------------------
  // show logout modal
  //----------------------------------------------
  handleShowLogoutWindow = () => {
    this.setState({ modalWindowsVisible: true });
  };

  //----------------------------------------------
  // hide logout modal
  //----------------------------------------------
  handleHideLogoutWindow = () => {
    this.setState({ modalWindowsVisible: false });
  };

  //----------------------------------------------
  // navigate to logout
  //----------------------------------------------
  handleLogon = () => {
    const { navigation } = this.props;

    this.setState({ modalWindowsVisible: false });

    appState.sessionParameters.logout();

    // logout out firebase
    firebase.auth().signOut();

    // clear storage
    AsyncStorage.clear();

    // change state
    appState.uiState.reset();

    // navigate to screen
    navigation.navigate('logon');
  };

  //----------------------------------------------
  // handle menu navigation
  //----------------------------------------------
  handleMenuNavigate = (menuSection) => {
    appState.uiState.navigateDrawerSection(menuSection);
  };

  //----------------------------------------------
  // render main screen
  //----------------------------------------------
  renderMainState = () => {
    const { modalWindowsVisible } = this.state;

    const { firstname, lastname } = appState.sessionParameters.currentUser;

    const accountName = `${lastname} ${firstname}`;

    return (
      <View style={Styles.container}>
        <DrawerMenuHeader accountName={accountName} onPressMenuButton={this.handleHideMenu} />

        <TouchableOpacity onPress={this.handleMenuNavigate.bind(this, 'MY_PROFILE')}>
          <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
            <Text style={Styles.mainText}>Mon profil</Text>
            <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleMenuNavigate.bind(this, 'MY_PAYMENT_METODS')}>
          <View style={[{ marginTop: 10 }, Styles.itemMenuContainer]}>
            <Text style={Styles.mainText}>Mes moyens de paiement</Text>
            <View style={[{ width: 115 }, Styles.itemMenuDivider]} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleMenuNavigate.bind(this, 'MY_ADRESSES')}>
          <View style={[{ marginTop: 10 }, Styles.itemMenuContainer]}>
            <Text style={Styles.mainText}>Mes adresses</Text>
            <View style={[{ width: 162.5 }, Styles.itemMenuDivider]} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleMenuNavigate.bind(this, 'MY_TRIPS')}>
          <View style={[{ marginTop: 10 }, Styles.itemMenuContainer]}>
            <Text style={Styles.mainText}>Mes courses</Text>
            <View style={[{ width: 200.5 }, Styles.itemMenuDivider]} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleMenuNavigate.bind(this, 'MY_VOUCHERS')}>
          <View style={[{ marginTop: 10 }, Styles.itemMenuContainer]}>
            <Text style={Styles.mainText}>Mes bons de réduction</Text>
          </View>
        </TouchableOpacity>
        <View style={Styles.drawerMenuFooterContainer}>
          <TouchableOpacity onPress={this.handleShowLogoutWindow}>
            <View style={[{ marginTop: 10 }, Styles.itemMenuContainer]}>
              <View style={[{ width: 183, marginBottom: 8.5 }, Styles.itemMenuDivider]} />
              <Text style={Styles.mainText}>Déconnexion</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Modal
          visible={modalWindowsVisible}
          backdropColor="rgba(255, 255, 255, 0.8)"
          onRequestClose={() => {}}
        >
          <View style={Styles.modalContainer}>
            <View style={Styles.modalTextContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                Etes-vous sûr de vouloir déconnecter votre compte ?
              </Text>
            </View>
            <View style={Styles.modalFooter}>
              <Button style={Styles.modalButton} bordered onPress={this.handleLogon}>
                <Text style={Styles.modalButtonText}>CONFIRMER</Text>
              </Button>
              <Button style={Styles.modalButton} bordered onPress={this.handleHideLogoutWindow}>
                <Text style={Styles.modalButtonText}>ANNULER</Text>
              </Button>
            </View>
          </View>
        </Modal>
      </View>
    );
  };

  //----------------------------------------------------
  // render screen based on state
  //----------------------------------------------------
  renderScreen = () => {
    switch (appState.uiState.drawerMenuSection) {
      case DRAWER_MENU_SECTIONS.MAIN:
        return this.renderMainState();
      case DRAWER_MENU_SECTIONS.MY_PROFILE:
        return <DrawerMenuMyProfile />;
      case DRAWER_MENU_SECTIONS.MY_ADRESSES:
        return <DrawerMenuMyAddresses />;
      case DRAWER_MENU_SECTIONS.MY_TRIPS:
        return <DrawerMenuMyTrips />;
      case DRAWER_MENU_SECTIONS.MY_PAYMENT_METODS:
        return <DrawerMenuMyPaymentsMetods />;
      case DRAWER_MENU_SECTIONS.MY_VOUCHERS:
        return <DrawerMenuMyVouchers />;
      default:
        return this.renderMainState();
    }
  };

  render() {
    const { children } = this.props;

    return (
      <Drawer
        captureGestures={false}
        disabled
        open={appState.uiState.drawerMenuVisible}
        openDrawerOffset={
          appState.uiState.drawerMenuSection === DRAWER_MENU_SECTIONS.MAIN ? 0.32 : 0
        }
        tapToClose={false}
        styles={Styles}
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        content={this.renderScreen()}
      >
        {children}
      </Drawer>
    );
  }
}

export default withNavigation(DrawerMenu);
