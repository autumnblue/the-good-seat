import React, { Component } from 'react';
import { Text } from 'native-base';
import { View, TouchableOpacity, Image } from 'react-native';
import { observer, inject } from 'mobx-react';
import { ProgressIndicator } from '../../../Components';

import Styles from './Styles/DrawerMenuUserAddressStyles';
import { Images } from '../../../Theme';

@inject('appState')
@observer
class DrawerMenuUserAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  //----------------------------------------------------
  // on press edit
  //----------------------------------------------------
  onPressEdit = () => {};

  //----------------------------------------------------
  // delete address
  //----------------------------------------------------
  deleteAddress = () => {};

  //-----------------------------------------------------
  // render button state
  //-----------------------------------------------------
  renderButtonState = () => {
    const { onPress, labelPicture, text } = this.props;

    return (
      <TouchableOpacity
        onPress={() => {
          onPress();
        }}
      >
        <View style={Styles.menuItemContainer}>
          <View style={Styles.menuItemPictureContainer}>{labelPicture}</View>
          <View style={Styles.menuItemDivider} />
          <Text numberOfLines={2} style={Styles.menuText}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  //-----------------------------------------------------
  // render loading state
  //-----------------------------------------------------
  renderLoadingState = () => (
    <View style={{ paddingRight: 20, marginTop: 30 }}>
      <ProgressIndicator color="#D3D9E5" />
    </View>
  );

  //-----------------------------------------------------
  // render control state
  //-----------------------------------------------------
  renderControlState = () => {
    const {
      labelPicture, addressData, onEdit, onDelete,
    } = this.props;

    return (
      <View style={Styles.container}>
        <View style={Styles.menuItemPictureContainer}>{labelPicture}</View>
        <View style={Styles.menuItemDivider} />
        <Text
          numberOfLines={2}
          style={[Styles.menuText, Styles.locationContainer]}
          onPress={() => {
            onEdit(addressData._id);
          }}
        >
          {addressData.title}
        </Text>
        <TouchableOpacity
          onPress={() => {
            onEdit(addressData._id);
          }}
        >
          <Image style={Styles.actionImage} source={Images.inputEdit} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            onDelete(addressData._id);
          }}
        >
          <Image style={Styles.actionImage} source={Images.modalClose} />
        </TouchableOpacity>
      </View>
    );
  };

  //-----------------------------------------------------
  // render current state
  //-----------------------------------------------------
  renderCurrentState = () => {
    const { addressData } = this.props;

    if (addressData) {
      if (addressData.title) {
        return this.renderControlState();
      }
    }

    return this.renderButtonState();
  };

  render() {
    return this.renderCurrentState();
  }
}

export default DrawerMenuUserAddress;
