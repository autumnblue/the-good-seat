import React, { Component } from 'react';
import { Text } from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import { observer, inject } from 'mobx-react';
import { withNavigation } from 'react-navigation';
import DrawerMenuHeader from './DrawerMenuHeader';
import DrawerMenuUserAddress from './DrawerMenuUserAddress';
import { Images } from '../../../Theme';
import { BubblesSpinner } from '../../../Components';

import Styles from './Styles/DrawerMenuMyAddressesStyles';
import {
  getUserAddresses,
  addUserAddress,
  deleteUserAddress,
  updateUserAddress,
} from '../../../Services/BackEnd/TheGoodSeat';
import { convertFeatureDataToAddressData } from '../../../Services/BackEnd/MapBox';

let appState = {};

const SCREEN_RENDER_STATE = {
  LOADING: 0,
  DATA: 1,
};

@inject('appState')
@observer
class DrawerMenuMyAddresses extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      userAddresses: [],
      screenRenderState: SCREEN_RENDER_STATE.LOADING,
    };
  }

  componentDidMount() {
    this.updateAddressesList();
  }

  //--------------------------------------------------
  // update addresses list
  //--------------------------------------------------
  updateAddressesList = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    getUserAddresses(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
    ).then((data) => {
      console.log('addresses', data);
      this.setState(
        {
          userAddresses: data.body,
        },
        () => this.setState({ screenRenderState: SCREEN_RENDER_STATE.DATA }),
      );
    });
  };

  //---------------------------------------------------
  // (drawer menu back to main section) вернуться назад
  //---------------------------------------------------
  handlePressBack = () => {
    appState.uiState.navigateDrawerSection('MAIN');
  };

  //----------------------------------------------------
  // address press
  //----------------------------------------------------
  openFindAddressScreen = (addressType) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    let screenTitle = 'Enregistrer un domicile';

    if (addressType === 'HOME') {
      screenTitle = 'Domicile';
    } else if (addressType === 'WORK') {
      screenTitle = 'Lieu de travail';
    } else {
      screenTitle = 'Lieu favori';
    }

    const { navigation } = this.props;
    navigation.navigate('findAddressScreen', {
      title: screenTitle,
      onSelect: (selectedAddress) => {
        const addressData = convertFeatureDataToAddressData(selectedAddress);

        if (addressType === 'HOME') {
          addressData.type = addressType;
        } else if (addressType === 'WORK') {
          addressData.type = addressType;
        } else {
          addressData.type = null;
        }

        // show loading
        this.setState({
          screenRenderState: SCREEN_RENDER_STATE.LOADING,
        });

        // update on server
        addUserAddress(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          _id,
          addressData,
        ).then(() => {
          // update list
          this.updateAddressesList();
        });
      },
    });
  };

  //-----------------------------------------------------
  // edit favorite address
  //-----------------------------------------------------
  editFavoriteAddress = (addressId) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const screenTitle = 'Lieu favori';

    const { navigation } = this.props;
    navigation.navigate('findAddressScreen', {
      title: screenTitle,
      onSelect: (selectedAddress) => {
        const addressData = convertFeatureDataToAddressData(selectedAddress);
        addressData.type = null;

        // show loading
        this.setState({
          screenRenderState: SCREEN_RENDER_STATE.LOADING,
        });

        // update on server
        updateUserAddress(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          _id,
          addressId,
          addressData,
        ).then(() => {
          // update list
          this.updateAddressesList();
        });
      },
    });
  };

  //-----------------------------------------------------
  // edit home address
  //-----------------------------------------------------
  editHomeAddress = (addressId) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const screenTitle = 'Domicile';

    const { navigation } = this.props;
    navigation.navigate('findAddressScreen', {
      title: screenTitle,
      onSelect: (selectedAddress) => {
        const addressData = convertFeatureDataToAddressData(selectedAddress);
        addressData.type = 'HOME';

        // show loading
        this.setState({
          screenRenderState: SCREEN_RENDER_STATE.LOADING,
        });

        // update on server
        updateUserAddress(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          _id,
          addressId,
          addressData,
        ).then(() => {
          // update list
          this.updateAddressesList();
        });
      },
    });
  };

  //-----------------------------------------------------
  // edit work address
  //-----------------------------------------------------
  editWorkAddress = (addressId) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const screenTitle = 'Lieu de travail';

    const { navigation } = this.props;
    navigation.navigate('findAddressScreen', {
      title: screenTitle,
      onSelect: (selectedAddress) => {
        const addressData = convertFeatureDataToAddressData(selectedAddress);
        addressData.type = 'WORK';

        // show loading
        this.setState({
          screenRenderState: SCREEN_RENDER_STATE.LOADING,
        });

        // update on server
        updateUserAddress(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          _id,
          addressId,
          addressData,
        ).then(() => {
          // update list
          this.updateAddressesList();
        });
      },
    });
  };

  //----------------------------------------------------
  // render loading state
  //----------------------------------------------------
  renderLoadingState = () => (
    <View
      style={{
        flex: 0.6,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 40,
      }}
    >
      <Text style={Styles.secondText}>
        {appState.appTextData.get('drawer.menu.useraddresses.loading').value}
      </Text>
      <View style={{ height: 20 }} />
      <BubblesSpinner size={40} />
    </View>
  );

  //----------------------------------------------------
  // delete address
  //----------------------------------------------------
  deleteAddress = (addressId) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    // show loading
    this.setState({
      screenRenderState: SCREEN_RENDER_STATE.LOADING,
    });

    deleteUserAddress(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      addressId,
    ).then(() => {
      this.updateAddressesList();
    });
  };

  //----------------------------------------------------
  // render home address
  //----------------------------------------------------
  renderUserHomeAddress = () => {
    const { userAddresses } = this.state;

    let userAddressRenderData = (
      <DrawerMenuUserAddress
        text={appState.appTextData.get('drawer.menu.useraddress.home').value}
        labelPicture={<Image source={Images.home} />}
        onPress={this.openFindAddressScreen.bind(this, 'HOME')}
      />
    );

    userAddresses.forEach((addressData) => {
      if (addressData.type === 'HOME') {
        userAddressRenderData = (
          <DrawerMenuUserAddress
            text={appState.appTextData.get('drawer.menu.useraddress.home').value}
            addressData={addressData}
            labelPicture={<Image source={Images.home} />}
            onPress={this.openFindAddressScreen.bind(this, 'HOME')}
            onEdit={this.editHomeAddress}
            onDelete={this.deleteAddress}
          />
        );
      }
    });

    return userAddressRenderData;
  };

  //----------------------------------------------------
  // render work address
  //----------------------------------------------------
  renderUserWorkAddress = () => {
    const { userAddresses } = this.state;

    let userAddressRenderData = (
      <DrawerMenuUserAddress
        text={appState.appTextData.get('drawer.menu.useraddress.work').value}
        labelPicture={<Image source={Images.briefcase} />}
        onPress={this.openFindAddressScreen.bind(this, 'WORK')}
      />
    );

    userAddresses.forEach((addressData) => {
      if (addressData.type === 'WORK') {
        userAddressRenderData = (
          <DrawerMenuUserAddress
            text={appState.appTextData.get('drawer.menu.useraddress.work').value}
            addressData={addressData}
            labelPicture={<Image source={Images.briefcase} />}
            onPress={this.openFindAddressScreen.bind(this, 'WORK')}
            onEdit={this.editWorkAddress}
            onDelete={this.deleteAddress}
          />
        );
      }
    });

    return userAddressRenderData;
  };

  //----------------------------------------------------
  // render addresses list
  //----------------------------------------------------
  renderUserFavoritesAddresses = () => {
    const { userAddresses } = this.state;

    let userAddressRenderData = null;

    userAddressRenderData = userAddresses.map((addressData) => {
      if (addressData.type === null) {
        return (
          <React.Fragment key={addressData._id}>
            <View style={{ height: 30 }} />
            <DrawerMenuUserAddress
              text={appState.appTextData.get('drawer.menu.useraddress.favorite').value}
              addressData={addressData}
              labelPicture={<Image source={Images.favoriteOutline} />}
              onPress={this.openFindAddressScreen.bind(this, 'FAVORITE')}
              onEdit={this.editFavoriteAddress}
              onDelete={this.deleteAddress}
            />
          </React.Fragment>
        );
      }

      return null;
    });

    return userAddressRenderData;
  };

  //-----------------------------------------------
  // render data state
  //-----------------------------------------------
  renderDataState = () => (
    <View style={[Styles.formContainer, { flex: 1 }]}>
      <View style={{ flexDirection: 'column' }}>
        {this.renderUserHomeAddress()}
        <View style={{ height: 30 }} />
        {this.renderUserWorkAddress()}
        <View style={{ height: 30 }} />
        <DrawerMenuUserAddress
          text={appState.appTextData.get('drawer.menu.useraddress.favorite').value}
          labelPicture={<Image source={Images.favoriteOutline} />}
          onPress={this.openFindAddressScreen.bind(this, 'FAVORITE')}
        />
        {this.renderUserFavoritesAddresses()}
      </View>
    </View>
  );

  //----------------------------------------------
  // render cuurrent state
  //----------------------------------------------
  renderCurrentState = () => {
    const { screenRenderState } = this.state;
    switch (screenRenderState) {
      case SCREEN_RENDER_STATE.LOADING:
        return this.renderLoadingState();
      case SCREEN_RENDER_STATE.DATA:
        return this.renderDataState();
      default:
        return this.renderLoadingState();
    }
  };

  render() {
    const { firstname, lastname } = appState.sessionParameters.currentUser;

    const accountName = `${lastname} ${firstname}`;

    return (
      <View style={Styles.container}>
        <View style={{ marginLeft: 24 }}>
          <DrawerMenuHeader
            opened
            accountName={accountName}
            onPressMenuButton={this.handlePressBack}
          />
        </View>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mes adresses</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <ScrollView contentContainerStyle={{ flexGrow: 1, paddingBottom: 50 }}>
          {this.renderCurrentState()}
        </ScrollView>
      </View>
    );
  }
}

export default withNavigation(DrawerMenuMyAddresses);
