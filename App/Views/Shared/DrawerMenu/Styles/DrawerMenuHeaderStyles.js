import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    // flex: 1,
  },
  drawerMenuButton: {
    alignItems: 'flex-end',
    marginRight: 6,
    marginTop: 6.5,
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  itemMenuContainer: {
    height: 32,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
});
