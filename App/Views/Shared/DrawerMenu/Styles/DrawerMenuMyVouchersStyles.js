import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    paddingTop: 51,
    flex: 1,
  },
  drawerMenuButton: {
    alignItems: 'flex-end',
    marginRight: 6,
    marginTop: 6.5,
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  secondText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
  itemMenuContainer: {
    height: 32,
    paddingLeft: 24,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  formContainer: {
    paddingLeft: 23,
    marginTop: 74.5,
  },
  gridRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  gridCol: {
    justifyContent: 'center',
  },
  divider: {
    backgroundColor: '#F4F4F6',
    width: 1,
    height: 24,
  },
  voucherText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
  },
  helpText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 8,
    color: Colors.onPrimaryText,
  },
  voucherInputContainer: {
    borderWidth: 1,
    borderColor: '#78849E',
    borderRadius: 5,
    width: 93,
    height: 23,
  },
  voucherInputField: {
    color: '#78849E',
    textAlign: 'center',
    ...addFontFamily('SegoeUI'),
    fontSize: 10,
  },
});
