import { StyleSheet } from 'react-native';
import { Colors, addFontFamily, Metrics } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  newCardContainer: {
    paddingHorizontal: 46,
  },
  centerContainer: {
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
  },
  secondaryText: {
    ...addFontFamily('SegoeUI'),
    fontSize: 14,
    color: '#454f63',
  },
  divider: {
    height: 1,
    backgroundColor: 'rgb(120, 132, 158)',
  },
  menuItemDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.2)',
  },
  paymentCardsContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 21.5 * Metrics.scaleFactor,
    width: 148,
    height: 31,
  },
  cardDataContainer: {
    marginTop: 46,
    flex: 1,
    width: Metrics.screenWidth,
  },
  cardDataItem: {
    height: 28,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionContainer: {
    marginTop: 19.5,
    justifyContent: 'flex-start',
    paddingLeft: 25.5,
  },
  actionContainerNewCard: {
    marginTop: 19.5,
    marginBottom: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 50,
  },
  actionContainerEditCard: {
    marginTop: 19.5,
    marginBottom: 20,
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: 70,
  },
  inputContainer: {
    height: 28,
    borderBottomColor: 'transparent',
  },
  textInputContainer: {
    flex: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    height: 30,
  },
  textInputContainerError: {
    flex: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    borderColor: '#ff0000',
    borderWidth: 1,
    height: 30,
  },
  inputText: {
    ...addFontFamily('SegoeUI', '400'),
    fontSize: 14,
    color: '#454f63',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
    paddingBottom: 5,
  },
  actionButton: {
    height: 30,
    backgroundColor: '#009FE3',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  actionButtonDisabled: {
    height: 30,
    backgroundColor: Colors.disable,
    borderRadius: 4,
  },
  actionButtonText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 15,
    color: '#fff',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
});
