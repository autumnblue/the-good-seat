import { StyleSheet } from 'react-native';
import { addFontFamily, Metrics, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    paddingTop: 51,
    flex: 1,
  },
  drawerMenuButton: {
    alignItems: 'flex-end',
    marginRight: 6,
    marginTop: 6.5,
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  itemMenuContainer: {
    height: 32,
    paddingLeft: 24,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  carouselPageContainer: {
    width: Metrics.screenWidth,
    backgroundColor: 'transparent',
  },
  tripListContainer: {
    paddingHorizontal: 24,
    marginTop: 28.5,
  },
});
