import { StyleSheet } from 'react-native';
import { addFontFamily, Metrics, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    paddingTop: 51,
    flex: 1,
  },
  drawerMenuButton: {
    alignItems: 'flex-end',
    marginRight: 6,
    marginTop: 6.5,
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  secondText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
  itemMenuContainer: {
    height: 32,
    paddingLeft: 24,
  },
  paymentLogoContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 31.5,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  carouselPageContainer: {
    width: Metrics.screenWidth,
    marginTop: 26,
    flex: 1,
  },
  actionContainer: {
    marginTop: 19.5,
    justifyContent: 'flex-start',
    paddingLeft: 25.5,
  },
  actionButton: {
    height: 30,
    backgroundColor: '#009FE3',
    borderRadius: 4,
  },
  actionButtonText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 15,
    color: '#fff',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
});
