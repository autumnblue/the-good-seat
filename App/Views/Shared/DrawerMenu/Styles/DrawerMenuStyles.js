import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  drawer: {
    backgroundColor: 'rgba(255, 255, 255, 0.96)',
  },
  container: {
    paddingTop: 51,
    paddingLeft: 24,
    flex: 1,
  },
  drawerMenuFooterContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 17,
  },
  drawerMenuButton: {
    alignItems: 'flex-end',
    marginRight: 6,
    marginTop: 6.5,
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  itemMenuContainer: {
    height: 32,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  modalContainer: {
    width: 320,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  modalTextContainer: {
    marginHorizontal: 27,
    marginTop: 29,
    alignItems: 'center',
  },
  modalFooter: {
    flexDirection: 'row',
    marginHorizontal: 18.5,
    justifyContent: 'space-between',
    marginBottom: 37,
    marginTop: 54,
  },
  modalButton: {
    minWidth: 125,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 10,
    color: Colors.onPrimaryText,
  },
});
