import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    minHeight: 24,
    flexDirection: 'row',
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  locationContainer: {
    flex: 0.9,
    flexWrap: 'wrap',
  },
  actionImage: {
    width: 15,
    height: 15,
    marginHorizontal: 7,
  },
  itemMenuContainer: {
    height: 32,
    paddingLeft: 24,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  formContainer: {
    paddingLeft: 34,
    marginTop: 74.5,
  },
  searchResultItem: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderBottomColor: 'rgba(120,132,158, 0.2)',
    borderBottomWidth: 1,
  },
  searchResultText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
    alignItems: 'center',
  },
  menuItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageGPS: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 37,
    flex: 1,
  },
  imageClose: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 37,
    marginTop: 10,
    flex: 1,
    zIndex: 1000,
  },
  closeButon: { position: 'absolute', top: 10, left: -70 },
  clockImage: {
    width: 19,
    height: 19,
  },
  menuText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    paddingHorizontal: 5,
    flexWrap: 'wrap',
  },
  menuItemInput: {
    //  flexDirection: 'row',
    //  alignItems: 'center',
    marginRight: 40,
  },
  menuItemPictureContainer: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuItemDivider: {
    backgroundColor: '#F4F4F6',
    width: 1,
    height: 24,
    marginHorizontal: 16,
  },
  inputAddressContainer: {
    paddingRight: 32,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
