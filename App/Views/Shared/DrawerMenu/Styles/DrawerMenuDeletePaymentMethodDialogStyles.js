import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    marginHorizontal: 10,
    borderRadius: 12,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  bodyContainer: {
    marginHorizontal: 57,
    marginTop: 66,
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson'),
    fontSize: 16,
    color: Colors.onPrimaryText,
  },
  footer: {
    marginBottom: 64,
    marginTop: 35,
    flexDirection: 'row',
    marginHorizontal: 86,
  },
  modalButton: {
    minWidth: 82,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    ...addFontFamily('Gibson'),
    fontSize: 13,
    color: Colors.onPrimaryText,
  },
});
