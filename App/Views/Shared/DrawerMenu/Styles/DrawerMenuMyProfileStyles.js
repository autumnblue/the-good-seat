import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Metrics, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    paddingTop: 51,
    flex: 1,
  },
  drawerMenuButton: {
    alignItems: 'flex-end',
    marginRight: 6,
    marginTop: 6.5,
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlignVertical: 'center',
  },
  itemMenuContainer: {
    height: 32,
    paddingLeft: 24,
  },
  itemMenuDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.15)',
    marginTop: 10,
  },
  itemMenuContainerUnderline: {
    borderBottomColor: 'rgba(120, 132, 158, 0.15)',
    borderBottomWidth: 1,
  },
  formContainer: {
    marginTop: 45.5,
  },
  inputText: {
    ...addFontFamily('SegoeUI', '400'),
    fontSize: 14,
    color: '#454f63',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
  placeholderStyle: {
    ...addFontFamily('SegoeUI', '300'),
    fontSize: 14,
    color: '#454f63',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
  inputContainer: {
    height: 60,
    borderBottomColor: 'transparent',
  },
  inputEditContainer: {
    marginRight: 19,
  },
  inputContainerDivider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.2)',
  },
  menuFooter: {
    justifyContent: 'center',
    marginTop: 52,
    marginHorizontal: 20,
  },
  footerButton: {
    borderColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 12,
  },
  footerButtonText: {
    ...addFontFamily('SegoeUI', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
  },
  modalContainer: {
    width: 320,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  modalTextContainer: {
    marginHorizontal: 27,
    marginTop: 29,
    alignItems: 'center',
  },
  modalFooter: {
    marginBottom: 37,
    marginTop: 54,
    flexDirection: 'row',
    marginHorizontal: 18.5,
  },
  modalButton: {
    minWidth: 125,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonDisable: {
    minWidth: 125,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 10,
    color: Colors.onPrimaryText,
  },
  modalButtonTextDisable: {
    ...addFontFamily('Gibson', '400'),
    fontSize: 10,
    color: Colors.disable,
  },
  processingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '100%',
    height: Metrics.screenHeight,
    flex: 1,
  },
  processingText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
  textInputContainer: {
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 39,
    height: 32,
  },
  textInputContainerError: {
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: Colors.error,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 39,
    height: 32,
  },
  textInputField: {
    color: '#78849E',
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    textAlign: 'center',
  },
});
