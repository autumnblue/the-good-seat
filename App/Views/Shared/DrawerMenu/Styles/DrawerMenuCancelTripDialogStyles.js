import { StyleSheet, Platform } from 'react-native';
import { Colors, addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '95%',
    paddingTop: 58,
    borderRadius: 12,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  centerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 46,
  },
  centerConfirmerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 94,
  },
  mainText: {
    ...addFontFamily('Gibson'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlign: 'center',
  },
  secondaryText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 12,
    color: Colors.onPrimaryText,
  },
  footer: {
    marginTop: 16,
    marginBottom: 53,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 86,
  },
  footerConfirmer: {
    marginTop: 53,
    marginBottom: 53,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 136,
  },
  footerButton: {
    width: 82,
    height: 52,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    ...addFontFamily('Gibson'),
    fontSize: 13,
    color: Colors.onPrimaryText,
  },
});
