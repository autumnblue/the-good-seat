import React, { Component } from 'react';
import { Text, Button } from 'native-base';
import {
  View, TouchableOpacity, Image, ScrollView,
} from 'react-native';
import { observer, inject } from 'mobx-react';
import { Carousel, BubblesSpinner } from '../../../Components';
// import appState from '../../../MobX';
import PaymentMethodForm from './DrawerMenuPaymetMethodForm';
import DrawerMenuHeader from './DrawerMenuHeader';
import DrawerMenuDeletePaymentMethodDialog from './DrawerMenuDeletePaymentMethodDialog';

import Styles from './Styles/DrawerMenuMyPaymentsMetodsStyles';
import { Images } from '../../../Theme';
import { getCardList, changePaymentCard } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

const SCREEN_RENDER_STATE = {
  EMPTY_CARDS: 0,
  ARRAY_CARDS: 1,
  NEW_CARD: 2,
  RENAME_CARD: 3,
  CARD_PROCESSING: 4,
};

@inject('appState')
@observer
class DrawerMenuMyPaymentsMetods extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      screenRenderState: SCREEN_RENDER_STATE.ARRAY_CARDS,
      activePaymetMethodPage: 0,
      currentPaymentCard: {},
      paymentCards: [],
      paymentDeletedDialogVisible: false,
    };
  }

  componentDidMount() {
    this.updateCardsList();
  }

  //---------------------------------------------------
  // update cards list (load from local storage)
  //---------------------------------------------------
  updateCardsList = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    this.setState({
      screenRenderState: SCREEN_RENDER_STATE.CARD_PROCESSING,
    });

    getCardList(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken, _id).then(
      (data) => {
        if (data.body) {
          if (data.body.length) {
            console.log('card list', data.body);
            this.setState(
              {
                screenRenderState: SCREEN_RENDER_STATE.ARRAY_CARDS,
                paymentCards: data.body,
              },
              () => {
                // set current card id for payments
                data.body.forEach((cardData) => {
                  if (cardData.isFavorite) {
                    appState.sessionParameters.currentPaymentCardId = cardData.mongoPayCardId;
                  }
                });
              },
            );
          } else {
            this.setState({
              screenRenderState: SCREEN_RENDER_STATE.EMPTY_CARDS,
            });
            // reset current paymetn card ID
            appState.sessionParameters.currentPaymentCardId = false;
          }
        }
      },
    );
  };

  //---------------------------------------------------
  // cancel create card
  //---------------------------------------------------
  cancelCreateCard = () => {
    this.updateCardsList();
  };

  //---------------------------------------------------
  // end process
  //---------------------------------------------------
  endProcessCard = () => {
    this.updateCardsList();
  };

  //---------------------------------------------------
  // begin processing card
  //---------------------------------------------------
  beginProcessingCard = () => {
    this.setState({ screenRenderState: SCREEN_RENDER_STATE.CARD_PROCESSING });
  };

  //---------------------------------------------------
  // (drawer menu back to main section) вернуться назад
  //---------------------------------------------------
  handlePressBack = () => {
    appState.uiState.navigateDrawerSection('MAIN');
  };

  //---------------------------------------------------
  // delete payment card
  //---------------------------------------------------
  deletePaymentCard = (paymentCard) => {
    this.setState({ currentPaymentCard: paymentCard, paymentDeletedDialogVisible: true });
  };

  //---------------------------------------------------
  // close delete payment method dialog
  //---------------------------------------------------
  closeDeletePaymentMethodDialog = () => {
    this.setState({ paymentDeletedDialogVisible: false });
    this.endProcessCard();
  };

  //----------------------------------------------------
  // create new card
  //----------------------------------------------------
  createNewCard = () => {
    this.setState({ screenRenderState: SCREEN_RENDER_STATE.NEW_CARD });
  };

  //----------------------------------------------------
  // rename card
  //----------------------------------------------------
  renameCard = (paymentCard) => {
    this.setState({
      currentPaymentCard: paymentCard,
      screenRenderState: SCREEN_RENDER_STATE.RENAME_CARD,
    });
  };

  //----------------------------------------------------
  // submit card name
  //----------------------------------------------------
  submitCardName = (text) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;
    const { currentPaymentCard } = this.state;

    if (text) {
      if (text.length) {
        changePaymentCard(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          _id,
          {
            cardId: currentPaymentCard._id,
            name: text,
            mongoPayCardId: currentPaymentCard.mongoPayCardId,
          },
        ).then(() => {
          this.updateCardsList();
        });
      }
    } else {
      this.setState({
        screenRenderState: SCREEN_RENDER_STATE.ARRAY_CARDS,
      });
    }
  };

  //----------------------------------------------------
  // select active card
  //----------------------------------------------------
  selectActiveCard = (paymentCard) => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    this.beginProcessingCard();

    changePaymentCard(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      {
        cardId: paymentCard._id,
        name: paymentCard.name,
        mongoPayCardId: paymentCard.mongoPayCardId,
        isFavorite: true,
      },
    ).then(() => {
      this.updateCardsList();
    });
  };

  //-------------------------------------------------------------------
  // render action footer
  //-------------------------------------------------------------------
  renderFooter = paymentCard => (
    <View style={Styles.actionContainer}>
      <TouchableOpacity onPress={this.renameCard.bind(this, paymentCard)}>
        <Text style={Styles.mainText}>Renommer ce moyen de paiment</Text>
      </TouchableOpacity>
      <View style={[{ width: 75.5, marginTop: 10, marginBottom: 8.5 }, Styles.divider]} />
      <TouchableOpacity onPress={this.deletePaymentCard.bind(this, paymentCard)}>
        <Text style={Styles.mainText}>Supprimer ce moyen de paiement</Text>
      </TouchableOpacity>
      <View style={[{ width: 75.5, marginTop: 10, marginBottom: 8.5 }, Styles.divider]} />
      <TouchableOpacity onPress={this.createNewCard}>
        <Text style={Styles.mainText}>Ajouter un moyen de paiement</Text>
      </TouchableOpacity>
      <View style={{ marginTop: 14 }}>
        <Button style={Styles.actionButton} onPress={this.selectActiveCard.bind(this, paymentCard)}>
          <Text style={Styles.actionButtonText}>
            {paymentCard.isFavorite ? 'Actif' : 'Utiliser ce moyen de paiement'}
          </Text>
        </Button>
      </View>
    </View>
  );

  //---------------------------------------------------
  // render payment cards
  //---------------------------------------------------
  renderPaymentCards = () => {
    const { paymentCards } = this.state;

    const paymentsRenderData = paymentCards.map(paymentMethod => (
      <ScrollView key={paymentMethod._id} style={Styles.carouselPageContainer}>
        <PaymentMethodForm paymentTag={paymentMethod.name} />
        {this.renderFooter(paymentMethod)}
      </ScrollView>
    ));

    return paymentsRenderData;
  };

  //-------------------------------------------------
  // render screen array
  //-------------------------------------------------
  renderArrayCardsState = () => {
    const { activePaymetMethodPage } = this.state;

    return (
      <Carousel
        indicatorColor="rgba(69,79,99, 0.15)"
        indicatorColorActive="rgba(69,79,99, 0.5)"
        initialPage={parseInt(activePaymetMethodPage, 1)}
      >
        {this.renderPaymentCards()}
      </Carousel>
    );
  };

  //-------------------------------------------------
  // render screen empty
  //-------------------------------------------------
  renderEmptyCardsState = () => (
    <View
      style={{
        flex: 0.6,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 40,
      }}
    >
      <Text style={Styles.secondText}>Aucune carte de paiement trouvée</Text>
      <Button
        block
        style={{ marginTop: 30, backgroundColor: '#009FE3', borderRadius: 4 }}
        onPress={() => {
          this.setState({ screenRenderState: SCREEN_RENDER_STATE.NEW_CARD });
        }}
      >
        <Text style={Styles.textButtonStyle}>Ajouter un moyen de paiement</Text>
      </Button>
    </View>
  );

  //-------------------------------------------------
  // render screen new card
  //-------------------------------------------------
  renderNewCardState = () => (
    <View style={Styles.carouselPageContainer}>
      <PaymentMethodForm
        isNew
        onBeginPostingCard={this.beginProcessingCard}
        onEndPostingCard={this.endProcessCard}
        onCancelCreateCard={this.cancelCreateCard}
      />
    </View>
  );

  //-------------------------------------------------
  // render screen rename card
  //-------------------------------------------------
  renderRenameCardState = () => (
    <View style={Styles.carouselPageContainer}>
      <PaymentMethodForm
        isEdit
        onBeginPostingCard={this.beginProcessingCard}
        onEndPostingCard={this.endProcessCard}
        onCancelCreateCard={this.cancelCreateCard}
        onSubmitEditing={this.submitCardName}
      />
    </View>
  );

  //--------------------------------------------------
  // render processing card state
  //--------------------------------------------------
  renderProcessingCardState = () => (
    <View
      style={{
        flex: 0.6,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 40,
      }}
    >
      <Text style={Styles.secondText}>Chargement des cartes</Text>
      <View style={{ height: 20 }} />
      <BubblesSpinner size={40} />
    </View>
  );

  //-------------------------------------------------
  // render current state
  //-------------------------------------------------
  renderScreen = () => {
    const { screenRenderState } = this.state;

    switch (screenRenderState) {
      case SCREEN_RENDER_STATE.EMPTY_CARDS:
        return this.renderEmptyCardsState();
      case SCREEN_RENDER_STATE.ARRAY_CARDS:
        return this.renderArrayCardsState();
      case SCREEN_RENDER_STATE.NEW_CARD:
        return this.renderNewCardState();
      case SCREEN_RENDER_STATE.RENAME_CARD:
        return this.renderRenameCardState();
      case SCREEN_RENDER_STATE.CARD_PROCESSING:
        return this.renderProcessingCardState();
      default:
        return this.renderEmptyCardsState();
    }
  };

  render() {
    const { paymentDeletedDialogVisible, currentPaymentCard } = this.state;

    const { firstname, lastname } = appState.sessionParameters.currentUser;

    const accountName = `${lastname} ${firstname}`;

    const { modal } = this.props;

    return (
      <View style={Styles.container}>
        {!modal && (
          <View style={{ marginLeft: 24 }}>
            <DrawerMenuHeader
              opened
              accountName={accountName}
              onPressMenuButton={this.handlePressBack}
            />
          </View>
        )}
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mes moyens de paiement</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <View style={Styles.paymentLogoContainer}>
          <Image source={Images.mangoPayLogo} />
        </View>
        {this.renderScreen()}
        <DrawerMenuDeletePaymentMethodDialog
          visible={paymentDeletedDialogVisible}
          paymentCard={currentPaymentCard}
          onCancel={this.closeDeletePaymentMethodDialog}
          onOk={this.closeDeletePaymentMethodDialog}
          onBeginDeleting={this.beginProcessingCard}
        />
      </View>
    );
  }
}

DrawerMenuMyPaymentsMetods.defaultProps = {
  modal: false,
};

export default DrawerMenuMyPaymentsMetods;
