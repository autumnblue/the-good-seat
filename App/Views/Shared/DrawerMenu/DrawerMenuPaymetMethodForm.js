import React, { Component } from 'react';
import { Alert, Image, View } from 'react-native';
import {
  Text, Input, Item, Content, Button,
} from 'native-base';
import { observer, inject } from 'mobx-react';
import { TextInputMask } from 'react-native-masked-text';
import moment from 'moment';
import { registerCard, getCard } from '../../../Services/BackEnd/MangoPay';
import { validateFormValue, validateCheckDigit, formatCardDateExp } from '../../../Services/Utils';

import Styles from './Styles/DrawerMenuPaymentMethodFormStyles';
import { Images } from '../../../Theme';
import { addPaymentCard } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

/*
------------------------------------------------------
 menu item
------------------------------------------------------
*/

function MenuItem(props) {
  const { containerStyle, text } = props;

  return (
    <View style={[containerStyle, Styles.cardDataItem]}>
      <Text style={Styles.secondaryText}>{text}</Text>
    </View>
  );
}
/*
------------------------------------------------------
 payment method dialog
------------------------------------------------------
*/
@inject('appState')
@observer
class DrawerMenuPaymentMethodForm extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      disableApplyButton: true,
      errorInputs: {},
      formInputs: {},
    };

    // validation config
    this.validateNewCardFormConfig = {
      cardName: {
        required: true,
      },
      cardNumber: {
        required: true,
        validFunc: this.validateCardNumber,
      },
      cardDateExp: {
        required: true,
        validFunc: this.validateDateCardExp,
      },
      cardCVC: {
        required: true,
        validFunc: this.validateCardCVC,
      },
    };

    // track previous date input to apply custom mask logic
    this.prevCardDateExp = '';
  }

  componentDidMount() {
    const { isEdit } = this.props;

    if (isEdit) {
      // this.cardName.getElement()._textInput.focus();
    }
  }

  //------------------------------------------------
  // validate date card exp
  //------------------------------------------------
  validateDateCardExp = () => {
    const dateExp = this.cardDateExp.getRawValue();

    const year = dateExp.year();
    const month = dateExp.month() + 1;

    if (month > 0 && month <= 12) {
      const currentYear = moment().year();
      if (currentYear < year) return true;

      if (currentYear === year) {
        const currentMonth = moment().month() + 1;
        if (currentMonth <= month) return true;
      }
    }

    return false;
  };

  //------------------------------------------------
  // credit card is valid
  //------------------------------------------------
  validateCardNumber = text => validateCheckDigit(text);

  //-------------------------------------------------
  // validate card CVC
  //-------------------------------------------------
  validateCardCVC = (text) => {
    if (text) {
      return text.length === 3;
    }
    return false;
  };

  //---------------------------------------------------
  // post card to MangoPay service
  //---------------------------------------------------
  postCardToMangoPay = async () => {
    const { formInputs } = this.state;

    const cardData = await registerCard(
      appState.constants.MANGOPAY_API,
      appState.constants.MANGOPAY_CLIENTID,
      appState.constants.MANGOPAY_TOKEN,

      {
        tag: formInputs.cardName,
        userId: appState.sessionParameters.currentUser.mangoPayUserId,
        currencyCode: 'EUR',
        cardNumber: formInputs.cardNumber.replace(/\s+/g, ''),
        cardExpirationDate: formInputs.cardDateExp.replace('/', ''),
        cardCvx: formInputs.cardCVC,
      },
    );

    const result = await getCard(
      appState.constants.MANGOPAY_API,
      appState.constants.MANGOPAY_CLIENTID,
      appState.constants.MANGOPAY_TOKEN,
      cardData.CardId,
    );

    if (result.errors) {
      Alert.alert('Get MangoPay card', result.Message);
    }

    return result;
  };

  //-------------------------------------------------------------------
  // handle save new card
  //-------------------------------------------------------------------
  handleOnSaveCard = () => {
    const { formInputs } = this.state;

    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { onBeginPostingCard, onEndPostingCard } = this.props;

    if (onBeginPostingCard) onBeginPostingCard();

    this.postCardToMangoPay().then((cardData) => {
      if (cardData) {
        addPaymentCard(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          _id,
          {
            name: formInputs.cardName,
            mongoPayCardId: cardData.Id,
            isFavorite: true,
          },
        ).then(() => {
          if (onEndPostingCard) onEndPostingCard();
        });
      }
    });
  };

  //-------------------------------------------------------------------
  // handle cancel card
  //-------------------------------------------------------------------
  handleOnCancelCreateCard = () => {
    const { onCancelCreateCard } = this.props;
    if (onCancelCreateCard) onCancelCreateCard();
  };

  //-------------------------------------------------------------------
  // handle submit editing
  //-------------------------------------------------------------------
  handleSubmitEditing = () => {
    const { formInputs } = this.state;
    const { onSubmitEditing } = this.props;

    if (onSubmitEditing) onSubmitEditing(formInputs.cardName !== null ? formInputs.cardName : '');
  };

  //-------------------------------------------------------------------
  // handle text input
  //-------------------------------------------------------------------
  handleTextInput = (type, text) => {
    this.setState(
      ({ formInputs }) => ({
        formInputs: {
          ...formInputs,
          [type]: this.formatCreditCardTextInput(type, text),
        },
      }),
      () => {
        this.validateCardForm();
      },
    );
  };

  //-------------------------------------------------------------------
  // handle text input
  //-------------------------------------------------------------------
  handleTextInputRename = (type, text) => {
    this.setState(({ formInputs }) => ({
      formInputs: {
        ...formInputs,
        [type]: text,
      },
    }));
  };

  //-------------------------------------------------
  // handle on blur input field
  //-------------------------------------------------
  onBlurInputField = (type) => {
    const { formInputs } = this.state;

    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        [type]: !validateFormValue(this.validateNewCardFormConfig, type, formInputs[type]),
      },
    }));
  };

  //------------------------------------------------
  // validate card input fields
  //------------------------------------------------
  validateCardForm = () => {
    const { formInputs } = this.state;

    let error = false;

    const keys = Object.keys(this.validateNewCardFormConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      error = !validateFormValue(this.validateNewCardFormConfig, key, formInputs[key]);
      // if error == true
      if (error) break;
    }

    // show button
    this.setState({ disableApplyButton: error });
  };

  //------------------------------------------------
  // format text before applying to form
  //------------------------------------------------
  formatCreditCardTextInput = (type, textIn) => {
    if (type === 'cardDateExp') {
      this.prevCardDateExp = formatCardDateExp(textIn, this.prevCardDateExp);
    }
    return textIn;
  }

  //-------------------------------------------------------------------
  // handle delete payment method dialog
  //-------------------------------------------------------------------
  handleOnDelete = () => {
    const { onDelete } = this.props;
    if (onDelete) onDelete();
  };

  //-------------------------------------------------------------------
  // render action footer (new card)
  //-------------------------------------------------------------------
  renderFooterNewCard = () => {
    const { disableApplyButton } = this.state;
    return (
      <View style={Styles.actionContainerNewCard}>
        <Button
          style={disableApplyButton ? Styles.actionButtonDisabled : Styles.actionButton}
          disabled={disableApplyButton}
          onPress={this.handleOnSaveCard}
        >
          <Text style={Styles.actionButtonText}>Confirmer</Text>
        </Button>
        <Button style={Styles.actionButton} onPress={this.handleOnCancelCreateCard}>
          <Text style={Styles.actionButtonText}>Annuler</Text>
        </Button>
      </View>
    );
  };

  //-------------------------------------------------------------------
  // render action footer (edit card)
  //-------------------------------------------------------------------
  renderFooterEditCard = () => (
    <View style={Styles.actionContainerEditCard}>
      <Button style={Styles.actionButton} onPress={this.handleOnCancelCreateCard}>
        <Text style={Styles.actionButtonText}>Annuler</Text>
      </Button>
    </View>
  );

  //-------------------------------------------------------------------
  // render dialog (new card)
  //-------------------------------------------------------------------
  renderStateNewCard = () => {
    const { formInputs, errorInputs } = this.state;

    return (
      <Content>
        <View style={Styles.container}>
          <View style={Styles.newCardContainer}>
            <View
              style={
                errorInputs.cardName ? Styles.textInputContainerError : Styles.textInputContainer
              }
            >
              <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
                <TextInputMask
                  style={Styles.inputText}
                  autoFocus
                  type="custom"
                  options={{
                    mask: '********************************',
                  }}
                  placeholder={appState.appTextData.get('newcard.form.cardName').value}
                  placeholderTextColor="#78849E"
                  value={formInputs.cardName}
                  customTextInput={Input}
                  onChangeText={this.handleTextInput.bind(this, 'cardName')}
                  onBlur={this.onBlurInputField.bind(this, 'cardName')}
                />
              </Item>
            </View>
          </View>
          <View style={Styles.centerContainer}>
            <View style={[{ width: 75, marginTop: 5.5 }, Styles.divider]} />
          </View>
          <Image
            style={[{ marginTop: 21.5 }, Styles.paymentCardsContainer]}
            source={Images.paymentCards}
          />

          <View style={Styles.newCardContainer}>
            <View
              style={[
                errorInputs.cardNumber ? Styles.textInputContainerError : Styles.textInputContainer,
                { marginTop: 42 },
              ]}
            >
              <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
                <TextInputMask
                  ref={(ref) => {
                    this.cardNumber = ref;
                  }}
                  style={Styles.inputText}
                  type="credit-card"
                  options={{
                    format: '9999 9999 9999 9999',
                  }}
                  placeholder={appState.appTextData.get('registration.form.cardNumber').value}
                  placeholderTextColor="#78849E"
                  value={formInputs.cardNumber}
                  customTextInput={Input}
                  onChangeText={this.handleTextInput.bind(this, 'cardNumber')}
                  onBlur={this.onBlurInputField.bind(this, 'cardNumber')}
                />
              </Item>
            </View>
          </View>
          <View style={[{ marginTop: 15.5 }, Styles.menuItemDivider]} />

          <View style={Styles.newCardContainer}>
            <View
              style={[
                errorInputs.cardDateExp
                  ? Styles.textInputContainerError
                  : Styles.textInputContainer,
                { marginTop: 17.5 },
              ]}
            >
              <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
                <TextInputMask
                  ref={(ref) => {
                    this.cardDateExp = ref;
                  }}
                  style={Styles.inputText}
                  type="datetime"
                  options={{
                    format: 'MM/YY',
                  }}
                  placeholder={
                    appState.appTextData.get('registration.form.cardDateExpiration').value
                  }
                  placeholderTextColor="#78849E"
                  value={formInputs.cardDateExp}
                  customTextInput={Input}
                  onChangeText={this.handleTextInput.bind(this, 'cardDateExp')}
                  onBlur={this.onBlurInputField.bind(this, 'cardDateExp')}
                />
              </Item>
            </View>
          </View>
          <View style={[{ marginTop: 15.5 }, Styles.menuItemDivider]} />

          <View style={Styles.newCardContainer}>
            <View
              style={[
                errorInputs.cardCVC ? Styles.textInputContainerError : Styles.textInputContainer,
                { marginTop: 17.5 },
              ]}
            >
              <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
                <TextInputMask
                  type="custom"
                  options={{
                    mask: '999',
                  }}
                  style={Styles.inputText}
                  placeholder={appState.appTextData.get('registration.form.cardCVC').value}
                  placeholderTextColor="#78849E"
                  value={formInputs.cardCVC}
                  customTextInput={Input}
                  onChangeText={this.handleTextInput.bind(this, 'cardCVC')}
                  onBlur={this.onBlurInputField.bind(this, 'cardCVC')}
                />
              </Item>
            </View>
          </View>
          <View style={[{ marginTop: 15.5 }, Styles.menuItemDivider]} />
        </View>
        {this.renderFooterNewCard()}
      </Content>
    );
  };

  //-------------------------------------------------------------------
  // render edit name state
  //-------------------------------------------------------------------
  renderStateEditName = () => {
    const { formInputs } = this.state;

    return (
      <Content>
        <View style={Styles.container}>
          <View style={Styles.newCardContainer}>
            <View style={Styles.textInputContainer}>
              <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
                <TextInputMask
                  ref={(ref) => {
                    this.cardName = ref;
                  }}
                  autoFocus
                  style={Styles.inputText}
                  type="custom"
                  options={{
                    mask: '********************************',
                  }}
                  placeholder={appState.appTextData.get('newcard.form.cardRename').value}
                  placeholderTextColor="#78849E"
                  value={formInputs.cardName}
                  customTextInput={Input}
                  onChangeText={this.handleTextInputRename.bind(this, 'cardName')}
                  onSubmitEditing={this.handleSubmitEditing}
                />
              </Item>
            </View>
          </View>
          <View style={Styles.centerContainer}>
            <View style={[{ width: 75, marginTop: 5.5 }, Styles.divider]} />
          </View>
          <Image
            style={[{ marginTop: 21.5 }, Styles.paymentCardsContainer]}
            source={Images.paymentCards}
          />

          <MenuItem containerStyle={{ marginTop: 46 }} text="XXX-XXX-XXX-XXX" />
          <View style={[{ marginTop: 12.5 }, Styles.menuItemDivider]} />

          <MenuItem containerStyle={{ marginTop: 14.5 }} text="MM / AA" />
          <View style={[{ marginTop: 12.5 }, Styles.menuItemDivider]} />

          <MenuItem containerStyle={{ marginTop: 14.5 }} text="XXX" />
          <View style={[{ marginTop: 12.5 }, Styles.menuItemDivider]} />
        </View>
        {this.renderFooterEditCard()}
      </Content>
    );
  };

  //-------------------------------------------------------------------
  // render view state
  //-------------------------------------------------------------------
  renderStateView = () => {
    const { paymentTag } = this.props;

    return (
      <View style={Styles.container}>
        <View style={Styles.cardDataItem}>
          <Text style={Styles.mainText}>{paymentTag}</Text>
        </View>
        <View style={Styles.centerContainer}>
          <View style={[{ width: 75, marginTop: 5.5 }, Styles.divider]} />
        </View>
        <Image
          style={[{ marginTop: 21.5 }, Styles.paymentCardsContainer]}
          source={Images.paymentCards}
        />

        <MenuItem containerStyle={{ marginTop: 46 }} text="XXX-XXX-XXX-XXX" />
        <View style={[{ marginTop: 12.5 }, Styles.menuItemDivider]} />

        <MenuItem containerStyle={{ marginTop: 14.5 }} text="MM / AA" />
        <View style={[{ marginTop: 12.5 }, Styles.menuItemDivider]} />

        <MenuItem containerStyle={{ marginTop: 14.5 }} text="XXX" />
        <View style={[{ marginTop: 12.5 }, Styles.menuItemDivider]} />
      </View>
    );
  };

  render() {
    const { isNew, isEdit } = this.props;

    if (isNew) {
      return this.renderStateNewCard();
    }

    if (isEdit) {
      return this.renderStateEditName();
    }

    return this.renderStateView();
  }
}

export default DrawerMenuPaymentMethodForm;
