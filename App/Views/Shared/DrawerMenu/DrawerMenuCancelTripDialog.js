import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, Text } from 'native-base';

import { observer, inject } from 'mobx-react';

import Styles from './Styles/DrawerMenuCancelTripDialogStyles';


@inject('appState')
@observer
class DrawerMenuCancelTripDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showConfirmer: false,
    };
  }

  //-------------------------------------------------
  // close dialog
  //-------------------------------------------------
  handleOnCloseDialog = () => {
    const { onCancel } = this.props;
    if (onCancel) onCancel();
  };

  //----------------------------------------------------
  // switch to next
  //----------------------------------------------------
  showConfirmer = () => {
    this.setState({ showConfirmer: true });
  };

  //----------------------------------------------------
  // OK
  //----------------------------------------------------
  handleOnOK = () => {
    const { onOK } = this.props;
    if (onOK) onOK();
  };

  render() {
    const { showConfirmer } = this.state;

    if (showConfirmer) {
      return (
        <View style={Styles.container}>
          <View style={Styles.centerConfirmerContainer}>
            <Text style={Styles.mainText}>Votre Course Uber a bien été supprimée</Text>
          </View>

          <View style={Styles.footerConfirmer}>
            <Button bordered style={Styles.footerButton} onPress={this.handleOnOK}>
              <Text style={Styles.buttonText}>OK</Text>
            </Button>
          </View>
        </View>
      );
    }
    return (
      <View style={Styles.container}>
        <View style={Styles.centerContainer}>
          <Text style={Styles.mainText}>
            Etes vous sûr(e) de vouloir supprimer votre Course Uber ?
          </Text>
          <Text style={[{ marginTop: 21 }, Styles.secondaryText]}>
            Frais d&apos;annulation : 0 €
          </Text>
        </View>

        <View style={Styles.footer}>
          <Button bordered style={Styles.footerButton} onPress={this.showConfirmer}>
            <Text style={Styles.buttonText}>OK</Text>
          </Button>

          <Button bordered style={Styles.footerButton} onPress={this.handleOnCloseDialog}>
            <Text style={Styles.buttonText}>Annuler</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default DrawerMenuCancelTripDialog;
