import React, { Component } from 'react';
import { Text } from 'native-base';
import { View, FlatList } from 'react-native';
import { observer, inject } from 'mobx-react';
// import appState from '../../../MobX';
import { Carousel, TripCard, Modal } from '../../../Components';

import StagesMenuEvaluateDriverDialog from '../StagesMenu/StagesMenuEvaluateDriverDialog';

import DrawerMenuHeader from './DrawerMenuHeader';

import { Colors } from '../../../Theme';
import { TYPE_CAR_COLOR_MAPPING, TRIP_CARD_TYPES } from '../../../MobX/Constants';

import Styles from './Styles/DrawerMenuMyTripsStyles';
import { getLastestRides } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

@inject('appState')
@observer
class DrawerMenuMyTrips extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      activeTripPage: 0,
      showEvaluate: false,
    };
  }

  //------------------------------------------------------
  // fetch lastes rides
  //------------------------------------------------------
  fetchLastesRides = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    getLastestRides(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
    ).then((data) => {
      appState.driveStageState.lastestRides = data.body;
      // switch state
      if (data.body.length) {
        // this.setState({ renderState: SCREEN_RENDER_STATE.DATA });
      } else {
        // this.setState({ renderState: SCREEN_RENDER_STATE.EMPTY });
      }
    });
  };

  //----------------------------------------------------
  // sent review trip
  //----------------------------------------------------
  onSentReviewTrip = (rideData) => {
    console.log('rideID', rideData._id);
    appState.driveStageState.currentRideData = rideData;
    this.setState({ showEvaluate: true });
  };

  //---------------------------------------------------
  // post ride
  //---------------------------------------------------
  postReviewRide = () => {
    this.setState({ showEvaluate: false });
    // reset ride engine to default state
    appState.driveStageState.resetRideEngine();
    // fetch lastest rides
    this.fetchLastesRides();
  };

  //----------------------------------------------------
  // close evaluate driver dialog
  //----------------------------------------------------
  closeEvaluateDriver = () => {
    this.setState({ showEvaluate: false });
    // reset ride engine to default state
    appState.driveStageState.resetRideEngine();
  };

  //---------------------------------------------------
  // render ended trip card
  //---------------------------------------------------
  renderEndedTripCard = ({ item }) => {
    if (item && !item.status.includes('user_cancel')) {
      return (
        <TripCard
          type={TRIP_CARD_TYPES.ENDED_CUT}
          rideData={item}
          onSentReviewTrip={this.onSentReviewTrip.bind(this, item)}
        />
      );
    }
    return null;
  };

  //---------------------------------------------------
  // render canceled trip card
  //---------------------------------------------------
  renderCanceledTripCard = ({ item }) => {
    if (item && item.status === 'user_cancelled') {
      return <TripCard type={TRIP_CARD_TYPES.CANCELED} rideData={item} />;
    }
    if (item && item.status === 'user_canceled') {
      return <TripCard type={TRIP_CARD_TYPES.CANCELED} rideData={item} />;
    }
    return null;
  };

  //---------------------------------------------------
  // render futured trip card
  //---------------------------------------------------
  renderFuturedTripCard = ({ item }) => {
    if (item && item.status === 'futured') {
      return <TripCard type={TRIP_CARD_TYPES.FUTURED} rideData={item} />;
    }
    return null;
  };

  //---------------------------------------------------
  // render comming trip card
  //---------------------------------------------------
  renderCommingTripCard = ({ item }) => (
    <TripCard
      type={TRIP_CARD_TYPES.COMMING}
      typeCar={item.typeCar}
      typeTrip={item.typeTrip}
      rating={item.rating}
      tripStart={item.tripStart}
      tripEnd={item.tripEnd}
      typeCarColor={Colors[TYPE_CAR_COLOR_MAPPING[item.typeCar]]}
      onCancelTrip={this.cancelTrip}
    />
  );

  //---------------------------------------------------
  // render ended trips page
  //---------------------------------------------------
  renderEndedTripsPage = () => {
    const { lastestRides } = appState.driveStageState;
    return (
      <View key={1} style={Styles.carouselPageContainer}>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mes courses terminées</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <FlatList
          contentContainerStyle={Styles.tripListContainer}
          refreshing={false}
          data={lastestRides}
          renderItem={this.renderEndedTripCard}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  //---------------------------------------------------
  // render comming trips page
  //---------------------------------------------------
  renderCommingTripsPage = () => {
    const { lastestRides } = appState.driveStageState;

    return (
      <View style={Styles.carouselPageContainer}>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mes courses à venir</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <FlatList
          contentContainerStyle={Styles.tripListContainer}
          refreshing={false}
          data={lastestRides}
          renderItem={this.renderLastestTripCard}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  //---------------------------------------------------
  // render canceled trip list
  //---------------------------------------------------
  renderCanceledTripsPage = () => {
    const { lastestRides } = appState.driveStageState;

    return (
      <View style={Styles.carouselPageContainer}>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Courses annulées</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <FlatList
          contentContainerStyle={Styles.tripListContainer}
          refreshing={false}
          data={lastestRides}
          renderItem={this.renderCanceledTripCard}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  //---------------------------------------------------
  // render futured trip list
  //---------------------------------------------------
  renderFuturedTripsPage = () => {
    const { lastestRides } = appState.driveStageState;

    return (
      <View style={Styles.carouselPageContainer}>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mes courses à venir</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <FlatList
          contentContainerStyle={Styles.tripListContainer}
          refreshing={false}
          data={lastestRides}
          renderItem={this.renderFuturedTripCard}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  //---------------------------------------------------
  // (drawer menu back to main section)
  //---------------------------------------------------
  handlePressBack = () => {
    appState.uiState.navigateDrawerSection('MAIN');
  };

  render() {
    const { activeTripPage, showEvaluate } = this.state;

    const { firstname, lastname } = appState.sessionParameters.currentUser;

    const accountName = `${lastname} ${firstname}`;

    return (
      <View style={Styles.container}>
        <View style={{ marginLeft: 24 }}>
          <DrawerMenuHeader
            opened
            accountName={accountName}
            onPressMenuButton={this.handlePressBack}
          />
        </View>
        <Carousel
          indicatorColor="rgba(69,79,99, 0.15)"
          indicatorColorActive="rgba(69,79,99, 0.5)"
          initialPage={parseInt(activeTripPage, 1)}
        >
          {this.renderEndedTripsPage()}
          {this.renderCanceledTripsPage()}
          {this.renderFuturedTripsPage()}
        </Carousel>
        <Modal visible={showEvaluate}>
          <StagesMenuEvaluateDriverDialog
            onOK={this.postReviewRide}
            onCancel={this.closeEvaluateDriver}
          />
        </Modal>
      </View>
    );
  }
}

export default DrawerMenuMyTrips;
