import React, { Component } from 'react';
import {
  Text, Input, Item, Button,
} from 'native-base';
import { View } from 'react-native';
import { observer, inject } from 'mobx-react';
import moment from 'moment';
import { BubblesSpinner, ErrorMessageBox } from '../../../Components';
import DrawerMenuHeader from './DrawerMenuHeader';

import Styles from './Styles/DrawerMenuMyVouchersStyles';
import { Buttons } from '../../../Theme';
import { getCagnotte, redeemCode, getBonuses } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

const SCREEN_RENDER_STATE = {
  LOADING: 0,
  DATA: 1,
};

@inject('appState')
@observer
class DrawerMenuMyVouchers extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      screenRenderState: SCREEN_RENDER_STATE.LOADING,
      balance: 0,
      promoCode: '',
      bonuses: [],
    };
  }

  componentDidMount() {
    this.updateBalance();
    this.updateBonusesList();
  }

  //---------------------------------------------------
  // get balance
  //---------------------------------------------------
  updateBalance = async () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { cagnotteId, _id } = appState.sessionParameters.currentUser;

    const data = await getCagnotte(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      cagnotteId,
    );

    if (data.errors) {
      this.setState({
        screenRenderState: SCREEN_RENDER_STATE.DATA,
      });
      ErrorMessageBox(language, data.message, errorsDictionary);
    } else {
      this.setState({
        screenRenderState: SCREEN_RENDER_STATE.DATA,
        balance: parseInt(data.body.amount, 10),
      });
    }
  };

  //----------------------------------------------------
  // update bonuses list
  //----------------------------------------------------
  updateBonusesList = async () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { _id } = appState.sessionParameters.currentUser;

    const data = await getBonuses(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
    );

    if (data.errors) {
      this.setState({
        screenRenderState: SCREEN_RENDER_STATE.DATA,
      });
      ErrorMessageBox(language, data.message, errorsDictionary);
    } else {
      console.log(data.body);
      this.setState({ bonuses: data.body });
    }
  };

  //---------------------------------------------------
  // (drawer menu back to main section) вернуться назад
  //---------------------------------------------------
  handlePressBack = () => {
    appState.uiState.navigateDrawerSection('MAIN');
  };

  //----------------------------------------------------
  // change promocode
  //----------------------------------------------------
  changeTextInput = (text) => {
    this.setState({ promoCode: text });
  };

  //----------------------------------------------------
  // redeem code
  //----------------------------------------------------
  redeemCode = () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { promoCode } = this.state;

    this.setState({
      screenRenderState: SCREEN_RENDER_STATE.LOADING,
    });

    redeemCode(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      promoCode,
    ).then((data) => {
      this.setState({ promoCode: '' });
      if (data.errors) {
        this.setState({
          screenRenderState: SCREEN_RENDER_STATE.DATA,
        });

        ErrorMessageBox(language, data.message, errorsDictionary);
      } else {
        this.updateBalance();
        this.updateBonusesList();
      }
    });
  };

  //----------------------------------------------------
  // render loading state
  //----------------------------------------------------
  renderLoadingState = () => (
    <View
      style={{
        flex: 0.6,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 40,
      }}
    >
      <Text style={Styles.secondText}>
        {appState.appTextData.get('drawer.menu.bonus.loading').value}
      </Text>
      <View style={{ height: 20 }} />
      <BubblesSpinner size={40} />
    </View>
  );

  //----------------------------------------------------
  // render bonuses list
  //----------------------------------------------------
  renderBonusesList = () => {
    const { bonuses } = this.state;

    const bonusesRenderData = bonuses.map((bonusData, index) => (
      <View key={bonusData._id} style={[Styles.gridRow, { marginBottom: 20 }]}>
        <View style={[{ width: 86 }, Styles.gridCol]}>
          <Text style={Styles.voucherText}>
            Bon numéro&nbsp;
            {index + 1}
          </Text>
        </View>
        <View style={Styles.divider} />
        <View style={[{ width: 56, marginLeft: 17 }, Styles.gridCol]}>
          <Text style={Styles.voucherText}>
            {bonusData.promocode ? bonusData.promocode.code : bonusData.promocodeId}
          </Text>
        </View>
        <View style={[{ marginLeft: 55 }, Styles.gridCol]}>
          <Text style={Styles.helpText}>
            Bon utilisé le
            {' '}
            {moment(bonusData.createdOn).format('DD/MM/YYYY')}
          </Text>
        </View>
      </View>
    ));

    return bonusesRenderData;
  };

  //----------------------------------------------------
  // render data state
  //----------------------------------------------------
  renderDataState = () => {
    const { balance, promoCode } = this.state;

    return (
      <View style={Styles.formContainer}>
        {this.renderBonusesList()}
        <View style={[{ marginTop: 32 }, Styles.gridRow]}>
          <View style={[{ width: 175 }, Styles.gridCol]}>
            <Text style={Styles.voucherText}>Ajouter un bon de réduction</Text>
          </View>
          <View style={[{ marginLeft: 40 }, Styles.gridCol, Styles.voucherInputContainer]}>
            <Item style={{ borderBottomColor: 'transparent' }}>
              <Input
                style={Styles.voucherInputField}
                onChangeText={this.changeTextInput}
                onSubmitEditing={this.redeemCode}
              />
            </Item>
          </View>
        </View>
        <View
          style={{
            marginTop: 5,
            marginRight: 40,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}
        >
          <Button
            disabled={!promoCode}
            full
            bordered
            onPress={this.redeemCode}
            style={!promoCode ? [Buttons.actionButtonDisabled] : [Buttons.actionButton]}
          >
            <Text style={[Buttons.actionButtonText, { fontSize: 8 }]}>AJouter</Text>
          </Button>
        </View>
        <View style={[{ marginTop: 32 }, Styles.gridRow]}>
          <View style={[{ width: 86 }, Styles.gridCol]}>
            <Text style={Styles.voucherText}>Ma cagnotte</Text>
          </View>
          <View style={Styles.divider} />

          <View style={[{ marginLeft: 17 }, Styles.gridCol]}>
            <Text style={Styles.voucherText}>
Solde :
              {balance}
              {' '}
euros
            </Text>
          </View>
        </View>
      </View>
    );
  };

  //----------------------------------------------
  // render cuurrent state
  //----------------------------------------------
  renderCurrentState = () => {
    const { screenRenderState } = this.state;
    switch (screenRenderState) {
      case SCREEN_RENDER_STATE.LOADING:
        return this.renderLoadingState();
      case SCREEN_RENDER_STATE.DATA:
        return this.renderDataState();
      default:
        return this.renderLoadingState();
    }
  };

  render() {
    const { firstname, lastname } = appState.sessionParameters.currentUser;

    const accountName = `${lastname} ${firstname}`;

    return (
      <View style={Styles.container}>
        <View style={{ marginLeft: 24 }}>
          <DrawerMenuHeader
            opened
            accountName={accountName}
            onPressMenuButton={this.handlePressBack}
          />
        </View>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mes bons de réduction</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        {this.renderCurrentState()}
      </View>
    );
  }
}

export default DrawerMenuMyVouchers;
