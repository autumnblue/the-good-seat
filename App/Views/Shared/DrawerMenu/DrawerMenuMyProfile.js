import React, { Component } from 'react';
import {
  Button, Text, Form, Content, Input,
} from 'native-base';
import { View, AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';
import { withNavigation } from 'react-navigation';
import { observer, inject } from 'mobx-react';
import {
  Modal, TextInputMyProfile, BubblesSpinner, ErrorMessageBox,
} from '../../../Components';
import DrawerMenuHeader from './DrawerMenuHeader';
// import appState from '../../../MobX';

import Styles from './Styles/DrawerMenuMyProfileStyles';
import {
  updateUserInfo,
  deleteUserAcoount,
  loginUser,
} from '../../../Services/BackEnd/TheGoodSeat';
import aliases from '../../../Services/BackEnd/Aliases';
import { Colors } from '../../../Theme';

let appState = {};

/*
-------------------------------------------------
 view types of modal
-------------------------------------------------
*/
const MODAL_VIEW_TYPE = {
  NONE: 0,
  QUESTION: 1,
  INPUT_PASSWORD: 2,
  CONFIRMATION: 3,
};

@inject('appState')
@observer
class DrawerMenuMyProfile extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.unsubscribe = null;
    this.state = {
      modalType: MODAL_VIEW_TYPE.NONE,
      firstName: '',
      lastName: '',
      phoneNumber: '',
      email: '',
      passwordForDeleteAccount: '',
      showDeletingAccountProcess: false,
      deleteAccountButtonDisable: true,
      accountPasswordError: false,
      placeholderDeletePasswordErrorText: '',
    };
  }

  componentDidMount() {
    // firebase
    if (this.unsubscribe) this.unsubscribe();
  }

  //----------------------------------------------
  // delete user acoount
  //----------------------------------------------
  deleteUserAccount = () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    // get user id
    const { _id, email } = appState.sessionParameters.currentUser;

    const { passwordForDeleteAccount } = this.state;

    this.setState({ showDeletingAccountProcess: true });

    try {
      loginUser(appState.constants.THEGOODSEAT_API, {
        password: passwordForDeleteAccount,
        email,
      }).then((data) => {
        if (data.errors) {
          this.setState({
            showDeletingAccountProcess: false,
            accountPasswordError: true,
            placeholderDeletePasswordErrorText: data.message,
          });
          ErrorMessageBox(language, data.message, errorsDictionary);
        } else {
          deleteUserAcoount(
            appState.constants.THEGOODSEAT_API,
            appState.sessionParameters.appToken,
            _id,
          ).then(() => {
            // delete from firebase account
            const currentFirebaseUser = firebase.auth().currentUser;
            if (currentFirebaseUser) {
              console.log('DELETE FROM FIREBASE', currentFirebaseUser);
              currentFirebaseUser.delete();
            }
            // reset app state
            appState.appShutdown();
            // remove local storage
            AsyncStorage.clear().then(() => {
              // success
              this.setState({
                showDeletingAccountProcess: false,
                modalType: MODAL_VIEW_TYPE.CONFIRMATION,
              });
            });
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  //----------------------------------------------
  // logout from app
  //----------------------------------------------
  logoutApp = () => {
    const { navigation } = this.props;

    this.changeModalAccoutType('NONE');

    // сменить состояния
    appState.uiState.reset();

    // перейти на экран входа
    navigation.navigate('logon');
  };

  //---------------------------------------------------
  // go back
  //---------------------------------------------------
  handlePressBack = () => {
    appState.uiState.navigateDrawerSection('MAIN');
  };

  //----------------------------------------------
  // change modal window style
  //----------------------------------------------
  changeModalAccoutType = (type) => {
    this.setState({ modalType: MODAL_VIEW_TYPE[type] });
  };

  //-----------------------------------------------
  // handle text input
  //-----------------------------------------------
  handleTextInput = (type, text) => {
    this.setState({ [type]: text });
  };

  //-----------------------------------------------
  // handle delete password input
  //-----------------------------------------------
  handlePasswordInput = (text) => {
    this.setState({ passwordForDeleteAccount: text });

    if (text.length) {
      this.setState({ deleteAccountButtonDisable: false });
    } else {
      this.setState({ deleteAccountButtonDisable: true });
    }
  };

  //-----------------------------------------------
  // handle submit
  //-----------------------------------------------
  onSubmit = (type) => {
    const { language, errorsDictionary } = appState.sessionParameters;
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { ...inputValue } = this.state;

    // if length > 0 update info in profile
    if (inputValue[type].length > 0) {
      const userData = {};
      userData[aliases.userProfile[type]] = inputValue[type];
      userData.userId = _id;
      updateUserInfo(
        appState.constants.THEGOODSEAT_API,
        appState.sessionParameters.appToken,
        userData,
      ).then((data) => {
        if (data.errors) {
          ErrorMessageBox(language, data.message, errorsDictionary);
        } else {
          appState.sessionParameters.currentUser = data.user;
        }
      });
    }
  };

  //-----------------------------------------------
  // render question state
  //-----------------------------------------------
  renderQuestionState = () => (
    <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
      <View style={Styles.modalContainer}>
        <View style={Styles.modalTextContainer}>
          <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
            Etes-vous sûr de vouloir supprimer votre compte ? Supprimer votre compte est
            irréversible et votre cagnotte sera perdue ?
          </Text>
        </View>
        <View style={[{ justifyContent: 'space-between' }, Styles.modalFooter]}>
          <Button
            style={Styles.modalButton}
            bordered
            onPress={this.changeModalAccoutType.bind(this, 'INPUT_PASSWORD')}
          >
            <Text style={Styles.modalButtonText}>CONFIRMER</Text>
          </Button>
          <Button
            style={Styles.modalButton}
            bordered
            onPress={this.changeModalAccoutType.bind(this, 'NONE')}
          >
            <Text style={Styles.modalButtonText}>ANNULER</Text>
          </Button>
        </View>
      </View>
    </Modal>
  );

  //-----------------------------------------------
  // render password input state
  //-----------------------------------------------
  renderPasswordInputState = () => {
    const {
      deleteAccountButtonDisable,
      accountPasswordError,
      placeholderDeletePasswordErrorText,
    } = this.state;
    return (
      <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
        <View style={Styles.modalContainer}>
          <View style={Styles.modalTextContainer}>
            <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
              Veuillez entrer votre mot de passe pour confirmer la suppression de votre compte
            </Text>
          </View>
          <View
            style={[
              accountPasswordError ? Styles.textInputContainerError : Styles.textInputContainer,
              { marginTop: 20 },
            ]}
          >
            <Input
              secureTextEntry
              placeholderTextColor={accountPasswordError ? Colors.error : '#8A94AA'}
              style={Styles.textInputField}
              placeholder={
                accountPasswordError
                  ? placeholderDeletePasswordErrorText
                  : appState.appTextData.get('registration.form.password').value
              }
              onChangeText={this.handlePasswordInput}
            />
          </View>
          <View style={[{ justifyContent: 'space-between' }, Styles.modalFooter]}>
            <Button
              disabled={deleteAccountButtonDisable}
              style={Styles.modalButton}
              bordered
              onPress={this.deleteUserAccount}
            >
              <Text
                style={
                  deleteAccountButtonDisable
                    ? Styles.modalButtonTextDisable
                    : Styles.modalButtonText
                }
              >
                CONFIRMER
              </Text>
            </Button>
            <Button
              style={Styles.modalButton}
              bordered
              onPress={this.changeModalAccoutType.bind(this, 'NONE')}
            >
              <Text style={Styles.modalButtonText}>ANNULER</Text>
            </Button>
          </View>
        </View>
      </Modal>
    );
  };

  //-----------------------------------------------
  // render confirm state
  //-----------------------------------------------
  renderConfirmationState = () => (
    <Modal visible backdropColor="rgba(255, 255, 255, 0.8)">
      <View style={Styles.modalContainer}>
        <View style={Styles.modalTextContainer}>
          <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
            Votre compte a bien été supprimé
          </Text>
        </View>
        <View style={[{ justifyContent: 'center' }, Styles.modalFooter]}>
          <Button style={Styles.modalButton} bordered onPress={this.logoutApp}>
            <Text style={Styles.modalButtonText}>OK</Text>
          </Button>
        </View>
      </View>
    </Modal>
  );

  //-----------------------------------------------
  //
  //-----------------------------------------------
  renderModalCurrentState = () => {
    const { modalType } = this.state;

    switch (modalType) {
      case MODAL_VIEW_TYPE.QUESTION:
        return this.renderQuestionState();
      case MODAL_VIEW_TYPE.INPUT_PASSWORD:
        return this.renderPasswordInputState();
      case MODAL_VIEW_TYPE.CONFIRMATION:
        return this.renderConfirmationState();
      case MODAL_VIEW_TYPE.NONE:
        return <View />;
      default:
        return <View />;
    }
  };

  render() {
    const {
      firstname, lastname, phonenumber, email,
    } = appState.sessionParameters.currentUser;

    const { showDeletingAccountProcess } = this.state;

    const accountName = `${lastname} ${firstname}`;

    if (showDeletingAccountProcess) {
      return (
        <View style={Styles.processingContainer}>
          <Text style={Styles.processingText}>Supprime le compte utilisateur</Text>
          <View style={{ height: 20 }} />
          <BubblesSpinner size={40} />
        </View>
      );
    }

    return (
      <View style={Styles.container}>
        <View style={{ marginLeft: 24 }}>
          <DrawerMenuHeader
            opened
            accountName={accountName}
            onPressMenuButton={this.handlePressBack}
          />
        </View>
        <View style={[{ marginTop: 7 }, Styles.itemMenuContainer]}>
          <Text style={Styles.mainText}>Mon profil</Text>
          <View style={[{ width: 76 }, Styles.itemMenuDivider]} />
        </View>
        <Content>
          <Form style={Styles.formContainer}>
            <TextInputMyProfile
              disabledText={firstname}
              placeholder="Entrez votre prénom"
              placeholderTextColor="#78849E"
              onChangeText={this.handleTextInput.bind(this, 'firstName')}
              onSubmit={this.onSubmit.bind(this, 'firstName')}
            />
            <View style={Styles.inputContainerDivider} />
            <TextInputMyProfile
              disabledText={lastname}
              placeholder="Entrez votre nom"
              placeholderTextColor="#78849E"
              onChangeText={this.handleTextInput.bind(this, 'lastName')}
              onSubmit={this.onSubmit.bind(this, 'lastName')}
            />
            <View style={Styles.inputContainerDivider} />
            <TextInputMyProfile
              disabledText={phonenumber}
              autoCapitalize="none"
              placeholder="Entrer votre numéro de téléphone +33X XXXX XXXX"
              placeholderTextColor="#78849E"
              onChangeText={this.handleTextInput.bind(this, 'phoneNumber')}
              onSubmit={this.onSubmit.bind(this, 'phoneNumber')}
            />
            <View style={Styles.inputContainerDivider} />
            <TextInputMyProfile
              disabledText={email}
              autoCapitalize="none"
              placeholder="Entrez votre Email"
              placeholderTextColor="#78849E"
              onChangeText={this.handleTextInput.bind(this, 'email')}
              onSubmit={this.onSubmit.bind(this, 'email')}
            />
            <View style={Styles.inputContainerDivider} />
            <TextInputMyProfile
              secureText
              disabledText="12345678"
              placeholder="Entrez votre Mot de passe"
              autoCapitalize="none"
              placeholderTextColor="#78849E"
              onChangeText={this.handleTextInput.bind(this, 'password')}
              onSubmit={this.onSubmit.bind(this, 'password')}
            />
          </Form>
          <View style={Styles.menuFooter}>
            <Button
              block
              style={Styles.footerButton}
              bordered
              onPress={this.changeModalAccoutType.bind(this, 'QUESTION')}
            >
              <Text style={Styles.footerButtonText}>Supprimer mon compte</Text>
            </Button>
          </View>
        </Content>
        {this.renderModalCurrentState()}
      </View>
    );
  }
}

export default withNavigation(DrawerMenuMyProfile);
