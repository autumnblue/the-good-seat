import React, { Component } from 'react';
import { Alert, View, AsyncStorage } from 'react-native';
import { Button, Text } from 'native-base';
import { observer } from 'mobx-react';
import { Modal } from '../../../Components';
import appState from '../../../MobX';

import Styles from './Styles/DrawerMenuDeletePaymentMethodDialogStyles';
import { deleteCardFromServer } from '../../../Services/BackEnd/MangoPay';
import { deletePaymentCard } from '../../../Services/BackEnd/TheGoodSeat';

/*
-------------------------------------------------
 modal view state
-------------------------------------------------
*/
const MODAL_VIEW_STATE = {
  NONE: 0,
  QUESTION: 1,
  CONFIRMATION: 2,
};

@observer
class DrawerMenuDeletePaymentMethodDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalViewState: MODAL_VIEW_STATE.QUESTION,
    };
  }

  //-------------------------------------------------------------------
  // remove card from local storage
  //-------------------------------------------------------------------
  removeCardFromLocalStorage = (cardId) => {
    const { onEndDeleting } = this.props;

    let cardsList = [];

    // marker deleting card is active ?
    let deletingCardIsActive = false;

    // 1. load cards list
    AsyncStorage.getItem('paymentCards', (err, result) => {
      if (result !== null) {
        // just copy array
        const cachedCardsList = JSON.parse(result);

        cardsList = cachedCardsList.filter((cardData) => {
          if (cardData.cardId === cardId) {
            deletingCardIsActive = cardData.selected;
          }
          return cardData.cardId !== cardId;
        });
      }

      // just 0 in array is selected (active)
      if (deletingCardIsActive) {
        if (cardsList.length) cardsList[0].selected = true;
      }

      // save new cards list
      AsyncStorage.setItem('paymentCards', JSON.stringify(cardsList), () => {
        if (onEndDeleting) onEndDeleting();
        this.setState({ modalViewState: MODAL_VIEW_STATE.CONFIRMATION });
      });
    });
  };

  //---------------------------------------------------------
  // on cancel
  //---------------------------------------------------------
  handleOnCancel = () => {
    const { onCancel } = this.props;

    if (onCancel) onCancel();
  };

  //----------------------------------------------------------
  // on ok press
  //----------------------------------------------------------
  handleOnOk = () => {
    const { onOk } = this.props;
    this.setState({ modalViewState: MODAL_VIEW_STATE.QUESTION });
    if (onOk) onOk();
  };

  //----------------------------------------------------------
  // handle confirm
  //----------------------------------------------------------
  handleConfirm = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { paymentCard, onBeginDeleting, onEndDeleting } = this.props;

    if (onBeginDeleting) onBeginDeleting();

    deleteCardFromServer(
      appState.constants.MANGOPAY_API,
      appState.constants.MANGOPAY_CLIENTID,
      appState.constants.MANGOPAY_TOKEN,
      paymentCard.cardId,
    ).then(() => {
      deletePaymentCard(
        appState.constants.THEGOODSEAT_API,
        appState.sessionParameters.appToken,
        _id,
        paymentCard._id,
      ).then((data) => {
        if (data.errors) {
          Alert.alert('delete card', data.message);
        }
        if (onEndDeleting) onEndDeleting();
        this.setState({ modalViewState: MODAL_VIEW_STATE.CONFIRMATION });
      });
    });
  };

  render() {
    const { modalViewState } = this.state;

    const { paymentCard, visible } = this.props;

    if (modalViewState === MODAL_VIEW_STATE.QUESTION) {
      return (
        <Modal visible={visible} backdropColor="rgba(255, 255, 255, 0.8)">
          <View style={Styles.container}>
            <View style={Styles.bodyContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                {`Etes vous sûr(e) de vouloir supprimer ${paymentCard.name}?`}
              </Text>
            </View>
            <View style={[{ justifyContent: 'space-between' }, Styles.footer]}>
              <Button style={Styles.modalButton} bordered onPress={this.handleConfirm}>
                <Text style={Styles.modalButtonText}>OK</Text>
              </Button>
              <Button style={Styles.modalButton} bordered onPress={this.handleOnCancel}>
                <Text style={Styles.modalButtonText}>Annuler</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }
    if (modalViewState === MODAL_VIEW_STATE.CONFIRMATION) {
      return (
        <Modal visible={visible} backdropColor="rgba(255, 255, 255, 0.8)">
          <View style={Styles.container}>
            <View style={Styles.bodyContainer}>
              <Text style={[{ textAlign: 'center' }, Styles.mainText]}>
                {`${paymentCard.name} a bien été supprimé`}
              </Text>
            </View>
            <View style={[{ justifyContent: 'center' }, Styles.footer]}>
              <Button style={Styles.modalButton} bordered onPress={this.handleOnOk}>
                <Text style={Styles.modalButtonText}>OK</Text>
              </Button>
            </View>
          </View>
        </Modal>
      );
    }

    return null;
  }
}

export default DrawerMenuDeletePaymentMethodDialog;
