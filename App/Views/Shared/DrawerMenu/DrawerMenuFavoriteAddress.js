import React, { Component } from 'react';
import { Text } from 'native-base';
import { Image, View, TouchableOpacity } from 'react-native';
import { observer, inject } from 'mobx-react';
import MyAddressesInput from '../../Entities';
import { Images } from '../../../Theme';
import { ProgressIndicator } from '../../../Components';

import {
  MAPBOXsearchGeocoding,
  convertFeatureDataToAddressData,
} from '../../../Services/BackEnd/MapBox';

import Styles from './Styles/DrawerMenuMyAddressesStyles';
import {
  addUserAddress,
  deleteUserAddress,
} from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

const SCREEN_RENDER_STATE = {
  LOADING: 0,
  CONTROL: 1,
};

@inject('appState')
@observer
class DrawerMenuFavoriteAddressItem extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      resultsSearch: [],
      editable: false,
      addressDataModel: {},
      screenRenderState: SCREEN_RENDER_STATE.CONTROL,
    };
  }

  componentDidMount() {
    const { addressData } = this.props;

    if (addressData) {
      this.setState({
        addressDataModel: addressData,
        screenRenderState: SCREEN_RENDER_STATE.CONTROL,
      });
    }
  }

  //----------------------------------------------------
  // handle search address
  //----------------------------------------------------
  onSearchAddress = (text) => {
    // only if symbols > 1
    if (text.length > 1) {
      MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
        source: 'mapbox.places',
        accessToken: appState.constants.MAPBOX_TOKEN,
        proximity: '',
        bbox: '',
        autocomplete: true,
        types: 'address,poi',
        limit: 5,
        query: `${text}`,
      })
        .then((data) => {
          if (data && data.features && data.features.length) {
            this.setState({ resultsSearch: data.features });
          }
        })
        .catch((error) => {
          console.error('geocoding API', error.message);
          this.setState({ resultsSearch: [] });
        });
    } else {
      this.setState({ resultsSearch: [] });
    }

    this.setState({ resultsSearch: [] });
  };

  //----------------------------------------------------
  // on press edit
  //----------------------------------------------------
  onPressEdit = () => {
    this.setState({ editable: true });
  };

  //----------------------------------------------------
  // delete address
  //----------------------------------------------------
  deleteAddress = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { addressDataModel } = this.state;

    const { onPostDelete } = this.props;

    this.setState({ screenRenderState: SCREEN_RENDER_STATE.LOADING });

    deleteUserAddress(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      addressDataModel._id,
    ).then(() => {
      this.setState({ addressDataModel: {}, screenRenderState: SCREEN_RENDER_STATE.CONTROL });
      if (onPostDelete) onPostDelete();
    });
  };

  //----------------------------------------------------
  // handle item (adress) select
  //----------------------------------------------------
  handleItemSelect = (feature) => {
    // set state
    this.setState({ screenRenderState: SCREEN_RENDER_STATE.LOADING });

    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { onPostSelected } = this.props;

    const addressData = convertFeatureDataToAddressData(feature);
    addressData.type = null;

    addUserAddress(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      addressData,
    ).then(() => {
      // change state
      this.setState({
        resultsSearch: [],
        editable: false,
        // addressDataModel: data.body,
        screenRenderState: SCREEN_RENDER_STATE.CONTROL,
      });

      if (onPostSelected) onPostSelected();
    });
  };

  //----------------------------------------------------
  // render search results item
  //----------------------------------------------------
  renderResultItem = feature => (
    <TouchableOpacity key={feature.id} onPress={this.handleItemSelect.bind(this, feature)}>
      <View style={Styles.searchResultItem}>
        <Text style={Styles.searchResultText}>{feature.place_name}</Text>
      </View>
    </TouchableOpacity>
  );

  //-----------------------------------------------------
  // render loading state
  //-----------------------------------------------------
  renderLoadingState = () => (
    <View style={{ paddingRight: 20, marginTop: 30 }}>
      <ProgressIndicator color="#D3D9E5" />
    </View>
  );

  //-----------------------------------------------------
  // render control state
  //-----------------------------------------------------
  renderControlState = () => {
    const { resultsSearch, editable, addressDataModel } = this.state;

    const { containerStyle } = this.props;

    return (
      <MyAddressesInput
        editable={editable}
        containerStyle={containerStyle}
        imageLabel={<Image source={Images.favoriteOutline} />}
        labelText="Enregistrer un lieu favori"
        value={addressDataModel.title}
        resultsSearch={resultsSearch}
        renderResultItem={this.renderResultItem}
        onSearchChange={this.onSearchAddress}
        onPressEdit={this.onPressEdit}
        onPostClear={this.deleteAddress}
      />
    );
  };

  //-----------------------------------------------------
  // render current state
  //-----------------------------------------------------
  renderCurrentState = () => {
    const { screenRenderState } = this.state;

    switch (screenRenderState) {
      case SCREEN_RENDER_STATE.LOADING:
        return this.renderLoadingState();
      case SCREEN_RENDER_STATE.CONTROL:
        return this.renderControlState();
      default:
        return this.renderLoadingState();
    }
  };

  render() {
    return this.renderCurrentState();
  }
}

DrawerMenuFavoriteAddressItem.defaultProps = {
  empty: false,
};

export default DrawerMenuFavoriteAddressItem;
