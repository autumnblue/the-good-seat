import React, { Component } from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { Button, Text } from 'native-base';
import { observer, inject } from 'mobx-react';
import moment from 'moment';
import { Modal, DateTimePicker } from '../../../Components';

import Styles from './Styles/DateAndTimeSelectDialogStyles';

let appState = {};

@inject('appState')
@observer
class DateAndTimeSelectDialog extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      dateTimePickerVisible: false,
      dateTimePickerMode: 'date',
    };
  }

  //-------------------------------------------------
  // close dialog
  //-------------------------------------------------
  handleActionDialog = () => {
    const { onOk } = this.props;
    if (onOk) onOk();
    appState.uiState.hideDateAndTimeSelect();
  };

  //-------------------------------------------------
  // cancel
  //-------------------------------------------------
  cancelDateTimeSelect = () => {
    const { onCancel } = this.props;
    if (onCancel) onCancel();
    appState.driveStageState.bookingDateTime = { dateTime: '', changed: false };
    // appState.uiState.hideDateAndTimeSelect();
  };

  //-------------------------------------------------
  // show select date
  //-------------------------------------------------
  openDateSelect = () => {
    appState.uiState.hideDateAndTimeSelect();
    this.setState({ dateTimePickerVisible: true, dateTimePickerMode: 'date' });
  };

  //-------------------------------------------------
  // show time select
  //-------------------------------------------------
  openTimeSelect = () => {
    appState.uiState.hideDateAndTimeSelect();
    this.setState({ dateTimePickerVisible: true, dateTimePickerMode: 'time' });
  };

  //--------------------------------------------------
  // cancel select
  //--------------------------------------------------
  cancelSelect = () => {
    this.setState({ dateTimePickerVisible: false }, () => {
      appState.uiState.showDateAndTimeSelect();
    });
  };

  //--------------------------------------------------
  // handle date select
  //--------------------------------------------------
  handleDateTimeSelect = (dateTime) => {
    this.setState({ dateTimePickerVisible: false, dateTimePickerMode: 'date' }, () => {
      if (dateTime) {
        appState.driveStageState.bookingDateTime = { dateTime, changed: true };
      }
      appState.uiState.showDateAndTimeSelect();
    });
  };

  render() {
    const { changed, dateTime } = appState.driveStageState.bookingDateTime;
    const { visible } = this.props;

    const { dateTimePickerVisible, dateTimePickerMode } = this.state;

    let dateString = moment(new Date(), 'DD/MM/YYYY').format('dddd DD, MMMM');
    let timeString = moment(new Date(), 'DD/MM/YYYY').format('hh:mm');
    let defaultDate = new Date();

    if (changed) {
      timeString = moment(dateTime).format('hh:mm');
      dateString = moment(dateTime, 'DD/MM/YYYY').format('dddd DD, MMMM');
      defaultDate = moment(dateTime).toDate();
      //  console.log('CHANGED!!', timeString);
    }

    console.log('default date - comp', dateString);

    if (dateTimePickerVisible) {
      return (
        <DateTimePicker
          visible={dateTimePickerVisible}
          defaultDate={defaultDate}
          mode={dateTimePickerMode}
          onSelect={this.handleDateTimeSelect}
          onCancel={this.cancelSelect}
        />
      );
    }

    return (
      <Modal backdropColor="rgba(255, 255, 255, 0.8)" visible={visible}>
        <View style={Styles.modalContainer}>
          <TouchableWithoutFeedback onPress={this.openDateSelect}>
            <View style={Styles.dateContainer}>
              <Text style={Styles.mainText}>Dès que possible</Text>
              <View style={Styles.gridRow}>
                <View style={Styles.divider} />
                <Text style={[{ marginHorizontal: 9 }, Styles.secondaryText]}>
                  ou choisissez la date de votre choix
                </Text>
                <View style={Styles.divider} />
              </View>
              <Text style={Styles.mainText}>{dateString}</Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={this.openTimeSelect}>
            <View style={[{ marginTop: 9 }, Styles.timeContainer]}>
              <Text style={Styles.mainText}>Dès que possible</Text>
              <View style={Styles.gridRow}>
                <View style={Styles.divider} />
                <Text style={[{ marginHorizontal: 9 }, Styles.secondaryText]}>
                  ou choisissez l&apos;heure de votre choix
                </Text>
                <View style={Styles.divider} />
              </View>
              <Text style={Styles.mainText}>{timeString}</Text>
            </View>
          </TouchableWithoutFeedback>

          <View style={Styles.actionBar}>
            <Button disabled={!changed} transparent onPress={this.cancelDateTimeSelect}>
              <Text style={!changed ? Styles.buttonTextDisabled : Styles.buttonText}>
                {String('Dès que possible').toUpperCase()}
              </Text>
            </Button>
            <Button transparent onPress={this.handleActionDialog}>
              <Text style={Styles.buttonText}>OK</Text>
            </Button>
          </View>
        </View>
      </Modal>
    );
  }
}

export default DateAndTimeSelectDialog;
