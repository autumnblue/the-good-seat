import React, { Component } from 'react';
import {
  Alert, View, TouchableOpacity, Image,
} from 'react-native';

import { Text, Input as TextInput } from 'native-base';

import { observer, inject } from 'mobx-react';
import { Images } from '../../../Theme';

import {
  MAPBOXsearchGeocoding,
  convertFeatureDataToAddressData,
} from '../../../Services/BackEnd/MapBox';

import Styles from './Styles/DestinationAddressInputStyles';

let appState = {};

@inject('appState')
@observer
class DestinationAddressInput extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      value: '',
      results: [],
    };
  }

  componentDidMount() {
    if (this.addressInputRef) {
      this.addressInputRef._root.focus();
    }
  }

  //---------------------------------------------------------
  // search address (MAPBOX forward geocoding)
  //---------------------------------------------------------
  handleSearchAddress = (text) => {
    this.setState({ value: text });

    // only if symbols > 1
    if (text.length > 1) {
      let { latitude, longitude } = appState.sessionParameters.userLocation;
      const {
        latitude: latitudeStartPoint,
        longitude: longitudeStartPoint,
      } = appState.driveStageState.startingAddressData;
      if (
        appState.driveStageState.startingAddressData
        && appState.driveStageState.startingAddressData.latitude
        && appState.driveStageState.startingAddressData.longitude
      ) {
        latitude = latitudeStartPoint;
        longitude = longitudeStartPoint;
      }
      MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
        source: 'mapbox.places',
        accessToken: appState.constants.MAPBOX_TOKEN,
        proximity: `${longitude},${latitude}`,
        bbox: '',
        autocomplete: true,
        types: 'address,poi',
        limit: 5,
        query: `${text}`,
      })
        .then((data) => {
          if (data && data.features && data.features.length) {
            this.setState({ results: data.features });
          }
        })
        .catch((error) => {
          Alert.alert('[destinationinput]geocoding API', error.message);
          this.setState({ results: [] });
        });
    } else {
      this.setState({ results: [] });
    }

    this.setState({ results: [] });
  };

  //---------------------------------------------------------
  // clear when on focus
  //---------------------------------------------------------
  handleOnFocus = () => {
    this.setState({ value: '' });
  };

  //-----------------------------------------------------------
  // handle select address
  //-----------------------------------------------------------
  handleSelectAddress = (feature) => {
    const { onSelect } = this.props;

    this.setState({ value: feature.place_name, results: [] });

    // app state
    appState.driveStageState.setDestinationAddress(convertFeatureDataToAddressData(feature));

    if (onSelect) onSelect();
  };

  //-----------------------------------------------------------
  // handle on submit
  //-----------------------------------------------------------
  handleOnSubmit = () => {};

  //-----------------------------------------------------------
  // render search result
  //-----------------------------------------------------------
  renderSearchResult = () => {
    const { results } = this.state;

    const resultRenderData = results.map(feature => (
      <TouchableOpacity key={feature.id} onPress={this.handleSelectAddress.bind(this, feature)}>
        <View style={Styles.searchResultItem}>
          <Text style={Styles.searchResultText}>{feature.place_name}</Text>
        </View>
      </TouchableOpacity>
    ));

    return resultRenderData;
  };

  render() {
    const { value } = this.state;

    return (
      <View style={Styles.container}>
        <View style={Styles.inputAddressContainer}>
          <View style={Styles.menuItemPictureContainer}>
            <Image source={Images.flag} />
          </View>
          <View style={Styles.colorContainer}>
            <TextInput
              ref={(ref) => {
                this.addressInputRef = ref;
              }}
              style={Styles.menuInputText}
              value={value}
              placeholder={"Entrez une adresse d'arrivée"}
              placeholderTextColor="#78849E"
              underlineColorAndroid="transparent"
              returnKeyType="search"
              onChangeText={this.handleSearchAddress}
              onFocus={this.handleOnFocus}
              onSubmitEditing={this.handleOnSubmit}
            />
          </View>
        </View>

        <View style={Styles.searchResultContainer}>{this.renderSearchResult()}</View>
      </View>
    );
  }
}

export default DestinationAddressInput;
