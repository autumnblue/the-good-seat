import React, { Component } from 'react';
import {
  Form, Item, Input, View, Text,
} from 'native-base';
import { observer, inject } from 'mobx-react';
import {
  Image, TouchableOpacity, FlatList, Platform,
} from 'react-native';
import StartingAdressInput from './StartingAddressInput';
import DestinationAddressInput from './DestinationAddressInput';
import DateAndTimeSelectDialog from './DateAndTimeSelectDialog';
import { Images, Colors } from '../../../Theme';

import Styles from './Styles/AddressSelectedMenuStyles';
import { getUserAddresses } from '../../../Services/BackEnd/TheGoodSeat';
import {
  MAPBOXsearchGeocoding,
  convertFeatureDataToAddressData,
} from '../../../Services/BackEnd/MapBox';
import { ADDRESS_BAR_VIEW_STATE, MAP_VIEW_STATE } from '../../../MobX/Constants';

let appState = {};

/*
 <TouchableOpacity onPress={onPicturePress}>
        <View style={Styles.menuItemPictureContainer}>{labelPicture}</View>
      </TouchableOpacity>
      <View style={selected ? Styles.dividerSelected : Styles.divider} />
      <TouchableOpacity onPress={onPress}>
        <View style={Styles.mainButton}>
          {pointPicture && <View style={Styles.menuItemPictureContainer}>{pointPicture}</View>}
          <View style={{backgroundColor: '#ff3466'}}>
            <Text numberOfLines={2} style={Styles.menuText}>
              {text}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      {locationButton && (
        <View style={Styles.imageGPS}>
          <TouchableOpacity onPress={onLocationPress}>
            <Image source={Images.gpsPosition} />
          </TouchableOpacity>
        </View>
      )}
      */

/*
--------------------------------------------------------------
 pure function menu item
--------------------------------------------------------------
*/
function MenuItem(props) {
  const {
    selected,
    text,
    labelPicture,
    onPress,
    onPicturePress,
    onLocationPress,
    height,
    locationButton,
  } = props;
  return (
    <TouchableOpacity
      style={[{ height: height && height }, Styles.menuItemContainer]}
      onPress={onPress}
    >
      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity onPress={onPicturePress}>
          <View style={Styles.menuItemPictureContainer}>{labelPicture}</View>
        </TouchableOpacity>
        <View style={selected ? Styles.dividerSelected : Styles.divider} />
        <View style={{ flex: 0.9 }}>
          <Text numberOfLines={2} style={Styles.menuText}>
            {text}
          </Text>
        </View>
        {locationButton && (
          <View style={Styles.imageGPS}>
            <TouchableOpacity onPress={onLocationPress}>
              <Image source={Images.gpsPosition} />
            </TouchableOpacity>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
}

MenuItem.defaultProps = {
  selected: false,
};
/*
----------------------------------------------------------------
 active address
----------------------------------------------------------------
*/
const SELECTED_ADDRESS = {
  STARTING: 0,
  DESTINATION: 1,
};
/*
===================================================================
   menu selected address destination and location point
==================================================================
*/
@inject('appState')
@observer
class AddressSelectedMenu extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      selectedAddress: SELECTED_ADDRESS.STARTING,
      userAddresses: [],
    };
  }

  componentDidMount() {
    this.updateUserAddressesList();
  }

  componentDidUpdate(prevProps) {
    // const { addressBarViewState } = appState.uiState;
    //  if (addressBarViewState === ADDRESS_BAR_VIEW_STATE.OPENED) {
    //    this.updateUserAddressesList();
    // }
    console.log('Did UPDATE', prevProps);
  }

  //--------------------------------------------------
  // update addresses list
  //--------------------------------------------------
  updateUserAddressesList = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    getUserAddresses(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
    ).then((data) => {
      this.setState({
        userAddresses: data.body,
      });
    });
  };

  //-------------------------------------------------------
  // open menu
  //-------------------------------------------------------
  openMenu = () => {
    const { placeName: startingAddressName } = appState.driveStageState.startingAddressData;
    // update and open
    this.updateUserAddressesList();
    appState.uiState.setAddressBarViewState('OPENED');
    // find user location
    if (!startingAddressName) {
      this.gpsLocationPress();
    }
  };

  //-------------------------------------------------------
  // next stage
  //-------------------------------------------------------
  finishEntering = () => {
    if (appState.driveStageState.currentStage === 1) {
      const {
        placeName: startingAddressName,
        latitude,
        longitude,
      } = appState.driveStageState.startingAddressData;
      const { placeName: destinationAddressName } = appState.driveStageState.destinationAddressData;

      if (startingAddressName && destinationAddressName) {
        // centering map (again)
        appState.sessionParameters.mapCenter = {
          longitude: longitude + 0.0000000000045,
          latitude,
        };
        appState.uiState.setAddressBarViewState('CLOSED');
        appState.driveStageState.setStage(2);
        appState.uiState.openStagesMenu();
        appState.uiState.setMapViewState('TAXI_OFFERS');
      } else {
        appState.uiState.setAddressBarViewState('OPENED');
      }
    }
  };

  //------------------------------------------------------
  // select date and time state
  //------------------------------------------------------
  handleSelectDateAndTimeState = () => {
    appState.uiState.showDateAndTimeSelect();
  };

  //-------------------------------------------------------
  // switch view state
  //-------------------------------------------------------
  setViewState = (newState) => {
    if (newState === 'OPENED') {
      this.updateUserAddressesList();
    }
  };

  //-------------------------------------------------------
  // action control
  //-------------------------------------------------------
  controlAction = (typeControl) => {
    const { selectedAddress } = this.state;

    if (typeControl === 'STARTING_ADDRESS') {
      if (selectedAddress === SELECTED_ADDRESS.STARTING) {
        appState.uiState.setAddressBarViewState('ENTER_STARTING_ADDRESS');
      } else {
        this.setState({ selectedAddress: SELECTED_ADDRESS.STARTING });
      }
    } else if (typeControl === 'DESTINATION_ADDRESS') {
      if (selectedAddress === SELECTED_ADDRESS.DESTINATION) {
        appState.uiState.setAddressBarViewState('ENTER_DESTINATION_ADDRESS');
      } else {
        this.setState({ selectedAddress: SELECTED_ADDRESS.DESTINATION });
      }
    }
  };

  //-------------------------------------------------------
  // gps location button press
  //-------------------------------------------------------
  gpsLocationPress = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        MAPBOXsearchGeocoding(
          appState.constants.MAPBOX_GEOCODING_API,
          {
            source: 'mapbox.places',
            accessToken: appState.constants.MAPBOX_TOKEN,
            proximity: `${position.coords.longitude},${position.coords.latitude}`,
            bbox: '',
            autocomplete: true,
            limit: 1,
            // types: '',
            types: 'address,poi',
            query: `${position.coords.longitude},${position.coords.latitude}`,
          },
          error => console.warn('gps API', error.message),
          Platform.OS === 'android'
            ? { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 }
            : { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        )
          .then((data) => {
            if (data && data.features && data.features.length) {
              // app state
              appState.driveStageState.setStartingAddress(
                convertFeatureDataToAddressData(data.features[0]),
              );

              // switch map view
              appState.sessionParameters.setUserLocation({
                longitude: data.features[0].center[0],
                latitude: data.features[0].center[1],
              });
              this.finishEntering();
            }
          })
          .catch((error) => {
            console.warn('geocoding API', error.message);
          });
      },
      error => console.warn('[addressselectedmenu]geocoding API', error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  };

  //-------------------------------------------------------
  // select user address
  //-------------------------------------------------------
  selectUserAddress = (addressId) => {
    const { userAddresses, selectedAddress } = this.state;

    if (userAddresses.length) {
      if (userAddresses.length > addressId) {
        const addressData = userAddresses[addressId];

        if (selectedAddress === SELECTED_ADDRESS.STARTING) {
          // driver engine change state
          appState.driveStageState.setStartingAddress({
            latitude: addressData.lat,
            longitude: addressData.lon,
            placeName: addressData.title,
            region: addressData.region,
            street: addressData.street,
            city: addressData.city,
            country: addressData.country,
            zipCode: addressData.zipCode,
            internalId: addressData._id,
          });

          // switch map state
          appState.uiState.setMapViewState('SHOW_STARTING_POINT');

          // centering map
          appState.sessionParameters.mapCenter = {
            longitude: addressData.lon,
            latitude: addressData.lat,
          };
        } else if (selectedAddress === SELECTED_ADDRESS.DESTINATION) {
          appState.driveStageState.setDestinationAddress({
            latitude: addressData.lat,
            longitude: addressData.lon,
            placeName: addressData.title,
            region: addressData.region,
            street: addressData.street,
            city: addressData.city,
            country: addressData.country,
            zipCode: addressData.zipCode,
            internalId: addressData._id,
          });
        }
      }
    }
    this.finishEntering();
  };

  //-------------------------------------------------------
  // render user address item
  //-------------------------------------------------------
  renderUserAddressItem = ({ item, index }) => {
    if (item.type === 'HOME') {
      return (
        <MenuItem
          text={item.title}
          labelPicture={<Image source={Images.home} />}
          onPress={this.selectUserAddress.bind(this, index)}
        />
      );
    }
    if (item.type === 'WORK') {
      return (
        <MenuItem
          text={item.title}
          labelPicture={<Image source={Images.briefcase} />}
          onPress={this.selectUserAddress.bind(this, index)}
        />
      );
    }
    return (
      <MenuItem
        text={item.title}
        labelPicture={<Image source={Images.favoriteOutline} />}
        onPress={this.selectUserAddress.bind(this, index)}
      />
    );
  };

  //-------------------------------------------------------
  // render user addresses
  //-------------------------------------------------------
  renderSeparator = () => (
    <View style={[{ marginVertical: 6, backgroundColor: '#f4f4f6' }, Styles.menuItemDivider]} />
  );

  //-------------------------------------------------------
  // render user addresses
  //-------------------------------------------------------
  renderUserAddresses = () => {
    const { userAddresses } = this.state;

    return (
      <View style={{ maxHeight: 250 }}>
        <View
          style={[{ marginBottom: 12, backgroundColor: Colors.primary }, Styles.menuItemDivider]}
        />
        <FlatList
          contentContainerStyle={{ paddingBottom: 12 }}
          data={userAddresses}
          extraData={this.state}
          keyExtractor={item => item._id.toString()}
          renderItem={this.renderUserAddressItem}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    );
  };

  //-------------------------------------------------------
  // render covered menu
  //-------------------------------------------------------
  renderCoveredMenu = () => (
    <Form style={Styles.containerCover}>
      <Item style={{ borderBottomColor: 'transparent' }}>
        <Image style={Styles.imageContainer} source={Images.clock} />
        <View style={Styles.divider} />
        <Input style={Styles.menuText} />
      </Item>
    </Form>
  );

  //-------------------------------------------------------
  // render input startting address state
  //-------------------------------------------------------
  renderCloseButton = () => {
    const { addressBarViewState } = appState.uiState;

    const isOpened = addressBarViewState !== ADDRESS_BAR_VIEW_STATE.CLOSED;

    return (
      isOpened && (
        <View>
          <TouchableOpacity
            onPress={() => {
              appState.uiState.setAddressBarViewState('CLOSED');
            }}
            style={Styles.close}
          >
            <Image source={Images.banerClose} />
          </TouchableOpacity>
        </View>
      )
    );
  };

  //-------------------------------------------------------
  // render input startting address state
  //-------------------------------------------------------
  renderStartingAddressState = () => (
    <View style={Styles.container}>
      {this.renderCloseButton()}
      <View style={{ height: 16 }} />
      <StartingAdressInput
        onSelect={() => {
          appState.uiState.setAddressBarViewState('OPENED');
        }}
      />
      <View style={[{ marginVertical: 13.5 }, Styles.menuItemDivider]} />
      {this.renderUserAddresses()}
    </View>
  );

  //-------------------------------------------------------
  // render input destination address state
  //-------------------------------------------------------
  renderDestinationAddressState = () => (
    <View style={Styles.container}>
      {this.renderCloseButton()}
      <View style={{ height: 16 }} />
      <DestinationAddressInput onSelect={this.finishEntering} />
      <View style={[{ marginVertical: 13.5 }, Styles.menuItemDivider]} />
      {this.renderUserAddresses()}
    </View>
  );

  //-------------------------------------------------------
  // render opened menu
  //-------------------------------------------------------
  renderOpenedMenu() {
    const { placeName: starttingAddressName } = appState.driveStageState.startingAddressData;
    const { placeName: destinationAddressName } = appState.driveStageState.destinationAddressData;

    const { selectedAddress } = this.state;

    return (
      <View style={Styles.container}>
        {this.renderCloseButton()}
        <View style={{ height: 16 }} />

        <MenuItem
          selected={selectedAddress === SELECTED_ADDRESS.STARTING}
          locationButton
          text={starttingAddressName || 'Mon adresse de départ'}
          labelPicture={<Image source={Images.home} />}
          onPress={this.controlAction.bind(this, 'STARTING_ADDRESS')}
          onLocationPress={this.gpsLocationPress}
        />
        <View
          style={[{ marginVertical: 11, backgroundColor: '#f4f4f6' }, Styles.menuItemDivider]}
        />
        <MenuItem
          selected={selectedAddress === SELECTED_ADDRESS.DESTINATION}
          text={destinationAddressName || "Entrez une adresse d'arrivée"}
          labelPicture={<Image source={Images.flag} />}
          onPress={this.controlAction.bind(this, 'DESTINATION_ADDRESS')}
        />
        <View style={[{ marginTop: 12 }, Styles.menuItemDivider]} />
        {this.renderUserAddresses()}
      </View>
    );
  }

  //-------------------------------------------------------
  // render destination view state menu
  //-------------------------------------------------------
  renderDestinationViewState = () => {
    const { placeName } = appState.driveStageState.destinationAddressData;

    return (
      <React.Fragment>
        <View style={Styles.container}>
          <MenuItem
            height={47}
            text={placeName || 'Entrez une adresse de destination'}
            pointPicture={<Image style={Styles.clockImage} source={Images.flag} />}
            labelPicture={<Image style={Styles.clockImageDisable} source={Images.clock} />}
          />
        </View>
      </React.Fragment>
    );
  };

  //-------------------------------------------------------
  // render location view state menu
  //-------------------------------------------------------
  renderLocationViewState = () => {
    const { placeName } = appState.driveStageState.startingAddressData;

    return (
      <React.Fragment>
        <View style={Styles.container}>
          <MenuItem
            height={47}
            text={placeName || 'Entrez une adresse de destination'}
            pointPicture={<Image style={Styles.clockImage} source={Images.home} />}
            labelPicture={<Image style={Styles.clockImage} source={Images.clock} />}
            onPicturePress={this.handleSelectDateAndTimeState}
          />
        </View>
      </React.Fragment>
    );
  };

  //-------------------------------------------------------
  // render closed menu
  //-------------------------------------------------------
  renderClosedMenu() {
    return (
      <React.Fragment>
        <View style={Styles.container}>
          <MenuItem
            height={47}
            text="Entrez une adresse de destination"
            labelPicture={<Image style={Styles.clockImage} source={Images.clock} />}
            onPicturePress={this.handleSelectDateAndTimeState}
            onPress={this.openMenu}
          />
        </View>
        <DateAndTimeSelectDialog visible={appState.uiState.dateAndTimeDialogVisible} />
      </React.Fragment>
    );
  }

  render() {
    const { currentStage } = appState.driveStageState;
    const { addressBarViewState, mapViewState, drawerMenuVisible } = appState.uiState;

    if (drawerMenuVisible) return <View />;

    if (currentStage === 1) {
      switch (addressBarViewState) {
        case ADDRESS_BAR_VIEW_STATE.CLOSED:
          return this.renderClosedMenu();
        case ADDRESS_BAR_VIEW_STATE.OPENED:
          return this.renderOpenedMenu();
        case ADDRESS_BAR_VIEW_STATE.ENTER_STARTING_ADDRESS:
          return this.renderStartingAddressState();
        case ADDRESS_BAR_VIEW_STATE.ENTER_DESTINATION_ADDRESS:
          return this.renderDestinationAddressState();
        default:
          return this.renderClosedMenu();
      }
    } else if (
      mapViewState === MAP_VIEW_STATE.WAITING_TAXI
      || mapViewState === MAP_VIEW_STATE.TAXI_OFFERS
    ) {
      return this.renderLocationViewState();
    } else if (mapViewState === MAP_VIEW_STATE.RIDING) {
      return this.renderDestinationViewState();
    } else if (MAP_VIEW_STATE.SHOW_DESTINATION_ROUTE) {
      return this.renderDestinationViewState();
    }

    return this.renderClosedMenu();
  }
}

export default AddressSelectedMenu;
