import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  modalContainer: {
    width: 343,
    borderRadius: 12,
    paddingHorizontal: 7,
    paddingTop: 44,
    paddingBottom: 7,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  gridRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    backgroundColor: '#fff',
    height: 1,
    width: 23,
  },
  mainText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 16,
    lineHeight: 30,
    color: '#fff',
  },
  secondaryText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: '#fff',
  },
  dateContainer: {
    backgroundColor: Colors.primary,
    paddingVertical: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },
  timeContainer: {
    backgroundColor: Colors.secondary,
    paddingVertical: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },
  actionBar: {
    marginTop: 20,
    marginRight: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: '#009FE3',
  },
  buttonTextDisabled: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: Colors.disable,
  },
});
