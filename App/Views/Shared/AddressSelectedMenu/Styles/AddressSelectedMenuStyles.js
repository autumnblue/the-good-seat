import { StyleSheet, Platform } from 'react-native';
import { addFontFamily, Metrics, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    ...Platform.select({
      ios: { marginTop: 36 },
      android: {
        marginTop: 36,
      },
    }),
    marginBottom: 18 * Metrics.scaleFactor,
    marginHorizontal: 16,
    paddingLeft: 16,
    borderRadius: 12,

    ...Platform.select({
      ios: {
        shadowColor: 'rgba(69, 91, 99, 0.08)',
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowRadius: 16,
        shadowOpacity: 1,
      },
      android: {
        elevation: 6,
      },
    }),
  },

  menuItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    // flex: 1,
  },
  mainButton: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 33,
    // width: 210,
  },
  imageGPS: {
    // justifyContent: 'flex-end',
    // alignItems: 'flex-end',
    // marginRight: 37,
    // flex: 1,
  },
  imageClose: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 37,
    marginTop: 10,
    flex: 1,
    zIndex: 1000,
  },
  closeButon: { position: 'absolute', top: 10, left: -70 },
  clockImage: {
    width: 19,
    height: 19,
  },
  clockImageDisable: {
    width: 19,
    height: 19,
  },
  divider: {
    backgroundColor: '#F4F4F6',
    width: 1,
    height: 24,
    marginLeft: 16,
    marginRight: 16,
  },
  dividerSelected: {
    backgroundColor: Colors.primary,
    width: 3,
    height: 24,
    marginLeft: 16,
    marginRight: 16,
  },
  menuText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
    paddingHorizontal: 5,
    flexWrap: 'wrap',
  },
  menuItemInput: {
    //  flexDirection: 'row',
    //  alignItems: 'center',
    marginRight: 40,
  },
  menuItemPictureContainer: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
    // flex: 0.2,
  },
  menuItemDivider: {
    height: 1,
    alignSelf: 'center',
    width: '97%',
    marginRight: 20,
  },
  inputAddressContainer: {
    paddingRight: 32,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
