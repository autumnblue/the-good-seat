import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
  },
  searchResultContainer: {
    paddingRight: 32,
    flexDirection: 'column',
  },
  searchResultItem: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderBottomColor: 'rgba(120,132,158, 0.2)',
    borderBottomWidth: 1,
  },
  searchResultText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
    alignItems: 'center',
  },
  inputAddressContainer: {
    paddingRight: 25,
    flexDirection: 'row',
    alignItems: 'center',
  },
  colorContainer: {
    marginLeft: 16,
    paddingLeft: 17,
    paddingRight: 10,
    flex: 1,
    flexDirection: 'row',
    borderRadius: 20,
    alignItems: 'center',
    height: 33,
    backgroundColor: '#F8f8f8',
  },
  menuItemPictureContainer: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuInputText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
    paddingHorizontal: 5,
    flex: 1,
    alignItems: 'center',
  },
  imageGPS: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});
