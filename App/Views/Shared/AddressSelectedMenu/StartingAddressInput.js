import React, { Component } from 'react';
import {
  Alert, Image, View, TouchableOpacity,
} from 'react-native';

import { Text, Input as TextInput } from 'native-base';

import { observer, inject } from 'mobx-react';

import Styles from './Styles/StartingAddressInputStyles';
import { Images } from '../../../Theme';
import {
  MAPBOXsearchGeocoding,
  convertFeatureDataToAddressData,
} from '../../../Services/BackEnd/MapBox';

let appState = {};

@inject('appState')
@observer
class StartingAddressInput extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      value: '',
      results: [],
    };
  }

  componentDidMount() {
    if (this.addressInputRef) {
      this.addressInputRef._root.focus();
    }
  }

  //-------------------------------------------------------
  // find user position (MAPBOX reverse geocoding)
  //-------------------------------------------------------
  setStartingAddressToUserPosition = () => {
    const { latitude, longitude } = appState.sessionParameters.userLocation;

    const { onSelect } = this.props;

    MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
      source: 'mapbox.places',
      accessToken: appState.constants.MAPBOX_TOKEN,
      proximity: `${longitude},${latitude}`,
      bbox: '',
      autocomplete: true,
      limit: 1,
      // types: '',
      types: 'address,place,poi',
      query: `${longitude},${latitude}`,
    })
      .then((data) => {
        console.log('[STARTING ADDRESS INPUT] geocoding results', data.features);
        if (data && data.features && data.features.length) {
          this.setState({ value: data.features[0].place_name });

          // app state
          appState.driveStageState.setStartingAddress(
            convertFeatureDataToAddressData(data.features[0]),
          );

          if (onSelect) onSelect();
        }
      })
      .catch((error) => {
        Alert.alert('[startingaddress]geocoding API', error.message);
      });
  };

  //---------------------------------------------------------
  // search address (MAPBOX forward geocoding)
  //---------------------------------------------------------
  handleSearchAddress = (text, options = {}) => {
    this.setState({ value: text });

    let { latitude, longitude } = appState.sessionParameters.userLocation;

    const { latitude: latitudeIn, longitude: longitudeIn } = options;

    if (options.latitude && options.longitude) {
      latitude = latitudeIn;
      longitude = longitudeIn;
    }

    // only if symbols > 1
    if (text.length > 1) {
      MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
        source: 'mapbox.places',
        accessToken: appState.constants.MAPBOX_TOKEN,
        proximity: `${longitude},${latitude}`,
        bbox: '',
        autocomplete: true,
        types: 'address,poi',
        limit: 5,
        query: `${text}`,
      })
        .then((data) => {
          if (data && data.features && data.features.length) {
            this.setState({ results: data.features });
          }
        })
        .catch((error) => {
          Alert.alert('geocoding API', error.message);
          this.setState({ results: [] });
        });
    } else {
      this.setState({ results: [] });
    }

    this.setState({ results: [] });
  };

  //---------------------------------------------------------
  // clear when on focus
  //---------------------------------------------------------
  handleOnFocus = () => {
    this.setState({ value: '' });
  };

  //-----------------------------------------------------------
  // handle select address
  //-----------------------------------------------------------
  handleSelectAddress = (feature) => {
    const { onSelect } = this.props;

    this.setState({ value: feature.place_name, results: [] });

    // app state
    console.log('[STARTING ADDRESS INPUT] selecting result', feature);
    appState.driveStageState.setStartingAddress(convertFeatureDataToAddressData(feature));

    // switch map state
    appState.uiState.setMapViewState('SHOW_STARTING_POINT');

    // centering map
    appState.sessionParameters.mapCenter = {
      longitude: feature.center[0],
      latitude: feature.center[1],
    };

    if (onSelect) onSelect();
  };

  //-----------------------------------------------------------
  // render search result
  //-----------------------------------------------------------
  renderSearchResult = () => {
    const { results } = this.state;

    const resultRenderData = results.map(feature => (
      <TouchableOpacity key={feature.id} onPress={this.handleSelectAddress.bind(this, feature)}>
        <View style={Styles.searchResultItem}>
          <Text style={Styles.searchResultText}>{feature.place_name}</Text>
        </View>
      </TouchableOpacity>
    ));

    return resultRenderData;
  };

  render() {
    const { value } = this.state;

    return (
      <View style={Styles.container}>
        <View style={Styles.inputAddressContainer}>
          <View style={Styles.menuItemPictureContainer}>
            <Image source={Images.home} />
          </View>
          <View style={Styles.colorContainer}>
            <TextInput
              ref={(ref) => {
                this.addressInputRef = ref;
              }}
              style={Styles.menuInputText}
              value={value}
              placeholder="Mon adresse de départ"
              placeholderTextColor="#78849E"
              underlineColorAndroid="transparent"
              returnKeyType="search"
              onChangeText={this.handleSearchAddress}
              onFocus={this.handleOnFocus}
            />
            <TouchableOpacity onPress={this.setStartingAddressToUserPosition}>
              <View style={Styles.imageGPS}>
                <Image source={Images.gpsPosition} />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={Styles.searchResultContainer}>{this.renderSearchResult()}</View>
      </View>
    );
  }
}

export default StartingAddressInput;
