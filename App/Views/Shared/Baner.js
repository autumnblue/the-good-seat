import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Image, TouchableWithoutFeedback, View } from 'react-native';
import LoadingScreen from '../Screens/LoadingScreen';
import { Images } from '../../Theme';

import Styles from './Styles/BanerStyles';
import { getAdvertisement } from '../../Services/BackEnd/TheGoodSeat';

let appState = {};

/*
===================================================================
   advertising baner
==================================================================
*/
@inject('appState')
@observer
class Baner extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      banerURL: '',
      loading: true,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    getAdvertisement(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken)
      .then((data) => {
        if (!data.body) {
          this.setState({ loading: false });
          // no data force close
          this.onBanerClose();
        } else if (data.body) {
          this.setState({ banerURL: data.body.Advertisement.image }, () => {
            this.setState({ loading: false });
          });
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        this.onBanerClose();
        console.log('banner loading', err);
      });
  }

  //--------------------------------------------------
  // on end load image
  //--------------------------------------------------
  onLoadBanerEnd = () => {};

  //--------------------------------------------------
  // on close
  //--------------------------------------------------
  onBanerClose = () => {
    const { onClose } = this.props;

    if (onClose) onClose();
  };

  render() {
    const { banerURL, loading } = this.state;
    const { visible } = this.props;

    if (loading) {
      return <LoadingScreen visible processingText="Chargement" opacity={0.7} />;
    }

    if (visible) {
      return (
        <View style={Styles.container}>
          <TouchableWithoutFeedback onPress={this.onBanerClose}>
            <View style={Styles.banerCloseContainer}>
              <Image source={Images.banerClose} />
            </View>
          </TouchableWithoutFeedback>
          <View style={Styles.banerContainer}>
            <Image
              style={Styles.backgroundImage}
              source={{ uri: banerURL }}
              onLoadEnd={this.onLoadBanerEnd}
            />
          </View>
        </View>
      );
    }
    return null;
  }
}

Baner.defaultProps = {
  visible: false,
};

export default Baner;
