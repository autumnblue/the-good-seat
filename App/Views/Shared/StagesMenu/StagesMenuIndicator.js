import React, { Component } from 'react';
import { View } from 'react-native';

import { observer, inject } from 'mobx-react';

import Styles from './Styles/StagesMenuIndicatorStyles';

const INDICATOR_PERCENT_MAPPING = {
  1: '75%',
  2: '50%',
  3: '25%',
  4: '0%',
};
/*
===================================================================
   Stages menu botoom indicator (header)
==================================================================
*/
@inject('appState')
@observer
class StagesMenuIndicator extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { stage } = this.props;

    return (
      <View style={Styles.container}>
        <View
          style={[
            { width: INDICATOR_PERCENT_MAPPING[stage > 4 ? 4 : stage] },
            Styles.indicator,
          ]}
        />
      </View>
    );
  }
}

export default StagesMenuIndicator;
