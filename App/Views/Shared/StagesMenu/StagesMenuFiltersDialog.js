import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Button, Text, Item, Input,
} from 'native-base';

import { observer, inject } from 'mobx-react';
import { RadioButton } from '../../../Components';

import Styles from './Styles/StagesMenuFiltersDialogStyles';
import { Colors } from '../../../Theme';
import { OFFER_SORT, OFFER_FILTER } from '../../../MobX/Constants';

//--------------------------------------------------------
// pure function header section item menu
//--------------------------------------------------------
function HeaderSection(props) {
  const { text } = props;

  return (
    <View>
      <Text style={Styles.mainFont}>{text}</Text>
      <View
        style={[
          {
            backgroundColor: '#9b9b9b',
            width: 272,
            marginTop: 7.5,
            marginBottom: 30.5,
          },
          Styles.divider,
        ]}
      />
    </View>
  );
}

/*
----------------------------------------------------------

----------------------------------------------------------
*/
@inject('appState')
@observer
class StagesMenuFiltersDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortKey: props.sortKey,
      filterKey: props.filterKey,
      promoCode: props.promoCode,
    };
  }

  //------------------------------------------------
  // close dialog (confirm)
  //------------------------------------------------
  handleCloseDialog = () => {
    const { sortKey, filterKey, promoCode } = this.state;
    const { onClose } = this.props;
    if (onClose) onClose({ sortKey, filterKey, promoCode });
  };

  //------------------------------------------------
  // close dialog (cancel)
  //------------------------------------------------
  handleCancelDialog = () => {
    const { onCancel } = this.props;
    if (onCancel) onCancel();
  };

  //-------------------------------------------------
  // change sort
  //-------------------------------------------------
  changeSort = (sortKey) => {
    this.setState({ sortKey });
  };

  //-------------------------------------------------
  // change filter
  //-------------------------------------------------
  changeFilter = (filterKey) => {
    this.setState({ filterKey });
  };

  //----------------------------------------------------
  // change promocode
  //----------------------------------------------------
  changePromoCode = (text) => {
    this.setState({ promoCode: text });
  };

  render() {
    const { sortKey, filterKey } = this.state;

    return (
      <View style={Styles.container}>
        <HeaderSection text="Trier par" />

        <View style={Styles.section}>
          <RadioButton
            checked={sortKey === OFFER_SORT.PRICE_LOW_FIRST}
            label="Prix"
            onPress={this.changeSort.bind(this, OFFER_SORT.PRICE_LOW_FIRST)}
          />

          <RadioButton
            checked={sortKey === OFFER_SORT.WAIT_TIMES_LOW_FIRST}
            containerStyle={{ marginTop: 15 }}
            label="Temps d’attente"
            onPress={this.changeSort.bind(this, OFFER_SORT.WAIT_TIMES_LOW_FIRST)}
          />

          <View style={Styles.centerContainer}>
            <View
              style={[
                {
                  backgroundColor: Colors.primary,
                  width: 95,
                  marginTop: 27.5,
                  marginBottom: 27.5,
                },
                Styles.divider,
              ]}
            />
          </View>

          <HeaderSection text="Type de course" />

          <View style={Styles.gridContainer}>
            <View style={[Styles.rowContainer]}>
              <RadioButton
                checked={filterKey === OFFER_FILTER.TOUS}
                label="Tous"
                onPress={this.changeFilter.bind(this, OFFER_FILTER.TOUS)}
              />

              <RadioButton
                checked={filterKey === OFFER_FILTER.VAN}
                containerStyle={{ marginTop: 15 }}
                label="Van"
                onPress={this.changeFilter.bind(this, OFFER_FILTER.VAN)}
              />

              <RadioButton
                checked={filterKey === OFFER_FILTER.GREEN}
                containerStyle={{ marginTop: 15 }}
                label="Green"
                onPress={this.changeFilter.bind(this, OFFER_FILTER.GREEN)}
              />
            </View>
            <View style={Styles.rowContainer}>
              <RadioButton
                checked={filterKey === OFFER_FILTER.ECONOMIQUE}
                label="Economique"
                onPress={this.changeFilter.bind(this, OFFER_FILTER.ECONOMIQUE)}
              />

              <RadioButton
                checked={filterKey === OFFER_FILTER.TOP}
                containerStyle={{ marginTop: 15 }}
                label="Haut de gamme"
                onPress={this.changeFilter.bind(this, OFFER_FILTER.TOP)}
              />

              <RadioButton
                checked={filterKey === OFFER_FILTER.POOL}
                containerStyle={{ marginTop: 15 }}
                label="Pool"
                onPress={this.changeFilter.bind(this, OFFER_FILTER.POOL)}
              />
            </View>
          </View>

          <View style={Styles.inputContainer}>
            <Item style={{ borderBottomColor: 'transparent' }}>
              <Input style={Styles.inputText} placeholder="Code promo" placeholderTextColor="#78849E" onChangeText={this.changePromoCode} />
            </Item>
          </View>
        </View>
        <View style={Styles.footer}>
          <Button transparent style={Styles.footerButton} onPress={this.handleCancelDialog}>
            <Text style={Styles.buttonText}>ANNULER</Text>
          </Button>

          <Button transparent style={Styles.footerButton} onPress={this.handleCloseDialog}>
            <Text style={Styles.buttonText}>OK</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default StagesMenuFiltersDialog;
