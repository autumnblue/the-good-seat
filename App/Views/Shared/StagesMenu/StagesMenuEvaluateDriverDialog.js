import React, { Component } from 'react';
import { Image, View } from 'react-native';
import {
  Button, Text, Item, Input,
} from 'native-base';

import { observer, inject } from 'mobx-react';
import LoadingScreen from '../../Screens/LoadingScreen';

import { Rating, MiniCard, ErrorMessageBox } from '../../../Components';

// import appState from '../../../MobX';

import Styles from './Styles/StagesMenuEvaluateDriverDialogStyles';
import { leaveReview } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

/*
------------------------------------------------------
 menu item input
------------------------------------------------------
*/
function MenuItemInput(props) {
  const { containerStyle, ...attributes } = props;

  return (
    <Item style={[containerStyle, Styles.inputContainer]}>
      <Input {...attributes} style={Styles.inputText} />
    </Item>
  );
}

@inject('appState')
@observer
class StagesMenuEvaluateDriverDialog extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      comment: '',
      carRating: 1,
      driverRating: 1,
      reviewProcessing: false,
    };
  }

  //--------------------------------------------------
  // reset ride
  //--------------------------------------------------
  resetRide = () => {
    // cancel ride
    appState.uiState.setAddressBarViewState('CLOSED');
    // switch MAP state
    appState.uiState.setMapViewState('NAVIGATION');
    // reset ride engine to default state
    appState.driveStageState.resetRideEngine();
    // stage
    appState.driveStageState.setStage(1);
  };

  //-------------------------------------------------
  // handle text input
  //-------------------------------------------------
  handleTextInput = (text) => {
    if (text && text.length) {
      this.setState({ comment: text });
    }
  };

  //-------------------------------------------------
  // handle driver rating change
  //-------------------------------------------------
  driverRatingChange = (rating) => {
    this.setState({ driverRating: rating });
  };

  //-------------------------------------------------
  // handle car rating change
  //-------------------------------------------------
  carRatingChange = (rating) => {
    this.setState({ carRating: rating });
  };

  //-------------------------------------------------
  // on ok button
  //-------------------------------------------------
  handleOnOK = () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { currentRideData } = appState.driveStageState;
    const { _id } = appState.sessionParameters.currentUser;
    const { onOK } = this.props;
    const { comment, driverRating, carRating } = this.state;

    this.setState({ reviewProcessing: true });

    leaveReview(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      currentRideData._id,
      carRating,
      driverRating,
      comment,
    ).then((data) => {
      this.setState({ reviewProcessing: false });
      if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictionary);
      } else if (onOK) onOK();
    });
  };

  //----------------------------------------------------
  // on cancel button
  //----------------------------------------------------
  handleOnCancel = () => {
    const { onCancel } = this.props;
    this.resetRide();
    if (onCancel) onCancel();
  };

  render() {
    const { reviewProcessing } = this.state;
    const { currentRideData } = appState.driveStageState;

    if (reviewProcessing) {
      return <LoadingScreen visible processingText="Envoi de commentaires" opacity={0.7} />;
    }
    return (
      <View style={Styles.container}>
        <View style={Styles.centerContainer}>
          <Text style={Styles.headerText}>Evaluez votre chauffeur</Text>
        </View>

        <View style={Styles.bodyContainer}>
          <Text style={Styles.mainText}>Notez votre chauffeur</Text>
          <View style={Styles.miniCardContainer}>
            <MiniCard
              avatar={(
                <Image
                  style={{
                    borderRadius: 12,
                    width: 48,
                    height: 48,
                    borderColor: '#f3f3f3',
                    borderWidth: 1,
                  }}
                  source={{ uri: currentRideData.driverPhoto }}
                />
)}
              header={currentRideData.driverName}
              subheader={`${currentRideData.driverRate} / 5`}
            />
            <View style={Styles.ratingContainer}>
              <Rating
                startingRating={parseInt(currentRideData.driverRate, 10)}
                onRatingChange={this.driverRatingChange}
              />
            </View>
          </View>
          <View style={{ height: 19 }} />
          <Text style={Styles.mainText}>Notez le confort</Text>
          <View style={Styles.miniCardContainer}>
            <MiniCard
              avatar={(
                <Image
                  style={{
                    borderRadius: 12,
                    width: 48,
                    height: 48,
                    borderColor: '#f3f3f3',
                    borderWidth: 1,
                  }}
                  source={{ uri: currentRideData.carPhoto }}
                />
)}
              header={currentRideData.carModel}
              subheader={currentRideData.carLicense}
            />
            <View style={Styles.ratingContainer}>
              <Rating
                startingRating={parseInt(currentRideData.carRate, 10)}
                onRatingChange={this.carRatingChange}
              />
            </View>
          </View>
          <View style={{ height: 25 }} />
          <MenuItemInput
            placeholder="Ajouter un commentaire"
            placeholderTextColor="#8a94aa"
            onChangeText={this.handleTextInput}
          />
        </View>
        <View style={Styles.footer}>
          <Button bordered style={Styles.footerButton} onPress={this.handleOnOK}>
            <Text style={Styles.buttonText}>CONFIRMER</Text>
          </Button>

          <Button bordered style={Styles.footerButton} onPress={this.handleOnCancel}>
            <Text style={Styles.buttonText}>ANNULER</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default StagesMenuEvaluateDriverDialog;
