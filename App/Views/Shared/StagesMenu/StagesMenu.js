import React, { Component } from 'react';
import { Animated, View } from 'react-native';

import { observer, inject } from 'mobx-react';
import StagesMenuStage1 from './StagesMenuStage1';
import StagesMenuStage2 from './StagesMenuStage2';
import StagesMenuStage3 from './StagesMenuStage3';
import StagesMenuStage4 from './StagesMenuStage4';
import StagesMenuHeader from './StagesMenuHeader';

import Styles from './Styles/StagesMenuStyles';

let appState = {};

/*
===================================================================
   Меню управления заказом такси (этапы)
==================================================================
*/
@inject('appState')
@observer
class StagesMenu extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
    };
  }

  componentDidMount() {}

  //---------------------------------------------------
  // animate menu
  //---------------------------------------------------
  handleOpenMenu = () => {
    if (appState.uiState.stagesMenuOpen) {
      appState.uiState.closeStagesMenu();
    } else {
      appState.uiState.openStagesMenu();
    }
  };

  //---------------------------------------------------
  // press back button
  //----------------------------------------------------
  pressBackButton = () => {
    if (appState.driveStageState.currentStage === 2) {
      appState.uiState.setAddressBarViewState('CLOSED');
      // switch MAP state
      appState.uiState.setMapViewState('NAVIGATION');
      // reset ride engine to default state
      appState.driveStageState.resetRideEngine();
    } else if (appState.driveStageState.currentStage === 3) {
      // reset providers offers
      appState.driveStageState.providersOffers = [];
      appState.driveStageState.selectedOffer = {};
      appState.driveStageState.setWaitingRouteData(null);
      // switch MAP state
      appState.uiState.setMapViewState('TAXI_OFFERS');
    }

    appState.driveStageState.prevStage();
  };

  //---------------------------------------------------
  // render first stage
  //---------------------------------------------------
  renderFirstStage = () => (
    <React.Fragment>
      <StagesMenuHeader
        header="Etape 1: "
        subheader="Entrez une adresse de destination"
        onOpenMenu={this.handleOpenMenu}
        hasBackButton
      />
      <StagesMenuStage1 />
    </React.Fragment>
  );

  //---------------------------------------------------
  // render second stage
  //---------------------------------------------------
  renderSecondStage = () => (
    <View>
      <StagesMenuHeader
        header="Etape 2: "
        subheader="Choisissez votre compagnie VTC/Taxi"
        onOpenMenu={this.handleOpenMenu}
        onPressBack={this.pressBackButton}
        hasBackButton
      />
      <StagesMenuStage2 />
    </View>
  );

  //---------------------------------------------------
  // render thrid stage
  //---------------------------------------------------
  renderThridStage = () => (
    <React.Fragment>
      <StagesMenuHeader
        header="Etape 3: "
        subheader="Confirmez votre course"
        onOpenMenu={this.handleOpenMenu}
        onPressBack={this.pressBackButton}
      />
      <StagesMenuStage3 />
    </React.Fragment>
  );

  //---------------------------------------------------
  // render fourth stage
  //---------------------------------------------------
  renderFourthStage = () => (
    <React.Fragment>
      <StagesMenuHeader
        header="C'est terminé !"
        subheader="Préparez vous, votre chauffeur arrive"
        onOpenMenu={this.handleOpenMenu}
        onPressBack={this.pressBackButton}
      />
      <StagesMenuStage4 />
    </React.Fragment>
  );

  //---------------------------------------------------
  // render five stage
  //---------------------------------------------------
  renderFiveStage = () => (
    <React.Fragment>
      <StagesMenuHeader
        header="Course en cours"
        subheader="Savourez, vous roulez"
        onOpenMenu={this.handleOpenMenu}
        onPressBack={this.pressBackButton}
      />
      <StagesMenuStage4 />
    </React.Fragment>
  );

  //---------------------------------------------------
  // render current stage
  //---------------------------------------------------
  renderStage = () => {
    switch (appState.driveStageState.currentStage) {
      case 1:
        return this.renderFirstStage();
      case 2:
        return this.renderSecondStage();
      case 3:
        return this.renderThridStage();
      case 4:
        return this.renderFourthStage();
      case 5:
        return this.renderFiveStage();
      default:
        return this.renderFirstStage();
    }
  };

  render() {
    const { stagesMenuTopBorder } = appState.uiState;

    return (
      <Animated.View
        style={[
          Styles.container,
          {
            height: stagesMenuTopBorder,
          },
        ]}
      >
        {this.renderStage()}
      </Animated.View>
    );
  }
}

export default StagesMenu;
