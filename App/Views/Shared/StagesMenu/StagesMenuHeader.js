import React, { Component } from 'react';
import { Text } from 'native-base';
import {
  Image, TouchableWithoutFeedback, TouchableOpacity, View,
} from 'react-native';
import { observer, inject } from 'mobx-react';
import moment from 'moment/moment';
import StagesMenuIndicator from './StagesMenuIndicator';

import Styles from './Styles/StagesMenuStyles';
import { Images } from '../../../Theme';

let appState = {};

/*
===================================================================
   Stage one screen
==================================================================
*/
@inject('appState')
@observer
class StagesMenuHeader extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {};
  }

  componentDidMount() {}

  //---------------------------------------------------
  // open menu
  //---------------------------------------------------
  handleOpenMenu = () => {
    const { onOpenMenu } = this.props;

    if (onOpenMenu) onOpenMenu();
  };

  //---------------------------------------------------
  // show trip filters dialog
  //---------------------------------------------------
  handleShowTripFiltersDialog = () => {
    appState.uiState.openTripFiltersDialog();
  };

  render() {
    const { header, subheader, onPressBack } = this.props;

    if (appState.driveStageState.currentStage === 1) {
      return (
        <React.Fragment>
          <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
          <View style={Styles.headerContainer}>
            <View style={Styles.backButtonContainer} />
            <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
              <View style={Styles.stageContainer}>
                <Text style={Styles.stageText}>{header}</Text>
                <Text style={Styles.infoText}>{subheader}</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </React.Fragment>
      );
    }
    if (appState.driveStageState.currentStage === 2) {
      return (
        <View>
          <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
          <View style={Styles.headerContainer}>
            <TouchableOpacity onPress={onPressBack}>
              <View style={Styles.backButtonContainer}>
                <Text style={{ fontSize: 24, color: '#2a2e43' }}>&#8249;</Text>
              </View>
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
              <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                <View style={Styles.stageContainer}>
                  <Text style={Styles.stageText}>{header}</Text>
                  <Text style={Styles.infoText}>{subheader}</Text>
                </View>
                <View style={Styles.filterContainer}>
                  <TouchableOpacity onPress={this.handleShowTripFiltersDialog}>
                    <Image style={Styles.filterImage} source={Images.filters} />
                    <Text style={Styles.secondaryText}>Trier par</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      );
    }
    if (appState.driveStageState.currentStage === 3) {
      const { changed, dateTime } = appState.driveStageState.bookingDateTime;
      const { selectedOffer } = appState.driveStageState;

      if (changed) {
        return (
          <React.Fragment>
            <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
            <View style={Styles.headerContainer}>
              <TouchableOpacity onPress={onPressBack}>
                <View style={Styles.backButtonContainer}>
                  <Text style={{ fontSize: 24, color: '#2a2e43' }}>&#8249;</Text>
                </View>
              </TouchableOpacity>
              <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={Styles.stageContainer}>
                    <Text style={Styles.stageText}>{header}</Text>
                    <Text style={Styles.infoText}>{subheader}</Text>
                  </View>
                  <View style={Styles.dateTimeContainer}>
                    <Text style={Styles.dateTimeText}>
                                        Le
                      {' '}
                      {moment(dateTime, 'DD/MM/YYYY').format('DD/MM/YY')}
                    </Text>
                    <Text style={Styles.dateTimeText}>
                                        à
                      {' '}
                      {moment(dateTime).format('hh:mm')}
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </React.Fragment>
        );
      }
      return (
        <React.Fragment>
          <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
          <View style={Styles.headerContainer}>
            <TouchableOpacity onPress={onPressBack}>
              <View style={Styles.backButtonContainer}>
                <Text style={{ fontSize: 24, color: '#2a2e43' }}>&#8249;</Text>
              </View>
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
              <View style={{ flexDirection: 'row' }}>
                <View style={Styles.stageContainer}>
                  <Text style={Styles.stageText}>{header}</Text>
                  <Text style={Styles.infoText}>{subheader}</Text>
                </View>
                <View style={Styles.durationContainer}>
                  <Text style={Styles.durationText}>
                    {`${
                      selectedOffer.arrivalTime
                        ? `${Number(parseInt(selectedOffer.arrivalTime, 10) / 60).toFixed(0)} min`
                        : ''
                    }`}
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </React.Fragment>
      );
    }
    if (appState.driveStageState.currentStage === 4) {
      const { duration } = appState.driveStageState.currentRideData;

      return (
        <React.Fragment>
          <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
          <View style={Styles.headerContainer}>
            <View style={Styles.backButtonContainer} />
            <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
              <View style={{ flexDirection: 'row' }}>
                <View style={Styles.stageContainer}>
                  <Text style={Styles.stageText}>{header}</Text>
                  <Text style={Styles.infoText}>{subheader}</Text>
                </View>
                <View style={Styles.durationContainer}>
                  <Text style={Styles.durationText}>
                    {duration ? Number(parseInt(duration, 10) / 60).toFixed(0) : '-'}
&nbsp;min
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </React.Fragment>
      );
    }
    if (appState.driveStageState.currentStage === 5) {
      const { duration } = appState.driveStageState.currentRideData;

      return (
        <React.Fragment>
          <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
          <View style={Styles.headerContainer}>
            <View style={Styles.backButtonContainer} />
            <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
              <View style={{ flexDirection: 'row' }}>
                <View style={Styles.stageContainer}>
                  <Text style={Styles.stageText}>{header}</Text>
                  <Text style={Styles.infoText}>{subheader}</Text>
                </View>
                <View style={Styles.durationContainer}>
                  <Text style={Styles.durationText}>Arrivée estimée</Text>
                  <Text style={Styles.durationText}>
                    {duration ? Number(parseInt(duration, 10) / 60).toFixed(0) : '-'}
&nbsp;min
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </React.Fragment>
      );
    }
    // default
    return (
      <React.Fragment>
        <StagesMenuIndicator stage={appState.driveStageState.currentStage} />
        <View style={Styles.headerContainer}>
          <View style={Styles.backButtonContainer} />
          <TouchableWithoutFeedback onPress={this.handleOpenMenu}>
            <View style={Styles.stageContainer}>
              <Text style={Styles.stageText}>{header}</Text>
              <Text style={Styles.infoText}>{subheader}</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </React.Fragment>
    );
  }
}

export default StagesMenuHeader;
