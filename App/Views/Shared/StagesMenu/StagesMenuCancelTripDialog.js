import React, { Component } from 'react';
import { Image, View } from 'react-native';
import { Button, Text } from 'native-base';

import { observer, inject } from 'mobx-react';
import { MiniCard, Icon, ErrorMessageBox } from '../../../Components';
import LoadingScreen from '../../Screens/LoadingScreen';

import { Images } from '../../../Theme';

import Styles from './Styles/StagesMenuCancelTripDialogStyles';
import { cancelCurrentRide } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

@inject('appState')
@observer
class StagesMenuCancelTripDialog extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      showConfirmer: false,
      showProcessing: false,
    };
  }

  //-------------------------------------------------
  // close dialog
  //-------------------------------------------------
  closeDialog = () => {
    const { onPressCancel } = this.props;
    if (onPressCancel) onPressCancel();
  };

  //----------------------------------------------------
  // switch to next
  //----------------------------------------------------
  showConfirmer = () => {
    const { language, errorsDictonary } = appState.sessionParameters;
    const { providerCode } = appState.driveStageState.paidOffer;

    this.setState({ showProcessing: true });

    cancelCurrentRide(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      providerCode,
    ).then((data) => {
      if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictonary);
      }
      console.log('cancel trip data', data);
      this.setState({ showConfirmer: true, showProcessing: false });
    });
  };

  //----------------------------------------------------
  // after cancel trip
  //----------------------------------------------------
  afterCancelTrip = () => {
    const { onAfterCancelTrip } = this.props;

    if (onAfterCancelTrip) onAfterCancelTrip();
    // appState.uiState.setAddressBarViewState('CLOSED');
    // switch MAP state
    // appState.uiState.setMapViewState('NAVIGATION');
    // reset ride engine to default state
    //  appState.driveStageState.resetRideEngine();
    // appState.driveStageState.setStage(1);
    // appState.uiState.hideCancelTripDialog();
  };

  render() {
    const { currentRideData } = appState.driveStageState;

    const { showConfirmer, showProcessing } = this.state;

    if (showProcessing) {
      return <LoadingScreen visible processingText="Annulation du voyage" opacity={0.7} />;
    }

    if (showConfirmer) {
      return (
        <View style={Styles.container}>
          <View style={Styles.centerConfirmerContainer}>
            <Text style={Styles.mainText}>Votre course a bien été annulée</Text>
          </View>

          <View style={Styles.footerConfirmer}>
            <Button bordered style={Styles.footerButton} onPress={this.afterCancelTrip}>
              <Text style={Styles.buttonText}>OK</Text>
            </Button>
          </View>
        </View>
      );
    }
    return (
      <View style={Styles.container}>
        <View style={Styles.centerContainer}>
          <Text style={Styles.mainText}>Etes-vous sûr(e) d&apos;annuler votre course ?</Text>
          <Text style={[{ marginTop: 6 }, Styles.secondaryText]}>Frais d&apos;annulation 2€</Text>
        </View>

        <View style={Styles.miniCardContainer}>
          <MiniCard
            avatar={(
              <Image
                style={{
                  borderRadius: 12,
                  width: 48,
                  height: 48,
                  borderColor: '#f3f3f3',
                  borderWidth: 1,
                }}
                source={{ uri: currentRideData.driverPhoto }}
              />
)}
            header={currentRideData.driverName}
            subheader={`${currentRideData.driverRate} / 5`}
          />
          <MiniCard
            avatar={(
              <Image
                style={{
                  borderRadius: 12,
                  width: 48,
                  height: 48,
                  borderColor: '#f3f3f3',
                  borderWidth: 1,
                }}
                source={{ uri: currentRideData.carPhoto }}
              />
)}
            header={currentRideData.carModel}
            subheader={currentRideData.carLicense}
          />
        </View>

        <View style={Styles.contactActionContainer}>
          <Icon containerStyle={Styles.contactButton} color="#fff" size={25} name="N">
            <Image source={Images.phone} />
          </Icon>

          <Icon containerStyle={Styles.contactButton} color="#fff" size={25} name="N">
            <Image source={Images.message} />
          </Icon>
        </View>

        <View style={Styles.footer}>
          <Button bordered style={Styles.footerButton} onPress={this.showConfirmer}>
            <Text style={Styles.buttonText}>OUI</Text>
          </Button>

          <Button bordered style={Styles.footerButton} onPress={this.closeDialog}>
            <Text style={Styles.buttonText}>NON</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default StagesMenuCancelTripDialog;
