import React, { Component } from 'react';
import { Text } from 'native-base';
import {
  Alert, ScrollView, View, AsyncStorage,
} from 'react-native';
import { withNavigation } from 'react-navigation';

import { observer, inject } from 'mobx-react';

import {
  TripCard, BubblesSpinner, Modal, ErrorMessageBox,
} from '../../../Components';

import StagesMenuEvaluateDriverDialog from './StagesMenuEvaluateDriverDialog';

import Styles from './Styles/StagesMenuStyles';
import { TRIP_CARD_TYPES } from '../../../MobX/Constants';
import {
  getLastestRides,
  getCurrentRide,
  getProviders,
} from '../../../Services/BackEnd/TheGoodSeat';
import {
  MAPBOXsearchGeocoding,
  convertFeatureDataToAddressData,
} from '../../../Services/BackEnd/MapBox';

import LoadingScreen from '../../Screens/LoadingScreen';

const SCREEN_RENDER_STATE = {
  LOADING: 0,
  EMPTY: 1,
  DATA: 2,
};

let appState = {};

/*
===================================================================
   Stage one screen
==================================================================
*/
@inject('appState')
@observer
class StagesMenuStage1 extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      renderState: SCREEN_RENDER_STATE.LOADING,
      showEvaluate: false,
      restoreTrip: false,
    };
  }

  componentDidMount() {
    // fetch lastes rides
    this.fetchLastesRides();

    // reset states
    appState.driveStageState.setStartingAddress({});
    appState.driveStageState.setDestinationAddress({});
    appState.uiState.setMapViewState('NAVIGATE');

    // test ride engine
    // this.testRideEngine();
    this.testCurrentRide();
  }

  //------------------------------------------------------
  // fetch lastes rides
  //------------------------------------------------------
  fetchLastesRides = () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { navigation } = this.props;

    getLastestRides(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
    ).then((data) => {
      if (data.errors) {
        if (data.message.includes('error_invalid_token')) {
          // navigate to first screen
          AsyncStorage.clear();
          appState.uiState.reset();
          navigation.navigate('logon');
        }
      }

      appState.driveStageState.lastestRides = data.body;
      // switch state
      if (data.body.length) {
        this.setState({ renderState: SCREEN_RENDER_STATE.DATA });
      } else {
        this.setState({ renderState: SCREEN_RENDER_STATE.EMPTY });
      }
    });
  };

  //------------------------------------------------------
  // set starting (origin) address
  //------------------------------------------------------
  setStartingAddress = async ({ latitude, longitude }) => {
    if (latitude && longitude) {
      try {
        const data = await MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
          source: 'mapbox.places',
          accessToken: appState.constants.MAPBOX_TOKEN,
          proximity: `${longitude},${latitude}`,
          bbox: '',
          autocomplete: true,
          limit: 1,
          // types: '',
          types: 'address,place,poi',
          query: `${longitude},${latitude}`,
        });

        if (data) {
          if (data && data.features && data.features.length) {
            // app state
            appState.driveStageState.setStartingAddress(
              convertFeatureDataToAddressData(data.features[0]),
            );

            // switch map view
            appState.sessionParameters.setUserLocation({
              longitude: data.features[0].center[0],
              latitude: data.features[0].center[1],
            });

            console.log('address');

            return true;
          }
          return false;
        }
      } catch (error) {
        Alert.alert('geocoding API', error.message);
        return false;
      }
    }
    return false;
  };

  //------------------------------------------------------
  // set destination address
  //------------------------------------------------------
  setDestinationAddress = async ({ latitude, longitude }) => {
    if (latitude && longitude) {
      try {
        const data = await MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
          source: 'mapbox.places',
          accessToken: appState.constants.MAPBOX_TOKEN,
          proximity: `${longitude},${latitude}`,
          bbox: '',
          autocomplete: true,
          limit: 1,
          // types: '',
          types: 'address,poi',
          query: `${longitude},${latitude}`,
        });

        if (data) {
          if (data && data.features && data.features.length) {
            // app state
            appState.driveStageState.setDestinationAddress(
              convertFeatureDataToAddressData(data.features[0]),
            );

            console.log('dest address');

            return true;
          }
          return false;
        }
      } catch (error) {
        Alert.alert('geocoding API', error.message);
        return false;
      }
    }
    return false;
  };

  //-----------------------------------------------------
  // handle repeat trip
  //-----------------------------------------------------
  handleRepeatTrip = async (rideData) => {
    const {
      startLat, startLon, endLat, endLon,
    } = rideData;
    await this.setStartingAddress({ latitude: startLat, longitude: startLon });
    await this.setDestinationAddress({ latitude: endLat, longitude: endLon });
    // centering map
    appState.sessionParameters.mapCenter = {
      longitude: startLon,
      latitude: startLat,
    };
    appState.uiState.setMapViewState('TAXI_OFFERS');
    appState.driveStageState.setStage(2);
  };

  //----------------------------------------------------
  // sent review trip
  //----------------------------------------------------
  onSentReviewTrip = (rideData) => {
    console.log('ride', rideData.driverPhoto);
    appState.driveStageState.currentRideData = rideData;
    this.setState({ showEvaluate: true });
  };

  //----------------------------------------------------
  // close evaluate driver dialog
  //----------------------------------------------------
  closeEvaluateDriver = () => {
    this.setState({ showEvaluate: false });
    // reset ride engine to default state
    appState.driveStageState.resetRideEngine();
  };

  //---------------------------------------------------
  // finish ride!!!
  //---------------------------------------------------
  finishRide = () => {
    this.setState({ showEvaluate: false });
    // reset ride engine to default state
    appState.driveStageState.resetRideEngine();
    // fetch lastest rides
    this.fetchLastesRides();
  };

  //----------------------------------------------------
  // test current ride
  //----------------------------------------------------
  testCurrentRide = async () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    let rideData;
    try {
      rideData = await getCurrentRide(
        appState.constants.THEGOODSEAT_API,
        appState.sessionParameters.appToken,
      );
    } catch (e) {
      console.log('TRYING TO GET CURRENT TRIP ', e);
      return;
    }
    console.log('current trip[Stage1.js]', rideData);

    if (rideData && rideData.body) {
      if (rideData.body.status.length) {
        this.setState({ restoreTrip: true });
        // set need data
        const {
          startLat, startLon, endLat, endLon,
        } = rideData.body;
        await this.setStartingAddress({ latitude: startLat, longitude: startLon });
        await this.setDestinationAddress({ latitude: endLat, longitude: endLon });
        // switch user location
        appState.sessionParameters.userLocation = {
          latitude: startLat,
          longitude: startLon,
        };

        // centering map
        appState.sessionParameters.mapCenter = {
          longitude: startLon,
          latitude: startLat,
        };

        // cache data
        const offer = rideData.body;

        // fetch providers data
        const providersData = await getProviders(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
        );

        const currentProvider = providersData.body.find(
          provider => provider.code === offer.providerCode,
        );

        if (currentProvider) {
          // set current offfer
          appState.driveStageState.selectedOffer = {
            ...offer,
            providerCode: currentProvider.code,
            providerName: currentProvider.name,
            availableCars: offer.availableCars ? offer.availableCars : [],
            colorCode: currentProvider.colorCode,
            canCancelRide: currentProvider.canCancelRide,
            canModifyDestAddress: currentProvider.canModifyDestAddress,
            canModifyPickupAddress: currentProvider.canModifyPickupAddress,
            currentProvider,
          };
        } else {
          // set current offfer
          appState.driveStageState.selectedOffer = {
            ...offer,
          };
        }

        this.setState({ restoreTrip: false }, () => {
          appState.driveStageState.setStage(4);
          appState.uiState.openStagesMenu();
        });
      }
    } else if (rideData.message || rideData.errors) {
      if (!rideData.message === 'error_no_active_ride') {
        ErrorMessageBox(language, rideData.message, errorsDictionary);
      } else if (!rideData.message.includes('error_no_active_ride')) {
        ErrorMessageBox(language, rideData.message, errorsDictionary);
      }
    }
  };

  //----------------------------------------------------
  // start ride engine
  //----------------------------------------------------
  testRideEngine = async () => {
    console.log('ride engine - start');

    // const { providerCode } = appState.driveStageState.paidOffer;

    this.currentRideTimerID = setInterval(() => {
      getCurrentRide(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken).then(
        (result) => {
          console.log(result);

          if (result.body) {
            // set needed data

            // await this.setStartingAddress({ latitude: startLat, longitude: startLon });
            // await this.setStartingAddress({ latitude: startLat, longitude: startLon });
            // await this.setDestinationAddress({ latitude: endLat, longitude: endLon });

            if (result.body.status === 'accepted') {
              // set current ride data
              appState.driveStageState.currentRideData = result.body;

              // change map view render state
            } else if (result.body.status === 'completed') {
              console.log('ride engine - stop');
              clearInterval(this.currentRideTimerID);
              // show evaluate
              console.log('end ride');
            }
          } else {
            console.log('ride engine - empty');
            clearInterval(this.currentRideTimerID);
          }
        },
      );
    }, 1000);
  };

  //-----------------------------------------------------
  // render last trip
  //-----------------------------------------------------
  renderLastTrip = () => {
    const { lastestRides } = appState.driveStageState;

    const lastTripsRenderData = lastestRides.map((rideData) => {
      if (!rideData.status.includes('user_cancel')) {
        return (
          <React.Fragment key={rideData._id}>
            <TripCard
              containerStyle={{ backgroundColor: '#ffde23' }}
              type={TRIP_CARD_TYPES.ENDED}
              rideData={rideData}
              onRepeatTrip={this.handleRepeatTrip.bind(this, rideData)}
              onSentReviewTrip={this.onSentReviewTrip.bind(this, rideData)}
            />
            <View style={{ width: 20 }} />
          </React.Fragment>
        );
      }
      return null;
    });

    return lastTripsRenderData;
  };

  //---------------------------------------------------
  // render empty state
  //---------------------------------------------------
  renderEmptyState = () => (
    <View style={Styles.processingContainer}>
      <Text style={Styles.processingText}>Vous n&rsquo;avez pas encore de course enregistrée</Text>
    </View>
  );

  //---------------------------------------------------
  // render loading state
  //---------------------------------------------------
  renderLoadingState = () => (
    <View style={Styles.processingContainer}>
      <Text style={Styles.processingText}>Traitement de l&rsquo;information en cours</Text>
      <View style={{ height: 20 }} />
      <BubblesSpinner size={40} />
    </View>
  );

  //---------------------------------------------------
  // render array state
  //---------------------------------------------------
  renderDataState = () => (
    <React.Fragment>
      <View style={Styles.subheaderMenu}>
        <Text style={Styles.secondaryText}>Mes dernières courses</Text>
      </View>
      <ScrollView
        contentContainerStyle={Styles.horizontalScrollContainer}
        automaticallyAdjustContentInsets={false}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator
        bounces={false}
        scrollsToTop={false}
      >
        {this.renderLastTrip()}
      </ScrollView>
    </React.Fragment>
  );

  //---------------------------------------------------
  // render opened menu
  //---------------------------------------------------
  renderOpenedMenu = () => {
    const { renderState } = this.state;

    switch (renderState) {
      case SCREEN_RENDER_STATE.EMPTY:
        return this.renderEmptyState();
      case SCREEN_RENDER_STATE.LOADING:
        return this.renderLoadingState();
      case SCREEN_RENDER_STATE.DATA:
        return this.renderDataState();
      default:
        return this.renderLoadingState();
    }
  };

  render() {
    const { showEvaluate, restoreTrip } = this.state;

    if (restoreTrip) {
      return (
        <Modal visible>
          <LoadingScreen visible processingText="Remise en état du voyage" opacity={0.7} />
        </Modal>
      );
    }

    return (
      <React.Fragment>
        {appState.uiState.stagesMenuOpen && this.renderOpenedMenu()}
        <Modal visible={showEvaluate}>
          <StagesMenuEvaluateDriverDialog
            onOK={this.finishRide}
            onCancel={this.closeEvaluateDriver}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

export default withNavigation(StagesMenuStage1);
