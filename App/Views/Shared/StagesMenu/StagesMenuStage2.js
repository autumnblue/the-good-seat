import React, { Component } from 'react';
import { Text, Button } from 'native-base';
import {
  View, ScrollView, BackHandler, Platform,
} from 'react-native';
import { observer, inject } from 'mobx-react';
import {
  Modal,
  VTCButton,
  BubblesSpinner,
  ProgressIndicator,
  ErrorMessageBox,
  MessageBox,
} from '../../../Components';

import DateAndTimeSelectDialog from '../AddressSelectedMenu/DateAndTimeSelectDialog';

import StagesMenuFiltersDialog from './StagesMenuFiltersDialog';

import { OFFER_SORT } from '../../../MobX/Constants';

import SecondStyles from './Styles/StagesMenuStage2Styles';
import { providerOffers, getProviders, redeemCode } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

const SCREEN_RENDER_STATE = {
  LOADING: 0,
  EMPTY: 1,
  DATA: 2,
};

const FILTER_KEY_MAPPING = {
  0: 'all',
  1: 'van',
  2: 'green',
  3: 'eco',
  4: 'luxe',
  5: 'pool',
};

/*
===================================================================
Stage one screen
==================================================================
*/
@inject('appState')
@observer
class StagesMenuStage2 extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      renderState: SCREEN_RENDER_STATE.LOADING,
      sortKey: 0,
      filterKey: 0,
      isLoading: true,
    };

    this.componentIsMounted = false;
  }

  componentDidMount() {
    // mount
    this.componentIsMounted = true;
    // get offers (default sortKey and filterKey)
    this.getOffers(0, 0);
    if (Platform.OS === 'android') {
      console.log('add event');
      BackHandler.addEventListener('hardwareBackPress', this.backButtonPress);
    }
  }

  componentWillUnmount() {
    this.componentIsMounted = false;
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.backButtonPress);
    }
  }

  //------------------------------------------------
  // only for Android
  //------------------------------------------------
  backButtonPress = () => {
    appState.driveStageState.prevStage();
    return true;
  };

  //----------------------------------------------------
  // fetch offers
  //----------------------------------------------------
  getOffers = (sortKey, filterKey, promoCode) => {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { bookingDateTime } = appState.driveStageState;
    // reset
    appState.driveStageState.selectedOffer = false;
    if (this.componentIsMounted) appState.uiState.showTripButton = false;

    // calculate start date
    const startDate = bookingDateTime.changed ? bookingDateTime.dateTime.toISOString() : '';

    console.log('startDate', startDate);

    // prepare data for statistics
    const statisticsData = {
      startRegion: appState.driveStageState.startingAddressData.region,
      startStreet: appState.driveStageState.startingAddressData.street,
      startCity: appState.driveStageState.startingAddressData.city,
      startCountry: appState.driveStageState.startingAddressData.country,
      startZipCode: appState.driveStageState.startingAddressData.zipCode,
      endRegion: appState.driveStageState.destinationAddressData.region,
      endStreet: appState.driveStageState.destinationAddressData.street,
      endCity: appState.driveStageState.destinationAddressData.city,
      endCountry: appState.driveStageState.destinationAddressData.country,
      endZipCode: appState.driveStageState.destinationAddressData.zipCode,
      startFullAddress: appState.driveStageState.startingAddressData.placeName,
      endFullAddress: appState.driveStageState.destinationAddressData.placeName,
    };
    // fetch providers
    getProviders(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken).then(
      (providersData) => {
        console.log('providerData', providersData);
        if (providersData.errors) {
          // comment this https://trello.com/c/lHdSzkBD
          // all cancel
          // appState.uiState.setAddressBarViewState('CLOSED');
          // switch MAP state
          // appState.uiState.setMapViewState('NAVIGATION');
          // reset ride engine to default state
          // appState.driveStageState.resetRideEngine();
          // appState.driveStageState.setStage(1);
          ErrorMessageBox(language, providersData.message, errorsDictionary);
        }
        if (providersData.body) {
          const offerPromises = [];
          this.setState({ isLoading: true });
          providersData.body.forEach((provider) => {
            // fetch offers
            const p = providerOffers(
              appState.constants.THEGOODSEAT_API,
              appState.sessionParameters.appToken,
              provider.code,
              startDate,
              FILTER_KEY_MAPPING[filterKey],
              appState.driveStageState.startingAddressData,
              appState.driveStageState.destinationAddressData,
              statisticsData,
            );
            offerPromises.push(p);

            p.then((offerData) => {
              console.log('offerData', offerData);
              if (offerData.length && offerData[0].offers) {
                offerData[0].offers.forEach((offer) => {
                  // select first
                  if (!appState.driveStageState.selectedOffer) {
                    appState.driveStageState.selectedOffer = {
                      ...offer,
                      providerCode: provider.code,
                      providerName: provider.name,
                      availableCars: offer.availableCars ? offer.availableCars : [],
                      colorCode: provider.colorCode,
                      canCancelRide: provider.canCancelRide,
                      canModifyDestAddress: provider.canModifyDestAddress,
                      canModifyPickupAddress: provider.canModifyPickupAddress,
                      providerUrl: offer.providerUrl,
                      provider,
                    };
                    appState.uiState.showTripButton = true;
                  } else if (
                    !appState.uiState.showTripButton
                    && appState.driveStageState.currentStage === 2
                  ) {
                    appState.uiState.showTripButton = true;
                  }
                  // console.log(offer);
                  appState.driveStageState.providersOffers.push({
                    ...offer,
                    providerCode: provider.code,
                    providerName: provider.name,
                    availableCars: offer.availableCars,
                    colorCode: provider.colorCode,
                    canCancelRide: provider.canCancelRide,
                    canModifyDestAddress: provider.canModifyDestAddress,
                    canModifyPickupAddress: provider.canModifyPickupAddress,
                    provider,
                    providerRating: provider.providerRating, // FIXME hardcoded !!!!
                  });
                  if (this.componentIsMounted) {
                    this.setState({ renderState: SCREEN_RENDER_STATE.DATA });
                  }

                  // switch map state
                  if (this.componentIsMounted && !appState.uiState.showTripButton) {
                    appState.uiState.setMapViewState('TAXI_OFFERS');
                  }
                });
                // sort
                const cachedOffers = appState.driveStageState.providersOffers.slice();
                if (sortKey === OFFER_SORT.PRICE_LOW_FIRST) {
                  cachedOffers.sort((a, b) => a.minPrice - b.minPrice);
                } else if (sortKey === OFFER_SORT.WAIT_TIMES_LOW_FIRST) {
                  cachedOffers.sort((a, b) => a.arrivalTime - b.arrivalTime);
                } else if (sortKey === OFFER_SORT.RATING_GREAT_FIRST) {
                  cachedOffers.sort((a, b) => a.capacity - b.capacity);
                }
                // just copy new
                appState.driveStageState.providersOffers = cachedOffers;
              } else if (offerData.errors) {
                // all cancel
                if (this.componentIsMounted) {
                  appState.uiState.setAddressBarViewState('CLOSED');
                  // switch MAP state
                  appState.uiState.setMapViewState('NAVIGATION');
                  // reset ride engine to default state
                  appState.driveStageState.resetRideEngine();
                  appState.driveStageState.setStage(1);
                }
                console.log('error', offerData);
                ErrorMessageBox(language, offerData.message, errorsDictionary);
              }
            });
          });

          if (promoCode) {
            console.log('promoCode', promoCode);
            offerPromises.push(this.redeemCode(promoCode));
          }

          // show
          Promise.all(offerPromises)
            .then(() => {
              if (!appState.driveStageState.providersOffers.length) {
                if (this.componentIsMounted) {
                  this.setState({ renderState: SCREEN_RENDER_STATE.EMPTY, isLoading: false });
                }
              } else if (this.componentIsMounted) {
                this.setState({ isLoading: false });
              }
            })
            .catch((err) => {
              console.warn('Error on a provider', err);
              if (this.componentIsMounted) {
                this.setState({ isLoading: false });
              }
            });
        }
      },
    );
  };

  //----------------------------------------------------
  // redeem code
  //----------------------------------------------------
  redeemCode = (promoCode) => {
    const { language, errorsDictionary } = appState.sessionParameters;

    return redeemCode(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      promoCode,
    ).then((data) => {
      if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictionary);
      } else {
        MessageBox('Info', 'Сode promo a été appliqué avec succès');
      }
    });
  };

  //----------------------------------------------------
  // switch to next stage
  //----------------------------------------------------
  handleNextStage = () => {
    // next stage
    appState.uiState.showTripButton = false;
    appState.driveStageState.setStage(3);
  };

  //---------------------------------------------------
  // on close offers filter dialog
  //---------------------------------------------------
  onClose = ({ sortKey, filterKey, promoCode }) => {
    appState.uiState.hideTripFiltersDialog();
    // change state
    this.setState({ sortKey, filterKey, renderState: SCREEN_RENDER_STATE.LOADING });
    // clear data
    appState.driveStageState.providersOffers = [];
    // apply filter and sort
    this.getOffers(sortKey, filterKey, promoCode);
  };

  //---------------------------------------------------
  // on cancel filter dialog
  //---------------------------------------------------
  onCancel = () => {
    appState.uiState.hideTripFiltersDialog();
  };

  //---------------------------------------------------
  // on close offers filter dialog
  //---------------------------------------------------
  onSelectDateTime = () => {
    const { sortKey, filterKey, promoCode } = this.state;
    // change state
    this.setState({ renderState: SCREEN_RENDER_STATE.LOADING });
    // clear data
    appState.driveStageState.providersOffers = [];
    // apply filter and sort
    this.getOffers(sortKey, filterKey, promoCode);
  };

  //---------------------------------------------------
  // select current offer
  //---------------------------------------------------
  selectCurrentOffer = (offer) => {
    appState.driveStageState.selectedOffer = offer;
  };

  //---------------------------------------------------
  // render providers offers
  //---------------------------------------------------
  renderVTCProvidersOffers = () => {
    const { providersOffers, selectedOffer } = appState.driveStageState;

    const vtcProviderRenderData = providersOffers.map((offer) => {
      const borderColor = selectedOffer.offerId === offer.offerId ? offer.colorCode : 'rgba(120, 132, 158, 0.58)';
      return (
        <React.Fragment key={offer.guid}>
          <VTCButton
            borderColor={borderColor}
            offer={offer}
            onPress={this.selectCurrentOffer.bind(this, offer)}
          />
          <View style={{ width: 20 }} />
        </React.Fragment>
      );
    });

    return vtcProviderRenderData;
  };

  //---------------------------------------------------
  // render loading state
  //---------------------------------------------------
  renderLoadingState = () => (
    <View style={SecondStyles.processingContainer}>
      <Text style={SecondStyles.processingText}>Recherche en cours</Text>
      <View style={{ height: 20 }} />
      <BubblesSpinner size={40} />
    </View>
  );

  //---------------------------------------------------
  // render empty state
  //---------------------------------------------------
  renderEmptyState = () => (
    <View style={SecondStyles.processingContainer}>
      <Text style={SecondStyles.processingText}>Aucune offre trouvée.</Text>
    </View>
  );

  //---------------------------------------------------
  // render data state
  //---------------------------------------------------
  renderDataState = () => {
    const { selectedOffer, providersOffers } = appState.driveStageState;
    const { isLoading } = this.state;

    let disableButton = true;
    const buttonText = `CHOISIR UN ${selectedOffer.providerName}`;

    if (providersOffers) {
      if (selectedOffer) {
        disableButton = false;
      }
    }

    return (
      <View>
        <ScrollView
          contentContainerStyle={SecondStyles.container}
          automaticallyAdjustContentInsets={false}
          horizontal
          showsHorizontalScrollIndicator
          scrollsToTop={false}
        >
          {this.renderVTCProvidersOffers()}
        </ScrollView>

        {providersOffers.length && isLoading ? (
          <View style={SecondStyles.progressContainer}>
            <ProgressIndicator color="#D3D9E5" height={7} />
          </View>
        ) : (
          <View style={SecondStyles.progressContainer} />
        )}

        {providersOffers.length ? (
          <View style={SecondStyles.footer}>
            <Button
              disabled={disableButton}
              full
              bordered
              style={SecondStyles.footerButton}
              onPress={this.handleNextStage}
            >
              <Text
                style={
                  disableButton
                    ? SecondStyles.footerButtonTextDisabled
                    : SecondStyles.footerButtonText
                }
              >
                {buttonText.toUpperCase()}
              </Text>
            </Button>
          </View>
        ) : null}
      </View>
    );
  };

  //---------------------------------------------------
  // render opened menu
  //---------------------------------------------------
  renderOpenedMenu = () => {
    const { renderState } = this.state;

    switch (renderState) {
      case SCREEN_RENDER_STATE.EMPTY:
        return this.renderEmptyState();
      case SCREEN_RENDER_STATE.LOADING:
        return this.renderLoadingState();
      case SCREEN_RENDER_STATE.DATA:
        return this.renderDataState();
      default:
        return this.renderLoadingState();
    }
  };

  render() {
    const { sortKey, filterKey } = this.state;
    return (
      <View>
        {appState.uiState.stagesMenuOpen && this.renderOpenedMenu()}
        <Modal visible={appState.uiState.showTripFiltersDialog}>
          <StagesMenuFiltersDialog
            sortKey={sortKey}
            filterKey={filterKey}
            onClose={this.onClose}
            onCancel={this.onCancel}
          />
        </Modal>
        <DateAndTimeSelectDialog
          visible={appState.uiState.dateAndTimeDialogVisible}
          onOk={this.onSelectDateTime}
        />
      </View>
    );
  }
}

export default StagesMenuStage2;
