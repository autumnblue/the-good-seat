import React, { Component } from 'react';
import { Text, Button } from 'native-base';
import {
  Image, View, Linking, Platform, TouchableOpacity,
} from 'react-native';

import { observer, inject } from 'mobx-react';

import { withNavigation } from 'react-navigation';
import {
  Modal, MiniCard, Icon, BubblesSpinner, ErrorMessageBox,
} from '../../../Components';
import StagesMenuCancelTripDialog from './StagesMenuCancelTripDialog';
import StagesMenuModifyDestinationDialog from './StagesMenuModifyDestinationDialog';
import StagesMenuEvaluateDriverDialog from './StagesMenuEvaluateDriverDialog';
import { getCurrentRide, changeDestinationPointRide } from '../../../Services/BackEnd/TheGoodSeat';

import { Images } from '../../../Theme';

import SecondStyles from './Styles/StagesMenuStage4Styles';
import { convertFeatureDataToAddressData } from '../../../Services/BackEnd/MapBox';

let appState = {};

//--------------------------------------------------
// get SMS divider depend from Platform
//--------------------------------------------------
function getSMSDivider() {
  return Platform.OS === 'ios' ? '&' : '?';
}
//--------------------------------------------------
// open URL
//--------------------------------------------------
function openUrl(url) {
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log(`Don't know how to open URI: ${url}`);
    }
  });
}
//--------------------------------------------------
// open SMS
//--------------------------------------------------
function openSmsApp(phone, body) {
  openUrl(`sms:${phone}${getSMSDivider()}body=${body}`);
}
//--------------------------------------------------
// open Phone App
//--------------------------------------------------
function openPhoneApp(phone) {
  openUrl(`tel:${phone}`);
}
/*
===================================================================
   Stage one screen
==================================================================
*/
@inject('appState')
@observer
class StagesMenuStage4 extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      showEvaluate: false,
      showCancelTrip: false,
      showProcessingData: true,
      currentRideData: {},
    };

    this.interval = 5000;
    this.componentIsMounted = false;
  }

  //-------------------------------------------------------
  // flag status description
  // processing - processing booking
  // accepted - driver comming to user
  // in_progress - comming to destination point
  // completed - ride complete
  //-------------------------------------------------------
  componentDidMount() {
    this.timer = setInterval(() => this.getRideData(), this.interval);
    this.componentIsMounted = true;
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.componentIsMounted = false;
  }

  //----------------------------------------------------
  // get ride data
  //----------------------------------------------------
  async getRideData() {
    const { language, errorsDictionary } = appState.sessionParameters;

    getCurrentRide(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken).then(
      (result) => {
        console.log('getRideData()', result);
        if (result.body) {
          if (result.body.status === 'accepted') {
            // set current ride data
            if (this.componentIsMounted) {
              this.setState({ currentRideData: result.body }, () => {
                this.setState({ showProcessingData: false });
              });
            }

            appState.sessionParameters.userLocation = {
              latitude: appState.driveStageState.startingAddressData.latitude,
              longitude: appState.driveStageState.startingAddressData.longitude,
            };
            // change map view render state
            if (this.componentIsMounted) {
              appState.uiState.setMapViewState('WAITING_TAXI');
            }
            // stop this!!!!!!!!
            if (this.timer) {
              clearInterval(this.timer);
            }
            // start ride engine
            this.startRideEngine();
          } else if (result.body.status === 'in_progress') {
            if (this.timer) {
              clearInterval(this.timer);
            }
            this.setState({ showProcessingData: false });
            // start ride engine
            this.startRideEngine();
          }
        } else if (result.errors) {
          if (this.componentIsMounted) {
            this.setState({ showProcessingData: false });
          }
          // this is cancel ride!!!!
          if (result.message === 'error_no_active_ride') {
            if (this.timer) {
              clearInterval(this.timer);
            }

            if (this.currentRideTimerID) {
              clearInterval(this.currentRideTimerID);
            }
            this.setState({ showProcessingData: false });
            this.resetUIState();
          } else {
            // cancel ride
            appState.uiState.setAddressBarViewState('CLOSED');
            appState.uiState.setMapViewState('NAVIGATION');
            // reset ride engine to default state
            appState.driveStageState.resetRideEngine();
            appState.driveStageState.setStage(1);
            // show alert
            ErrorMessageBox(language, result.message, errorsDictionary);
          }
        }
      },
    );
    /* .catch((err) => {
        if (this.componentIsMounted) {
          this.setState({ showProcessingData: false });
        }
        // this is cancel ride!!!!
        if (err.message === 'error_no_active_ride') {
          if (this.timer) {
            clearInterval(this.timer);
          }

          if (this.currentRideTimerID) {
            clearInterval(this.currentRideTimerID);
          }
        } else {
          // cancel ride
          appState.uiState.setAddressBarViewState('CLOSED');
          appState.uiState.setMapViewState('NAVIGATION');
          // reset ride engine to default state
          appState.driveStageState.resetRideEngine();
          appState.driveStageState.setStage(1);
          // show alert
          ErrorMessageBox(language, err.message, errorsDictionary);
        }
      }); */
  }

  //-----------------------------------------------------
  // reset UI state
  //-----------------------------------------------------
  resetUIState = () => {
    // cancel ride
    appState.uiState.setAddressBarViewState('CLOSED');
    appState.uiState.setMapViewState('NAVIGATION');
    // reset ride engine to default state
    appState.driveStageState.resetRideEngine();
    appState.driveStageState.setStage(1);
  };

  //----------------------------------------------------
  // after cancel trip
  //----------------------------------------------------
  afterCancelTrip = async () => {
    if (this.timer) {
      clearInterval(this.timer);
    }

    if (this.currentRideTimerID) {
      clearInterval(this.currentRideTimerID);
    }

    this.setState({ showCancelTrip: false }, () => {
      this.resetUIState();
    });
  };

  //----------------------------------------------------
  // start ride engine
  //----------------------------------------------------
  startRideEngine = () => {
    console.log('ride engine - start');
    // const { language, errorsDictionary } = appState.sessionParameters;
    this.currentRideTimerID = setInterval(() => {
      getCurrentRide(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken).then(
        (result) => {
          if (result.body) {
            console.log(result);
            if (result.body.status === 'accepted') {
              // set current ride data
              appState.driveStageState.currentRideData = result.body;
              // change map view render state
            } else if (result.body.status === 'in_progress') {
              // set current ride data
              appState.driveStageState.setStage(5);
              appState.driveStageState.currentRideData = result.body;
              appState.uiState.setMapViewState('RIDING');
              // follow the car
              appState.sessionParameters.mapCenter = {
                longitude: result.body.currentLon,
                latitude: result.body.currentLat,
              };
            } else if (result.body.status === 'completed') {
              console.log('ride engine - stop');
              clearInterval(this.currentRideTimerID);
              // show evaluate
              if (this.componentIsMounted) {
                this.setState({ showEvaluate: true });
              }
            }
          } else {
            console.log('ride engine - empty');
            clearInterval(this.currentRideTimerID);
          }
        },
      );
    }, this.interval);
    /* .catch((err) => {
          console.log('CATCH');
          // this is cancel ride!!!!
          if (err.message === 'error_no_active_ride') {
            if (this.timer) {
              clearInterval(this.timer);
            }

            if (this.currentRideTimerID) {
              clearInterval(this.currentRideTimerID);
            }
          } else {
            // cancel ride
            appState.uiState.setAddressBarViewState('CLOSED');
            appState.uiState.setMapViewState('NAVIGATION');
            // reset ride engine to default state
            appState.driveStageState.resetRideEngine();
            appState.driveStageState.setStage(1);
            // show alert
            ErrorMessageBox(language, err.message, errorsDictionary);
          }
        });
    }, this.interval); */
  };

  //---------------------------------------------------
  // show cancel trip dialog
  //---------------------------------------------------
  showCancelTripDialog = () => {
    this.setState({ showCancelTrip: true });
  };

  //---------------------------------------------------
  // show modify destination address dialog
  //---------------------------------------------------
  handleShowModifyDestinationDialog = () => {
    appState.uiState.openModifyDestinationDialog();
  };

  //---------------------------------------------------
  // show evaluate driver
  //---------------------------------------------------
  showEvaluateDriver = () => {
    this.setState({ showEvaluate: true });
  };

  //----------------------------------------------------
  // close evaluate driver dialog
  //----------------------------------------------------
  closeEvaluateDriver = () => {
    this.setState({ showEvaluate: false });
  };

  //---------------------------------------------------
  // finish ride!!!
  //---------------------------------------------------
  finishRide = () => {
    this.setState({ showEvaluate: false }, () => {
      // cancel ride
      appState.uiState.setAddressBarViewState('CLOSED');
      appState.uiState.setMapViewState('NAVIGATION');
      // reset ride engine to default state
      appState.driveStageState.resetRideEngine();
      appState.driveStageState.setStage(1);
    });
  };

  //---------------------------------------------------
  // open change ride address
  //---------------------------------------------------
  openChangeRideAddress = () => {
    const { providerCode } = appState.driveStageState.currentRideData;
    const { navigation } = this.props;

    navigation.navigate('findAddressScreen', {
      onSelect: (selectedAddress) => {
        const addressData = convertFeatureDataToAddressData(selectedAddress);

        changeDestinationPointRide(
          appState.constants.THEGOODSEAT_API,
          appState.sessionParameters.appToken,
          {
            ...addressData,
            providerCode,
          },
        ).then(() => {
          // change destination point
          appState.driveStageState.setDestinationAddress(addressData);
        });
      },
    });
  };

  //---------------------------------------------------
  // render opened menu
  //---------------------------------------------------
  renderOpenedMenu = () => {
    const { currentStage, selectedOffer } = appState.driveStageState;
    const { showProcessingData, currentRideData } = this.state;

    // get permissions
    let abilityChangeAddress = true;

    if (currentStage === 4) {
      abilityChangeAddress = selectedOffer.canModifyPickupAddress;
    } else if (currentStage === 5) {
      abilityChangeAddress = selectedOffer.canModifyDestAddress;
    }

    console.log(
      'can cancel',
      !!parseInt(selectedOffer.canCancelRide, 10),
      !parseInt(selectedOffer.canCancelRide, 10),
    );

    // text button change address
    const changeAddressButtonText = currentStage === 4 ? 'MODIFIER VOTRE ADRESSE DE DEPART' : "MODIFIER VOTRE ADRESSE D'ARRIVEE";

    const driverPhoto = (
      <Image
        style={{ borderRadius: 12, width: 48, height: 48 }}
        source={{ uri: currentRideData.driverPhoto }}
      />
    );

    const carPhoto = (
      <Image
        style={{ borderRadius: 12, width: 48, height: 48 }}
        source={{ uri: currentRideData.carPhoto }}
      />
    );

    if (showProcessingData) {
      return (
        <View style={SecondStyles.processingContainer}>
          <Text style={SecondStyles.processingText}>Réservation en cours</Text>
          <View style={{ height: 20 }} />
          <BubblesSpinner size={40} />
        </View>
      );
    }

    return (
      <React.Fragment>
        <View style={SecondStyles.container}>
          <View style={SecondStyles.miniCardContainer}>
            <MiniCard
              avatar={driverPhoto}
              header={currentRideData.driverName}
              subheader={
                currentRideData.userDriverRating
                  ? `${currentRideData.userDriverRating} / 5`
                  : 'aucune note'
              }
            />
            <MiniCard
              avatar={carPhoto}
              header={currentRideData.carModel}
              subheader={currentRideData.carLicense}
            />
          </View>
        </View>

        <View style={SecondStyles.contactActionContainer}>
          <TouchableOpacity onPress={() => openPhoneApp(currentRideData.driverPhone)}>
            <Icon containerStyle={SecondStyles.contactButton} color="#fff" size={25} name="N">
              <Image source={Images.phone} />
            </Icon>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => openSmsApp(currentRideData.driverPhone, '')}>
            <Icon containerStyle={SecondStyles.contactButton} color="#fff" size={25} name="N">
              <Image source={Images.message} />
            </Icon>
          </TouchableOpacity>
        </View>

        <View style={SecondStyles.footer}>
          <Button
            disabled={!parseInt(selectedOffer.canCancelRide, 10)}
            bordered
            style={SecondStyles.footerButton}
            onPress={this.showCancelTripDialog}
          >
            <Text
              style={
                selectedOffer.canCancelRide
                  ? SecondStyles.buttonMainText
                  : SecondStyles.buttonMainTextDisabled
              }
            >
              ANNULER LA COURSE
            </Text>
          </Button>

          <Button
            disabled={!parseInt(abilityChangeAddress, 10)}
            bordered
            style={SecondStyles.footerButton}
            onPress={this.openChangeRideAddress}
          >
            <Text
              style={
                abilityChangeAddress
                  ? SecondStyles.buttonMainText
                  : SecondStyles.buttonMainTextDisabled
              }
            >
              {changeAddressButtonText}
            </Text>
          </Button>
        </View>
      </React.Fragment>
    );
  };

  render() {
    const { showEvaluate, showCancelTrip } = this.state;

    return (
      <React.Fragment>
        {appState.uiState.stagesMenuOpen && this.renderOpenedMenu()}
        <Modal visible={showCancelTrip}>
          <StagesMenuCancelTripDialog
            onAfterCancelTrip={this.afterCancelTrip}
            onPressCancel={() => this.setState({ showCancelTrip: false })}
          />
        </Modal>
        <Modal visible={appState.uiState.showModifyDestinationDialog}>
          <StagesMenuModifyDestinationDialog />
        </Modal>
        <Modal visible={showEvaluate}>
          <StagesMenuEvaluateDriverDialog
            onOK={this.finishRide}
            onCancel={this.closeEvaluateDriver}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

export default withNavigation(StagesMenuStage4);
