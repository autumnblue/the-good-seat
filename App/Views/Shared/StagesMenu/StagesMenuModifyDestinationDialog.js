import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import {
  Button, Text, Item, Input,
} from 'native-base';

import { observer, inject } from 'mobx-react';

import Styles from './Styles/StagesMenuModifyDestinationDialogStyles';
import { Images } from '../../../Theme';

let appState = {};

@inject('appState')
@observer
class StagesMenuModifyDestination extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {};
  }

  //-------------------------------------------------
  // close dialog
  //-------------------------------------------------
  closeDialog = () => {
    appState.uiState.hideModifyDestinationDialog();
  };

  //----------------------------------------------------
  // modify destination address confirm
  //----------------------------------------------------
  handleModifyConfirm = () => {
    appState.uiState.hideModifyDestinationDialog();
  };

  render() {
    return (
      <View style={Styles.container}>
        <View style={Styles.centerTextContainer}>
          <Text style={Styles.mainText}>Entrez votre adresse de départ</Text>
        </View>

        <View style={Styles.inputAddressContainer}>
          <Item style={{ borderBottomColor: 'transparent' }}>
            <Input
              style={Styles.inputText}
              defaultValue="8, Place de l'Eglise, 92500 Rueil Malmaison"
              placeholder="Entrez une adresse de destination"
              placeholderTextColor="#78849E"
            />
            <TouchableOpacity>
              <Image style={Styles.clockContainer} source={Images.gpsPosition} />
            </TouchableOpacity>
          </Item>
        </View>

        <View style={Styles.footer}>
          <Button bordered style={Styles.footerButton} onPress={this.handleModifyConfirm}>
            <Text style={Styles.buttonText}>OK</Text>
          </Button>

          <Button bordered style={Styles.footerButton} onPress={this.closeDialog}>
            <Text style={Styles.buttonText}>ANNULER</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default StagesMenuModifyDestination;
