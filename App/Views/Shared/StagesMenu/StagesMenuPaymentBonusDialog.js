import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, Text } from 'native-base';
import { observer, inject } from 'mobx-react';
import { ErrorMessageBox } from '../../../Components';

import Styles from './Styles/StagesMenuPaymentBonusStyles';
import { getCagnotte } from '../../../Services/BackEnd/TheGoodSeat';

let appState = {};

@inject('appState')
@observer
class StagesMenuPaymentBonus extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      balance: 0,
    };
  }

  componentDidMount() {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { cagnotteId, _id } = appState.sessionParameters.currentUser;

    getCagnotte(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      cagnotteId,
    ).then((data) => {
      if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictionary);
      }
      this.setState({
        balance: parseInt(data.body.amount, 10),
      });
    });
  }

  //-------------------------------------------------
  // close dialog
  //-------------------------------------------------
  closeDialog = () => {
    appState.uiState.hidePaymentBonusDialog();
  };

  //----------------------------------------------------
  // switch to next stage
  //----------------------------------------------------
  handleNextStage = () => {
    appState.uiState.hidePaymentBonusDialog();
    appState.driveStageState.setStage(4);
  };

  //----------------------------------------------------
  // handle pay
  //----------------------------------------------------
  handlePayRide = () => {
    const { onPayRide } = this.props;

    if (onPayRide) {
      onPayRide();
    }

    appState.uiState.hidePaymentBonusDialog();
  };

  render() {
    const { balance } = this.state;
    const { paidOffer } = appState.driveStageState;

    const { containerStyle } = this.props;

    return (
      <View style={[containerStyle, Styles.container]}>
        <View style={Styles.centerContainer}>
          <Text style={Styles.mainText}>Payez votre course avec votre cagnotte</Text>
          <View style={[{ width: 250, marginTop: 6.5 }, Styles.divider]} />
        </View>
        <View style={Styles.contentContainer}>
          <View style={Styles.gridRow}>
            <View style={Styles.leftCol}>
              <Text style={Styles.mainText}>Course</Text>
            </View>

            <View style={Styles.rightCol}>
              <Text style={Styles.mainText}>
                {paidOffer.minPrice}
                {' '}
€
              </Text>
            </View>
          </View>

          <View style={[{ marginTop: 19 }, Styles.gridRow]}>
            <View style={Styles.leftCol}>
              <Text style={Styles.mainText}>Cagnotte</Text>
            </View>

            <View style={Styles.rightCol}>
              <Text style={Styles.mainText}>
                {balance}
                {' '}
€
              </Text>
            </View>
          </View>

          <View style={[{ marginTop: 19 }, Styles.gridRow]}>
            <View style={Styles.leftCol}>
              <Text style={Styles.mainText}>Montant à payer</Text>
            </View>

            <View style={Styles.rightCol}>
              <Text style={Styles.mainText}>
                {paidOffer.minPrice - balance > 0
                  ? parseFloat(paidOffer.minPrice - balance).toFixed(2) : 0}
                {' '}
€
              </Text>
            </View>
          </View>
        </View>
        <View style={Styles.footer}>
          <Button bordered style={Styles.footerButton} onPress={this.handlePayRide}>
            <Text style={Styles.buttonText}>
              PAYER (
              {paidOffer.minPrice - balance > 0
                ? parseFloat(paidOffer.minPrice - balance).toFixed(2) : 0}
              &nbsp;
              {paidOffer.currency}
)
            </Text>
          </Button>

          <Button bordered style={Styles.footerButton} onPress={this.closeDialog}>
            <Text style={Styles.buttonText}>ANNULER</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default StagesMenuPaymentBonus;
