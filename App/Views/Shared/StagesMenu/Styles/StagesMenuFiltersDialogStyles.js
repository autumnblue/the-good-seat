import { StyleSheet, Platform } from 'react-native';
import { Colors, addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 337,
    paddingTop: 25,
    paddingLeft: 27,
    borderRadius: 12,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  section: {
    paddingRight: 26,
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  leftContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  rightContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  mainFont: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
  },
  divider: {
    height: 1,
  },
  gridRow: {
    // flex: 1,
    flexDirection: 'row',
  },
  inputText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 14,
    color: Colors.onPrimaryText,
  },
  inputContainer: {
    borderRadius: 12,
    height: 31,
    backgroundColor: '#f1f1f1',
    justifyContent: 'center',
    marginTop: 34,
    marginBottom: 14,
  },
  footer: {
    marginRight: 24,
    marginBottom: 6,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  footerButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    ...addFontFamily('Roboto', '500'),
    fontSize: 14,
    color: '#009fe3',
  },
  gridContainer: {
    flexDirection: 'row',
    //  alignItems: 'center',
    justifyContent: 'space-between',
  },
  rowContainer: {
    // width: '50%',
    flex: 0.48,
    //  justifyContent: 'flex-start',
    //  alignItems: 'center',
  },
});
