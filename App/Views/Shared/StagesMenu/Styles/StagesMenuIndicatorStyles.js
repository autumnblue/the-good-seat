import { StyleSheet } from 'react-native';
import { Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    height: 7.8,
    backgroundColor: Colors.primary,
    alignItems: 'flex-end',
  },
  indicator: {
    borderTopRightRadius: 12,
    height: 7.8,
    backgroundColor: Colors.secondary,
  },
});
