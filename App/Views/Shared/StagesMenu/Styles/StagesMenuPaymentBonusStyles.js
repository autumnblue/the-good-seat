import { StyleSheet } from 'react-native';
import { Colors, addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 320,
    paddingHorizontal: 29,
    paddingTop: 35,
  },
  centerContainer: {
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 16,
    color: Colors.onPrimaryText,
  },
  secondaryText: {
    ...addFontFamily('SegoeUI'),
    fontSize: 14,
    color: '#454f63',
  },
  divider: {
    height: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.4)',
  },
  contentContainer: {
    marginTop: 44.5,
    marginLeft: 6,
    marginRight: 39,
  },
  gridRow: {
    flexDirection: 'row',
  },
  leftCol: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  rightCol: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  footer: {
    marginTop: 42,
    marginBottom: 22.4,
    marginHorizontal: 32,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerButton: {
    width: 90.9,
    height: 43.3,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    ...addFontFamily('Gibson', '300'),
    fontSize: 10,
    color: Colors.onPrimaryText,
  },
});
