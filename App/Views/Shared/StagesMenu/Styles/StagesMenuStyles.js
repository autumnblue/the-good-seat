import { StyleSheet } from 'react-native';
import { addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: '#fff',
    height: 86,
  },
  headerContainer: {
    flexDirection: 'row',
    height: 55,
    alignItems: 'center',
    paddingLeft: 14,
    paddingTop: 20,

  },
  backButtonContainer: {
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stageContainer: {
    // marginTop: 20,
    // paddingHorizontal: 23,
    marginLeft: 14,
  },
  stageText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    color: '#000',
    borderBottomWidth: 0,
  },
  secondaryText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 12,
    color: '#78849e',
  },
  infoText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    color: '#78849E',
    borderBottomWidth: 0,
  },
  subheaderMenu: {
    paddingLeft: 23,
    marginTop: 16,
    marginBottom: 10,
  },
  horizontalScrollContainer: {
    marginHorizontal: 23,
    paddingRight: 23,
  },
  durationContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 55,
    flex: 1,
  },
  dateTimeContainer: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    marginRight: 55,
    flex: 1,
  },
  durationText: {
    ...addFontFamily('Open Sans', '700'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
  dateTimeText: {
    ...addFontFamily('Open Sans', '700'),
    fontSize: 18,
    textAlign: 'left',
    color: '#78849e',
  },
  filterContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 19,
    alignSelf: 'flex-end',
  },
  filterImage: {
    width: 23,
    height: 23,
    alignSelf: 'center',
    marginBottom: 5,
  },
  processingContainer: {
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  processingText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
});
