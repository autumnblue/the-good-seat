import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 18,
    marginTop: 20,
    marginBottom: 12,
  },
  miniCardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  contactActionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 122,
  },
  contactButton: {
    width: 32.7,
    height: 32.7,
    borderRadius: 50,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer: {
    marginTop: 11,
    flexDirection: 'row',
    paddingHorizontal: 18,
    justifyContent: 'space-between',
  },
  buttonAction: {
    width: 150,
    height: 52,
    borderRadius: 12,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonMainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 13,
    textAlign: 'center',
    color: '#78849e',
  },
  buttonMainTextDisabled: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 13,
    textAlign: 'center',
    color: Colors.disable,
  },
  buttonSecondText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    opacity: 0.56,
    textAlign: 'center',
    color: '#78849e',
  },
  footerButton: {
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    width: 158,
    height: 52,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerButtonText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    textAlign: 'center',
    color: '#78849e',
  },
  processingContainer: {
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  processingText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
});
