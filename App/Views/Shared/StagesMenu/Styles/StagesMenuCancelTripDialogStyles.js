import { StyleSheet, Platform } from 'react-native';
import { Colors, addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 355,
    paddingTop: 19,
    borderRadius: 12,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  centerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 29,
  },
  centerConfirmerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 94,
    marginTop: 50,
  },
  mainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    color: Colors.onPrimaryText,
    textAlign: 'center',
  },
  secondaryText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 10,
    color: Colors.onPrimaryText,
  },
  miniCardContainer: {
    marginTop: 28,
    paddingHorizontal: 13,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  contactActionContainer: {
    marginTop: 12.5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 141,
  },
  contactButton: {
    width: 32.7,
    height: 32.7,
    borderRadius: 50,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer: {
    marginTop: 12,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
  },
  footerConfirmer: {
    marginTop: 37,
    marginBottom: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 102,
  },
  footerButton: {
    width: 150,
    height: 52,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 13,
    color: Colors.onPrimaryText,
  },
});
