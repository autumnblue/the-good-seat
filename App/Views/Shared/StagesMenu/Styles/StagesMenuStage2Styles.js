import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    paddingHorizontal: 24,
    marginTop: 19,
    marginBottom: 10,
  },
  footer: {
    paddingHorizontal: 24,
  },
  footerButton: {
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    height: 52,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerButtonText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 15,
    textAlign: 'center',
    color: '#78849e',
  },
  footerButtonTextDisabled: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 15,
    textAlign: 'center',
    color: Colors.disable,
  },
  processingContainer: {
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  processingText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
  progressContainer: {
    marginBottom: 7,
    height: 7,
    paddingHorizontal: 10,
  },
});
