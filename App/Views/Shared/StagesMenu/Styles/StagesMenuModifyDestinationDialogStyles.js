import { StyleSheet, Platform } from 'react-native';
import { Colors, addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 355,
    paddingTop: 38,
    paddingBottom: 14,
    borderRadius: 12,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  centerTextContainer: {
    alignItems: 'center',
  },
  mainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    color: Colors.onPrimaryText,
  },
  inputText: {
    ...addFontFamily('SegoeUI'),
    fontSize: 12,
    color: Colors.onPrimaryText,
  },
  inputAddressContainer: {
    marginHorizontal: 14,
    marginTop: 56,
    marginBottom: 43,
    paddingLeft: 29,
    paddingRight: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
  },
  footer: {
    marginHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerButton: {
    width: 150,
    height: 52,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 13,
    color: Colors.onPrimaryText,
  },
});
