import { StyleSheet, Platform } from 'react-native';
import { Colors, addFontFamily } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '95%',
    paddingTop: 19,
    borderRadius: 12,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  centerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  centerConfirmerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 94,
    marginTop: 50,
  },
  headerText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 18,
    color: Colors.onPrimaryText,
  },
  mainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    textDecorationLine: 'underline',
    color: Colors.onPrimaryText,
  },
  secondaryText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 10,
    color: Colors.onPrimaryText,
  },
  bodyContainer: {
    marginTop: 16,
    paddingHorizontal: 14,
    justifyContent: 'flex-start',
  },
  miniCardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  ratingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 35,
  },
  footer: {
    marginTop: 24,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
  },
  footerConfirmer: {
    marginTop: 37,
    marginBottom: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 102,
  },
  footerButton: {
    width: 150,
    height: 52,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(120, 132, 158, 0.25)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 13,
    color: Colors.onPrimaryText,
  },
  inputContainer: {
    height: 34,
    paddingHorizontal: 20,
    borderBottomColor: 'transparent',
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
  },
  textInputContainer: {
    flex: 1,
    backgroundColor: 'rgba(120, 132, 158, 0.1)',
    borderRadius: 20,
    height: 30,
  },
  inputText: {
    ...addFontFamily('SegoeUI', '400'),
    fontSize: 14,
    color: '#454f63',
    textAlignVertical: 'center',
    textAlign: 'center',
    lineHeight: 19,
  },
});
