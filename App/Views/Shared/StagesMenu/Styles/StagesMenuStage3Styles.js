import { StyleSheet } from 'react-native';
import { addFontFamily, Colors } from '../../../../Theme';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 60,
    marginTop: 24,
    marginBottom: 29,
  },
  footer: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    justifyContent: 'space-between',
  },
  buttonAction: {
    width: 101,
    height: 76,
    borderRadius: 8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonMainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    textAlign: 'center',
    color: '#78849e',
  },
  buttonSecondText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    opacity: 0.56,
    textAlign: 'center',
    color: '#78849e',
  },
  footerButton: {
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    width: 128,
    height: 52,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerPayButton: {
    borderColor: 'rgba(120, 132, 158, 0.25)',
    borderRadius: 12,
    width: '100%',
    height: 52,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerButtonText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    textAlign: 'center',
    color: '#78849e',
  },
  footerButtonTextDisabled: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    textAlign: 'center',
    color: Colors.disable,
  },
  processingContainer: {
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  processingText: {
    ...addFontFamily('Gibson', '500'),
    fontSize: 18,
    textAlign: 'center',
    color: '#78849e',
  },
  offerContainer: {
    width: 101,
    height: 76,
    borderWidth: 1,
    borderRadius: 8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  offerCardMainText: {
    ...addFontFamily('Gibson', '600'),
    fontSize: 16,
    textAlign: 'center',
    color: '#78849e',
  },
  offerCardSecondText: {
    ...addFontFamily('Gibson'),
    fontSize: 12,
    opacity: 0.56,
    textAlign: 'center',
    color: '#78849e',
  },
});
