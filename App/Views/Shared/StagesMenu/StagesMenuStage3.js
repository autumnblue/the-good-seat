import React, { Component } from 'react';
import { Text, Button } from 'native-base';
import {
  View, Alert, Linking, Platform, BackHandler,
} from 'react-native';

import { observer, inject } from 'mobx-react';
import { withNavigation } from 'react-navigation';
/* eslint import/no-extraneous-dependencies: ["off", {"devDependencies": false}] */
import { lineString as makeLineString } from '@turf/helpers';

// import SafariView from 'react-native-safari-view';
import {
  Modal, BubblesSpinner, MessageBox, ErrorMessageBox,
} from '../../../Components';

import StagesMenuPaymentBonusDialog from './StagesMenuPaymentBonusDialog';
import SecondStyles from './Styles/StagesMenuStage3Styles';
import SecondStageStyles from './Styles/StagesMenuStage2Styles';
import { payRide, getCardList, getCagnotte } from '../../../Services/BackEnd/TheGoodSeat';
import EventTracker from '../../../Services/BackEnd/thegoodseat/EventTracker';

import { getUrlParams } from '../../../Services/Utils';
import { MAPBOXgetDirection } from '../../../Services/BackEnd/MapBox';

let appState = {};

// https://developer.uber.com/docs/riders/references/api/v1.2/requests-current-get

//-----------------------------------------------------------------
// offer card
//-----------------------------------------------------------------
function OfferCard(props) {
  const { text, subtext, borderColor } = props;
  return (
    <View style={[SecondStyles.offerContainer, { borderColor }]}>
      <Text style={SecondStyles.offerCardMainText}>{text}</Text>
      <Text style={SecondStyles.offerCardSecondText}>{subtext}</Text>
    </View>
  );
}

/*
===================================================================
   Stage one screen
==================================================================
*/
@inject('appState')
@observer
class StagesMenuStage3 extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      selectedOffer: { minPrice: 0, currency: '€' },
      abilityPayBonuses: false,
      showProcessingData: false,
    };

    // default type of payment
    this.typeOfPayment = '';
  }

  componentDidMount() {
    const { language, errorsDictionary } = appState.sessionParameters;
    const { cagnotteId, _id } = appState.sessionParameters.currentUser;

    getCagnotte(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      _id,
      cagnotteId,
    ).then((data) => {
      if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictionary);
      } else {
        this.setState({
          abilityPayBonuses: parseInt(data.body.amount, 10) > 0,
        });
      }
    });

    // Add event listener to handle thegoodseat:// URLs
    Linking.addEventListener('url', this.handleOpenURL);
    // Launched from an external URL
    Linking.getInitialURL().then((url) => {
      if (url) {
        this.handleOpenURL({ url });
      }
    });

    if (Platform.OS === 'android') {
      console.log('add event');
      BackHandler.addEventListener('hardwareBackPress', this.backButtonPress);
    }

    // build route data for selected offer
    this.handleBuildRideRouteFromOffer().then((result) => {
      if (result) {
        appState.uiState.setMapViewState('SHOW_ROUTE_FROM_OFFER');
      }
    });

    appState.uiState.showTripButton = false;
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.backButtonPress);
    }
  }

  //------------------------------------------------
  // only for Android
  //------------------------------------------------
  backButtonPress = () => {
    appState.driveStageState.prevStage();
    return true;
  };

  //---------------------------------------------------
  // handle build ride route
  //---------------------------------------------------
  handleBuildRideRouteFromOffer = async () => {
    const { selectedOffer } = appState.driveStageState;
    const { availableCars } = selectedOffer;

    console.log('direction data');
    if (selectedOffer) {
      if (availableCars && availableCars.length >= 1) {
        try {
          const directionData = await MAPBOXgetDirection(
            appState.constants.MAPBOX_DIRECTION_API,
            [
              {
                latitude: appState.driveStageState.startingAddressData.latitude,
                longitude: appState.driveStageState.startingAddressData.longitude,
              },
              {
                latitude: availableCars[0].lat,
                longitude: availableCars[0].lon,
              },
            ],
            {
              profile: 'driving',
              geometry: 'geojson',
              accessToken: appState.constants.MAPBOX_TOKEN,
            },
          );

          if (directionData) {
            console.log('direction data', directionData);
            appState.driveStageState.setWaitingRouteData(
              makeLineString(directionData.routes[0].geometry.coordinates),
            );
            return true;
          }
        } catch (error) {
          Alert.alert('getDirection', `stagesmenu3.js-${error.message}`);
          return false;
        }
      }
    }

    return false;
  };

  //------------------------------------------------------------
  // handle open URL linking
  //------------------------------------------------------------
  handleOpenURL = ({ url }) => {
    const { language, errorsDictionary } = appState.sessionParameters;
    if (getUrlParams(url).error) {
      ErrorMessageBox(language, getUrlParams(url).error, errorsDictionary);
      return;
    }
    if (Platform.OS === 'ios') {
      // SafariView.dismiss();
    }

    // pay ride
    this.paymentProcess();
  };

  //------------------------------------------------------
  // Open URL in a browser
  //------------------------------------------------------
  openURL = (url) => {
    // Use SafariView on iOS
    // if (Platform.OS === 'ios') {
    //   SafariView.show({
    //     url,
    //    fromBottom: true,
    //   });
    // } else {
    Linking.openURL(url);
    // }
  };

  //------------------------------------------------------
  // Open Offer URL
  //------------------------------------------------------
  openOfferUrl = (url) => {
    const { selectedOffer } = appState.driveStageState;
    Alert.alert(
      `Connectez vous à ${selectedOffer.providerName}`,
      `Vous allez être redirigé-e sur le site de ${
        selectedOffer.providerName
      } pour finaliser votre commande. Pensez à rester connecté-e pour commander votre course en 1 clic la prochaine fois !`,
      [
        {
          text: 'OK',
          onPress: () => {
            this.openURL(url);
            EventTracker.track('app:offer:redirect', { offer: selectedOffer });
          },
        },
        {
          text: 'Annuler',
          style: 'cancel',
        },
      ],
      { cancelable: false },
    );
  };

  //-----------------------------------------------
  // uber auth
  //-----------------------------------------------
  uberAuth = async () => {
    const { THEGOODSEAT_API, UBER_CLIENT_ID } = appState.constants;
    const { _id } = appState.sessionParameters.currentUser;

    this.logonIsSocial = false;

    this.openURL(
      [
        'https://login.uber.com/oauth/v2/authorize?response_type=code',
        `&client_id=${UBER_CLIENT_ID}&scope=all_trips+profile+request+ride_widgets`,
        `&redirect_uri=${THEGOODSEAT_API}/auth/uber&state=${_id}`,
      ].join(''),
    );
  };

  //---------------------------------------------------
  // show payment bonus dialog
  //---------------------------------------------------
  handleShowPaymentBonusDialog = () => {
    const { selectedOffer } = appState.driveStageState;
    // this is paid!
    appState.driveStageState.paidOffer = selectedOffer;
    appState.uiState.openPaymentBonusDialog();
  };

  //----------------------------------------------------
  // get active card id
  //----------------------------------------------------
  getCurrentPaymentCard = async () => {
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    let currentPaymentCardId = -1;

    try {
      const data = await getCardList(
        appState.constants.THEGOODSEAT_API,
        appState.sessionParameters.appToken,
        _id,
      );

      if (data) {
        if (data.body) {
          // set current card id for payments
          data.body.forEach((cardData) => {
            if (cardData.isFavorite) {
              currentPaymentCardId = cardData.mongoPayCardId;
            }
          });
        }

        return currentPaymentCardId;
      }
    } catch (error) {
      return -1;
    }

    return currentPaymentCardId;
  };

  //------------------------------------------------------
  // show payments metods screen
  //------------------------------------------------------
  showPaymentMetodsScreen = () => {
    const { navigation } = this.props;
    // stop
    this.setState({ showProcessingData: false });
    // navigate
    navigation.navigate('paymentsCardsScreen');
  };

  //----------------------------------------------------
  // post payment
  //----------------------------------------------------
  paymentPost = async () => {
    // show process
    this.setState({ showProcessingData: true });

    // get need info
    const { selectedOffer } = appState.driveStageState;

    // this is paid! (cache)
    appState.driveStageState.paidOffer = selectedOffer;

    // gather need data
    // prepare data for statistics
    const statisticsData = {
      startRegion: appState.driveStageState.startingAddressData.region,
      startStreet: appState.driveStageState.startingAddressData.street,
      startCity: appState.driveStageState.startingAddressData.city,
      startCountry: appState.driveStageState.startingAddressData.country,
      startZipCode: appState.driveStageState.startingAddressData.zipCode,
      endRegion: appState.driveStageState.destinationAddressData.region,
      endStreet: appState.driveStageState.destinationAddressData.street,
      endCity: appState.driveStageState.destinationAddressData.city,
      endCountry: appState.driveStageState.destinationAddressData.country,
      endZipCode: appState.driveStageState.destinationAddressData.zipCode,
      startFullAddress: appState.driveStageState.startingAddressData.placeName,
      endFullAddress: appState.driveStageState.destinationAddressData.placeName,
    };

    // pay (on server)
    const data = await payRide(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      appState.driveStageState.paidOffer.providerCode,
      appState.driveStageState.paidOffer.displayName,
      {
        price: appState.driveStageState.paidOffer.minPrice,
        offerId: appState.driveStageState.paidOffer.offerId,
        paymentType: this.typeOfPayment,
        startLat: appState.driveStageState.startingAddressData.latitude,
        startLong: appState.driveStageState.startingAddressData.longitude,
        endLat: appState.driveStageState.destinationAddressData.latitude,
        endLong: appState.driveStageState.destinationAddressData.longitude,
      },
      statisticsData,
    );

    console.log('pay data', data);

    return data;
  };

  //----------------------------------------------------
  // payment process
  //----------------------------------------------------
  paymentProcess = () => {
    const { language, errorsDictionary } = appState.sessionParameters;

    this.paymentPost().then((data) => {
      console.log('int pay', data);

      if (data.errors) {
        if (data.message) {
          if (data.message === 'error_missing_payment_info') {
            MessageBox(
              'Info',
              'Veuillez enregistrer un moyen de paiement',
              this.showPaymentMetodsScreen,
            );
          } else {
            appState.uiState.setAddressBarViewState('CLOSED');
            // switch MAP state
            appState.uiState.setMapViewState('NAVIGATION');
            // reset ride engine to default state
            appState.driveStageState.resetRideEngine();
            appState.driveStageState.setStage(1);
            ErrorMessageBox(language, data.message, errorsDictionary);
          }
        } else {
          appState.uiState.setAddressBarViewState('CLOSED');
          // switch MAP state
          appState.uiState.setMapViewState('NAVIGATION');
          // reset ride engine to default state
          appState.driveStageState.resetRideEngine();
          appState.driveStageState.setStage(1);
          ErrorMessageBox(language, data.message, errorsDictionary);
        }
      } else if (data.body) {
        // next
        appState.driveStageState.setStage(4);
      } else {
        Alert.alert(data);
      }
    });
  };

  //---------------------------------------------------
  // pay rider
  //---------------------------------------------------
  payRider = (typeOfPayment) => {
    // save payment type
    this.typeOfPayment = typeOfPayment;

    // get current provider
    const { selectedOffer } = appState.driveStageState;

    console.log('provider', selectedOffer);
    this.paymentProcess();
  };

  //---------------------------------------------------
  // render pay buttons
  //---------------------------------------------------
  renderPayButtons = () => {
    const { selectedOffer } = appState.driveStageState;
    const { abilityPayBonuses } = this.state;
    if (
      selectedOffer.provider
      && selectedOffer.provider.isOfferOnlyProvider
      && selectedOffer.providerUrl
    ) {
      return (
        <Button
          full
          bordered
          style={SecondStyles.footerPayButton}
          onPress={this.openOfferUrl.bind(this, selectedOffer.providerUrl)}
        >
          <Text style={SecondStyles.buttonMainText}>
            {`Commander sur ${
              selectedOffer.providerName
            }`}
          </Text>
        </Button>
      );
    }

    if (selectedOffer.provider && !selectedOffer.provider.paymentByTgs) {
      return (
        <View style={[SecondStageStyles.footerButton, { flexGrow: 1 }]}>
          <Button
            full
            bordered
            style={[SecondStageStyles.footerButton, { width: '100%' }]}
            onPress={this.payRider.bind(this, 'NOPAYMENT')}
          >
            <Text style={SecondStageStyles.footerButtonText}>CONFIRMER LA COURSE</Text>
          </Button>
        </View>
      );
    }
    return (
      <React.Fragment>
        <Button
          bordered
          style={SecondStyles.footerButton}
          onPress={this.payRider.bind(this, 'CARD')}
        >
          <Text style={SecondStyles.buttonMainText}>
            PAYER (
            {selectedOffer.minPrice}
            {selectedOffer.currency}
)
          </Text>
        </Button>

        <Button
          bordered
          disabled={!abilityPayBonuses}
          style={SecondStyles.footerButton}
          onPress={this.handleShowPaymentBonusDialog}
        >
          <Text
            style={
              abilityPayBonuses
                ? SecondStyles.footerButtonText
                : SecondStyles.footerButtonTextDisabled
            }
          >
            Utiliser ma cagnotte
          </Text>
        </Button>
      </React.Fragment>
    );
  };

  //---------------------------------------------------
  // render opened menu
  //---------------------------------------------------
  renderOpenedMenu = () => {
    const { selectedOffer } = appState.driveStageState;
    const { showProcessingData } = this.state;

    if (showProcessingData) {
      return (
        <View style={SecondStyles.processingContainer}>
          <Text style={SecondStyles.processingText}>Paiement en cours</Text>
          <View style={{ height: 20 }} />
          <BubblesSpinner size={40} />
        </View>
      );
    }

    return (
      <React.Fragment>
        <View style={{ height: 125 }}>
          <View style={SecondStyles.container}>
            <OfferCard
              borderColor="#009FE3"
              text={selectedOffer.displayName}
              subtext={`${selectedOffer.capacity ? selectedOffer.capacity : '1-4'} personne(s)`}
            />
            <OfferCard
              borderColor={selectedOffer.colorCode}
              text={selectedOffer.providerName}
              subtext={`${
                selectedOffer.arrivalTime
                  ? `${Number(parseInt(selectedOffer.arrivalTime, 10) / 60).toFixed(0)} min`
                  : ''
              } ${
                selectedOffer.providerName === 'G7' || selectedOffer.providerName === 'Uber'
                  ? '≈'
                  : '-'
              } ${selectedOffer.minPrice + selectedOffer.currency || '-'}`}
            />
          </View>
        </View>

        <View style={SecondStyles.footer}>{this.renderPayButtons()}</View>
      </React.Fragment>
    );
  };

  render() {
    const { selectedOffer } = this.state;

    return (
      <React.Fragment>
        {appState.uiState.stagesMenuOpen && this.renderOpenedMenu()}
        <Modal visible={appState.uiState.showPaymentBonusDialog}>
          <StagesMenuPaymentBonusDialog
            selectedOffer={selectedOffer}
            onPayRide={this.payRider.bind(this, 'FUNDS')}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

export default withNavigation(StagesMenuStage3);
