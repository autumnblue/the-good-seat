import React, { Component } from 'react';

import { View } from 'react-native';

import { observer, inject } from 'mobx-react';
import PaymentsMetods from './DrawerMenu/DrawerMenuMyPaymentsMetods';

import Styles from './Styles/PaymentsCardsScreenStyles';

/*
===================================================================
   find address screen
==================================================================
*/
@inject('appState')
@observer
class PaymentsCardsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {}

  render() {
    return (
      <View style={Styles.container}>
        <PaymentsMetods modal />
      </View>
    );
  }
}

export default PaymentsCardsScreen;
