import React, { Component } from 'react';
import { Input, Icon } from 'native-base';

import {
  Alert, View, Text, TouchableOpacity, FlatList,
} from 'react-native';

import { observer, inject } from 'mobx-react';

import Styles from './Styles/FindAddressStyles';
import { MAPBOXsearchGeocoding } from '../../Services/BackEnd/MapBox';

let appState = {};
/*
===================================================================
   find address screen
==================================================================
*/
@inject('appState')
@observer
class FindAddressScreen extends Component {
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      resultsSearch: [],
      searchText: '',
    };
  }

  componentDidMount() {
    if (this.addressInputRef) {
      this.addressInputRef._root.focus();
    }
  }

  //----------------------------------------------------
  // handle search address
  //----------------------------------------------------
  onSearchAddress = (text) => {
    const { latitude, longitude } = appState.sessionParameters.userLocation;
    // only if symbols > 1
    if (text.length > 1) {
      MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
        source: 'mapbox.places',
        accessToken: appState.constants.MAPBOX_TOKEN,
        proximity: `${longitude},${latitude}`,
        bbox: '',
        autocomplete: true,
        types: 'address,place,poi',
        limit: 20,
        query: `${text}`,
      })
        .then((data) => {
          if (data && data.features && data.features.length) {
            this.setState({ resultsSearch: data.features });
          }
        })
        .catch((error) => {
          console.error('geocoding API', error.message);
          this.setState({ resultsSearch: [] });
        });
    } else {
      this.setState({ resultsSearch: [] });
    }

    this.setState({ searchText: text, resultsSearch: [] });
  };

  //-----------------------------------------------------------
  // handle select address
  //-----------------------------------------------------------
  handleSelectAddress = (feature) => {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params) {
      if (navigation.state.params.onSelect) {
        navigation.state.params.onSelect(feature);
      }
    }
  };

  //------------------------------------------------------------
  // clear input field
  //------------------------------------------------------------
  clearInput = () => {
    this.setState({ searchText: '', resultsSearch: [] });
  };

  //----------------------------------------------------------
  // get user position
  //----------------------------------------------------------
  getUserGPSPosition = () => {
    const { latitude, longitude } = appState.sessionParameters.userLocation;

    MAPBOXsearchGeocoding(appState.constants.MAPBOX_GEOCODING_API, {
      source: 'mapbox.places',
      accessToken: appState.constants.MAPBOX_TOKEN,
      proximity: `${longitude},${latitude}`,
      bbox: '',
      autocomplete: true,
      limit: 1,
      // types: '',
      types: 'address,poi',
      query: `${longitude},${latitude}`,
    })
      .then((data) => {
        if (data && data.features && data.features.length) {
          this.handleSelectAddress(data.features[0]);
        }
      })
      .catch((error) => {
        Alert.alert('geocoding API', error.message);
      });
  };

  //----------------------------------------------------
  // render search results item
  //----------------------------------------------------
  renderResultItem = ({ item }) => (
    <TouchableOpacity key={item.id} onPress={this.handleSelectAddress.bind(this, item)}>
      <View style={Styles.searchResultItem}>
        <Text style={Styles.searchResultText}>{item.place_name}</Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    const { resultsSearch, searchText } = this.state;

    return (
      <View style={Styles.container}>
        <View style={{ height: 12 }} />
        <View style={Styles.textInputContainer}>
          <Input
            ref={(ref) => {
              this.addressInputRef = ref;
            }}
            value={searchText}
            placeholderTextColor="#8A94AA"
            style={[Styles.textInputField, { flex: 0.99 }]}
            placeholder={appState.appTextData.get('shared.findaddress.screen').value}
            onChangeText={this.onSearchAddress}
          />
          {searchText.length ? (
            <TouchableOpacity onPress={this.clearInput}>
              <Icon style={{ fontSize: 20, color: '#8A94AA' }} name="close-circle" />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={this.getUserGPSPosition}>
              <Icon style={{ fontSize: 20, color: '#8A94AA' }} name="locate" />
            </TouchableOpacity>
          )}
        </View>
        {resultsSearch.length ? (
          <View style={{ marginTop: 10, paddingLeft: 20 }}>
            <Text style={Styles.resultInfoText}>
              résultat:&nbsp;
              {resultsSearch.length}
              {' '}
            </Text>
          </View>
        ) : null}
        <FlatList
          contentContainerStyle={Styles.searchResultContainer}
          refreshing={false}
          data={resultsSearch}
          renderItem={this.renderResultItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

export default FindAddressScreen;
