import React, { Component } from 'react';
import {
  Button, Form, Item, Input, View, Text, Textarea, Icon, Radio,
} from 'native-base';

import {
  Alert, Image, TouchableWithoutFeedback, ScrollView, TouchableOpacity,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { observer, inject } from 'mobx-react';
import VersionNumber from 'react-native-version-number';
import { Modal, ErrorMessageBox } from '../../Components';

import Styles from './Styles/HelperScreenStyles';
import { Images } from '../../Theme';
import { getFAQ, sendProblemTicket, getCurrentTerm } from '../../Services/BackEnd/TheGoodSeat';
import { validateEmail, validateFormValue } from '../../Services/Utils';

let appState = {};

/*
============================================
 screen render states
============================================
*/
const SCREEN_RENDER_STATE = {
  MAIN: 0,
  FAQ: 1,
  TERMSOFCONDITIONS: 2,
  PROBLEM_TYPE: 3,
  CONTACTUS: 4,
  DESCRIPTION: 5,
};

const { appVersion, buildVersion } = VersionNumber;

const PROBLEM_TYPE_MAPPING = {
  1: 'J’ai eu un accident',
  2: 'Je ne suis pas d’accord avec le montant de la course',
  3: 'J’ai perdu quelque chose',
  4: 'J’ai eu un problème avec le chauffeur',
  5: 'Autre problème',
};

/*
===================================================================
   Помошник
==================================================================
*/
@inject('appState')
@observer
class HelperScreen extends Component {
  //-------------------------------------------------
  // constructor
  //-------------------------------------------------
  constructor(props) {
    super(props);
    appState = props.appState; /* eslint-disable-line prefer-destructuring */
    this.state = {
      screenRenderState: SCREEN_RENDER_STATE.MAIN,
      problemType: 0,
      faqTextData: [],
      errorInputs: {},
      formInputs: {},
      disableSentButton: true,
      termText: '',
    };

    // validation config
    this.validationConfig = {
      firstName: {
        required: true,
      },
      email: {
        required: true,
        validFunc: validateEmail,
      },
      message: {
        required: true,
      },
    };
  }

  componentDidMount() {
    const { language, errorsDictionary } = appState.sessionParameters;

    getCurrentTerm(appState.constants.THEGOODSEAT_API).then((data) => {
      console.log('TERMS', data);
      if (data.body && data.body.content) {
        this.setState({ termText: data.body.content });
      } else if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictionary);
      }
    });
  }

  //-------------------------------------------------
  // handle text input
  //-------------------------------------------------
  handleChangeTextInput = (type, text) => {
    this.setState(
      ({ formInputs }) => ({
        formInputs: {
          ...formInputs,
          [type]: text,
        },
      }),
      () => {
        this.validateContactuUsForm();
      },
    );
  };

  //------------------------------------------------
  // validate input fields
  //------------------------------------------------
  validateContactuUsForm = () => {
    const { formInputs } = this.state;

    let error = false;

    const keys = Object.keys(this.validationConfig);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      error = !validateFormValue(this.validationConfig, key, formInputs[key]);
      // if error == true
      if (error) break;
    }

    // show button
    this.setState({ disableSentButton: error });
  };

  //-------------------------------------------------
  // handle on blur input field
  //-------------------------------------------------
  onBlurInputField = (type) => {
    const { formInputs } = this.state;

    this.setState(({ errorInputs }) => ({
      errorInputs: {
        ...errorInputs,
        [type]: !validateFormValue(this.validationConfig, type, formInputs[type]),
      },
    }));
  };

  //-----------------------------------------------
  // fetch help text
  //-----------------------------------------------
  fetchFAQText = () => {
    getFAQ(appState.constants.THEGOODSEAT_API, appState.sessionParameters.appToken).then((data) => {
      if (data.body) {
        this.setState({ faqTextData: data.body });
      }
    });
  };

  //-----------------------------------------------
  // on close helper
  //-----------------------------------------------
  onHelperClose = () => {
    const { onClose } = this.props;

    if (onClose) onClose();
  };

  //-----------------------------------------------
  // change render state
  //-----------------------------------------------
  handleChangeScreenRenderState = (renderState) => {
    if (renderState === 'FAQ') {
      this.fetchFAQText();
    }
    this.setState({ screenRenderState: SCREEN_RENDER_STATE[renderState] });
  };

  //----------------------------------------------
  // set problem type
  //----------------------------------------------
  setProblemType = (problemType) => {
    this.setState({ problemType });
  };

  //----------------------------------------------
  // select probleme
  //----------------------------------------------
  selectProblemType = () => {
    this.setState({
      screenRenderState: SCREEN_RENDER_STATE.CONTACTUS,
    });
  };

  //----------------------------------------------
  // send problem ticket
  //----------------------------------------------
  sendProblemTicket = () => {
    const { language, errorsDictionary } = appState.sessionParameters;
    // get user id
    const { _id } = appState.sessionParameters.currentUser;

    const { formInputs, problemType } = this.state;

    const ticketData = {
      userId: _id,
      type: PROBLEM_TYPE_MAPPING[problemType],
      status: 'new',
      regionId: '01232',
      subject: 'probleme',
      body: formInputs.message,
    };

    sendProblemTicket(
      appState.constants.THEGOODSEAT_API,
      appState.sessionParameters.appToken,
      ticketData,
    ).then((data) => {
      if (data.errors) {
        ErrorMessageBox(language, data.message, errorsDictionary);
      } else {
        Alert.alert('', 'Votre problème a bien été signalé');
      }
      this.handleChangeScreenRenderState('MAIN');
    });
  };

  //----------------------------------------------
  // render state FAQ
  //----------------------------------------------
  renderFAQState = () => {
    const { faqTextData } = this.state;

    const faqTextRenderData = faqTextData.map(qaBlock => (
      <React.Fragment key={qaBlock._id}>
        <Text style={[Styles.headerFAQText, { marginTop: 15 }]}>{qaBlock.question}</Text>
        <Text style={Styles.blockFAQText}>{qaBlock.answer}</Text>
      </React.Fragment>
    ));

    return (
      <View style={[Styles.mainContainer, { height: 601 }]}>
        <View style={Styles.helperHeader}>
          <View
            style={[
              { marginTop: 14, width: 56 },
              Styles.itemMenuContainer,
              Styles.itemMenuContainerUnderline,
            ]}
          >
            <Text style={Styles.secondaryText}>F.A.Q</Text>
          </View>
        </View>
        <ScrollView contentContainerStyle={Styles.faqScrollContainer}>
          <View style={Styles.faqTextContainer}>{faqTextRenderData}</View>
          <View style={Styles.helperFooter}>
            <Button transparent onPress={this.handleChangeScreenRenderState.bind(this, 'MAIN')}>
              <Text style={Styles.buttonText}>RETOUR</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  };

  //----------------------------------------------
  // render state Terms of conditions
  //----------------------------------------------
  renderTermsOfConditionsState = () => {
    const { termText } = this.state;

    return (
      <View style={[Styles.mainContainer, { height: 601 }]}>
        <ScrollView contentContainerStyle={Styles.termsScrollContainer}>
          <Text style={Styles.blockTermsText}>{termText}</Text>
          <View style={Styles.helperFooter}>
            <Button transparent onPress={this.handleChangeScreenRenderState.bind(this, 'MAIN')}>
              <Text style={Styles.buttonText}>RETOUR</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  };

  //----------------------------------------------
  // render contact us state
  //----------------------------------------------
  renderContactUs = () => {
    const {
      problemType, errorInputs, formInputs, disableSentButton,
    } = this.state;
    return (
      <KeyboardAwareScrollView
        extraScrollHeight={55}
        contentContainerStyle={Styles.helperContactUsContainer}
        showsVerticalScrollIndicator={false}
      >
        <View style={Styles.helperHeader}>
          <View
            style={[
              { marginTop: 9, width: 186 },
              Styles.itemMenuContainer,
              Styles.itemMenuContainerUnderline,
            ]}
          >
            <Text style={Styles.mainText}>Signalement</Text>
          </View>
        </View>
        <Form style={Styles.formContainer}>
          <View
            style={
              errorInputs.firstName ? Styles.textInputContainerError : Styles.textInputContainer
            }
          >
            <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
              <Input
                placeholderTextColor="#78849E"
                style={Styles.textInputField}
                placeholder="Votre Prénom"
                value={formInputs.firstName}
                onChangeText={this.handleChangeTextInput.bind(this, 'firstName')}
                onBlur={this.onBlurInputField.bind(this, 'firstName')}
              />
            </Item>
          </View>
          <View
            style={[
              { marginTop: 30 },
              errorInputs.email ? Styles.textInputContainerError : Styles.textInputContainer,
            ]}
          >
            <Item style={{ height: 30, borderBottomColor: 'transparent' }}>
              <Input
                placeholderTextColor="#78849E"
                style={Styles.textInputField}
                placeholder="Votre Email"
                value={formInputs.email}
                autoCapitalize="none"
                onChangeText={this.handleChangeTextInput.bind(this, 'email')}
                onBlur={this.onBlurInputField.bind(this, 'email')}
              />
            </Item>
          </View>
          <View style={[{ marginTop: 33 }, Styles.textInputContainer]}>
            <TouchableOpacity
              onPress={this.handleChangeScreenRenderState.bind(this, 'PROBLEM_TYPE')}
            >
              <View style={Styles.pickerContainer}>
                <Text numberOfLines={1} style={[Styles.textInputField, Styles.textProblemType]}>
                  {problemType > 0 ? PROBLEM_TYPE_MAPPING[problemType] : 'Type de problème'}
                </Text>
                <Icon
                  name="chevron-up"
                  type="Feather"
                  style={{
                    position: 'absolute',
                    right: 12,
                    fontSize: 20,
                    color: '#707070',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={[
              { marginTop: 33 },
              errorInputs.message ? Styles.textInputContainerError : Styles.textInputContainer,
            ]}
          >
            <Textarea
              multiline
              rowSpan={5}
              numberOfLines={5}
              editable
              maxLength={140}
              style={Styles.textareaEditInput}
              value={formInputs.message}
              placeholder="Votre message"
              placeholderTextColor="#78849E"
              onChangeText={this.handleChangeTextInput.bind(this, 'message')}
              onBlur={this.onBlurInputField.bind(this, 'message')}
            />
          </View>
          <View style={Styles.helperFooter}>
            <Button transparent onPress={this.handleChangeScreenRenderState.bind(this, 'MAIN')}>
              <Text style={Styles.buttonText}>ANNULER</Text>
            </Button>
            <Button transparent disabled={disableSentButton} onPress={this.sendProblemTicket}>
              <Text style={disableSentButton ? Styles.buttonTextDisable : Styles.buttonText}>
                ENVOYER
              </Text>
            </Button>
          </View>
        </Form>
      </KeyboardAwareScrollView>
    );
  };

  //----------------------------------------------
  // render state problem type
  //----------------------------------------------
  renderProblemTypeState = () => {
    const { problemType } = this.state;

    return (
      <KeyboardAwareScrollView style={Styles.helperContactUsContainer}>
        <View style={Styles.helperHeader}>
          <View
            style={[
              { marginTop: 9, width: 258 },
              Styles.itemMenuContainer,
              Styles.itemMenuContainerUnderline,
            ]}
          >
            <Text style={Styles.mainText}>Type de problème</Text>
          </View>
        </View>
        <Form style={Styles.formProblemTypeContainer}>
          <View style={{ paddingHorizontal: 10 }}>
            <TouchableOpacity onPress={this.setProblemType.bind(this, 1)}>
              <View style={Styles.problemTypeContainer}>
                <Radio selected={problemType === 1} style={{ width: 20 }} />
                <Text style={[Styles.secondaryText, { marginLeft: 10 }]}>
                  {PROBLEM_TYPE_MAPPING[1]}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.setProblemType.bind(this, 2)}>
              <View style={Styles.problemTypeContainer}>
                <Radio selected={problemType === 2} style={{ width: 20 }} />
                <Text style={[Styles.secondaryText, { marginLeft: 10 }]}>
                  {PROBLEM_TYPE_MAPPING[2]}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.setProblemType.bind(this, 3)}>
              <View style={Styles.problemTypeContainer}>
                <Radio selected={problemType === 3} style={{ width: 20 }} />
                <Text style={[Styles.secondaryText, { marginLeft: 10 }]}>
                  {PROBLEM_TYPE_MAPPING[3]}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.setProblemType.bind(this, 4)}>
              <View style={Styles.problemTypeContainer}>
                <Radio selected={problemType === 4} style={{ width: 20 }} />
                <Text style={[Styles.secondaryText, { marginLeft: 10 }]}>
                  {PROBLEM_TYPE_MAPPING[4]}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.setProblemType.bind(this, 5)}>
              <View style={Styles.problemTypeContainer}>
                <Radio selected={problemType === 5} style={{ width: 20 }} />
                <Text style={[Styles.secondaryText, { marginLeft: 10 }]}>
                  {PROBLEM_TYPE_MAPPING[5]}
                </Text>
              </View>
            </TouchableOpacity>
            {problemType === 5 ? (
              <View style={[{ marginTop: 12 }, Styles.textInputContainer]}>
                <Textarea
                  multiline
                  rowSpan={5}
                  numberOfLines={3}
                  disabled={!problemType === 5}
                  maxLength={140}
                  style={Styles.textareaProblemEditInput}
                  placeholder="Précisez"
                  placeholderTextColor="#78849E"
                />
              </View>
            ) : (
              <View style={{ height: 100, marginTop: 12 }} />
            )}
          </View>
          <View style={Styles.helperFooter}>
            <Button
              transparent
              onPress={this.handleChangeScreenRenderState.bind(this, 'CONTACTUS')}
            >
              <Text style={Styles.buttonText}>RETOUR</Text>
            </Button>
            <Button
              transparent
              onPress={this.handleChangeScreenRenderState.bind(this, 'CONTACTUS')}
            >
              <Text style={Styles.buttonText}>OK</Text>
            </Button>
          </View>
        </Form>
      </KeyboardAwareScrollView>
    );
  };

  //----------------------------------------------
  // render state (main)
  //----------------------------------------------
  renderMainState = () => {
    const { _id } = appState.sessionParameters.currentUser;
    const apiUrl = appState.constants.THEGOODSEAT_API;

    return (
      <View style={Styles.helperMainContainer}>
        <View style={Styles.helperMainHeader}>
          <View style={Styles.headerLeftBlock}>
            <View style={[{ marginTop: 16, marginLeft: 22 }, Styles.itemMenuContainer]}>
              <Text style={Styles.mainText}>Menu Aide</Text>
              <View style={[{ width: 64 }, Styles.itemMenuContainerUnderline]} />
            </View>
          </View>
          <View style={Styles.closeButtonContainer}>
            <TouchableWithoutFeedback onPress={this.onHelperClose}>
              <Image style={Styles.imageCloseButton} source={Images.modalClose} />
            </TouchableWithoutFeedback>
          </View>
        </View>
        <TouchableWithoutFeedback onPress={this.handleChangeScreenRenderState.bind(this, 'FAQ')}>
          <View
            style={[
              { marginTop: 20, width: 74 },
              Styles.itemMenuContainer,
              Styles.itemMenuContainerUnderline,
            ]}
          >
            <Text style={Styles.secondaryText}>F.A.Q</Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={this.handleChangeScreenRenderState.bind(this, 'CONTACTUS')}
        >
          <View
            style={[
              { marginTop: 10, width: 186 },
              Styles.itemMenuContainer,
              Styles.itemMenuContainerUnderline,
            ]}
          >
            <Text style={Styles.secondaryText}>Signaler un problème</Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={this.handleChangeScreenRenderState.bind(this, 'TERMSOFCONDITIONS')}
        >
          <View style={[{ marginTop: 5 }, Styles.itemMenuContainer]}>
            <Text style={Styles.secondaryText}>Conditions Générales d&rsquo;Utilisation</Text>
          </View>
        </TouchableWithoutFeedback>
        <View
          style={[
            { marginTop: 5, justifyContent: 'center', alignItems: 'center' },
            Styles.itemMenuContainer,
          ]}
        >
          <Text style={{ ...Styles.secondaryText, fontSize: 10, textAlign: 'center' }}>
            Version :
            {' '}
            {appVersion}
            {' '}
(
            {buildVersion}
) userID:
            {' '}
            {_id}
            {'\n'}
            {' '}
apiUrl
            {apiUrl}
          </Text>
        </View>
      </View>
    );
  };

  //----------------------------------------------------
  // рисовать экран в зависимости от состояния
  //----------------------------------------------------
  renderScreen = () => {
    const { screenRenderState } = this.state;

    switch (screenRenderState) {
      case SCREEN_RENDER_STATE.MAIN:
        return this.renderMainState();
      case SCREEN_RENDER_STATE.FAQ:
        return this.renderFAQState();
      case SCREEN_RENDER_STATE.TERMSOFCONDITIONS:
        return this.renderTermsOfConditionsState();
      case SCREEN_RENDER_STATE.CONTACTUS:
        return this.renderContactUs();
      case SCREEN_RENDER_STATE.PROBLEM_TYPE:
        return this.renderProblemTypeState();
      default:
        return this.renderMainState();
    }
  };

  render() {
    const { visible } = this.props;

    return (
      <Modal visible={visible} backdropColor="rgba(255, 255, 255, 0.8)" onRequestClose={() => {}}>
        <View style={Styles.container}>{this.renderScreen()}</View>
      </Modal>
    );
  }
}

export default HelperScreen;
