import React, { Component } from 'react';
import Mapbox from '@mapbox/react-native-mapbox-gl';
import { Images } from '../../../Theme';

/*
----------------------------------------------------
 mapping images for car rendering on map
----------------------------------------------------
*/
const VTC_CAR_IMAGE_MAPPING = {
  letaxi: Images.carTaxi,
  lecab: Images.carLecab,
  uber: Images.carUber,
};

/*
----------------------------------------------------
 VTC car
----------------------------------------------------
*/
class VTCCar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      VTC, glRenderIndex, longitude, latitude, angle = 0,
    } = this.props;

    // make styles
    const layerStyles = Mapbox.StyleSheet.create({
      carImage: {
        iconImage: VTC_CAR_IMAGE_MAPPING[VTC],
        iconAllowOverlap: true,
        iconColor: '#fff',
        iconRotate: angle,
      },
      carShadow: {
        iconImage: Images.carShadow,
        iconAllowOverlap: true,
        iconTranslate: [7, 10],
        iconRotate: angle,
      },
    });

    return (
      <Mapbox.ShapeSource
        id={`VTCCar${glRenderIndex}`}
        shape={Mapbox.geoUtils.makePoint([parseFloat(longitude), parseFloat(latitude)])}
      >
        <Mapbox.SymbolLayer id={`carShadow${glRenderIndex}`} minZoomLevel={1} style={layerStyles.carShadow} />

        <Mapbox.SymbolLayer id={`carVTC${glRenderIndex}`} minZoomLevel={1} style={layerStyles.carImage} />
      </Mapbox.ShapeSource>
    );
  }
}

VTCCar.defaultProps = {
  VTC: 'uber',
  glRenderIndex: 1,
};

export default VTCCar;
