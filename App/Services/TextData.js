import { capitalizeFirstLetter } from './Utils';


const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\\/%?=~_|!:,.;]*[-A-Z0-9+&@#\\/%=~_|])/gi;

class TextValue {
  constructor(value) {
    this.value = value;
  }

  toJson(baseValue) {
    try {
      return JSON.parse(this.value);
    } catch (error) {
      return baseValue || this.value;
    }
  }

  get withoutTags() {
    return this.value.replace(new RegExp('<[^>]*>', 'gi'), '');
  }

  get url() {
    const url = this.value.match(urlRegex);
    if (url.length) return url[0];
    return null;
  }

  split(by = '', toUpperCaseFirst) {
    try {
      return toUpperCaseFirst
        ? this.value.split(by).map(c => capitalizeFirstLetter(c))
        : this.value.split(by);
    } catch (error) {
      console.error(error);
      return this.value;
    }
  }
}

class TextData {
  constructor(values) {
    if (!values) throw new Error('Array of aliases are required in params.');
    values.forEach((textValue) => {
      this[textValue.code] = new TextValue(textValue.value);
    });
  }

  getValue(code = '') {
    return this[code] || new TextValue(code);
  }

  get(code = '') {
    return this.getValue(code);
  }
}

export default TextData;
