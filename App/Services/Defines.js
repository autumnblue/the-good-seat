const userProfile = {
  firstName: 'firstname',
  lastName: 'lastname',
  phoneNumber: 'phonenumber',
  email: 'email',
  password: 'password',
  dateOfBirth: 'birthdate',
};

export default userProfile;
