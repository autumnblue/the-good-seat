import { Platform } from 'react-native';
import VersionNumber from 'react-native-version-number';
import appState from '../../../MobX';

/* eslint no-undef: "off" */
const EventTracker = {
  track(event, dataArg = {}) {
    const data = Object.assign({}, dataArg);
    console.log('event tracking', event);
    data.OS = Platform.OS;

    try {
      data.appVersion = VersionNumber.appVersion;
      data.buildVersion = VersionNumber.buildVersion;
      data.bundleIdentifier = VersionNumber.bundleIdentifier;
    } catch (e) {
      console.warn(e);
    }

    return appState.$http
      .post('/event_tracking', {
        eventTitle: event,
        eventType: 'mobile',
        userId:
          appState.sessionParameters.currentUser && `${appState.sessionParameters.currentUser._id}`,
        data,
      })
      .catch(err => console.warn(JSON.stringify(err.response)));
  },
};

export default EventTracker;
