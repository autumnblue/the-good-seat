import appState from '../../../MobX';

const userId = appState.sessionParameters.currentUser && appState.sessionParameters.currentUser._id;

const AddressApi = {
  list(query) {
    return appState.$http.post(`/user/${userId}/address`, { params: query });
  },

  get(id, query) {
    return appState.$http.get(`/user/${userId}/address/${id}`, { params: query });
  },

  post(data) {
    return appState.$http.post(`/user/${userId}/address`, data);
  },

  put(id, data) {
    return appState.$http.post(`/user/${userId}/address/${id}`, data);
  },
};

export default AddressApi;
