//---------------------------------------------------------------
// add parameters to get
//---------------------------------------------------------------
function R_GET(urlParams, params) {
  let url = urlParams;
  if (params) {
    const paramsArray = [];
    Object.keys(params).forEach(key => paramsArray.push(`${key}=${encodeURIComponent(params[key])}`));

    if (url.search(/\?/) === -1) {
      url += `?${paramsArray.join('&')}`;
    } else {
      url += `&${paramsArray.join('&')}`;
    }
  }

  return url;
}

//--------------------------------------------------------
// send event to server
//--------------------------------------------------------
export async function sendEventToServer(eventTitle, data) {
  try {
    const response = await fetch(
      'https://thegoodseat-api.dev.enyosolutions.com/api/event_tracking',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          eventTitle,
          data,
        }),
      },
    );

    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}

//--------------------------------------------------------
// get error codes
//--------------------------------------------------------
export async function getErrorCodes(URL) {
  const query = 'error_codes';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
//--------------------------------------------------------
// get error TOC
//--------------------------------------------------------
export async function getCurrentTerm(URL) {
  const query = 'terms/current';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
//--------------------------------------------------------
// register new user
//--------------------------------------------------------
export async function registerUser(URL, user = {}) {
  const {
    firstname,
    lastname,
    phonenumber,
    password,
    birthdate,
    city,
    postalcode,
    address,
    society,
    email,
    username,
    isPhonenumberVerified,
  } = user;

  const query = 'auth/register';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstname,
        lastname,
        phonenumber,
        username,
        birthdate,
        city,
        postalcode,
        address,
        society,
        password,
        email,
        isPhonenumberVerified,
      }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
// logon user
//--------------------------------------------------------
export async function loginUser(URL, user = {}) {
  const { email, password } = user;

  const query = 'auth/login';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    });

    const data = await response.json();
    console.log('USER', email, password, data);
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
// update user info
//--------------------------------------------------------
export async function updateUserInfo(URL, token, user = {}) {
  const {
    firstname,
    lastname,
    phonenumber,
    username,
    password,
    birthdate,
    city,
    postalcode,
    address,
    society,
    userId,
    country,
    termsAccepted,
    email,
    isPhonenumberVerified,
  } = user;

  const query = `user/${userId}`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        firstname,
        lastname,
        phonenumber,
        username,
        birthdate,
        city,
        postalcode,
        address,
        society,
        password,
        country,
        termsAccepted,
        email,
        isPhonenumberVerified,
      }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
//--------------------------------------------------------
// forgot password
//--------------------------------------------------------
export async function forgotPassword(URL, user = {}) {
  const { email } = user;

  const query = 'auth/forgot';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
      }),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
// delete user account
//--------------------------------------------------------
export async function deleteUserAcoount(URL, token, userId) {
  const query = `user/${userId}`;
  const uri = `${URL}/${query}`;

  console.log(uri);

  try {
    const response = await fetch(uri, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
// register new user
//--------------------------------------------------------
export async function sendProblemTicket(URL, token, tiketData = {}) {
  const {
    type, status, regionId, userId, subject, body,
  } = tiketData;

  const query = 'ticket';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        type,
        status,
        regionId,
        userId,
        subject,
        body,
      }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
// get providers
//--------------------------------------------------------
export async function getProviders(URL, token) {
  const query = 'provider';
  const uri = R_GET(`${URL}/${query}`);

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
//
//--------------------------------------------------------
export async function providerOffers(
  URL,
  token,
  providerCode,
  startDate,
  type,
  startLocation = {},
  endLocation = {},
  statisticsData = {},
) {
  const { longitude: startLong, latitude: startLat } = startLocation;
  const { longitude: endLong, latitude: endLat } = endLocation;
  const {
    startRegion,
    startStreet,
    startCity,
    startCountry,
    startZipCode,
    endRegion,
    endStreet,
    endCity,
    endCountry,
    endZipCode,
    startFullAddress,
    endFullAddress,
  } = statisticsData;

  const query = `provider/${providerCode}/offers`;
  const uri = R_GET(`${URL}/${query}`, {
    type,
    startLat,
    startLong,
    endLat,
    endLong,
    startRegion,
    startStreet,
    startCity,
    startCountry,
    startZipCode,
    endRegion,
    endStreet,
    endCity,
    endCountry,
    endZipCode,
    startFullAddress,
    endFullAddress,
    startDate,
  });

  console.log('offer URI', uri);

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return 'CATCH ERROR';
  }
}
//--------------------------------------------------------
//
//--------------------------------------------------------
export async function getRide(
  URL,
  token,
  startLocation = {},
  endLocation = {},
  providerName,
  offerId,
  statisticsData = {},
) {
  const { longitude: startLong, latitude: startLat } = startLocation;
  const { longitude: endLong, latitude: endLat } = endLocation;
  const {
    startRegion,
    startStreet,
    startCity,
    startCountry,
    startZipCode,
    endRegion,
    endStreet,
    endCity,
    endCountry,
    endZipCode,
    startFullAddress,
    endFullAddress,
  } = statisticsData;

  const query = 'provider/ride';
  const uri = R_GET(`${URL}/${query}`);

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        startLat,
        startLong,
        endLat,
        endLong,
        providerName,
        offerId,
        startRegion,
        startStreet,
        startCity,
        startCountry,
        startZipCode,
        endRegion,
        endStreet,
        endCity,
        endCountry,
        endZipCode,
        startFullAddress,
        endFullAddress,
      }),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
//---------------------------------------------------------------
//
//---------------------------------------------------------------
export async function rideStatus(URL, token) {
  const query = 'provider/ride/current';
  const uri = R_GET(`${URL}/${query}`);

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//--------------------------------------------------------
// PAY RIDE
//--------------------------------------------------------
export async function payRide(
  URL,
  token,
  providerCode,
  providerOfferName,
  {
    startLat, startLong, endLat, endLong, offerId, price, paymentType,
  },
  statisticsData = {},
) {
  const {
    startRegion,
    startStreet,
    startCity,
    startCountry,
    startZipCode,
    endRegion,
    endStreet,
    endCity,
    endCountry,
    endZipCode,
    startFullAddress,
    endFullAddress,
  } = statisticsData;

  const query = 'ride/request';
  const uri = `${URL}/${query}`;

  console.log(
    'pay',
    JSON.stringify({
      startLat,
      startLong,
      endLat,
      endLong,
      offerId,
      price,
      paymentType,
      providerCode,
      startRegion,
      startStreet,
      startCity,
      startCountry,
      startZipCode,
      endRegion,
      endStreet,
      endCity,
      endCountry,
      endZipCode,
      startFullAddress,
      endFullAddress,
      providerOfferName,
    }),
  );

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        startLat,
        startLong,
        endLat,
        endLong,
        offerId,
        price,
        paymentType,
        providerCode,
        startRegion,
        startStreet,
        startCity,
        startCountry,
        startZipCode,
        endRegion,
        endStreet,
        endCity,
        endCountry,
        endZipCode,
        startFullAddress,
        endFullAddress,
        providerOfferName,
      }),
    });

    const data = await response.json();
    console.log('pay rider: ', data);
    return data;
  } catch (error) {
    console.warn('pay error', error);
    return error;
  }
}
//--------------------------------------------------------
// get current ride
//--------------------------------------------------------
export async function getCurrentRide(URL, token) {
  const query = 'ride/current';
  const uri = `${URL}/${query}`; // ?providerCode=${providerCode}

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    if (error && !error.message === 'error_no_active_ride') {
      console.warn(error);
    }
    return error;
  }
}
//---------------------------------------------
// cancel ride
//---------------------------------------------
export async function cancelCurrentRide(URL, token, providerCode) {
  const query = 'ride/current';
  const uri = `${URL}/${query}?providerCode=${providerCode}`;

  try {
    const response = await fetch(uri, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
//---------------------------------------------
// change destination point
//---------------------------------------------
export async function changeDestinationPointRide(URL, token, newPoint = {}) {
  const query = 'ride/current/destination';
  const uri = `${URL}/${query}`;

  const {
    latitude,
    longitude,
    street,
    city,
    region,
    country,
    zipCode,
    placeName,
    providerCode,
  } = newPoint;

  try {
    const response = await fetch(uri, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        lat: latitude,
        long: longitude,
        street,
        city,
        region,
        country,
        zipCode,
        fullAddress: placeName,
        providerCode,
      }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
//------------------------------------------------
// get lastes rides
//------------------------------------------------
export async function getLastestRides(URL, token, userId) {
  const query = `user/${userId}/ride?sort=%7B%22startDate%22:%22DESC%22%7D&perPage=10`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    console.log('lastest ride', data);
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
//------------------------------------------------------
// leave riview
//------------------------------------------------------
export async function leaveReview(URL, token, userId, rideId, carRate, driverRate, comment) {
  const query = `user/${userId}/ride/${rideId}/comment`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        userCarRating: carRate,
        userDriverRating: driverRate,
        comment,
      }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.warn(error);
    return error;
  }
}
//------------------------------------------------------
// get faq
//------------------------------------------------------
export async function getFAQ(URL, token) {
  const query = 'faq/';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    console.log('FAQ:', data);

    return data;
  } catch (error) {
    console.warn(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// get card list
//-----------------------------------------------------
export async function getCardList(URL, token, userId) {
  const query = `user/${userId}/payment_method/`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.warn(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// post card on server
//-----------------------------------------------------
export async function addPaymentCard(URL, token, userId, cardData = {}) {
  const { name, mongoPayCardId, isFavorite } = cardData;

  const query = `user/${userId}/payment_method/`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name,
        mongoPayCardId,
        isFavorite,
        userId,
      }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
//-----------------------------------------------------
// change card on server
//-----------------------------------------------------
export async function changePaymentCard(URL, token, userId, cardData = {}) {
  const {
    cardId, name, mongoPayCardId, isFavorite,
  } = cardData;

  const query = `user/${userId}/payment_method/${cardId}`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name,
        mongoPayCardId,
        isFavorite,
      }),
    });

    const data = await response.json();
    console.log('change card:', data);

    return data;
  } catch (error) {
    console.warn(error);
    return error;
  }
}
//-----------------------------------------------------
// delete card from server
//-----------------------------------------------------
export async function deletePaymentCard(URL, token, userId, cardId) {
  const query = `user/${userId}/payment_method/${cardId}`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// get card list
//-----------------------------------------------------
export async function getCagnotte(URL, token, userId, cagnotteId) {
  const query = `user/${userId}/cagnotte/${cagnotteId}`;
  const uri = `${URL}/${query}`;

  console.log(uri);
  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// redeem code
//-----------------------------------------------------
export async function redeemCode(URL, token, code) {
  const query = `promocode/${code}/redeem`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
//-----------------------------------------------------
// get promocodes code
//-----------------------------------------------------
export async function getBonuses(URL, token, userId) {
  const query = `user/${userId}/promocode_operation`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
//-----------------------------------------------------
// get user addresses list
//-----------------------------------------------------
export async function getUserAddresses(URL, token, userId) {
  const query = `user/${userId}/address/`;
  const uri = `${URL}/${query}`;

  console.log(uri);

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    console.log(data);
    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//----------------------------------------------
// add user favorite address
//----------------------------------------------
export async function addUserAddress(URL, token, userId, addressData = {}) {
  const {
    placeName,
    latitude,
    longitude,
    type,
    street,
    country,
    city,
    region,
    zipCode,
  } = addressData;

  const query = `user/${userId}/address/`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        title: placeName,
        type,
        lat: latitude,
        lon: longitude,
        street,
        country,
        city,
        region,
        zipCode,
      }),
    });

    const data = await response.json();
    console.log('add address:', data);

    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// update user address from server
//-----------------------------------------------------
export async function updateUserAddress(URL, token, userId, addressId, addressData = {}) {
  const query = `user/${userId}/address/${addressId}`;
  const uri = `${URL}/${query}`;

  const {
    placeName,
    latitude,
    longitude,
    type,
    street,
    country,
    city,
    region,
    zipCode,
  } = addressData;

  try {
    const response = await fetch(uri, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        title: placeName,
        type,
        lat: latitude,
        lon: longitude,
        street,
        country,
        city,
        region,
        zipCode,
      }),
    });

    const data = await response.json();
    console.log('update address:', data);

    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// delete user address from server
//-----------------------------------------------------
export async function deleteUserAddress(URL, token, userId, addressId) {
  const query = `user/${userId}/address/${addressId}`;
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    console.log('delete address:', data);

    return data;
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//-----------------------------------------------------
// get advertisement
//-----------------------------------------------------
export async function getAdvertisement(URL, token) {
  const query = 'advertisement/current';
  const uri = `${URL}/${query}`;

  try {
    const response = await fetch(uri, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.warn(error);
    return error;
  }
}
