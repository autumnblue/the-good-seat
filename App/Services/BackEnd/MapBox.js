/*
----------------------------------------------------------------
 search geocoding api
-----------------------------------------------------------------
*/
export async function MAPBOXsearchGeocoding(URL, options = {}) {
  const {
    source, accessToken, proximity, bbox, types, limit, autocomplete, query,
  } = options;

  const uri = `${URL + source}/${query}.json`
    + `?access_token=${accessToken}${proximity ? `&proximity=${proximity}` : ''}${
      bbox ? `&bbox=${bbox}` : ''
    }${autocomplete ? `&autocomplete=${autocomplete}` : ''}${limit ? `&limit=${limit}` : ''} ${
      types ? `&types=${types}` : ''
    }`
    + '&country=fr,pt,es'
    + '&language=fr';

  return fetch(uri)
    .then((response) => {
      if (response.status === 401) {
        return {
          error: 'error fetch mapbox geocoding api',
        };
      }
      if (response.ok) {
        return response.json();
      }

      return response.json();
    })
    .then(json => json);
}
/*
--------------------------------------------------------------
 get direction for routing build
--------------------------------------------------------------
*/
export function MAPBOXgetDirection(URL, waypoints, options = {}) {
  const { geometry, profile, accessToken } = options;

  const encodedWaypoints = waypoints
    .map(location => `${location.longitude},${location.latitude}`)
    .join(';');

  const uri = `${URL + profile}/${encodedWaypoints}`
    + `?access_token=${accessToken}${geometry ? `&geometries=${geometry}` : ''}`;

  console.log('URI', uri);

  return fetch(uri)
    .then((response) => {
      if (response.status === 401) {
        return {
          error: 'error fetch mapbox direction api',
        };
      }
      if (response.ok) {
        return response.json();
      }
      return response.json();
    })
    .then(json => json);
}

export function convertFeatureDataToAddressData(feature) {
  const statData = {
    region: '',
    street: '',
    city: '',
    country: '',
    zipCode: '',
    longitude: 0.0,
    latitude: 0.0,
    placeName: '',
  };

  // collect statistics data
  feature.context.forEach((element) => {
    if (element.id.startsWith('postcode')) {
      statData.zipCode = element.text;
    }

    if (element.id.startsWith('region')) {
      statData.region = element.text;
    }

    if (element.id.startsWith('country')) {
      statData.country = element.text;
    }

    if (element.id.startsWith('place')) {
      statData.city = element.text;
    }

    if (element.id.startsWith('neighborhood')) {
      statData.street = element.text;
    }

    if (element.id.startsWith('address')) {
      statData.street = element.text;
    }
  });

  const longitude = feature.center[0];
  const latitude = feature.center[1];

  // get data
  statData.longitude = longitude;
  statData.latitude = latitude;
  statData.placeName = feature.place_name;

  return statData;
}
