const aliases = {
  userProfile: {
    firstName: 'firstname',
    lastName: 'lastname',
    phoneNumber: 'phonenumber',
    email: 'email',
    password: 'password',
  },
};

export default aliases;
