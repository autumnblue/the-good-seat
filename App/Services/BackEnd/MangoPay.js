import Base64 from 'react-native-base64';

//---------------------------------------------------------------
// add parameters to get
//---------------------------------------------------------------
function R_GET(urlParam, params) {
  let url = urlParam;
  if (params) {
    const paramsArray = [];
    Object.keys(params).forEach(key => paramsArray.push(`${key}=${encodeURIComponent(params[key])}`));

    if (url.search(/\?/) === -1) {
      url += `?${paramsArray.join('&')}`;
    } else {
      url += `&${paramsArray.join('&')}`;
    }
  }

  return url;
}
//--------------------------------------------------------
//
//--------------------------------------------------------
async function finishRegistration(URL, token, paylineData) {
  try {
    const response = await fetch(`${URL}/cardregistrations/${paylineData.Id}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`,
      },
      body: JSON.stringify(paylineData),
    });

    return await response.json();
  } catch (error) {
    return error;
  }
}
//--------------------------------------------------------
//
//--------------------------------------------------------
async function tokenizeCard(data) {
  const uri = R_GET(`${data.CardRegistrationURL}`, {
    data: data.PreregistrationData,
    accessKeyRef: data.AccessKey,
    cardNumber: data.cardNumber,
    cardExpirationDate: data.cardExpirationDate,
    cardCvx: data.cardCvx,
  });

  try {
    const response = await fetch(uri, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const dataReg = await response.text();

    console.log('reg - token', dataReg);

    // complete card registration with MangoPay API
    return await finishRegistration(data.baseURL, data.token, {
      Id: data.Id,
      RegistrationData: dataReg,
    });
  } catch (error) {
    return error;
  }
}
//--------------------------------------------------------
//
//--------------------------------------------------------
export async function registerCard(
  URL,
  clientId,
  clientPassword,
  cardData = {},
  version = 'v2.01',
) {
  const {
    tag,
    userId,
    currencyCode,
    cardType = 'CB_VISA_MASTERCARD',
  } = cardData;

  console.log('card data', cardData);

  const baseURL = `${URL}/${version}/${clientId}`;

  const token = `Basic ${Base64.encode(`${clientId}:${clientPassword}`)}`;

  try {
    // get card registration data
    const response = await fetch(`${baseURL}/cardregistrations`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`,
      },
      body: JSON.stringify({
        Tag: tag,
        UserId: userId,
        Currency: currencyCode,
        CardType: cardType,
      }),
    });

    const regData = await response.json();

    console.log('reg data', regData);

    return await tokenizeCard({
      baseURL,
      token,
      ...regData,
      ...cardData,
    });
  } catch (error) {
    return error;
  }
}
//--------------------------------------------------------
// get card info
//--------------------------------------------------------
export async function getCard(URL, clientId, clientPassword, cardId, version = 'v2.01') {
  const baseURL = `${URL}/${version}/${clientId}`;

  const token = `Basic ${Base64.encode(`${clientId}:${clientPassword}`)}`;

  try {
    // get card registration data
    const response = await fetch(`${baseURL}/cards/${cardId}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`,
      },
    });

    return await response.json();
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
//-------------------------------------------------------
// delete card from server
//-------------------------------------------------------
export async function deleteCardFromServer(
  URL,
  clientId,
  clientPassword,
  cardId,
  version = 'v2.01',
) {
  const baseURL = `${URL}/${version}/${clientId}`;

  const token = `Basic ${Base64.encode(`${clientId}:${clientPassword}`)}`;

  try {
    // get card registration data
    const response = await fetch(`${baseURL}/cards/${cardId}`, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`,
      },
      body: JSON.stringify({}),
    });

    return await response.json();
  } catch (error) {
    console.error(error);
    return 'ERROR';
  }
}
