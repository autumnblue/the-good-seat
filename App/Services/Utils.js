import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');

export const isIphoneX = !!(Platform.OS === 'ios' && (height === 812 || width === 812));

export const isLargeScreen = !!(Platform.OS === 'android' && (height >= 810 || width >= 810));

export const removeNonNumber = (string = '') => string.replace(/[^\d]/g, '');

export const removeLeadingSpaces = (string = '') => string.replace(/^\s+/g, '');

export const limitLength = (string = '', maxLength) => string.substr(0, maxLength);

//-----------------------------------------------------------
// date formatn (date = new Date(value))
//-----------------------------------------------------------
export function dateToDMY(date, spliter = '.') {
  const d = date.getDate();

  const m = date.getMonth() + 1;
  const y = date.getFullYear();
  return `${d <= 9 ? `0${d}` : d}${spliter}${m <= 9 ? `0${m}` : m}${spliter}${y}`;
}

export function secondsToHHMMSS(totalSeconds) {
  const hours = Math.floor(totalSeconds / 3600);
  const minutes = Math.floor((totalSeconds - hours * 3600) / 60);
  let seconds = totalSeconds - hours * 3600 - minutes * 60;

  // round seconds
  seconds = Math.round(seconds * 100) / 100;

  let result = hours < 10 ? `0${hours}` : hours;
  result += `-${minutes < 10 ? `0${minutes}` : minutes}`;
  result += `-${seconds < 10 ? `0${seconds}` : seconds}`;
  return result;
}
//-----------------------------------------------------------
// add gaps to string
//-----------------------------------------------------------
export const addGaps = (string = '', gaps) => {
  const offsets = [0].concat(gaps).concat([string.length]);

  return offsets
    .map((end, index) => {
      if (index === 0) return '';
      const start = offsets[index - 1];
      return string.substr(start, end - start);
    })
    .filter(part => part !== '')
    .join(' ');
};

//-------------------------------------------------
//
//-------------------------------------------------
export function capitalizeFirstLetter(string) {
  return string[0].toUpperCase() + string.slice(1);
}

//--------------------------------------------------
// validate email
// Thanks to:
// http://fightingforalostcause.net/misc/2006/compare-email-regex.php
// http://thedailywtf.com/Articles/Validating_Email_Addresses.aspx
// http://stackoverflow.com/questions/201323/what-is-the-best-regular-expression-for-validating-email-addresses/201378#201378
//--------------------------------------------------
export function validateEmail(email) {
  const regTester = /^[-!#$%&'*+\\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

  if (!email) return false;

  if (email.length > 254) return false;

  const valid = regTester.test(email);
  if (!valid) return false;

  const parts = email.split('@');
  if (parts[0].length > 64) return false;

  const domainParts = parts[1].split('.');
  if (domainParts.some(part => part.length > 63)) return false;

  return true;
}
/*
--------------------------------------------
// validate form value
validationConfig = {
  firstName :{
    required: true,
    regex: '[]'
  }
}
--------------------------------------------
*/
export function validateFormValue(validationConfig, name, value) {
  const rules = validationConfig[name];

  let valid = true;

  // no rules !!! return true
  if (!rules) return true;

  if (typeof rules.validFunc === 'function') {
    valid = rules.validFunc(value);
  }

  if (rules.required && !value) {
    valid = false;
  } else if (value && rules.regex && !rules.regex.test(value)) {
    valid = false;
  }
  return valid;
}
//-----------------------------------------------
//
//-----------------------------------------------
export function getUrlParams(searchParams) {
  let search = searchParams;
  if (search.indexOf('#_=_') > -1) {
    search = search.replace('#_=_', '');
  }
  const hashes = search.slice(search.indexOf('?') + 1).split('&');
  const params = {};

  hashes.map((hash) => {
    const [key, val] = hash.split('=');
    params[key] = decodeURIComponent(val);
    return params;
  });

  return params;
}
//-----------------------------------------------
//
//-----------------------------------------------
export function validateCheckDigit(cardNumber) {
  // From https://stackoverflow.com/questions/12310837/implementation-of-luhn-algorithm
  let nCheck = 0;
  let nDigit = 0;
  let bEven = false;

  if (cardNumber) {
    const value = cardNumber.replace(/\D/g, '');

    for (let n = value.length - 1; n >= 0; n -= 1) {
      const cDigit = value.charAt(n);

      nDigit = parseInt(cDigit, 10);
      if (bEven) {
        nDigit *= 2;
        if (nDigit > 9) nDigit -= 9;
      }
      nCheck += nDigit;
      bEven = !bEven;
    }

    return nCheck % 10 === 0;
  }
  return false;
}
//------------------------------------------------
// custom logic for card expiration date
//------------------------------------------------
export function formatCardDateExp(curValue, prevValue) {
  return prevValue && curValue && prevValue.length === 1 && curValue.length === 2
    ? `${curValue}/`
    : curValue;
}

//-----------------------------------------------
// score strech password
//-----------------------------------------------
export function strengthPassword(password) {
  if (password.length < 8) {
    return false;
  }

  if (
    password.length >= 8
    && password.match(/[a-z]/)
    && password.match(/[A-Z]/)
    && password.match(/[0-9]/)
  ) {
    return true;
  }

  return false;
}
