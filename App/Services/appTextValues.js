const appTextValues = [
  {
    code: 'helper.faq.header.01',
    value: 'Comment fait-on pour commander une course sur The Good Seat ?',
  },
  {
    code: 'helper.faq.text.01',
    value:
      'Pour commander une course, il vous suffit de suivre'
      + 'les étapes renseignées en bas de votre écran. Entrez'
      + 'une adresse de destination, sélectionnez vos critères,'
      + 'Choisissez une offre puis laissez vous conduire.',
  },
  {
    code: 'helper.faq.header.02',
    value: 'Est-il possible de commander une course à l’avance ?',
  },
  {
    code: 'helper.faq.header.03',
    value: 'Peut-on annuler ou modifier une course après l’avoir commandée ?',
  },
  {
    code: 'helper.faq.text.02',
    value:
      'Il est tout à fait possible de réserver une course à'
      + 'l’avance. Il vous suffit de paramétrer la date et l’heure'
      + 'souhaitées après avoir cliqué sur la petite horloge'
      + 'située à gauche de la barre de destination.',
  },
  {
    code: 'helper.faq.text.03',
    value:
      'Après avoir commandé une course en réservation'
      + 'instantanée, vous pouvez modifier votre adresse de'
      + 'départ ou annuler la course sans frais dans un délai'
      + 'de 5 minutes après la réservation. Une fois passé ce'
      + 'délai, il vous sera facturé la moitié du prix de la course.'
      + 'Dans le cas d’une réservation à l’avance, vous pouvez'
      + 'modifier ou annuler la course sans frais jusqu’à une'
      + 'heure avant la course. Moins d’une heure avant la'
      + 'course, il vous sera facturé la moitié du prix de cette'
      + 'dernière.',
  },
  {
    code: 'helper.terms.header.01',
    value: 'Article 1. Acceptation en ligne des Conditions Générales d’Utilisation',
  },
  {
    code: 'helper.terms.header.02',
    value: 'Article 2. Modification des Conditions Générales d’Utilisation',
  },
  {
    code: 'helper.terms.header.03',
    value: 'Article 3. Objet',
  },
  {
    code: 'helper.terms.text.01',
    value:
      'Conditions Générales d’Utilisation'
      + 'L’utilisation du Site est subordonnée à l’acceptation des présentes CGU.'
      + 'Au moment de la création de leur compte utilisateur, les Utilisateurs (tel que'
      + 'ce terme est ci-après défini) doivent cocher la case “J’accepte les Conditions'
      + 'Générales d’Utilisation du site et du service proposé”. Seule l’acceptation de'
      + 'ces CGU permet aux Utilisateurs d’accéder aux services proposés par le'
      + 'l’application (tel que ce terme est ci-après défini). L’acceptation des présentes'
      + 'CGU est entière et forme un tout indivisible, et les Utilisateurs ne peuvent'
      + 'choisir de voir appliquer une partie des CGU seulement ou encore formuler'
      + 'des réserves. En acceptant les CGU, l’Utilisateur accepte notamment l’article'
      + ' 6 des CGU concernant le “Traitement des données personnelles des'
      + 'utilisateurs” de même que de la Charte d’utilisation des Cookies dans laquelle'
      + 'les termes définis précédés d’une majuscule auront le sens qui leur est'
      + 'attribué aux présentes CGU. En cas de manquement à l’une des obligations'
      + 'prévues par les présentes, The Good Seat se réserve la possibilité de'
      + 'supprimer le compte Utilisateur concerné.',
  },
  {
    code: 'helper.terms.text.02',
    value:
      'Générales d’Utilisation'
      + 'The Good Seat se réserve le droit de modifier à tout moment les CGU, les'
      + 'fonctionnalités offertes sur l’application ou les règles de fonctionnement de'
      + 'l’Application (tel que ce terme est ci-après défini). La modification prendra'
      + 'effet immédiatement dès la mise en ligne des CGU que tout Utilisateur'
      + 'reconnait avoir préalablement consultées. Lorsque la modification survient'
      + 'après le paiement par l’Utilisateur d’une somme d’argent correspondant à une'
      + 'Commande, la modification ne s’applique pas à la transaction en cours.'
      + 'The Good Seat se réserve notamment le droit de proposer des services'
      + 'nouveaux, gratuits ou payants sur le l’application.',
  },
  {
    code: 'helper.terms.text.03',
    value:
      'The Good Seat est un comparateur de prix en ligne effectuant pour un trajet,'
      + 'une date, un horaire et un type de véhicule donné, la comparaison des prix'
      + 'entre différents transporteurs, qu’ils soient chauffeurs privés, centrales de'
      + 'réservations de VTC ou encore Taxis. The Good Seat ne prétend pas'
      + 'comparer tous les Transporteurs, mais uniquement ceux avec qui elle a conclu'
      + 'un contrat de partenariat permettant de réserver une course via l’application'
      + 'The Good Seat. L’application garantit en revanche que le classement résultant'
      + 'de son comparateur dépend uniquement des critères fixés par les utilisateurs'
      + 'et en aucune manière d’accords passés avec les Transporteurs du type achat'
      + 'd’espace publicitaire, taux de commission etc…L’Application permet également'
      + ' aux personnes souhaitant effectuer un déplacement urbain en voiture de'
      + 'transport avec chauffeur ou en Taxi de mettre à leur disposition une application'
      + 'logicielle de commande de voitures de transport avec chauffeur ou de Taxi. Le'
      + 'transport qui résulte de cette commande est effectué par un Transporteur,'
      + 'lequel réalise pour son propre compte la course commandée via l’Application.'
      + 'Il est ici précisé que The Good Seat ne fournit pas de services de transport et'
      + 'n’opère ni comme transporteur, ni comme centrale de réservations.',
  },
  {
    code: 'registration.social.form.header',
    value: 'Veuillez indiquer les informations manquantes',
  },
  {
    code: 'registration.form.lastName',
    value: 'Votre Nom',
  },
  {
    code: 'registration.form.firstName',
    value: 'Votre Prénom',
  },
  {
    code: 'registration.form.country',
    value: 'Votre pays',
  },
  {
    code: 'registration.form.dateOfBirth',
    value: 'Votre Date de naissance (DD/MM/YYYY)',
  },
  {
    code: 'registration.form.phoneNumber',
    value: 'Votre Numéro de téléphone',
  },
  {
    code: 'registration.form.email',
    value: 'Votre Email',
  },
  {
    code: 'registration.form.emailConfirm',
    value: 'Confirmez votre email',
  },
  {
    code: 'registration.form.password',
    value: 'Votre Mot de passe',
  },
  {
    code: 'registration.form.passwordConfirm',
    value: 'Confirmez votre Mot de passe',
  },
  {
    code: 'registration.form.cardNumber',
    value: 'Numéro de carte',
  },
  {
    code: 'registration.form.cardDateExpiration',
    value: "Date d'expiration",
  },
  {
    code: 'registration.form.cardCVC',
    value: 'Code de protection',
  },
  {
    code: 'newcard.form.cardName',
    value: 'Nom du nouveau moyen de paiment',
  },
  {
    code: 'newcard.form.cardRename',
    value: 'Renommer votre moyen de paiement',
  },
  {
    code: 'drawer.menu.bonus.loading',
    value: "Traitement de l'information en cours",
  },
  {
    code: 'drawer.menu.useraddresses.loading',
    value: 'Chargement des adresses',
  },
  {
    code: 'shared.findaddress.screen',
    value: 'Saisissez une adresse...',
  },
  {
    code: 'drawer.menu.useraddress.home',
    value: 'Enregistrer un domicile',
  },
  {
    code: 'drawer.menu.useraddress.work',
    value: 'Enregistrer un lieu de travail',
  },
  {
    code: 'drawer.menu.useraddress.favorite',
    value: 'Enregistrer un lieu favori',
  },
  {
    code: 'country.picker.name',
    value: [
      'Afghanistan',
      'Afrique du Sud',
      'Albanie',
      'Algérie',
      'Allemagne',
      'Andorre',
      'Angola',
      'Anguilla',
      'Antarctique',
      'Antigua-et-Barbuda',
      'Antilles néerlandaises',
      'Arabie saoudite',
      'Argentine',
      'Arménie',
      'Aruba',
      'Australie',
      'Autriche',
      'Azerbaïdjan',
      'Bahamas',
      'Bahreïn',
      'Bangladesh',
      'Barbade',
      'Bélarus',
      'Belgique',
      'Belize',
      'Bénin',
      'Bermudes',
      'Bhoutan',
      'Bolivie',
      'Bosnie-Herzégovine',
      'Botswana',
      'Brésil',
      'Brunéi Darussalam',
      'Bulgarie',
      'Burkina Faso',
      'Burundi',
      'Cambodge',
      'Cameroun',
      'Canada',
      'Cap-Vert',
      'Ceuta et Melilla',
      'Chili',
      'Chine',
      'Chypre',
      'Colombie',
      'Comores',
      'Congo-Brazzaville',
      'Corée du Nord',
      'Corée du Sud',
      'Costa Rica',
      'Côte d’Ivoire',
      'Croatie',
      'Cuba',
      'Danemark',
      'Diego Garcia',
      'Djibouti',
      'Dominique',
      'Égypte',
      'El Salvador',
      'Émirats arabes unis',
      'Équateur',
      'Érythrée',
      'Espagne',
      'Estonie',
      'État de la Cité du Vatican',
      'États fédérés de Micronésie',
      'États-Unis',
      'Éthiopie',
      'Fidji',
      'Finlande',
      'France',
      'Gabon',
      'Gambie',
      'Géorgie',
      'Géorgie du Sud et les îles Sandwich du Sud',
      'Ghana',
      'Gibraltar',
      'Grèce',
      'Grenade',
      'Groenland',
      'Guadeloupe',
      'Guam',
      'Guatemala',
      'Guernesey',
      'Guinée',
      'Guinée équatoriale',
      'Guinée-Bissau',
      'Guyana',
      'Guyane française',
      'Haïti',
      'Honduras',
      'Hongrie',
      'Île Bouvet',
      'Île Christmas',
      'Île Clipperton',
      "Île de l'Ascension",
      'Île de Man',
      'Île Norfolk',
      'Îles Åland',
      'Îles Caïmans',
      'Îles Canaries',
      'Îles Cocos - Keeling',
      'Îles Cook',
      'Îles Féroé',
      'Îles Heard et MacDonald',
      'Îles Malouines',
      'Îles Mariannes du Nord',
      'Îles Marshall',
      'Îles Mineures Éloignées des États-Unis',
      'Îles Salomon',
      'Îles Turks et Caïques',
      'Îles Vierges britanniques',
      'Îles Vierges des États-Unis',
      'Inde',
      'Indonésie',
      'Irak',
      'Iran',
      'Irlande',
      'Islande',
      'Israël',
      'Italie',
      'Jamaïque',
      'Japon',
      'Jersey',
      'Jordanie',
      'Kazakhstan',
      'Kenya',
      'Kirghizistan',
      'Kiribati',
      'Koweït',
      'Laos',
      'Lesotho',
      'Lettonie',
      'Liban',
      'Libéria',
      'Libye',
      'Liechtenstein',
      'Lituanie',
      'Luxembourg',
      'Macédoine',
      'Madagascar',
      'Malaisie',
      'Malawi',
      'Maldives',
      'Mali',
      'Malte',
      'Maroc',
      'Martinique',
      'Maurice',
      'Mauritanie',
      'Mayotte',
      'Mexique',
      'Moldavie',
      'Monaco',
      'Mongolie',
      'Monténégro',
      'Montserrat',
      'Mozambique',
      'Myanmar',
      'Namibie',
      'Nauru',
      'Népal',
      'Nicaragua',
      'Niger',
      'Nigéria',
      'Niue',
      'Norvège',
      'Nouvelle-Calédonie',
      'Nouvelle-Zélande',
      'Oman',
      'Ouganda',
      'Ouzbékistan',
      'Pakistan',
      'Palaos',
      'Panama',
      'Papouasie-Nouvelle-Guinée',
      'Paraguay',
      'Pays-Bas',
      'Pérou',
      'Philippines',
      'Pitcairn',
      'Pologne',
      'Polynésie française',
      'Porto Rico',
      'Portugal',
      'Qatar',
      'R.A.S. chinoise de Hong Kong',
      'R.A.S. chinoise de Macao',
      'régions éloignées de l’Océanie',
      'République centrafricaine',
      'République démocratique du Congo',
      'République dominicaine',
      'République tchèque',
      'Réunion',
      'Roumanie',
      'Royaume-Uni',
      'Russie',
      'Rwanda',
      'Sahara occidental',
      'Saint-Barthélémy',
      'Saint-Kitts-et-Nevis',
      'Saint-Marin',
      'Saint-Martin',
      'Saint-Pierre-et-Miquelon',
      'Saint-Vincent-et-les Grenadines',
      'Sainte-Hélène',
      'Sainte-Lucie',
      'Samoa',
      'Samoa américaines',
      'Sao Tomé-et-Principe',
      'Sénégal',
      'Serbie',
      'Serbie-et-Monténégro',
      'Seychelles',
      'Sierra Leone',
      'Singapour',
      'Slovaquie',
      'Slovénie',
      'Somalie',
      'Soudan',
      'Sri Lanka',
      'Suède',
      'Suisse',
      'Suriname',
      'Svalbard et Île Jan Mayen',
      'Swaziland',
      'Syrie',
      'Tadjikistan',
      'Taïwan',
      'Tanzanie',
      'Tchad',
      'Terres australes françaises',
      "Territoire britannique de l'océan Indien",
      'Territoire palestinien',
      'Thaïlande',
      'Timor oriental',
      'Togo',
      'Tokelau',
      'Tonga',
      'Trinité-et-Tobago',
      'Tristan da Cunha',
      'Tunisie',
      'Turkménistan',
      'Turquie',
      'Tuvalu',
      'Ukraine',
      'Union européenne',
      'Uruguay',
      'Vanuatu',
      'Venezuela',
      'Viêt Nam',
      'Wallis-et-Futuna',
      'Yémen',
      'Zambie',
      'Zimbabwe',
    ],
  },
  {
    code: 'tocscreen.checkbox.allowText01',
    value:
      'J’ai lu et accepte les conditions générales d’utilisations de The Good Seat, dont la création automatique de mon compte utilisateur sur MySAM',
  },

  {
    code: 'tocscreen.checkbox.allowText02',
    value:
      "J'accepte que The Good Seat me crée un compte utilisateur chez Youngo et AlloVTCLyon afin d'optimiser mon expérience et accepte leurs CGU respectives. Mon identifiant et mon mot de passe y seront identiques à ceux choisis sur The Good Seat",
  },
];

export default appTextValues;
