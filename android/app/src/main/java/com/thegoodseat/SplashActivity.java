package com.thegoodseat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.graphics.BitmapFactory;
import android.widget.LinearLayout;

import com.facebook.react.ReactActivity;


public class SplashActivity extends ReactActivity {

    private final int SPLASH_TIMEOUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout= new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        ImageView splashScreen = new ImageView(this);
        splashScreen.setScaleType(ImageView.ScaleType.CENTER_CROP);
        splashScreen.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        splashScreen.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.splashscreen) );

        linearLayout.addView(splashScreen);
        setContentView(linearLayout);

        // This handler would close the splash screen once the time is
        // out, and will show the main activity instead.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openMainActivity();
            }
         }, SPLASH_TIMEOUT);
    }

    private void openMainActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
