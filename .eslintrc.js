module.exports = {
    'extends': 'airbnb',
    'parser': 'babel-eslint',
    "globals":
    {
      "fetch": true,
      "navigator": true, 
    },
    rules: {
        'react/jsx-filename-extension': '[1, { "extensions": [".js"] }]',
        'no-console': 'off',
        'react/prop-types': 'off',
        'react/jsx-no-bind': 'off',
        'no-underscore-dangle': ["error", { "allow": ["_id", "_root"] }],
        'no-nested-ternary': 'off',
    }
};
